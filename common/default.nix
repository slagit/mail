_: {
  flake = {
    common.hardware.nixosModules = {
      gaze16 = ./modules/hardware/gaze16.nix;
      linode = ./modules/hardware/linode.nix;
      r610 = ./modules/hardware/r610.nix;
      virtualbox = ./modules/hardware/virtualbox.nix;
      zimaboard = ./modules/hardware/zimaboard.nix;
    };
    nixosModules.common = ./modules;
  };
}
