# Common NixOS Modules

This repository contains common NixOS modules used by various SLAGIT.net
configurations.

- **backup.nix** - borgmatic backup configuration
- **common.nix** - standard settings common to most/all environments
- **hardware/linode.nix** - disk/boot settings for Linode installs
- **hardware/virtualbox.nix** - disk/boot settings for Virtualbox installs
- **users.nix** - standard user configuration

## Usage

For a minimal configuration, include the following:

```flake
{
  inputs.nixos-common.url = "gitlab:slagit/nixos-common";
  outputs = { nixos-common, nixpkgs, ... }: {
    packages.x86_64-linux.nixosConfigurations.default = nixpkgs.lib.nixosSystem {
      modules = [
        nixos-common.nixosModules.hardware.virtualbox
        nixos-common.nixosModules.common
        nixos-common.nixosModules.users
        ./hosts/default.nix
      ];
      system = "x86_64-linux";
    };
  };
}
```
