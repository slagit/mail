{
  config,
  lib,
  pkgs,
  ...
}: {
  services = {
    borgmatic = {
      settings = {
        encryption_passcommand = "${pkgs.coreutils-full}/bin/cat \${CREDENTIALS_DIRECTORY}/borgmatic_passphrase";
        keep_daily = 7;
        keep_weekly = 4;
        keep_monthly = 6;
        keep_yearly = 1;
        ssh_command = "ssh -i \${CREDENTIALS_DIRECTORY}/borgmatic_key";
      };
    };
    postgresql = {
      ensureUsers = [
        {
          name = "root";
          ensureClauses.superuser = true;
        }
      ];
    };
  };
  systemd = lib.mkIf config.services.borgmatic.enable {
    services.borgmatic.serviceConfig.LoadCredential = [
      "borgmatic_key:/run/systemd-vaultd.sock"
      "borgmatic_passphrase:/run/systemd-vaultd.sock"
    ];
    timers.borgmatic = {
      enable = true;
      wantedBy = ["timers.target"];
    };
  };
  virtualisation.vmVariant.environment.systemPackages = [
    (pkgs.writeShellScriptBin "devenv-init-backup" ''
      set -euo pipefail

      systemd-run \
          --pipe \
          --property LoadCredential=borgmatic_key:/run/systemd-vaultd.sock \
          --property LoadCredential=borgmatic_passphrase:/run/systemd-vaultd.sock \
          --wait \
          -- \
          borgmatic rcreate --encryption repokey-blake2
    '')
  ];
}
