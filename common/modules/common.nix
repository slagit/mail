{
  config,
  lib,
  ...
}: {
  config = {
    console.useXkbConfig = true;
    nix = {
      gc = {
        automatic = true;
        options = "--delete-older-than 7d";
      };
      optimise.automatic = true;
      settings = {
        experimental-features = ["nix-command" "flakes"];
        substituters = [
          "https://slagit-nix-cache.s3.amazonaws.com/"
        ];
        trusted-public-keys = [
          "stirling.slagit.net-1:aLl3hPDNtLhs0lH9XKtHLvnatBM4IkHyJ/B5lo3udb0="
        ];
      };
    };
    nixpkgs.config.allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) config.slagit.unfree;
    services.xserver.xkb.options = "ctrl:nocaps";
    system.stateVersion = "22.11";
    virtualisation.vmVariant = {
      nix = {
        gc.automatic = lib.mkForce false;
        settings = {
          substituters = [
            "https://nix-cache.s3.dev-slagit.net"
          ];
          trusted-public-keys = [
            "stirling.dev-slagit.net-1:VrwaVVeQYaJuzwqA5gKL7Lid8CAFw+cR7Gwg691c/1E="
          ];
        };
      };
      security = {
        acme.defaults.server = "https://acme.dev-slagit.net/acme/acme/directory";
        pki.certificates = [
          (builtins.readFile ../../devenv/root.pem)
        ];
      };
    };
  };
  options.slagit.unfree = lib.mkOption {
    default = [];
    type = lib.types.listOf lib.types.str;
  };
}
