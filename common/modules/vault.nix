{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.slagit.vault-agent;
in {
  config = {
    systemd.services =
      lib.mapAttrs' (k: v: {
        name = "vault-agent@${k}";
        value = let
          agentConfig = {
            inherit (v) template;
            auto_auth.method = [
              {
                type = "approle";
                config = {
                  role_id_file_path = "role-id";
                  secret_id_file_path = "secret-id";
                  secret_id_response_wrapping_path = "auth/approle/role/${v.role}/secret-id";
                  remove_secret_id_file_after_reading = false;
                };
              }
            ];
            template_config = {
              exit_on_retry_failure = true;
            };
          };
        in {
          description = "vault agent for ${k}";
          environment.VAULT_ADDR = "https://slagit-vault.net:8200";
          reload = "kill -HUP $MAINPID";
          requires = [
            "systemd-vaultd.socket"
          ];
          script = "/bin/sh -c \"cd \${CREDENTIALS_DIRECTORY}; ${pkgs.vault}/bin/vault agent -config ${pkgs.writeText "vault-agent.hcl" (builtins.toJSON agentConfig)}\"";
          serviceConfig = {
            AmbientCapabilities = "CAP_IPC_LOCK";
            CapabilityBoundingSet = "CAP_SYSLOG CAP_IPC_LOCK";
            LimitMEMLOCK = "infinity";
            LoadCredential = [
              "role-id:/run/systemd-vaultd.sock"
              "secret-id:/run/systemd-vaultd.sock"
            ];
            NoNewPrivileges = "yes";
            ProtectHome = "read-only";
            ProtectSystem = "full";
            PrivateTmp = "yes";
            Restart = "always";
            RestartSec = "5";
            RuntimeDirectory = "vault-agent@${v.role}";
            User = v.user;
          };
          unitConfig = {
            StopWhenUnneeded = "yes";
          };
        };
      })
      cfg;
    slagit.unfree = ["vault"];
  };
  options.slagit.vault-agent = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.submodule {
      options = {
        role = lib.mkOption {
          type = lib.types.str;
        };
        template =
          lib.mkOption {
          };
        user = lib.mkOption {
          type = lib.types.str;
        };
      };
    });
  };
}
