{
  config,
  lib,
  ...
}: {
  config = {
    users = lib.mkIf config.slagit.users.enable {
      mutableUsers = !config.slagit.impermanence.enable;
      users.albert = {
        description = "Albert Koch";
        isNormalUser = true;
        extraGroups = ["wheel"];
        hashedPasswordFile = lib.mkIf config.slagit.impermanence.enable "/persist/passwords/albert";
      };
    };
    slagit.users.enable = lib.mkDefault true;
    virtualisation = lib.mkIf config.slagit.users.enable {
      vmVariant.users.users.albert.initialPassword = "password";
    };
  };
  options.slagit.users.enable = lib.mkEnableOption "default users";
}
