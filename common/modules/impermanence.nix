{
  config,
  lib,
  ...
}: let
  cfg = config.slagit.impermanence;
in {
  config = {
    fileSystems = lib.mkMerge [
      (lib.mkIf cfg.enable {
        "/" = {
          device = "none";
          fsType = "tmpfs";
          options = ["size=3G" "mode=755"];
        };
        "/nix" = {
          device = "/dev/disk/by-label/nix";
          fsType = "ext4";
        };
        "/persist" = {
          device = "/dev/disk/by-label/persist";
          fsType = "ext4";
          neededForBoot = true;
        };
      })
      (lib.mkIf cfg.enable (builtins.listToAttrs (builtins.map (name: {
          inherit name;
          value = {
            depends = ["/persist"];
            device = "/persist${name}";
            noCheck = true;
            options = ["bind" "X-fstrim.notrim"];
          };
        })
        cfg.persist)))
      (lib.mkIf (!cfg.enable) {
        "/" = {
          device = "/dev/disk/by-label/nixos";
          fsType = "ext4";
        };
      })
    ];
    virtualisation.vmVariant.slagit.impermanence.enable = lib.mkForce false;
  };
  options.slagit.impermanence = {
    enable = lib.mkEnableOption "impermanence";
    persist = lib.mkOption {
      default = [];
      type = lib.types.listOf lib.types.str;
    };
  };
}
