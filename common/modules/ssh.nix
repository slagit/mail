{
  config,
  lib,
  pkgs,
  ...
}: let
  has_ssh_nebula = config.services.openssh.enable && (builtins.hasAttr "slagit" config.services.nebula.networks) && config.services.nebula.networks.slagit.enable;
  ssh_ca_pub = pkgs.writeText "ca.pub" ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGpblcVzzHoULrq+lSGgIKhdVCZNCC9Yj6PlBI6j4LgR slagit-vault.net ssh-client-signer
  '';
  dev_ssh_ca_pub = pkgs.writeText "ca.pub" ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOaPQOuykIx1nbRgkbyR+9NA4ohru6XkRRQ1FyqStmXX dev-slagit-vault.net ssh-client-signer
  '';
in {
  networking.firewall.interfaces."nebula.slagit".allowedTCPPorts = lib.mkIf has_ssh_nebula [22];
  services = {
    openssh = {
      hostKeys = lib.mkIf config.slagit.impermanence.enable [
        {
          path = "/persist/etc/ssh/ssh_host_ed25519_key";
          type = "ed25519";
        }
        {
          bits = 4096;
          path = "/persist/etc/ssh/ssh_host_rsa_key";
          type = "rsa";
        }
      ];
      openFirewall = !has_ssh_nebula;
      settings = {
        PasswordAuthentication = false;
        PermitRootLogin = "no";
        TrustedUserCAKeys = toString ssh_ca_pub;
      };
    };
  };
  virtualisation.vmVariant.services.openssh.settings.TrustedUserCAKeys = lib.mkForce (toString dev_ssh_ca_pub);
}
