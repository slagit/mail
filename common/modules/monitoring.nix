_: {
  networking.firewall.interfaces."nebula.slagit".allowedTCPPorts = [9100];
  services.prometheus.exporters.node.enable = true;
}
