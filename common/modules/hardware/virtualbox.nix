_: {
  boot = {
    initrd.availableKernelModules = ["ata_piix" "ohci_pci" "ehci_pci" "ahci" "sd_mod" "sr_mod"];
    loader.grub = {
      enable = true;
      device = "/dev/sda";
    };
  };
  fileSystems."/repos" = {
    device = "nix_desktop";
    fsType = "vboxsf";
    options = ["uid=1000" "gid=100"];
  };
  nixpkgs.hostPlatform = "x86_64-linux";
  virtualisation.virtualbox.guest.enable = true;
}
