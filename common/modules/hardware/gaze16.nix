{
  config,
  lib,
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];
  boot = {
    initrd = {
      availableKernelModules = ["xhci_pci" "ahci" "nvme" "usb_storage" "usbhid" "sd_mod" "sdhci_pci"];
      kernelModules = [];
      luks.devices = {
        "luks-db847a88-1cbf-4e0c-ab7a-b5fbbae14b81".device = "/dev/disk/by-uuid/db847a88-1cbf-4e0c-ab7a-b5fbbae14b81";
        "luks-e2b4892f-de89-403b-90d7-120cc39246ac" = {
          device = "/dev/disk/by-uuid/e2b4892f-de89-403b-90d7-120cc39246ac";
          keyFile = "/crypto_keyfile.bin";
        };
      };
      secrets."/crypto_keyfile.bin" = null;
    };
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernelModules = ["kvm-intel"];
    extraModulePackages = [];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/3D46-EC6B";
    fsType = "vfat";
  };

  swapDevices = [
    {device = "/dev/disk/by-label/swap";}
  ];

  networking.useDHCP = lib.mkDefault true;
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware = {
    cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    system76.enableAll = true;
  };
}
