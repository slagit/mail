{modulesPath, ...}: {
  boot = {
    initrd.availableKernelModules = ["virtio_pci" "virtio_scsi" "ahci" "sd_mod"];
    kernelParams = ["console=ttyS0,19200n8"];
    loader = {
      grub = {
        device = "nodev";
        enable = true;
        extraConfig = ''
          serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
          terminal_input serial;
          terminal_output serial;
        '';
        forceInstall = true;
      };
      timeout = 10;
    };
  };
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];
  networking.tempAddresses = "disabled";
  nixpkgs.hostPlatform = "x86_64-linux";
  virtualisation.vmVariant.virtualisation = {
    cores = 2;
    diskSize = 81920;
    memorySize = 4096;
  };
}
