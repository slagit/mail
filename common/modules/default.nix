_: {
  imports = [
    ./backup.nix
    ./common.nix
    ./impermanence.nix
    ./monitoring.nix
    ./ssh.nix
    ./users.nix
    ./vault.nix
  ];
}
