# SLAGIT.net Nix Configurations

## Development

### SOPS Admin Key Rotation

1. Generate a new age keypair

   ```bash
   age-keygen -o newkey.txt
   ```

   Prepend the private key from `newkey.txt` into `$HOME/.config/sops/age/keys.txt`

1. Update `.sops.yaml` with the new admin key

1. For each `secrets` file, run the following:

   ```bash
   sops updatekeys path/to/file.yaml
   sops -r -i path/to/file.yaml
   ```
