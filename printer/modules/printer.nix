{pkgs, ...}: {
  networking.firewall.allowedTCPPorts = [631];
  services = {
    avahi = {
      allowInterfaces = ["wlan0"];
      enable = true;
      publish = {
        enable = true;
        userServices = true;
      };
    };
    printing = {
      allowFrom = ["all"];
      browsing = true;
      defaultShared = true;
      drivers = [pkgs.gutenprint];
      enable = true;
      listenAddresses = ["*:631"];
    };
  };
}
