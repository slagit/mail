{config, ...}: {
  hardware.sane.enable = true;
  networking.firewall.allowedTCPPorts = [6566];
  services.saned = {
    enable = true;
    extraConfig = ''
      0.0.0.0/0
      [::]/0
    '';
  };
  users.users.albert.extraGroups = [
    config.users.groups.lp.name
    config.users.groups.scanner.name
  ];
}
