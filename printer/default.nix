{
  self,
  inputs,
  ...
}: {
  flake = {
    nixosConfigurations.gutenberg = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        inputs.sops-nix.nixosModules.sops
        self.common.hardware.nixosModules.zimaboard
        self.nixosModules.common
        self.nixosModules.nebula
        self.printer.nixosModules.printer
        self.printer.nixosModules.scanner
        ./hosts/gutenberg.nix
      ];
    };
    printer.nixosModules.printer = ./modules/printer.nix;
    printer.nixosModules.scanner = ./modules/scanner.nix;
  };
}
