_: {
  networking = {
    hostName = "gutenberg";
    wireless = {
      enable = true;
      environmentFile = "/run/secrets/wireless.env";
      networks.slagit.psk = "@PSK_SLAGIT@";
    };
  };
  services.openssh.enable = true;
  sops = {
    defaultSopsFile = ../secrets/gutenberg.yaml;
    secrets = {
      "wireless.env" = {};
    };
  };
}
