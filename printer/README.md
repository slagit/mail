# Printer/Scanner Support

From a client, scan an image with the following command:

```bash
scanimage \
    --device-name "net:gutenberg.local:pixma:05A9176D_3EF395" \
    --format pdf \
    --output-file test.pdf \
    --progress
```
