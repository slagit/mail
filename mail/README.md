# SLAGIT.net Email Server

## Installation

1. Create a new Linode

   Follow instructions
   [here](https://gitlab.com/slagit/nixos-install-linode) to provision
   a new linode.

   ```bash
   sudo nixos-install --flake gitlab:slagit/mail#franklin
   sudo nixos-enter -- passwd albert
   ```

   _or_

   ```bash
   sudo nixos-install --flake gitlab:slagit/mail#franklin-staging
   sudo nixos-enter -- passwd albert
   ```

   Ensure DNS records (below) are set before the final reboot.

1. Add A/AAAA, CNAME, and reverse DNS records

   ```text
   franklin.slagit.net. 300 IN A 203.0.113.34
   franklin.slagit.net. 300 IN AAAA 2001:db8::34
   mail.slagit.net. 300 IN CNAME franklin.slagit.net.
   paperless.slagit.net. 300 IN CNAME franklin.slagit.net.
   ```

1. Boot _NixOS_ Profile

1. Set OpenDKIM PostgreSQL password

   ```console
   $ sudo -u postgres psql mail
   mail=# ALTER USER opendkim PASSWORD 'password';
   ALTER ROLE
   mail=# \q
   $ sudo systemctl restart opendkim.service
   ```

1. Initialize Backup Repository

   ```bash
   sudo borgmatic init -e repokey-blake2
   ```

## Upgrades

1. Upgrade a nix flake input

   ```bash
   git checkout -b nixpkgs
   nix flake lock --update-input nixpkgs
   git add -p flake.nix flake.lock
   git commit -m 'fix(deps): upgrade nixpkgs'
   git push -u origin nixpkgs
   ```

1. Merge upgrade branch in GitLab

1. Build and push all builds to the cache

   ```bash
   for x in -vbox -staging ""
   do
     nix run --inputs-from . nixpkgs#nixos-rebuild -- build --flake ".#franklin$x"
     nix run --inputs-from . nixpkgs#cachix -- push slagit-mail ./result
   done
   ```

1. On each server, apply the updates

   ```bash
   sudo nixos-rebuild switch --flake gitlab:slagit/mail
   ```

1. The differences between two generations can be compared:

   ```bash
   nix run --inputs-from . nixpkgs#nvd diff result.orig result
   ```

## Configuration

### Adding a domain

1. Generate an RSA private key for DKIM signatures

   ```bash
   openssl genrsa 2048
   ```

1. Extract RSA public key for DKIM signatures

   ```bash
   openssl rsa -pubout -outform der | base64 -w0
   ```

1. Add MX, SPF, and DMARC records for the domain

   The DKIM `p=` value is the base64 encoded DKIM public key extracted above

   ```text
   example.com. 300 IN MX 10 franklin.slagit.net.
   example.com. 300 IN TXT v=spf1 mx -all
   _dmarc.example.com. 300 IN TXT v=DMARC1; p=none
   mail._domainkey.example.com. 300 IN TXT v=DKIM1; p=MIIBI.....gQIDAQAB
   ```

1. Add database configuration

   ```console
   $ sudo -u postgres psql mail
   mail=# INSERT INTO keys (data) VALUES ('-----BEGIN PRIVATE KEY-----
   mail-# ...
   mail-# -----END PRIVATE KEY-----') RETURNING id;
   INSERT
   mail=# INSERT INTO domains (name, key_id)
   mail-# VALUES ('example.com', 1) RETURNING id;
   INSERT
   mail=# \q
   ```

### Adding a user

1. Generate a password for the user

   ```bash
   doveadm pw -s ARGON2ID
   ```

1. Add database configuration

   ```console
   $ sudo -u postgres psql mail
   mail=# INSERT INTO users (name, password, domain_id) VALUES ('jdoe', '', 1);
   mail=# \q
   ```

### Wipeing paperless permissions

Paperless often assigns ownership to new
corespondents/documents/types/tags that restrict view.

1. Open sqlite database

   ```bash
   nix shell nixpkgs#sqlite -c sudo sqlite3 /var/lib/paperless/db.sqlite3
   ```

1. Run the following queries

   ```sql
   UPDATE documents_correspondent SET owner_id=NULL;
   UPDATE documents_document SET owner_id=NULL;
   UPDATE documents_documenttype SET owner_id=NULL;
   UPDATE documents_tag SET owner_id=NULL;
   ```

## Install vbox VM

1. Boot NixOS install media
1. Setup up disks

   ```bash
   sudo mkfs.ext4 -L nixos /dev/sda1
   sudo mount /dev/disk/by-label/nixos /mnt
   ```

1. Setup Install Cache

   ```sh
   sudo nix-shell -p cachix --run "cachix use slagit-mail"
   # Add ./cachix.nix to imports in /etc/nixos/configuration.nix
   sudo nixos-rebuild switch
   ```

1. Install NixOS

   ```bash
   sudo nixos-install --flake gitlab:slagit/mail#franklin-vbox
   ```

1. Set User Password

   ```bash
   sudo nixos-enter -- passwd albert
   ```

1. Reboot

   ```bash
   sudo reboot
   ```

1. Set OpenDKIM PostgreSQL password

   ```console
   $ sudo -u postgres psql mail
   mail=# ALTER USER opendkim PASSWORD 'password';
   ALTER ROLE
   mail=# \q
   $ sudo systemctl restart opendkim.service
   ```

   The web interface will be available at <https://mail.127.0.0.1.nip.io:9443/>
