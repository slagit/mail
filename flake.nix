{
  inputs = {
    cargo2nix = {
      inputs = {
        nixpkgs.follows = "nixpkgs";
        rust-overlay.follows = "rust-overlay";
      };
      url = "github:cargo2nix/cargo2nix/main";
    };
    devenv.url = "github:cachix/devenv";
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    rust-overlay.url = "github:oxalica/rust-overlay";
    sops-nix = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:Mic92/sops-nix";
    };
    terranix = {
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
      url = "github:terranix/terranix";
    };
  };
  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        ./ci
        ./common
        ./desktop
        ./devenv
        ./hosts
        ./infrastructure
        ./install
        ./printer
        ./router
        ./services
        ./utils
        ./vault
      ];
      systems = ["x86_64-linux"];
      perSystem = {
        lib,
        system,
        ...
      }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          config.allowUnfreePredicate = pkg:
            builtins.elem (lib.getName pkg) [
              "terraform"
              "vault"
            ];
          overlays = [
            inputs.cargo2nix.overlays.default
          ];
        };
      };
    };
}
