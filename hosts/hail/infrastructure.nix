_: {
  resource.linode_domain_record = {
    hail_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "hail";
      record_type = "A";
      target = "10.10.10.5";
    };
  };
}
