_: {
  resource.linode_domain_record.compute2_idrac_slagit_net = {
    domain_id = "\${linode_domain.slagit_net.id}";
    name = "compute2.idrac";
    record_type = "A";
    target = "10.10.3.22";
  };
}
