{
  config,
  lib,
  ...
}: {
  networking = {
    domain = "slagit.net";
    hostName = "compute1";
  };
  services.openssh.enable = true;
  virtualisation.vmVariant = {
    environment.etc = {
      "nebula/node.crt" = {
        source = ../../devenv/nebula/compute1.slagit.net.crt;
      };
      "nebula/node.key" = {
        source = ../../devenv/nebula/compute1.slagit.net.key;
        group = "nebula-slagit";
        mode = "0640";
      };
    };
    virtualisation.qemu.networkingOptions = lib.mkForce (config.dev-lib.mkNetworkingOptions "cloud" "cloud-compute1" config.dev-lib.networking.networks.cloud.ports.compute1);
  };
}
