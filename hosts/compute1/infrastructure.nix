_: {
  resource.linode_domain_record = {
    compute1_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "compute1";
      record_type = "A";
      target = "10.10.10.6";
    };
    compute1_idrac_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "compute1.idrac";
      record_type = "A";
      target = "10.10.3.21";
    };
  };
}
