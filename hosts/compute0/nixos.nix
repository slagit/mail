{
  config,
  lib,
  ...
}: {
  networking = {
    domain = "slagit.net";
    hostName = "compute0";
  };
  services = {
    borgmatic = {
      enable = true;
      settings.repositories = [
        {
          label = "remote";
          path = "ssh://f7xv163z@f7xv163z.repo.borgbase.com/./repo";
        }
      ];
    };
    openssh = {
      enable = true;
      knownHosts."f7xv163z.repo.borgbase.com".publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGU0mISTyHBw9tBs6SuhSq8tvNM8m9eifQxM+88TowPO";
    };
  };
  virtualisation.vmVariant = {
    environment.etc = {
      "nebula/node.crt" = {
        source = ../../devenv/nebula/compute0.slagit.net.crt;
      };
      "nebula/node.key" = {
        source = ../../devenv/nebula/compute0.slagit.net.key;
        group = "nebula-slagit";
        mode = "0640";
      };
    };
    virtualisation.qemu.networkingOptions = lib.mkForce (config.dev-lib.mkNetworkingOptions "cloud" "cloud-compute0" config.dev-lib.networking.networks.cloud.ports.compute0);
    services = {
      borgmatic.settings.repositories = lib.mkForce [
        {
          label = "remote";
          path = "ssh://compute0@backup.dev-slagit.net/./repo";
        }
      ];
      openssh.knownHosts."backup.dev-slagit.net".publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKdQrG1DaZ+mTuNBQE1RmveX6bzs+q06JrPB8RZonlrH";
    };
  };
}
