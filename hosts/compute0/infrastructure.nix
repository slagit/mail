_: {
  resource.linode_domain_record = {
    compute0_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "compute0";
      record_type = "A";
      target = "10.10.10.3";
    };
    compute0_idrac_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "compute0.idrac";
      record_type = "A";
      target = "10.10.3.20";
    };
  };
}
