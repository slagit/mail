_: {
  resource.linode_domain_record = {
    gutenberg_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "gutenberg";
      record_type = "A";
      target = "10.10.10.7";
    };
  };
}
