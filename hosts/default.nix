{
  inputs,
  self,
  ...
}: {
  flake.nixosConfigurations = {
    compute0 = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        self.common.hardware.nixosModules.r610
        self.nixosModules.common
        self.nixosModules.dev-lib
        self.nixosModules.homepage
        self.nixosModules.monitoring
        self.nixosModules.nebula
        self.nixosModules.paperless
        self.nixosModules.renovate
        self.nixosModules.systemd-vaultd
        ./compute0/nixos.nix
      ];
    };
    compute1 = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        self.ci.nixosModules.gitlab-runner
        self.common.hardware.nixosModules.r610
        self.nixosModules.common
        self.nixosModules.dev-lib
        self.nixosModules.nebula
        self.nixosModules.nix-cache
        self.nixosModules.systemd-vaultd
        ./compute1/nixos.nix
      ];
    };
    franklin = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        self.common.hardware.nixosModules.linode
        self.nixosModules.common
        self.nixosModules.dev-lib
        self.nixosModules.keycloak
        self.nixosModules.mail
        self.nixosModules.nebula
        self.nixosModules.vacation
        self.nixosModules.systemd-vaultd
        ./franklin/nixos.nix
      ];
    };
  };
}
