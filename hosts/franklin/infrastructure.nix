_: {
  resource = {
    linode_domain_record = {
      franklin_slagit_net_A = {
        domain_id = "\${linode_domain.slagit_net.id}";
        name = "franklin";
        record_type = "A";
        target = "\${linode_instance.franklin_slagit_net.ip_address}";
      };
      franklin_slagit_net_AAAA = {
        domain_id = "\${linode_domain.slagit_net.id}";
        name = "franklin";
        record_type = "AAAA";
        target = "\${substr(linode_instance.franklin_slagit_net.ipv6, 0, length(linode_instance.franklin_slagit_net.ipv6) - 4)}";
      };
      franklin_zt_slagit_net = {
        domain_id = "\${linode_domain.slagit_net.id}";
        name = "franklin.zt";
        record_type = "A";
        target = "10.10.10.1";
      };
    };
    linode_instance.franklin_slagit_net = {
      region = "us-east";
      type = "g6-standard-2";
    };
  };
}
