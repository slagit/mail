{
  config,
  lib,
  ...
}: {
  networking = {
    domain = "slagit.net";
    hostName = "franklin";
  };
  services = {
    borgmatic = {
      enable = true;
      settings.repositories = [
        {
          label = "remote";
          path = "ssh://edf9l1cc@edf9l1cc.repo.borgbase.com/./repo";
        }
      ];
    };
    nebula.networks.slagit = {
      isLighthouse = true;
      lighthouses = lib.mkForce [];
    };
    openssh = {
      enable = true;
      knownHosts."edf9l1cc.repo.borgbase.com".publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGU0mISTyHBw9tBs6SuhSq8tvNM8m9eifQxM+88TowPO";
    };
  };
  virtualisation.vmVariant = {
    environment.etc = {
      "nebula/node.crt" = {
        source = ../../devenv/nebula/franklin.slagit.net.crt;
      };
      "nebula/node.key" = {
        source = ../../devenv/nebula/franklin.slagit.net.key;
        group = "nebula-slagit";
        mode = "0640";
      };
    };
    virtualisation.qemu.networkingOptions = lib.mkForce (config.dev-lib.mkNetworkingOptions "linode" "linode-franklin" config.dev-lib.networking.networks.linode.ports.franklin);
    services = {
      borgmatic.settings.repositories = lib.mkForce [
        {
          label = "remote";
          path = "ssh://franklin@backup.dev-slagit.net/./repo";
        }
      ];
      openssh.knownHosts."backup.dev-slagit.net".publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKdQrG1DaZ+mTuNBQE1RmveX6bzs+q06JrPB8RZonlrH";
    };
  };
}
