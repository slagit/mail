_: {
  resource.linode_domain_record.controller_idrac_slagit_net = {
    domain_id = "\${linode_domain.slagit_net.id}";
    name = "controller.idrac";
    record_type = "A";
    target = "10.10.3.10";
  };
}
