{
  config,
  lib,
  ...
}: {
  imports = [
    ./alejandra.nix
    ./devShells.nix
    ./gitlab.nix
    ./homeConfigurations.nix
    ./markdownlint.nix
    ./nixosConfigurations.nix
    ./packages.nix
    ./pipelines.nix
    ./prettier.nix
    ./renovate-config-validator.nix
    ./rust.nix
    ./statix.nix
    ./yamllint.nix
  ];
  ci = {
    alejandra.default = {};
    devShells.default = lib.foldlAttrs (acc: system: shells: acc ++ (builtins.attrValues (lib.mapAttrs (name: _: {inherit name system;}) shells))) [] config.flake.devShells;
    gitlab.default.stages = lib.mkDefault [
      "lint"
      "build"
      "report"
    ];
    homeConfigurations.default = builtins.attrNames (config.flake.homeConfigurations or {});
    markdownlint = ["default"];
    nixosConfigurations.default = builtins.attrNames config.flake.nixosConfigurations;
    packages.default = lib.foldlAttrs (acc: system: pkgs: acc ++ (builtins.attrValues (lib.mapAttrs (name: _: {inherit name system;}) pkgs))) [] config.flake.packages;
    prettier = ["default"];
    renovate-config-validator = ["default"];
    statix.default = {};
    yamllint = ["default"];
  };
}
