{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.listToAttrs
    (builtins.map (name:
      lib.nameValuePair name {
        jobs.renovate-config-validator = {
          enable = lib.mkDefault true;
          jobConfiguration = {
            interruptible = true;
            needs = [];
            rules = [
              {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
            ];
            script = [
              "nix shell .#renovate -c \"renovate-config-validator\""
            ];
            stage = "lint";
          };
        };
      })
    config.ci.renovate-config-validator);
  options.ci.renovate-config-validator = lib.mkOption {
    default = [];
    type = lib.types.listOf lib.types.str;
  };
}
