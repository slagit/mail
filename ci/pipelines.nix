{
  flake-parts-lib,
  lib,
  ...
}:
flake-parts-lib.mkTransposedPerSystemModule {
  name = "gitlabPipelines";
  option = lib.mkOption {
    type = lib.types.lazyAttrsOf lib.types.package;
    default = {};
  };
  file = ./pipelines.nix;
}
