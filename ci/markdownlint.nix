{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.listToAttrs
    (builtins.map (name:
      lib.nameValuePair name {
        jobs.markdownlint = {
          enable = lib.mkDefault true;
          jobConfiguration = {
            interruptible = true;
            needs = [];
            rules = [
              {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
            ];
            script = [
              "nix run --inputs-from . nixpkgs#nodePackages.markdownlint-cli -- ."
            ];
            stage = "lint";
          };
        };
      })
    config.ci.markdownlint);
  options.ci.markdownlint = lib.mkOption {
    default = [];
    type = lib.types.listOf lib.types.str;
  };
}
