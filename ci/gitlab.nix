{
  config,
  lib,
  ...
}: {
  config.perSystem = {pkgs, ...}: {
    gitlabPipelines = builtins.mapAttrs (_: cfg: let
      yaml =
        {
          inherit (cfg) stages;
        }
        // (lib.filterAttrsRecursive (k: v: v != null) (lib.mapAttrs (k: v: v.jobConfiguration) (lib.filterAttrs (k: v: v.enable) cfg.jobs)));
    in
      pkgs.writeText "gitlab-ci.yml" (builtins.toJSON yaml))
    config.ci.gitlab;
  };
  options = {
    ci.gitlab = lib.mkOption {
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          jobs = lib.mkOption {
            default = {};
            type = lib.types.attrsOf (lib.types.submodule {
              options = {
                enable = lib.mkEnableOption "Whether to enable job";
                jobConfiguration = lib.mkOption {
                  type = lib.types.submodule {
                    options = {
                      artifacts = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr (lib.types.submodule {
                          options = {
                            expose_as = lib.mkOption {
                              default = null;
                              type = lib.types.nullOr lib.types.str;
                            };
                            paths = lib.mkOption {
                              default = null;
                              type = lib.types.listOf lib.types.str;
                            };
                          };
                        });
                      };
                      allow_failure = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr lib.types.bool;
                      };
                      interruptible = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr lib.types.bool;
                      };
                      needs = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr (lib.types.listOf lib.types.str);
                      };
                      rules = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr (lib.types.listOf (lib.types.submodule {
                          options = {
                            "if" = lib.mkOption {
                              default = null;
                              type = lib.types.nullOr lib.types.str;
                            };
                          };
                        }));
                      };
                      script = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr (lib.types.listOf lib.types.str);
                      };
                      stage = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr lib.types.str;
                      };
                      variables = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr (lib.types.attrsOf lib.types.str);
                      };
                      when = lib.mkOption {
                        default = null;
                        type = lib.types.nullOr (lib.types.enum ["on_success" "on_failure" "never" "always" "manual" "delayed"]);
                      };
                    };
                  };
                };
              };
            });
          };
          stages = lib.mkOption {
            type = lib.types.listOf lib.types.str;
          };
        };
      });
    };
  };
}
