{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.listToAttrs
    (builtins.map (name:
      lib.nameValuePair name {
        jobs.yamllint = {
          enable = lib.mkDefault true;
          jobConfiguration = {
            interruptible = true;
            needs = [];
            rules = [
              {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
            ];
            script = [
              "nix run --inputs-from . nixpkgs#yamllint -- ."
            ];
            stage = "lint";
          };
        };
      })
    config.ci.yamllint);
  options.ci.yamllint = lib.mkOption {
    default = [];
    type = lib.types.listOf lib.types.str;
  };
}
