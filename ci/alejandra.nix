{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.mapAttrs (_: cfg: {
      jobs.alejandra = {
        enable = lib.mkDefault true;
        jobConfiguration = {
          interruptible = true;
          needs = [];
          rules = [
            {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
          ];
          script =
            (builtins.map (i: "rm ${i}") cfg.ignore)
            ++ [
              "nix run --inputs-from . nixpkgs#alejandra -- --check ."
            ];
          stage = "lint";
        };
      };
    })
    config.ci.alejandra;
  options.ci.alejandra = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.submodule {
      options.ignore = lib.mkOption {
        default = [];
        type = lib.types.listOf lib.types.str;
      };
    });
  };
}
