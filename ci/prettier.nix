{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.listToAttrs
    (builtins.map (name:
      lib.nameValuePair name {
        jobs.prettier = {
          enable = lib.mkDefault true;
          jobConfiguration = {
            interruptible = true;
            needs = [];
            rules = [
              {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
            ];
            script = [
              "nix run --inputs-from . nixpkgs#nodePackages.prettier -- --check ."
            ];
            stage = "lint";
          };
        };
      })
    config.ci.prettier);
  options.ci.prettier = lib.mkOption {
    default = [];
    type = lib.types.listOf lib.types.str;
  };
}
