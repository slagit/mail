{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.mapAttrs (_: cfg: {
      jobs.statix = {
        enable = lib.mkDefault true;
        jobConfiguration = {
          interruptible = true;
          needs = [];
          rules = [
            {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
          ];
          script =
            (builtins.map (i: "rm ${i}") cfg.ignore)
            ++ [
              "nix run --inputs-from . nixpkgs#statix -- check"
            ];
          stage = "lint";
        };
      };
    })
    config.ci.statix;
  options.ci.statix = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.submodule {
      options.ignore = lib.mkOption {
        default = [];
        type = lib.types.listOf lib.types.str;
      };
    });
  };
}
