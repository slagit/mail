{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.mapAttrs (_: cfg: {
      jobs = builtins.listToAttrs (builtins.map ({
        name,
        system,
      }:
        lib.nameValuePair "package:${system}:${name}" {
          enable = lib.mkDefault true;
          jobConfiguration = {
            interruptible = true;
            needs = [];
            rules = [
              {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
            ];
            script = [
              "nix build .#packages.${system}.${name}"
            ];
            stage = "build";
          };
        })
      cfg);
    })
    config.ci.packages;
  options.ci.packages = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.listOf (lib.types.submodule {
      options = {
        name = lib.mkOption {
          type = lib.types.str;
        };
        system = lib.mkOption {
          type = lib.types.str;
        };
      };
    }));
  };
}
