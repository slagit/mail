{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.mapAttrs (_: cfg: {
      jobs = builtins.listToAttrs (map (name:
        lib.nameValuePair "home:${name}" {
          enable = lib.mkDefault true;
          jobConfiguration = {
            interruptible = true;
            needs = [];
            rules = [
              {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
            ];
            script = [
              "nix run --inputs-from . home-manager -- build --flake .#${name}"
            ];
            stage = "build";
          };
        })
      cfg);
    })
    config.ci.homeConfigurations;
  options.ci.homeConfigurations = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.listOf lib.types.str);
  };
}
