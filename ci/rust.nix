{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.mapAttrs (_: cfg: {
      jobs =
        lib.concatMapAttrs (k: v: {
          "rust:clippy:${k}" = {
            enable = lib.mkDefault true;
            jobConfiguration = {
              interruptible = true;
              needs = [];
              rules = [
                {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
              ];
              script = [
                "cd ${v.path}"
                "nix develop .#${k} -c cargo clippy --all-targets --all-features"
              ];
              stage = "lint";
              variables = {
                RUSTFLAGS = "-Dwarnings";
              };
            };
          };
          "rust:fmt:${k}" = {
            enable = lib.mkDefault true;
            jobConfiguration = {
              interruptible = true;
              needs = [];
              rules = [
                {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
              ];
              script = [
                "cd ${v.path}"
                "nix develop .#${k} -c cargo fmt --check"
              ];
              stage = "lint";
            };
          };
        })
        cfg;
    })
    config.ci.rust;
  options.ci.rust = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.attrsOf (lib.types.submodule {
      options = {
        path = lib.mkOption {
          type = lib.types.str;
        };
      };
    }));
  };
}
