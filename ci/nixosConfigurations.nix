{
  config,
  lib,
  ...
}: {
  config.ci.gitlab =
    builtins.mapAttrs (_: cfg: {
      jobs = builtins.listToAttrs (lib.concatMap (name: [
          (lib.nameValuePair "host:${name}" {
            enable = lib.mkDefault true;
            jobConfiguration = {
              interruptible = true;
              needs = [];
              rules = [
                {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
              ];
              script = [
                "nix run --inputs-from . nixpkgs#nixos-rebuild -- build --flake .#${name}"
              ];
              stage = "build";
            };
          })
          (lib.nameValuePair "nvd:host:${name}" {
            enable = lib.mkDefault true;
            jobConfiguration = {
              allow_failure = true;
              artifacts = {
                expose_as = "nvd-host";
                paths = ["nvd.txt"];
              };
              interruptible = true;
              needs = ["host:${name}"];
              rules = [
                {"if" = "$CI_PIPELINE_SOURCE == 'parent_pipeline'";}
              ];
              script = [
                "nix run --inputs-from . nixpkgs#git fetch origin \${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
                "nix build --out-link result.orig \".?ref=origin/\${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}#nixosConfigurations.${name}.config.system.build.toplevel\""
                "nix build \".#nixosConfigurations.${name}.config.system.build.toplevel\""
                "nix run --inputs-from . nixpkgs#nvd diff result.orig result | tee nvd.txt"
              ];
              stage = "report";
            };
          })
        ])
        cfg);
    })
    config.ci.nixosConfigurations;
  options.ci.nixosConfigurations = lib.mkOption {
    default = {};
    type = lib.types.attrsOf (lib.types.listOf lib.types.str);
  };
}
