{pkgs, ...}: {
  environment.systemPackages = [
    pkgs.git
  ];
  services.gitlab-runner = {
    enable = true;
    settings.concurrent = 8;
    services.a = {
      executor = "shell";
      registrationConfigFile = "/run/credentials/gitlab-runner.service/gitlab-runner.env";
    };
  };
  systemd.services.gitlab-runner.serviceConfig.LoadCredential = "gitlab-runner.env:/run/systemd-vaultd.sock";
}
