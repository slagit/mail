{config, ...}: {
  imports = [
    ./flake-module.nix
  ];
  flake.ci = {
    flakeModule = ./flake-module.nix;
    nixosModules.gitlab-runner = ./modules/gitlab-runner.nix;
  };
}
