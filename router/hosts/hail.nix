{
  config,
  lib,
  ...
}: {
  networking = {
    domain = "slagit.net";
    hostName = "hail";
  };
  services = {
    nebula.networks.slagit = {
      cert = lib.mkForce "/persist/etc/nebula/node.crt";
      key = lib.mkForce "/persist/etc/nebula/node.key";
    };
    openssh.enable = true;
  };
  slagit = {
    impermanence.enable = true;
    router = {
      lan_if = "enp3s0";
      wan_if = "enp2s0";
    };
  };
  systemd.services.systemd-vaultd = {
    serviceConfig.LoadCredentialEncrypted = lib.mkForce "vault:/persist/etc/credstore.encrypted/vault";
    unitConfig.ConditionPathExists = lib.mkForce "/persist/etc/credstore.encrypted/vault";
  };
  virtualisation.vmVariant = {
    environment.etc = {
      "nebula/node.crt" = {
        source = ../../devenv/nebula/hail.slagit.net.crt;
      };
      "nebula/node.key" = {
        source = ../../devenv/nebula/hail.slagit.net.key;
        group = "nebula-slagit";
        mode = "0640";
      };
    };
    networking.usePredictableInterfaceNames = lib.mkForce true;
    services.nebula.networks.slagit = {
      cert = lib.mkVMOverride "/etc/nebula/node.crt";
      key = lib.mkVMOverride "/etc/nebula/node.key";
      settings.tun.unsafe_routes = lib.mkVMOverride [];
    };
    slagit.router = {
      lan_if = lib.mkForce "ens5";
      wan_if = lib.mkForce "ens4";
    };
    virtualisation.qemu.networkingOptions = lib.mkForce (
      (config.dev-lib.mkNetworkingOptions "ocrt" "ocrt-hail" config.dev-lib.networking.networks.ocrt.ports.hail)
      ++ (config.dev-lib.mkNetworkingOptions "lan" "lan-hail" config.dev-lib.networking.networks.lan.ports.hail)
    );
  };
}
