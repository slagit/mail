{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.slagit.router;
in {
  config = {
    boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;
    containers.ocrt = {
      autoStart = true;
      config = {
        boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;
        networking = {
          firewall.enable = false;
          nftables = {
            enable = true;
            ruleset = ''
              table inet filter {
                chain input {
                  type filter hook input priority filter; policy drop;
                  meta iif lo accept

                  meta nfproto ipv4 icmp type echo-request counter accept
                  meta iifname "eth0" udp dport 67 counter accept
                }
                chain forward {
                  type filter hook forward priority filter; policy drop;

                  iifname "eth0" oifname "${cfg.wan_if}" counter accept
                  iifname "${cfg.wan_if}" oifname "eth0" ct state established,related counter accept
                }
              }
              table ip nat {
                chain postrouting {
                  type nat hook postrouting priority srcnat;

                  # Masquerade outbound IPv4 traffic
                  oifname "${cfg.wan_if}" masquerade
                }
              }
            '';
          };
          useHostResolvConf = false;
        };
        system.stateVersion = config.system.stateVersion;
        systemd.network = {
          enable = true;
          networks = {
            "40-eth0" = {
              matchConfig.Name = "eth0";
              address = ["10.10.8.1/24"];
              dhcpServerConfig = {
                DNS = [
                  "1.1.1.1"
                  "1.0.0.1"
                ];
                PoolOffset = 100;
              };
              dhcpServerStaticLeases = [
                {
                  dhcpServerStaticLeaseConfig = {
                    Address = "10.10.8.2";
                    MACAddress = "52:54:00:71:43:b5";
                  };
                }
              ];
              networkConfig.DHCPServer = "yes";
              routes = [
                {
                  routeConfig = {
                    Destination = "10.10.1.0/24";
                    Gateway = "10.10.8.2";
                  };
                }
                {
                  routeConfig = {
                    Destination = "10.10.2.0/24";
                    Gateway = "10.10.8.2";
                  };
                }
                {
                  routeConfig = {
                    Destination = "10.10.4.0/24";
                    Gateway = "10.10.8.2";
                  };
                }
                {
                  routeConfig = {
                    Destination = "10.10.7.0/24";
                    Gateway = "10.10.8.2";
                  };
                }
              ];
            };
            "40-wan" = {
              dhcpV4Config.UseHostname = "no";
              matchConfig.Name = cfg.wan_if;
              networkConfig = {
                DHCP = "yes";
                IPv6AcceptRA = "no";
                IPv6PrivacyExtensions = "kernel";
              };
            };
          };
        };
      };
      hostBridge = "integration";
      interfaces = [cfg.wan_if];
      privateNetwork = true;
    };
    environment.systemPackages = [
      pkgs.tcpdump
      pkgs.conntrack-tools
    ];
    networking = {
      firewall.enable = false;
      nftables = {
        enable = true;
        ruleset = ''
          table inet filter {
            chain input {
              type filter hook input priority filter; policy drop;
              meta iif lo accept
              ct state { established, related } counter accept

              meta nfproto ipv4 icmp type echo-request counter accept

              # Allow Nebula connections
              meta iifname "wireless" udp dport 4242 counter accept

              # Allow Monitoring, SSH and Unifi Controller from Nebula network
              meta iifname "nebula.slagit" tcp dport { 22, 8443, 9100 } ct state new tcp flags & (syn | ack) == syn counter accept

              meta iifname "${cfg.lan_if}" tcp dport { 6789, 8080 } ct state new tcp flags & (syn | ack) == syn counter accept
              meta iifname "${cfg.lan_if}" udp dport { 3478, 10001 } ct state new counter accept

              # Allow DHCP, NTP connections
              meta iifname { "${cfg.lan_if}", "camera", "cloud", "wireless" } udp dport { 67, 123 } counter accept
            }
            chain forward {
              type filter hook forward priority filter; policy drop;

              # Allow internet access
              iifname { "${cfg.lan_if}", "cloud", "fish", "wireless" } oifname "integration" counter accept
              iifname "integration" oifname { "cloud", "fish", "wireless", "${cfg.lan_if}" } ct state established,related counter accept

              # Allow Nebula network to access camera and idrac
              iifname "nebula.slagit" oifname { "camera", "idrac" } counter accept
              iifname { "camera", "idrac" } oifname "nebula.slagit" ct state established,related counter accept

              # Allow Nebula network to access switch.slagit.net:80
              iifname "nebula.slagit" oifname "${cfg.lan_if}" ip daddr 10.10.1.2 tcp dport 80 ct state new tcp flags & (syn | ack) == syn counter accept
              iifname "nebula.slagit" oifname "${cfg.lan_if}" ip daddr 10.10.1.2 tcp dport 80 ct state established,related counter accept
              iifname "${cfg.lan_if}" oifname "nebula.slagit" ip saddr 10.10.1.2 tcp sport 80 ct state established,related counter accept

              # Allow internal Nebula connections
              iifname { "cloud", "wireless" } oifname { "cloud", "wireless" } udp sport 4242 udp dport 4242 counter accept
            }
          }
        '';
      };
      useDHCP = false;
    };
    services = {
      chrony = {
        enable = true;
        extraConfig = ''
          allow 10.10.1.0/24
          allow 10.10.2.0/24
          allow 10.10.4.0/24
          allow 10.10.6.0/24
        '';
      };
      nebula.networks.slagit = {
        firewall.inbound = [
          # switch.slagit.net
          {
            port = "80";
            proto = "tcp";
            group = "admins";
            local_cidr = "10.10.1.2/32";
          }
          # iDrac
          {
            port = "any";
            proto = "any";
            group = "admins";
            local_cidr = "10.10.3.0/24";
          }
          # NVR
          {
            port = "any";
            proto = "tcp";
            group = "users";
            local_cidr = "10.10.6.180/32";
          }
        ];
        settings.tun.unsafe_routes = lib.mkForce [];
      };
    };
    systemd.network = {
      enable = true;
      netdevs = {
        "40-camera" = {
          netdevConfig = {
            Kind = "vlan";
            Name = "camera";
          };
          vlanConfig.Id = 6;
        };
        "40-cloud" = {
          netdevConfig = {
            Kind = "vlan";
            Name = "cloud";
          };
          vlanConfig.Id = 4;
        };
        "40-fish" = {
          netdevConfig = {
            Kind = "vlan";
            Name = "fish";
          };
          vlanConfig.Id = 7;
        };
        "40-idrac" = {
          netdevConfig = {
            Kind = "vlan";
            Name = "idrac";
          };
          vlanConfig.Id = 3;
        };
        "40-integration" = {
          netdevConfig = {
            Kind = "bridge";
            Name = "integration";
            MACAddress = "52:54:00:71:43:b5";
          };
        };
        "40-wireless" = {
          netdevConfig = {
            Kind = "vlan";
            Name = "wireless";
          };
          vlanConfig.Id = 2;
        };
      };
      networks = {
        "40-camera" = {
          dhcpServerConfig = {
            EmitDNS = "no";
            NTP = "10.10.6.1";
          };
          matchConfig.Name = "camera";
          networkConfig = {
            Address = "10.10.6.1/24";
            DHCP = "no";
            DHCPServer = "yes";
            IPv6AcceptRA = "no";
            IPv6PrivacyExtensions = "kernel";
          };
        };
        "40-cloud" = {
          dhcpServerConfig = {
            DNS = [
              "1.1.1.1"
              "1.0.0.1"
            ];
            NTP = "10.10.4.1";
          };
          matchConfig.Name = "cloud";
          networkConfig = {
            Address = "10.10.4.1/24";
            DHCP = "no";
            DHCPServer = "yes";
            IPv6AcceptRA = "no";
            IPv6PrivacyExtensions = "kernel";
          };
        };
        "40-idrac" = {
          matchConfig.Name = "idrac";
          networkConfig = {
            Address = "10.10.3.1/24";
          };
        };
        "40-lan" = {
          dhcpServerConfig = {
            DNS = [
              "1.1.1.1"
              "1.0.0.1"
            ];
            NTP = "10.10.1.1";
            PoolOffset = 128;
          };
          matchConfig.Name = cfg.lan_if;
          networkConfig = {
            Address = "10.10.1.1/24";
            DHCP = "no";
            DHCPServer = "yes";
            IPv6AcceptRA = "no";
            IPv6PrivacyExtensions = "kernel";
          };
          vlan = [
            "camera"
            "cloud"
            "fish"
            "idrac"
            "wireless"
          ];
        };
        "40-fish" = {
          matchConfig.Name = "fish";
          networkConfig = {
            Address = "10.10.7.1/24";
            DHCP = "no";
            IPv6AcceptRA = "no";
            IPv6PrivacyExtensions = "kernel";
          };
        };
        "40-integration" = {
          matchConfig.Name = "integration";
          networkConfig.DHCP = "yes";
        };
        "40-wireless" = {
          dhcpServerConfig = {
            DNS = [
              "1.1.1.1"
              "1.0.0.1"
            ];
            NTP = "10.10.2.1";
          };
          matchConfig.Name = "wireless";
          networkConfig = {
            Address = "10.10.2.1/24";
            DHCP = "no";
            DHCPServer = "yes";
            IPv6AcceptRA = "no";
            IPv6PrivacyExtensions = "kernel";
          };
        };
      };
    };
    virtualisation.vmVariant.systemd.network.networks = {
      "40-cloud".dhcpServerConfig.DNS = lib.mkForce [
        "10.30.2.2"
      ];
      "40-wireless".dhcpServerConfig.DNS = lib.mkForce [
        "10.30.2.2"
      ];
    };
  };
  options.slagit.router = {
    lan_if = lib.mkOption {
      type = lib.types.str;
    };
    wan_if = lib.mkOption {
      type = lib.types.str;
    };
  };
}
