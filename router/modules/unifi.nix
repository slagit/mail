{
  config,
  pkgs,
  ...
}: {
  networking.firewall.interfaces."nebula.slagit".allowedTCPPorts = [8443];
  services = {
    nebula.networks.slagit.firewall.inbound = [
      {
        port = "8443";
        proto = "tcp";
        group = "admins";
        local_cidr = "10.10.10.5/32";
      }
    ];
    unifi = {
      enable = true;
      # TODO: Do this with nftables, and only open the firewall on the management network
      openFirewall = true;
      unifiPackage = pkgs.unifi8;
    };
  };
  slagit = {
    impermanence.persist = ["/var/lib/unifi"];
    unfree = [
      "mongodb"
      "unifi-controller"
    ];
    vault-agent.unifi = let
      update_unifi_cert = pkgs.writeShellScript "update.sh" ''
        set -eu

        umask 0077

        ${pkgs.openssl}/bin/openssl pkcs12 -export -inkey /run/vault-agent@unifi/unifi-key.pem -in /run/vault-agent@unifi/unifi.pem -out /run/vault-agent@unifi/unifi.p12 -name unifi -password pass:temppass
        ${config.services.unifi.jrePackage}/bin/keytool -importkeystore -deststorepass aircontrolenterprise -destkeypass aircontrolenterprise -destkeystore /run/vault-agent@unifi/keystore -srckeystore /run/vault-agent@unifi/unifi.p12 -srcstoretype PKCS12 -srcstorepass temppass -alias unifi -noprompt -deststoretype pkcs12
        ln -fs /run/vault-agent@unifi/keystore /var/lib/unifi/data/keystore
      '';
    in {
      template = [
        {
          contents = "{{- with secret \"secret/acme/unifi/unifi.slagit.net\" }}{{ index .Data.data \"fullchain.pem\" }}{{- end }}";
          destination = "/run/vault-agent@unifi/unifi.pem";
          error_on_missing_key = true;
          exec = {
            command = toString update_unifi_cert;
          };
          perms = "0644";
        }
        {
          contents = "{{- with secret \"secret/acme/unifi/unifi.slagit.net\" }}{{ index .Data.data \"private_key.pem\" }}{{- end }}";
          destination = "/run/vault-agent@unifi/unifi-key.pem";
          error_on_missing_key = true;
          exec = {
            command = toString update_unifi_cert;
          };
          perms = "0600";
        }
      ];
      role = "unifi";
      user = config.systemd.services.unifi.serviceConfig.User;
    };
  };
  systemd.services.unifi = {
    after = ["vault-agent@unifi.service"];
    bindsTo = ["vault-agent@unifi.service"];
  };
}
