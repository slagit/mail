{
  self,
  inputs,
  lib,
  ...
}: {
  flake = {
    nixosConfigurations.hail = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        self.common.hardware.nixosModules.zimaboard
        self.nixosModules.common
        self.nixosModules.dev-lib
        self.nixosModules.nebula
        self.nixosModules.systemd-vaultd
        self.router.nixosModules.router
        self.router.nixosModules.unifi
        ./hosts/hail.nix
      ];
    };
    router.nixosModules = {
      router = ./modules/router.nix;
      unifi = ./modules/unifi.nix;
    };
  };
}
