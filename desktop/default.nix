{
  self,
  inputs,
  ...
}: {
  flake = {
    nixosConfigurations.cyclone = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        self.common.hardware.nixosModules.gaze16
        self.desktop.nixosModules.desktop
        self.nixosModules.common
        self.nixosModules.nebula
        ./hosts/cyclone.nix
      ];
    };
    desktop.nixosModules.desktop = ./modules/desktop.nix;
  };
}
