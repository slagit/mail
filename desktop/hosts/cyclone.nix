_: {
  hardware = {
    opengl.driSupport32Bit = true;
    pulseaudio.support32Bit = true;
  };
  networking.hostName = "cyclone";
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };
  slagit.unfree = [
    "steam"
    "steam-original"
    "steam-run"
  ];
  time.timeZone = "America/New_York";
  users.users.albert.extraGroups = ["docker" "networkmanager" "libvirtd"];
  virtualisation = {
    docker.enable = true;
    libvirtd.enable = true;
  };
}
