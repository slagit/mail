_: {
  resource = {
    vault_approle_auth_backend_role = {
      monitoring-nginx = {
        backend = "\${vault_auth_backend.approle.path}";
        role_name = "monitoring-nginx";
        secret_id_num_uses = 1;
        token_period = 3600;
      };
    };
  };
  slagit.vault = {
    identities = {
      app-monitoring-nginx = {
        aliases = [
          {
            backend = "vault_auth_backend.approle";
            name = "\${vault_approle_auth_backend_role.monitoring-nginx.role_id}";
          }
        ];
        policies = [
          "monitoring-nginx"
        ];
      };
      host-compute0-slagit-net.policies = ["monitoring-nginx-host"];
    };
    policies = {
      monitoring-nginx-host.paths = {
        "auth/approle/role/monitoring-nginx/role-id".capabilities = ["read"];
        "auth/approle/role/monitoring-nginx/secret-id" = {
          capabilities = ["create" "update"];
          min_wrapping_ttl = "1s";
          max_wrapping_ttl = "10m";
        };
      };
      monitoring-nginx.paths = {
        "secret/data/acme/monitoring-nginx/grafana.slagit.net".capabilities = ["read"];
      };
    };
  };
}
