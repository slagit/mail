{config, ...}: {
  networking.firewall.interfaces."nebula.slagit".allowedTCPPorts = [80 443];
  services = {
    grafana = {
      enable = true;
      provision = {
        dashboards.settings.providers = [
          {
            name = "dashboards";
            options = {
              foldersFromFilesStructure = true;
              path = ./dashboards;
            };
          }
        ];
        datasources.settings.datasources = [
          {
            name = "Ledger";
            type = "postgres";
            url = "/run/postgresql";
            user = "grafana";
            jsonData = {
              database = "ledger";
              sslmode = "disable";
            };
          }
          {
            name = "Prometheus";
            type = "prometheus";
            url = "http://localhost:9090";
          }
        ];
      };
      settings.server = {
        domain = "grafana.slagit.net";
        http_addr = "127.0.0.1";
        http_port = 3000;
      };
    };
    nebula.networks.slagit.firewall.inbound = [
      {
        port = "80";
        proto = "tcp";
        group = "users";
      }
      {
        port = "443";
        proto = "tcp";
        group = "users";
      }
    ];
    nginx = {
      enable = true;
      virtualHosts = {
        "grafana.slagit.net" = {
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://${toString config.services.grafana.settings.server.http_addr}:${toString config.services.grafana.settings.server.http_port}";
            proxyWebsockets = true;
            recommendedProxySettings = true;
          };
          sslCertificate = "/run/vault-agent@monitoring-nginx/grafana.slagit.net.pem";
          sslCertificateKey = "/run/vault-agent@monitoring-nginx/grafana.slagit.net-key.pem";
        };
      };
    };
    postgresql = {
      enable = true;
      ensureDatabases = ["ledger"];
      ensureUsers = [
        {name = "albert";}
        {name = "grafana";}
      ];
    };
    prometheus = {
      enable = true;
      listenAddress = "127.0.0.1";
      scrapeConfigs = [
        {
          job_name = "node";
          relabel_configs = [
            {
              source_labels = ["__address__"];
              regex = "([^:]+):\\d+";
              target_label = "instance";
            }
          ];
          static_configs = [
            {
              targets = [
                "compute0.slagit.net:${toString config.services.prometheus.exporters.node.port}"
                "compute1.slagit.net:${toString config.services.prometheus.exporters.node.port}"
                "franklin.zt.slagit.net:${toString config.services.prometheus.exporters.node.port}"
                "gutenberg.slagit.net:${toString config.services.prometheus.exporters.node.port}"
                "hail.slagit.net:${toString config.services.prometheus.exporters.node.port}"
                "morgan.zt.slagit-vault.net:${toString config.services.prometheus.exporters.node.port}"
              ];
            }
          ];
        }
      ];
    };
  };
  slagit.vault-agent.monitoring-nginx = {
    template = [
      {
        contents = "{{- with secret \"secret/acme/monitoring-nginx/grafana.slagit.net\" }}{{ index .Data.data \"fullchain.pem\" }}{{- end }}";
        destination = "/run/vault-agent@monitoring-nginx/grafana.slagit.net.pem";
        error_on_missing_key = true;
        perms = "0644";
      }
      {
        contents = "{{- with secret \"secret/acme/monitoring-nginx/grafana.slagit.net\" }}{{ index .Data.data \"private_key.pem\" }}{{- end }}";
        destination = "/run/vault-agent@monitoring-nginx/grafana.slagit.net-key.pem";
        error_on_missing_key = true;
        perms = "0600";
      }
    ];
    role = "monitoring-nginx";
    user = config.systemd.services.nginx.serviceConfig.User;
  };
  systemd.services.nginx = {
    after = ["vault-agent@monitoring-nginx.service"];
    bindsTo = ["vault-agent@monitoring-nginx.service"];
  };
}
