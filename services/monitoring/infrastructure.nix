_: {
  resource.linode_domain_record = {
    grafana_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "grafana";
      record_type = "CNAME";
      target = "compute0.slagit.net";
    };
    _acme_challenge_grafana_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "_acme-challenge.grafana";
      record_type = "CNAME";
      target = "grafana.slagit.net.slagit-cert.net";
    };
  };
}
