# Vault Backed systemd Credential Daemon

## Development

For development purposes, the software can all be run locally.

Run the server:

```bash
SECRET_PATH=servers/servername cargo run -- run --listen creds.sock
```

Run the client:

```bash
cargo run -- client creds.sock demo.service secret-id
```
