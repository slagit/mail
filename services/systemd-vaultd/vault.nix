_: {
  slagit.vault.policies = {
    admin.paths."secret/data/servers/*".capabilities = ["create" "read" "update"];
    host.paths."secret/data/servers/{{identity.entity.metadata.hostname}}/*".capabilities = ["read"];
  };
}
