{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.services.systemd-vaultd;
in {
  config = {
    environment.systemPackages = [
      cfg.package
    ];
    systemd = {
      services.systemd-vaultd = {
        after = ["network-online.target"];
        description = "Vault credential server for systemd units";
        environment = {
          SECRET_PATH = "servers/${config.networking.fqdn}";
          VAULT_ADDR = "https://slagit-vault.net:8200";
        };
        requires = ["network-online.target"];
        script = "exec ${cfg.package}/bin/systemd-vaultd run";
        serviceConfig = {
          DynamicUser = "yes";
          LoadCredentialEncrypted = "vault";
          NoNewPrivileges = "yes";
          ProtectSystem = "full";
        };
        unitConfig.ConditionPathExists = "/etc/credstore.encrypted/vault";
      };
      sockets.systemd-vaultd = {
        description = "Socket for vault credential server";
        listenStreams = ["/run/systemd-vaultd.sock"];
        socketConfig = {
          SocketUser = "root";
          SocketGroup = "root";
          SocketMode = 0600;
        };
        wantedBy = ["sockets.target"];
      };
    };
    virtualisation.vmVariant.environment.systemPackages = [
      (pkgs.writeShellScriptBin "devenv-register-self" ''
        set -euo pipefail

        . /tmp/shared/env_file
        export VAULT_ADDR=https://slagit-vault.net:8200

        systemd-vaultd register-self ${config.networking.fqdn}
      '')
    ];
  };
  options.services.systemd-vaultd = {
    package = lib.mkOption {
      type = lib.types.package;
    };
  };
}
