use clap::Parser;
use serde::{Deserialize, Serialize};
use std::{env, error::Error};
use vaultrs::{
    client::{VaultClient, VaultClientSettingsBuilder, VaultClientSettingsBuilderError},
    error::ClientError,
};

mod client;
mod init;
mod list;
mod register;
mod revoke;
mod run;

#[derive(Parser)]
#[command(about)]
enum Cli {
    /// Load credential from socket with test client
    Client(client::ClientArgs),
    /// Initialize vault credentials for a system
    Init(init::InitArgs),
    /// List active secret ids
    List(list::ListArgs),
    /// Register a new secret id
    Register(register::RegisterArgs),
    /// Register a new secret id and initialize this host
    RegisterSelf(register::RegisterArgs),
    /// Revoke a secret id
    Revoke(revoke::RevokeArgs),
    /// Run systemd credentials daemon
    Run(run::RunArgs),
}

#[derive(Debug, Deserialize, Serialize)]
struct Credentials {
    role_id: String,
    secret_id: String,
}

#[derive(Debug, thiserror::Error)]
enum AppError {
    #[error("failed to run test client")]
    Client(#[source] client::Error),
    #[error("failed to initialize systemd-vaultd")]
    Init(#[source] init::Error),
    #[error("failed to list secret_ids")]
    List(#[source] list::Error),
    #[error("failed to register new secret_id")]
    Register(#[source] register::Error),
    #[error("failed to revoke secret_id")]
    Revoke(#[source] revoke::Error),
    #[error("failed to run systemd credential daemon")]
    Run(#[source] run::Error),
}

#[tokio::main]
async fn main() {
    let args = Cli::parse();

    let result = match args {
        Cli::Client(args) => client::client(args).await.map_err(AppError::Client),
        Cli::Init(args) => init::init(args).await.map_err(AppError::Init),
        Cli::List(args) => list::list(args).await.map_err(AppError::List),
        Cli::Register(args) => register::register(args).await.map_err(AppError::Register),
        Cli::RegisterSelf(args) => match register::step(args).await {
            Err(err) => Err(AppError::Register(err)),
            Ok((role_id, wrapped_secret_id)) => init::step(
                role_id,
                wrapped_secret_id,
                "/etc/credstore.encrypted/vault".into(),
            )
            .await
            .map_err(AppError::Init),
        },
        Cli::Revoke(args) => revoke::revoke(args).await.map_err(AppError::Revoke),
        Cli::Run(args) => run::run(args).await.map_err(AppError::Run),
    };
    if let Err(err) = result {
        eprintln!("[ERROR] {}", err);
        if let Some(cause) = err.source() {
            eprintln!();
            eprintln!("Caused by:");
            for (i, e) in std::iter::successors(Some(cause), |e| (*e).source()).enumerate() {
                eprintln!("    {}: {}", i, e);
            }
        }
        std::process::exit(1);
    }
}

#[derive(Debug, thiserror::Error)]
pub enum NewClientError {
    #[error("failed to configure vault client")]
    Settings(#[source] VaultClientSettingsBuilderError),
    #[error("failed to create vault client")]
    Client(#[source] ClientError),
}

fn new_client() -> Result<VaultClient, NewClientError> {
    let mut paths = vec!["/etc/ssl/certs/ca-certificates.crt".to_owned()];
    if let Ok(s) = env::var("VAULT_CACERT") {
        paths.push(s);
    }
    VaultClient::new(
        VaultClientSettingsBuilder::default()
            .ca_certs(paths)
            .build()
            .map_err(NewClientError::Settings)?,
    )
    .map_err(NewClientError::Client)
}

fn new_client_with_token(token: &str) -> Result<VaultClient, NewClientError> {
    let mut paths = vec!["/etc/ssl/certs/ca-certificates.crt".to_owned()];
    if let Ok(s) = env::var("VAULT_CACERT") {
        paths.push(s);
    }
    VaultClient::new(
        VaultClientSettingsBuilder::default()
            .ca_certs(paths)
            .token(token)
            .build()
            .map_err(NewClientError::Settings)?,
    )
    .map_err(NewClientError::Client)
}
