use crate::{new_client_with_token, Credentials, NewClientError};
use clap::Parser;
use std::{fs::create_dir_all, io, path::PathBuf, process::Stdio};
use tokio::{
    fs::File,
    io::{copy, AsyncWriteExt},
    process::Command,
};
use vaultrs::{
    api::auth::approle::responses::GenerateNewSecretIDResponse, error::ClientError, sys::wrapping,
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create vault client")]
    Client(NewClientError),
    #[error("failed to create credential store parent directory")]
    CreateParent(#[source] io::Error),
    #[error("invalid credential store path: {0}")]
    InvalidCredStore(PathBuf),
    #[error("failed to launch systemd-creds")]
    LaunchSystemdCreds(#[source] io::Error),
    #[error("failed to open crediential store")]
    OpenCredStore(#[source] io::Error),
    #[error("failed to save encrypted credential")]
    Save(io::Error),
    #[error("failed to unwrap secret id")]
    UnwrapSecretId(#[source] ClientError),
    #[error("failed send secret id to systemd-creds")]
    WritePlain(#[source] io::Error),
}

#[derive(Parser)]
pub struct InitArgs {
    /// Role id for vault authentication
    role_id: String,
    /// Wrapped secret id for vault authentication
    wrapped_secret_token: String,
    /// Path to credential storage file
    #[arg(long, default_value = "/etc/credstore.encrypted/vault")]
    credstore: PathBuf,
}

pub async fn step(
    role_id: String,
    wrapped_secret_token: String,
    credstore: PathBuf,
) -> Result<(), Error> {
    let Some(name) = credstore.file_name().and_then(|filename| filename.to_str()) else {
        return Err(Error::InvalidCredStore(credstore));
    };
    if let Some(parent) = credstore.parent() {
        if !parent.is_dir() {
            create_dir_all(parent).map_err(Error::CreateParent)?;
        }
    }

    let client = new_client_with_token(&wrapped_secret_token).map_err(Error::Client)?;
    let s: GenerateNewSecretIDResponse = wrapping::unwrap(&client, None)
        .await
        .map_err(Error::UnwrapSecretId)?;
    let mut cmd = Command::new("systemd-creds")
        .args(["--name", name, "encrypt", "-", "-"])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .map_err(Error::LaunchSystemdCreds)?;
    let mut stdin = cmd.stdin.take().unwrap();
    let mut stdout = cmd.stdout.take().unwrap();
    stdin
        .write_all(
            &serde_json::to_vec(&Credentials {
                role_id,
                secret_id: s.secret_id,
            })
            .unwrap(),
        )
        .await
        .map_err(Error::WritePlain)?;
    drop(stdin);
    let mut file = File::options()
        .write(true)
        .mode(0o400)
        .create(true)
        .truncate(true)
        .open(&credstore)
        .await
        .map_err(Error::OpenCredStore)?;
    copy(&mut stdout, &mut file).await.map_err(Error::Save)?;
    file.flush().await.map_err(Error::Save)?;
    Ok(())
}

pub async fn init(args: InitArgs) -> Result<(), Error> {
    step(args.role_id, args.wrapped_secret_token, args.credstore).await
}
