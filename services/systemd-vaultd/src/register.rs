use crate::{new_client, NewClientError};
use clap::Parser;
use vaultrs::{
    api::{
        auth::approle::requests::{
            GenerateNewSecretIDRequest, GenerateNewSecretIDRequestBuilderError,
        },
        ResponseWrapper,
    },
    auth::approle::role,
    error::ClientError,
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create vault client")]
    Client(NewClientError),
    #[error("failed to read role_id")]
    RoleId(#[source] ClientError),
    #[error("failed to generate secret_id request")]
    SecretIdEndpoint(#[source] GenerateNewSecretIDRequestBuilderError),
    #[error("failed to generate secret_id")]
    SecretId(#[source] ClientError),
}

#[derive(Parser)]
pub struct RegisterArgs {
    /// Machine name
    machine: String,
    /// Mount point for vault approle auth mechanism
    #[arg(long, default_value = "approle")]
    mount: String,
}

pub async fn step(args: RegisterArgs) -> Result<(String, String), Error> {
    let client = new_client().map_err(Error::Client)?;
    let role_id = role::read_id(&client, &args.mount, &args.machine)
        .await
        .map_err(Error::RoleId)?
        .role_id;
    let wrapped_secret_id = GenerateNewSecretIDRequest::builder()
        .mount(&args.mount)
        .role_name(&args.machine)
        .build()
        .map_err(Error::SecretIdEndpoint)?
        .wrap(&client)
        .await
        .map_err(Error::SecretId)?;
    Ok((role_id, wrapped_secret_id.info.token))
}

pub async fn register(args: RegisterArgs) -> Result<(), Error> {
    let (role_id, wrapped_secret_id) = step(args).await?;
    println!("Run the following command on the target system:");
    println!();
    println!("systemd-vaultd init {} {}", role_id, wrapped_secret_id);
    Ok(())
}
