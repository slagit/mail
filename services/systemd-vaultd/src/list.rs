use crate::{new_client, NewClientError};
use clap::Parser;
use vaultrs::{auth::approle::role::secret, error::ClientError};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create vault client")]
    Client(NewClientError),
    #[error("failed to list secret_id accessors")]
    List(#[source] ClientError),
    #[error("failed to read secret_id accessor {0}")]
    Read(String, #[source] ClientError),
}

#[derive(Parser)]
pub struct ListArgs {
    /// Machine name
    machine: String,
    /// Mount point for vault approle auth mechanism
    #[arg(long, default_value = "approle")]
    mount: String,
}

pub async fn list(args: ListArgs) -> Result<(), Error> {
    let client = new_client().map_err(Error::Client)?;
    let mut secret_ids = vec![];
    for key in secret::list(&client, &args.mount, &args.machine)
        .await
        .map_err(Error::List)?
        .keys
    {
        secret_ids.push(
            secret::read_accessor(&client, &args.mount, &args.machine, &key)
                .await
                .map_err(|err| Error::Read(key, err))?,
        );
    }
    secret_ids.sort_by_key(|secret_id| secret_id.creation_time.clone());
    for secret_id in secret_ids {
        println!("Key: {}", secret_id.secret_id_accessor);
        println!("    Created: {}", secret_id.creation_time);
    }
    Ok(())
}
