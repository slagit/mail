use clap::Parser;
use std::path::PathBuf;
use tokio::{io::AsyncReadExt, net::UnixSocket};

#[derive(Parser)]
pub struct ClientArgs {
    socket: PathBuf,
    service: String,
    credential: String,
}

#[derive(Debug, thiserror::Error)]
pub enum Error {}

pub async fn client(args: ClientArgs) -> Result<(), Error> {
    let stream = UnixSocket::new_stream().unwrap();
    stream
        .bind(format!("\0a/b/{}/{}", args.service, args.credential))
        .unwrap();
    let mut stream = stream.connect(args.socket).await.unwrap();
    let mut dst = String::new();
    stream.read_to_string(&mut dst).await.unwrap();
    println!("{}", dst);
    Ok(())
}
