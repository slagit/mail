use crate::{new_client, new_client_with_token, Credentials, NewClientError};
use clap::Parser;
use libsystemd::{
    activation::{receive_descriptors, IsType},
    errors::SdError,
};
use std::{
    collections::HashMap,
    env::{self, VarError},
    io,
    ops::Deref,
    os::{
        fd::{FromRawFd, IntoRawFd},
        linux::net::SocketAddrExt,
        unix::net::SocketAddr,
    },
    path::{Path, PathBuf},
    sync::Arc,
};
use tokio::{
    fs,
    io::AsyncWriteExt,
    net::{UnixListener, UnixStream},
    select,
    sync::Mutex,
    time::{sleep, Duration},
};
use tokio_util::task::TaskTracker;
use tracing::{error, info, warn};
use vaultrs::{
    api::{auth::approle::requests::GenerateNewSecretIDRequest, ResponseWrapper},
    auth::approle::{login, role::read_id},
    client::VaultClient,
    error::ClientError,
    kv2::read,
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to activate socket from systemd")]
    Activation(#[from] ActivationError),
    #[error("failed to wrap async listener")]
    AsyncListener(#[source] io::Error),
    #[error("failed to create vault client")]
    Client(#[source] NewClientError),
    #[error("CREDENTIALS_DIRECTORY environment variable is not set")]
    CredentialsDirectory(#[source] VarError),
    #[error("failed to login")]
    Login(#[source] ClientError),
    #[error("failed to set listener to non-blocking mode")]
    NonBlocking(#[source] io::Error),
    #[error("failed to open unix socket")]
    Open(#[source] io::Error),
    #[error("failed to parse credentials")]
    Parse(#[source] serde_json::Error),
    #[error("failed to read credentials")]
    Read(#[source] io::Error),
    #[error("SECRET_PATH environment variable is not set")]
    SecretPath(#[source] VarError),
    #[error("failed to parse VAULT_TOKEN variable")]
    VaultToken(#[source] VarError),
}

#[derive(Parser)]
pub struct RunArgs {
    /// Unix socket to serve requests from [default: systemd activation]
    #[arg(long, default_value = None)]
    listen: Option<PathBuf>,
    /// Mount point for vault approle auth mechanism
    #[arg(long, default_value = "approle")]
    mount: String,
}

#[derive(Debug, thiserror::Error)]
pub enum ActivationError {
    #[error("received zero fds from systemd")]
    Count,
    #[error("received fd was not a unix socket")]
    NotUnix,
    #[error("failed to receive fds from systemd")]
    Receive(#[from] SdError),
}

fn receive_listener() -> Result<std::os::unix::net::UnixListener, ActivationError> {
    let descriptor = receive_descriptors(true)?
        .into_iter()
        .next()
        .ok_or(ActivationError::Count)?;
    if descriptor.is_unix() {
        let fd = descriptor.into_raw_fd();
        unsafe { Ok(std::os::unix::net::UnixListener::from_raw_fd(fd)) }
    } else {
        Err(ActivationError::NotUnix)
    }
}

pub async fn run(args: RunArgs) -> Result<(), Error> {
    tracing_subscriber::fmt().init();
    let listener = match args.listen {
        Some(path) => UnixListener::bind(path).map_err(Error::Open)?,
        None => {
            let listener = receive_listener()?;
            listener.set_nonblocking(true).map_err(Error::NonBlocking)?;
            UnixListener::from_std(listener).map_err(Error::AsyncListener)?
        }
    };

    let secret_path = env::var("SECRET_PATH").map_err(Error::SecretPath)?;
    let (client_token, timeout) = match env::var("VAULT_TOKEN") {
        Ok(vault_token) => (vault_token, Duration::from_secs(3600)),
        Err(VarError::NotPresent) => {
            let cred_dir =
                env::var("CREDENTIALS_DIRECTORY").map_err(Error::CredentialsDirectory)?;
            let cred_path = Path::new(&cred_dir).join("vault");
            let raw = fs::read(cred_path).await.map_err(Error::Read)?;
            let creds: Credentials = serde_json::from_slice(&raw).map_err(Error::Parse)?;
            let client = new_client().map_err(Error::Client)?;
            let s = login(&client, &args.mount, &creds.role_id, &creds.secret_id)
                .await
                .map_err(Error::Login)?;
            info!(
                accessor = s.accessor,
                policies = s.policies.join(", "),
                ttl = s.lease_duration,
                "Successfully logged in to vault"
            );
            (
                s.client_token,
                Duration::from_secs(s.lease_duration * 7 / 10),
            )
        }
        Err(err) => {
            return Err(Error::VaultToken(err));
        }
    };
    let client = new_client_with_token(&client_token).map_err(Error::Client)?;
    info!(timeout = timeout.as_secs(), "Listening for connections");
    let tracker = TaskTracker::new();
    select! {
        _ = serve_vault(listener, Arc::new(Mutex::new(client)), &tracker, secret_path) => {
        }
        _ = sleep(timeout) => {
            warn!("Server timeout expired. Shutting down.");
        }
    }
    tracker.close();
    select! {
        _ = tracker.wait() => {}
        _ = sleep(Duration::from_secs(30)) => {
            error!("Timeout waiting for existing connections to finish");
        }
    }
    Ok(())
}

async fn serve_vault(
    listener: UnixListener,
    client: Arc<Mutex<VaultClient>>,
    tracker: &TaskTracker,
    secret_path: String,
) {
    loop {
        match listener.accept().await {
            Ok((stream, _addr)) => {
                tracker.spawn(serve_conn(stream, client.clone(), secret_path.clone()));
            }
            Err(e) => {
                error!(error = e.to_string(), "Error listening for connections");
            }
        }
    }
}

async fn serve_conn(mut stream: UnixStream, client: Arc<Mutex<VaultClient>>, secret_path: String) {
    let addr: Option<SocketAddr> = stream.peer_addr().ok().map(Into::into);
    let request = addr
        .as_ref()
        .and_then(SocketAddr::as_abstract_name)
        .map(|a| a.split(|x| *x == b'/').collect())
        .and_then(|a: Vec<_>| {
            if let [_, _, service, credential] = a[..] {
                Some((
                    std::str::from_utf8(service).ok()?,
                    std::str::from_utf8(credential).ok()?,
                ))
            } else {
                None
            }
        });
    if let Some((service, credential_name)) = request {
        info!(
            service = service,
            credential_name = credential_name,
            "Handling credential request"
        );
        let service = match service.strip_suffix(".service") {
            Some(s) => s,
            None => service,
        };
        let service = match service.strip_prefix("vault-agent@") {
            Some(s) => s,
            None => service,
        };
        match credential_name {
            "role-id" => match read_id(client.lock().await.deref(), "approle", service).await {
                Ok(s) => {
                    write_credential(&mut stream, &s.role_id).await;
                }
                Err(err) => {
                    error!(error = err.to_string(), "Error fetching role id from vault");
                }
            },
            "secret-id" => {
                match GenerateNewSecretIDRequest::builder()
                    .mount("approle")
                    .role_name(service)
                    .build()
                {
                    Ok(endpoint) => match endpoint.wrap(client.lock().await.deref()).await {
                        Ok(s) => {
                            write_credential(&mut stream, &s.info.token).await;
                        }
                        Err(err) => {
                            error!(
                                error = err.to_string(),
                                "Error fetching secret id from vault"
                            );
                        }
                    },
                    Err(err) => {
                        error!(error = err.to_string(), "Error building secret-id request");
                    }
                }
            }
            _ => {
                match read::<HashMap<String, String>>(
                    client.lock().await.deref(),
                    "secret",
                    &format!("{}/{}", secret_path, credential_name),
                )
                .await
                {
                    Ok(s) => {
                        if let Some(credential) = s.get("credential") {
                            write_credential(&mut stream, credential).await;
                        } else {
                            error!(r#"Credential did not contain "credential" key"#);
                        }
                    }
                    Err(err) => {
                        error!(
                            error = err.to_string(),
                            "Error fetching credential from vault"
                        );
                    }
                }
            }
        }
    } else {
        error!("Rejecting invalid remote address");
    }
}

async fn write_credential(stream: &mut UnixStream, credential: &str) {
    if let Err(err) = stream.write(credential.as_bytes()).await {
        error!(
            error = err.to_string(),
            "Error writing credential to socket"
        );
    }
}
