use crate::{new_client, NewClientError};
use clap::Parser;
use vaultrs::{auth::approle::role::secret, error::ClientError};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create vault client")]
    Client(NewClientError),
    #[error("failed to delete secret id")]
    Delete(ClientError),
}

#[derive(Parser)]
pub struct RevokeArgs {
    /// Machine name
    machine: String,
    /// Secret id accessor
    accessor: String,
    /// Mount point for vault approle auth mechanism
    #[arg(long, default_value = "approle")]
    mount: String,
}

pub async fn revoke(args: RevokeArgs) -> Result<(), Error> {
    let client = new_client().map_err(Error::Client)?;
    secret::delete_accessor(&client, &args.mount, &args.machine, &args.accessor)
        .await
        .map_err(Error::Delete)?;
    Ok(())
}
