_: {
  resource.linode_domain_record.vacation_slagit_net = {
    domain_id = "\${linode_domain.slagit_net.id}";
    name = "vacation";
    record_type = "CNAME";
    target = "franklin.slagit.net";
  };
}
