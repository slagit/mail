{
  config,
  pkgs,
  ...
}: let
  permuteseq = pkgs.stdenv.mkDerivation rec {
    pname = "permuteseq";
    version = "1.2.2";
    src = pkgs.fetchFromGitHub {
      owner = "dverite";
      repo = "permuteseq";
      rev = "v${version}";
      hash = "sha256-mt95sCJ5F7ioOx6s1DaJbrCaK/R/2Ji/WT7JDd8+yZ8=";
    };
    buildInputs = [config.services.postgresql.package];
    installPhase = ''
      mkdir -p $out/lib
      mkdir -p $out/share/postgresql/extension
      install -D ${pname}.so -t $out/lib
      install -D ${pname}.control -t $out/share/postgresql/extension
      install -D sql/${pname}--1.2.sql -t $out/share/postgresql/extension
    '';
  };
  vacation-api = (import ./vacation-api {inherit pkgs;}).package;
  vacation-ui = let
    inherit (pkgs.callPackage ./vacation-ui/default.nix {inherit (pkgs) nodejs;}) nodeDependencies;
  in
    pkgs.stdenv.mkDerivation {
      name = "app-static";
      src = ./vacation-ui;
      buildInputs = [pkgs.nodejs];
      buildPhase = ''
        ls ${nodeDependencies}/lib/node_modules
        ln -s ${nodeDependencies}/lib/node_modules ./node_modules
        export PATH="${nodeDependencies}/bin:$PATH"
        npm run build
        cp -r dist $out/
      '';
    };
in {
  services = {
    nginx = {
      enable = true;
      virtualHosts."vacation.slagit.net" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/" = {
            root = vacation-ui;
            tryFiles = "$uri $uri/ /index.html";
          };
          "/graphql" = {
            proxyPass = "http://127.0.0.1:5678";
            recommendedProxySettings = true;
          };
          "/graphiql" = {
            proxyPass = "http://127.0.0.1:5678";
            recommendedProxySettings = true;
          };
        };
      };
    };
    postgresql = {
      enable = true;
      ensureDatabases = [
        "vacation"
      ];
      ensureUsers = [
        {
          name = "vacation";
        }
        {
          name = "vacation_anonymous";
        }
        {
          name = "vacation_user";
        }
        {
          name = "vacation_admin";
        }
      ];
      extraPlugins = [
        permuteseq
      ];
    };
  };
  systemd.services = {
    vacation = {
      after = ["postgresql.service"];
      bindsTo = ["postgresql.service"];
      wantedBy = ["multi-user.target"];
      serviceConfig = {
        User = "vacation";
        Group = "vacation";
        DynamicUser = true;
        LoadCredential = "vacation_client_secret:/run/systemd-vaultd.sock";
      };
      environment = {
        DATABASE_URL = "postgres://%%2Frun%%2Fpostgresql/vacation";
        GRAPHILE_ENV = "production";
        VACATION_AUTH_PROVIDER = "https://auth.slagit.net/realms/slagit/";
        VACATION_AUTH_CLIENT_ID = "vacation";
        VACATION_AUTH_CLIENT_SECRET_FILE = "/run/credentials/vacation.service/vacation_client_secret";
        VACATION_AUDIENCE = "vacation";
      };
      script = ''
        ${vacation-api}/bin/vacation-api
      '';
    };
    vacation-migrate = {
      after = ["postgresql.service"];
      before = ["keycloak.service"];
      bindsTo = ["postgresql.service"];
      description = "Apply vacation database migrations";
      environment.DATABASE_URL = "postgres://postgres@%%2Frun%%2Fpostgresql/vacation";
      requires = ["postgresql.service"];
      script = ''
        ${pkgs.refinery-cli}/bin/refinery migrate -e DATABASE_URL -p ${./migrations}
      '';
      serviceConfig = {
        RemainAfterExit = true;
        Type = "oneshot";
        User = config.users.users.postgres.name;
      };
    };
  };
}
