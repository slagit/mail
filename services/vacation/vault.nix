_: {
  data = {
    keycloak_openid_client.realm_management = {
      realm_id = "\${keycloak_realm.slagit_net.id}";
      client_id = "realm-management";
    };
    keycloak_role.view_users = {
      realm_id = "\${keycloak_realm.slagit_net.id}";
      client_id = "\${data.keycloak_openid_client.realm_management.id}";
      name = "view-users";
    };
  };
  slagit.keycloak.realms.slagit_net = {
    clients = {
      vacation = {
        access_type = "CONFIDENTIAL";
        admin_url = "";
        default_scopes = ["roles"];
        frontchannel_logout_enabled = true;
        full_scope_allowed = true;
        name = "Vacation Manager";
        roles = {
          admin = {};
          user = {};
        };
        root_url = "";
        service_accounts_enabled = true;
        standard_flow_enabled = false;
        valid_redirect_uris = [];
        web_origins = [];
      };
      vacation-dev = {
        access_type = "CONFIDENTIAL";
        admin_url = "";
        default_scopes = ["roles"];
        frontchannel_logout_enabled = true;
        full_scope_allowed = true;
        name = "Vacation Manager - Development";
        roles = {
          admin = {};
          user = {};
        };
        root_url = "";
        service_accounts_enabled = true;
        standard_flow_enabled = false;
        valid_redirect_uris = [];
        web_origins = [];
      };
      vacation-web = {
        access_type = "PUBLIC";
        admin_url = "https://vacation.slagit.net/";
        default_scopes = ["email" "profile" "vacation" "web-origins"];
        frontchannel_logout_enabled = true;
        full_scope_allowed = false;
        name = "Vacation Manager Website";
        root_url = "https://vacation.slagit.net/";
        service_accounts_enabled = false;
        standard_flow_enabled = true;
        valid_redirect_uris = ["https://vacation.slagit.net/*"];
        web_origins = ["https://vacation.slagit.net"];
      };
      vacation-web-dev = {
        name = "Vacation Manager Website - Development";
        access_type = "PUBLIC";
        default_scopes = ["email" "profile" "vacation-dev" "web-origins"];
        root_url = "http://localhost:5173/";
        valid_redirect_uris = ["http://localhost:5173/*"];
        standard_flow_enabled = true;
        web_origins = ["http://localhost:5173"];
        admin_url = "http://localhost:5173/";
        frontchannel_logout_enabled = true;
        full_scope_allowed = false;
        service_accounts_enabled = false;
      };
    };
    client_scopes = {
      vacation = {
        audience_protocol_mappers.vacation = {
          name = "Vacation Audience";
          included_client_audience = "vacation";
          add_to_id_token = false;
        };
        generic_role_mappers = {
          admin = {
            # TODO: This should be more eganomic since the author likely doesn't know the resource name
            role_id = "\${keycloak_role.slagit_net_client_vacation_admin.id}";
          };
          user = {
            # TODO: This should be more eganomic since the author likely doesn't know the resource name
            role_id = "\${keycloak_role.slagit_net_client_vacation_user.id}";
          };
        };
        user_client_role_protocol_mappers.vacation = {
          name = "Vacation Roles";
          client_id_for_role_mappings = "vacation";
          claim_name = "resource_access.\$\${client_id}.roles";
          multivalued = true;
        };
      };
      vacation-dev = {
        audience_protocol_mappers.vacation-dev = {
          name = "Vacation Audience";
          included_client_audience = "vacation-dev";
          add_to_id_token = false;
        };
        generic_role_mappers = {
          admin = {
            # TODO: This should be more eganomic since the author likely doesn't know the resource name
            role_id = "\${keycloak_role.slagit_net_client_vacation-dev_admin.id}";
          };
          user = {
            # TODO: This should be more eganomic since the author likely doesn't know the resource name
            role_id = "\${keycloak_role.slagit_net_client_vacation-dev_user.id}";
          };
        };
        user_client_role_protocol_mappers.vacation-dev = {
          name = "Vacation Roles";
          client_id_for_role_mappings = "vacation-dev";
          claim_name = "resource_access.\$\${client_id}.roles";
          multivalued = true;
        };
      };
    };
  };
  resource = {
    keycloak_user_roles = {
      sa-vacation = {
        realm_id = "\${keycloak_realm.slagit_net.id}";
        user_id = "\${keycloak_openid_client.slagit_net_vacation.service_account_user_id}";
        role_ids = [
          "\${data.keycloak_role.view_users.id}"
        ];
      };
      sa-vacation-dev = {
        realm_id = "\${keycloak_realm.slagit_net.id}";
        user_id = "\${keycloak_openid_client.slagit_net_vacation-dev.service_account_user_id}";
        role_ids = [
          "\${data.keycloak_role.view_users.id}"
        ];
      };
    };
  };
}
