{lib, ...}: {
  ci = {
    alejandra.default.ignore = [
      "services/vacation/vacation-api/default.nix"
      "services/vacation/vacation-api/node-env.nix"
      "services/vacation/vacation-api/node-packages.nix"
      "services/vacation/vacation-ui/default.nix"
      "services/vacation/vacation-ui/node-env.nix"
      "services/vacation/vacation-ui/node-packages.nix"
    ];
    gitlab.default.jobs = {
      "devShell:x86_64-linux:vacation".jobConfiguration.script = lib.mkForce [
        "nix build .#devShells.x86_64-linux.vacation --impure"
      ];
      "package:x86_64-linux:vacation-container-processes".jobConfiguration.when = "manual";
      "package:x86_64-linux:vacation-container-shell".jobConfiguration.when = "manual";
    };
    statix.default.ignore = [
      "services/vacation/vacation-api/node-packages.nix"
      "services/vacation/vacation-ui/node-packages.nix"
    ];
  };
  flake.nixosModules.vacation = ./nixos.nix;
  perSystem = {
    config,
    pkgs,
    ...
  }: let
    permuteseq = pkgs.stdenv.mkDerivation rec {
      pname = "permuteseq";
      version = "1.2.2";
      src = pkgs.fetchFromGitHub {
        owner = "dverite";
        repo = "permuteseq";
        rev = "v${version}";
        hash = "sha256-mt95sCJ5F7ioOx6s1DaJbrCaK/R/2Ji/WT7JDd8+yZ8=";
      };
      buildInputs = [config.devenv.shells.vacation.services.postgres.package];
      installPhase = ''
        mkdir -p $out/lib
        mkdir -p $out/share/postgresql/extension
        install -D ${pname}.so -t $out/lib
        install -D ${pname}.control -t $out/share/postgresql/extension
        install -D sql/${pname}--1.2.sql -t $out/share/postgresql/extension
      '';
    };
  in {
    devenv.shells.vacation = {
      env = {
        GRAPHILE_ENV = "development";
        VACATION_AUTH_PROVIDER = "https://auth.slagit.net/realms/slagit/";
        VACATION_AUTH_CLIENT_ID = "vacation-dev";
        VACATION_AUTH_CLIENT_SECRET_FILE = "client_secret";
        VACATION_AUDIENCE = "vacation-dev";
      };
      languages.javascript = {
        enable = true;
        npm.enable = true;
      };
      packages = [
        pkgs.nodePackages.node2nix
        pkgs.refinery-cli
      ];
      processes = {
        api-tsc = {
          exec = "cd vacation-api && npm run build -- --watch";
        };
        postgraphile = {
          exec = "cd vacation-api && npm run dev";
          process-compose.depends_on.postgres.condition = "process_healthy";
        };
        web.exec = "cd vacation-ui && npm run dev";
      };
      services.postgres = {
        enable = true;
        extensions = extensions: [
          permuteseq
        ];
        initialScript = ''
          create role vacation_owner superuser login password 'password';
          create role vacation_anonymous;
          create role vacation_user in role vacation_anonymous;
          create role vacation_admin in role vacation_user;
        '';
        settings = {
          log_connections = true;
          log_statement = "all";
          log_disconnections = true;
        };
      };
    };
    packages = {
      vacation-api = (import ./vacation-api {inherit pkgs;}).package;
      vacation-ui = let
        inherit (pkgs.callPackage ./vacation-ui/default.nix {inherit (pkgs) nodejs;}) nodeDependencies;
      in
        pkgs.stdenv.mkDerivation {
          name = "app-static";
          src = ./vacation-ui;
          buildInputs = [pkgs.nodejs];
          buildPhase = ''
            ln -s ${nodeDependencies}/lib/node_modules ./node_modules
            export PATH="${nodeDependencies}/bin:$PATH"
            npm run build
            cp -r dist $out/
          '';
        };
    };
  };
}
