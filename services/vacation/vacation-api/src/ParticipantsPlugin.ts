import { applyTransforms, each, lambda } from "postgraphile/grafast";
import { makeExtendSchemaPlugin, gql } from "postgraphile/utils";
import type { User } from "./UsersPlugin.js";
import { loadUserById, loadUserByUsername } from "./UsersPlugin.js";

const ParticipantsPlugin = makeExtendSchemaPlugin((build) => {
  const { idea_comments } = build.input.pgRegistry.pgResources;
  return {
    typeDefs: gql`
      extend type Idea {
        participants: [User!]!
      }
    `,
    plans: {
      Idea: {
        participants($idea) {
          const $comments = idea_comments.find({ idea_id: $idea.get("id") });
          const $allIds = lambda(
            [
              $idea.get("created_by_id"),
              applyTransforms(
                each($comments, ($comment) => $comment.get("created_by_id")),
              ),
            ],
            ([idea_id, comment_ids]) => {
              const ids = new Set(comment_ids);
              if (idea_id !== null) {
                ids.add(idea_id);
              }
              return [...ids];
            },
          );
          const $allUsernames = lambda(
            [
              $idea.get("description_mentions"),
              applyTransforms(
                each($comments, ($comment) => $comment.get("comment_mentions")),
              ),
            ],
            ([idea_usernames, comment_usernames]) => {
              const usernames = new Set(
                (idea_usernames as { USER?: string[] }).USER,
              );
              comment_usernames.forEach((m: { USER?: string[] }) =>
                m.USER?.forEach((u) => usernames.add(u)),
              );
              return [...usernames];
            },
          );
          const $usersFromIds = applyTransforms(each($allIds, loadUserById));
          const $usersFromUsernames = applyTransforms(
            each($allUsernames, loadUserByUsername),
          );
          const $merged = lambda(
            [$usersFromIds, $usersFromUsernames],
            (lists) => {
              const m = new Map();
              lists.forEach((list) =>
                list
                  .filter((user: User) => user !== null)
                  .forEach((user: User) => m.set(user.id, user)),
              );
              return [...m.values()];
            },
          );
          return $merged;
        },
      },
    },
  };
});

export default ParticipantsPlugin;
