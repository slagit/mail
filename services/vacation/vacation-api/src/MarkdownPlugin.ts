import { withPgClient } from "postgraphile/@dataplan/pg";
import type { AccessStep, ExecutableStep } from "postgraphile/grafast";
import {
  constant,
  each,
  filter,
  lambda,
  list,
  object,
  polymorphicBranch,
  specFromNodeId,
} from "postgraphile/grafast";
import { gql, makeExtendSchemaPlugin } from "postgraphile/utils";
import { remark } from "remark";
import type { Literal } from "unist";
import { visit } from "unist-util-visit";
import { loadUserByUsername } from "./UsersPlugin.js";

type Mentions = {
  IDEA?: string[];
  TRIP?: string[];
  USER?: string[];
};

const mention_types: { kind: keyof Mentions; regexp: RegExp }[] = [
  {
    kind: "IDEA",
    regexp: /#([A-Za-z0-9+/=]+)/g,
  },
  {
    kind: "TRIP",
    regexp: /trips\/([A-Za-z0-9_-]+)/g,
  },
  {
    kind: "USER",
    regexp: /@([0-9a-zA-Z]+)/g,
  },
];

async function buildMentions(markdown: string) {
  const mentions: Partial<Record<keyof Mentions, Set<string>>> = {};
  await remark()
    .use(() => (tree) => {
      visit(tree, "text", (node: Literal) => {
        if (typeof node.value === "string") {
          for (const { kind, regexp } of mention_types) {
            for (const match of node.value.matchAll(regexp)) {
              mentions[kind] = mentions[kind] || new Set();
              mentions[kind].add(match[1]);
            }
          }
        }
        return true;
      });
    })
    .process(markdown);
  return Object.fromEntries(
    Object.entries(mentions).map(([k, v]) => [k, [...v]]),
  );
}

function makeDefault<T>($obj: ExecutableStep<T | undefined>, value: T) {
  return lambda($obj, (obj) => (obj === undefined ? value : obj));
}

const MarkdownPlugin = makeExtendSchemaPlugin((build) => {
  const ideaHandler = build.getNodeIdHandler?.("Idea");
  if (!ideaHandler) {
    throw new Error("No handler found for Idea node");
  }
  const { ideas, idea_comments, trips } = build.input.pgRegistry.pgResources;
  const { executor } = ideas;

  function buildLinks(
    $mentions: AccessStep<{
      IDEA?: string[];
      TRIP?: string[];
      USER?: string[];
    }>,
  ) {
    const $ideas = filter(makeDefault($mentions.get("IDEA"), []), ($idea) =>
      ideas.get({ number: $idea }).get("id"),
    );
    const $trips = filter(makeDefault($mentions.get("TRIP"), []), ($trip) =>
      trips.get({ slug: $trip }).get("id"),
    );
    const $users = filter(makeDefault($mentions.get("USER"), []), ($user) =>
      loadUserByUsername($user),
    );
    const $mentionList = lambda(
      object({
        IDEA: $ideas,
        TRIP: $trips,
        USER: $users,
      }),
      (mentions) => {
        return Object.entries(mentions)
          .map(([k, v]) => v.map((vv) => ({ kind: k, value: vv })))
          .flat();
      },
    );
    return each($mentionList, ($mention) =>
      polymorphicBranch($mention, {
        Idea: {
          match: (mention) => mention.kind === "IDEA",
          plan: ($mention) =>
            ideas.get({
              number: lambda($mention, (mention) => mention.value),
            }),
        },
        Trip: {
          match: (mention) => mention.kind === "TRIP",
          plan: ($mention) =>
            trips.get({
              slug: lambda($mention, (mention) => mention.value),
            }),
        },
        User: {
          match: (mention) => mention.kind === "USER",
          plan: ($mention) =>
            loadUserByUsername(lambda($mention, (mention) => mention.value)),
        },
      }),
    );
  }

  return {
    typeDefs: gql`
      union MarkdownLink = Idea | Trip | User
      extend type IdeaComment {
        commentLinks: [MarkdownLink!]!
      }
      extend type Idea {
        descriptionLinks: [MarkdownLink!]!
      }
      input CreateIdeaCommentInput {
        clientMutationId: String
        ideaComment: IdeaCommentInput!
      }
      type CreateIdeaCommentPayload {
        clientMutationId: String
        ideaComment: IdeaComment
        ideaCommentEdge(
          orderBy: [IdeaCommentOrderBy!]! = [PRIMARY_KEY_ASC]
        ): IdeaCommentEdge
        query: Query
      }
      input CreateIdeaInput {
        clientMutationId: String
        idea: IdeaInput!
      }
      type CreateIdeaPayload {
        clientMutationId: String
        idea: Idea
        ideaEdge(orderBy: [IdeaOrderBy!]! = [PRIMARY_KEY_ASC]): IdeaEdge
        query: Query
      }
      input UpdateIdeaByNumberInput {
        clientMutationId: String
        number: BigInt!
        patch: IdeaPatch!
      }
      input UpdateIdeaByIdInput {
        clientMutationId: String
        id: ID!
        patch: IdeaPatch!
      }
      type UpdateIdeaPayload {
        clientMutationId: String
        idea: Idea
        query: Query
        ideaEdge(orderBy: [IdeaOrderBy!]! = [PRIMARY_KEY_ASC]): IdeaEdge
      }
      extend type Mutation {
        createIdea(input: CreateIdeaInput!): CreateIdeaPayload
        updateIdeaById(input: UpdateIdeaByIdInput!): UpdateIdeaPayload!
        updateIdeaByNumber(input: UpdateIdeaByNumberInput!): UpdateIdeaPayload!
        createIdeaComment(
          input: CreateIdeaCommentInput!
        ): CreateIdeaCommentPayload
      }
    `,
    plans: {
      Idea: {
        descriptionLinks($idea) {
          return buildLinks($idea.get("description_mentions"));
        },
      },
      IdeaComment: {
        commentLinks($comment) {
          return buildLinks($comment.get("comment_mentions"));
        },
      },
      Mutation: {
        createIdea(_, args) {
          const $idea = args.get(["input", "idea"]);
          const $ideaId = withPgClient(
            executor,
            $idea,
            async (client, idea) => {
              const {
                rows: [{ id }],
              } = await client.query<{ id: string }>({
                text: "INSERT INTO app_public.ideas (description, description_mentions, trip_id) VALUES ($1, $2, $3) RETURNING id",
                values: [
                  idea.description,
                  await buildMentions(idea.description),
                  idea.trip_id,
                ],
              });
              return id;
            },
          );

          return object({
            clientMutationId: args.get(["input", "clientMutationId"]),
            idea: ideas.get({ id: $ideaId }),
            ideaEdge: ideas.get({ id: $ideaId }),
            query: constant(true),
          });
        },
        createIdeaComment(_, args) {
          const $comment = args.get(["input", "ideaComment"]);
          const $commentId = withPgClient(
            executor,
            $comment,
            async (client, comment) => {
              const {
                rows: [{ id }],
              } = await client.query<{ id: string }>({
                text: "INSERT INTO app_public.idea_comments (comment, comment_mentions, idea_id) VALUES ($1, $2, $3) RETURNING id",
                values: [
                  comment.comment,
                  await buildMentions(comment.comment),
                  comment.idea_id,
                ],
              });
              return id;
            },
          );

          return object({
            clientMutationId: args.get(["input", "clientMutationId"]),
            ideaComment: idea_comments.get({ id: $commentId }),
            ideaCommentEdge: idea_comments.get({ id: $commentId }),
            query: constant(true),
          });
        },
        updateIdeaById(_, args) {
          const $id = args.get(["input", "id"]);
          const $spec = specFromNodeId(ideaHandler, $id);
          const $patch = args.get(["input", "patch"]);
          const $ideaId = withPgClient(
            executor,
            list([$spec.id, $patch]),
            async (client, [iid, patch]) => {
              const columns = [];
              const values = [];
              if ("description" in patch && patch.description !== undefined) {
                columns.push(`description=$${columns.length + 1}`);
                values.push(patch.description);
                columns.push(`description_mentions=$${columns.length + 1}`);
                values.push(await buildMentions(patch.description));
              }
              if ("status" in patch) {
                columns.push(`status=$${columns.length + 1}`);
                values.push(patch.status);
              }
              if ("trip_id" in patch) {
                columns.push(`trip_id=$${columns.length + 1}`);
                values.push(patch.trip_id);
              }
              const setters = columns.join(", ");
              values.push(iid);
              const {
                rows: [{ id }],
              } = await client.query<{ id: string }>({
                text: `UPDATE app_public.ideas SET ${setters} WHERE id=$${values.length} RETURNING id`,
                values,
              });
              return id;
            },
          );

          return object({
            clientMutationId: args.get(["input", "clientMutationId"]),
            idea: ideas.get({ id: $ideaId }),
            ideaEdge: ideas.get({ id: $ideaId }),
            query: constant(true),
          });
        },
        updateIdeaByNumber(_, args) {
          const $number = args.get(["input", "number"]);
          const $patch: ExecutableStep<{
            description?: string;
            trip_id?: string;
          }> = args.get(["input", "patch"]);
          const $ideaId = withPgClient(
            executor,
            list([$number, $patch]),
            async (client, [number, patch]) => {
              const columns = [];
              const values = [];
              if ("description" in patch && patch.description !== undefined) {
                columns.push(`description=$${columns.length + 1}`);
                values.push(patch.description);
                columns.push(`description_mentions=$${columns.length + 1}`);
                values.push(await buildMentions(patch.description));
              }
              if ("status" in patch) {
                columns.push(`status=$${columns.length + 1}`);
                values.push(patch.status);
              }
              if ("trip_id" in patch) {
                columns.push(`trip_id=$${columns.length + 1}`);
                values.push(patch.trip_id);
              }
              const setters = columns.join(", ");
              values.push(number);
              const {
                rows: [{ id }],
              } = await client.query<{ id: string }>({
                text: `UPDATE app_public.ideas SET ${setters} WHERE number=$${values.length} RETURNING id`,
                values,
              });
              return id;
            },
          );

          return object({
            clientMutationId: args.get(["input", "clientMutationId"]),
            idea: ideas.get({ id: $ideaId }),
            ideaEdge: ideas.get({ id: $ideaId }),
            query: constant(true),
          });
        },
      },
    },
  };
});
MarkdownPlugin.after = ["PgTableNodePlugin"];

export default MarkdownPlugin;
