import {
  PgDeleteSingleStep,
  PgInsertSingleStep,
  PgSelectSingleStep,
  PgUpdateSingleStep,
} from "postgraphile/@dataplan/pg";
import { lambda } from "postgraphile/grafast";
import { makeExtendSchemaPlugin, gql } from "postgraphile/utils";
import { loadHistoryUserById } from "./UsersPlugin.js";

type HistoryStep =
  | PgDeleteSingleStep
  | PgInsertSingleStep
  | PgSelectSingleStep
  | PgUpdateSingleStep;

const createHistory = {
  createdAt($obj: HistoryStep) {
    const $createdAt = $obj.get("created_at");
    return lambda([$createdAt], ([createdAt]) => {
      return `${createdAt}Z`;
    });
  },
  createdBy($obj: HistoryStep) {
    const $userId = $obj.get("created_by_id");
    return loadHistoryUserById($userId);
  },
};

const updateHistory = {
  updatedAt($obj: HistoryStep) {
    const $updatedAt = $obj.get("updated_at");
    return lambda([$updatedAt], ([updatedAt]) => {
      if (updatedAt === null) {
        return null;
      }
      return `${updatedAt}Z`;
    });
  },
  updatedBy($obj: HistoryStep) {
    const $userId = $obj.get("updated_by_id");
    return loadHistoryUserById($userId);
  },
};

const fullHistory = {
  ...createHistory,
  ...updateHistory,
};

const HistoryPlugin = makeExtendSchemaPlugin(() => {
  return {
    typeDefs: gql`
      extend type Idea {
        createdAt: Datetime!
        createdBy: HistoryUser
        updatedAt: Datetime
        updatedBy: HistoryUser
      }
      extend type IdeaComment {
        createdAt: Datetime!
        createdBy: HistoryUser!
        updatedAt: Datetime
        updatedBy: HistoryUser
      }
      extend interface IdeaHistory {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
      extend type IdeaHistoryChangeDescription {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
      extend type IdeaHistoryChangeTrip {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
      extend type IdeaHistoryClose {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
      extend type IdeaHistoryMentionIdea {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
      extend type IdeaHistoryMentionIdeaComment {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
      extend type IdeaHistoryReopen {
        updatedAt: Datetime!
        updatedBy: HistoryUser!
      }
    `,
    plans: {
      Idea: fullHistory,
      IdeaComment: fullHistory,
      IdeaHistoryChangeDescription: updateHistory,
      IdeaHistoryChangeTrip: updateHistory,
      IdeaHistoryClose: updateHistory,
      IdeaHistoryMentionIdea: updateHistory,
      IdeaHistoryMentionIdeaComment: updateHistory,
      IdeaHistoryReopen: updateHistory,
    },
  };
});

export default HistoryPlugin;
