import type {
  ExecutionDetails,
  LoadOptions,
  NodeIdHandler,
} from "postgraphile/grafast";
import {
  access,
  constant,
  context,
  ExecutableStep,
  lambda,
  list,
  loadOne,
  nodeIdFromNode,
  object,
  polymorphicBranch,
  specFromNodeId,
} from "postgraphile/grafast";
import { makeExtendSchemaPlugin, gql } from "postgraphile/utils";
import { z } from "zod";
import { getKeycloakCredentials, getRoles } from "./keycloak.js";

export class UserPermissionError extends Error {}

const User = z.object({
  id: z.string(),
  username: z.string(),
  firstName: z.string().nullable(),
  lastName: z.string().nullable(),
});

export type User = z.infer<typeof User>;

const Claims = z.object({
  resource_access: z.record(
    z.string(),
    z.object({
      roles: z.string().array(),
    }),
  ),
  sub: z.string(),
});

const LocalContext = z.object({
  claims: Claims,
});

const StepContext = z.object({
  values: z
    .object({
      value: Claims,
    })
    .array(),
});

class GetAllUsersStep extends ExecutableStep {
  constructor() {
    super();
    this.addDependency(context());
  }
  async execute(context: ExecutionDetails<readonly unknown[]>) {
    const stepContext = StepContext.parse(context);
    const roles = getRoles(stepContext.values[0].value);
    if (!roles.includes("user") && !roles.includes("admin")) {
      throw new UserPermissionError();
    }
    const { access_token } = await getKeycloakCredentials();
    const req = await fetch(
      "https://auth.slagit.net/admin/realms/slagit/users",
      {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      },
    );
    if (req.status !== 200) {
      throw new Error(`Failed to fetch user record status=${req.status}`);
    }
    return [User.array().parse(await req.json())];
  }
}

function getAllUsers() {
  return new GetAllUsersStep();
}

async function batchGetUserById(
  userIds: readonly string[],
  { unary: context }: LoadOptions<unknown, Record<string, unknown>, unknown>,
) {
  const localContext = LocalContext.parse(context);
  const roles = getRoles(localContext.claims);
  if (!roles.includes("user") && !roles.includes("admin")) {
    throw new UserPermissionError();
  }
  const { access_token } = await getKeycloakCredentials();
  const uniqueIds = [...new Set(userIds)];
  const results = Object.fromEntries(
    await Promise.all(
      uniqueIds.map(async (userId) => {
        const req = await fetch(
          `https://auth.slagit.net/admin/realms/slagit/users/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${access_token}`,
            },
          },
        );
        if (req.status === 404) {
          return [userId, null];
        }
        if (req.status === 200) {
          return [userId, User.parse(await req.json())];
        }
        throw new Error(`Failed to fetch user record status=${req.status}`);
      }),
    ),
  );
  return userIds.map((userId) => results[userId] ?? null);
}

async function batchGetUserByUsername(
  usernames: readonly string[],
  { unary: context }: LoadOptions<unknown, Record<string, unknown>, unknown>,
) {
  const localContext = LocalContext.parse(context);
  const roles = getRoles(localContext.claims);
  if (!roles.includes("user") && !roles.includes("admin")) {
    throw new UserPermissionError();
  }
  const { access_token } = await getKeycloakCredentials();
  const uniqueNames = [
    ...new Set(usernames.filter((username) => username !== null)),
  ];
  const results = Object.fromEntries(
    await Promise.all(
      uniqueNames.map(async (username) => {
        const req = await fetch(
          `https://auth.slagit.net/admin/realms/slagit/users?username=${username}&exact=true`,
          {
            headers: {
              Authorization: `Bearer ${access_token}`,
            },
          },
        );
        if (req.status === 200) {
          const users = User.array()
            .max(1)
            .parse(await req.json());
          if (users) {
            return [username, users[0]];
          }
          return [username, null];
        }
        throw new Error(`Failed to fetch user record status=${req.status}`);
      }),
    ),
  );
  return usernames.map((username) => results[username] ?? null);
}

export function loadHistoryUserById($id: ExecutableStep<string>) {
  const $user = loadUserById($id);
  const $parts = lambda([$id, $user], ([id, user]) =>
    id === null ? null : { id, user },
  );
  return polymorphicBranch($parts, {
    DeletedUser: {
      match: (parts) => parts !== null && parts.user === null,
      plan: ($parts) =>
        object({
          id: access($parts, ["id"]),
        }),
    },
    User: {
      match: (parts) => parts !== null && parts.user !== null,
      plan: ($parts) => access($parts, ["user"]),
    },
  });
}

export function loadUserById($id: ExecutableStep<string>) {
  return loadOne($id, context(), "id", batchGetUserById);
}

export function loadUserByUsername($username: ExecutableStep<string>) {
  return loadOne($username, context(), "username", batchGetUserByUsername);
}

const UsersPlugin = makeExtendSchemaPlugin((build) => {
  const codec = build?.getNodeIdCodec?.("base64JSON");
  if (codec === undefined) {
    throw new Error("Could not find base64JSON codec");
  }
  const userHandler: NodeIdHandler = {
    typeName: "User",
    codec,
    plan($user) {
      return list([constant("User"), access($user, ["id"])]);
    },
    match(list) {
      return list[0] === "User";
    },
    getSpec($list) {
      return access($list, 1);
    },
    get() {
      throw new Error("Not Implemented");
    },
  };

  return {
    typeDefs: gql`
      type DeletedUser {
        id: ID!
      }

      type User {
        id: ID!
        username: String!
        firstName: String
        lastName: String
      }

      union HistoryUser = DeletedUser | User

      extend type Query {
        users: [User!]
        userById(id: ID!): User
        userByUsername(username: String!): User
      }
    `,
    plans: {
      Query: {
        users() {
          return getAllUsers();
        },
        userById(_, { $id }) {
          const $spec = specFromNodeId(userHandler, $id);
          return loadUserById($spec);
        },
        userByUsername(
          _,
          { $username }: { $username: ExecutableStep<string> },
        ) {
          return loadUserByUsername($username);
        },
      },
      DeletedUser: {
        __assertStep: ExecutableStep,
        id($user) {
          return nodeIdFromNode(userHandler, $user);
        },
      },
      User: {
        __assertStep: ExecutableStep,
        id($user) {
          return nodeIdFromNode(userHandler, $user);
        },
      },
    },
  };
});

export default UsersPlugin;
