import fs from "node:fs/promises";
import jwksClient from "jwks-rsa";
import type { GetPublicKeyOrSecret, VerifyOptions } from "jsonwebtoken";
import jwt from "jsonwebtoken";
import { z } from "zod";

const OpenIdConfig = z.object({
  jwks_uri: z.string(),
  token_endpoint: z.string(),
});

async function getOpenIdConfig(auth_provider: string) {
  const response = await fetch(
    `${auth_provider}/.well-known/openid-configuration`,
  );
  if (!response.ok) {
    throw new Error("Failed to fetch openid configuration", {
      cause: response,
    });
  }
  return OpenIdConfig.parse(await response.json());
}

async function getAppConfig() {
  const auth_provider = process.env.VACATION_AUTH_PROVIDER;
  if (!auth_provider) {
    throw new Error("Please set VACATION_AUTH_PROVIDER");
  }

  const client_id = process.env.VACATION_AUTH_CLIENT_ID;
  if (!client_id) {
    throw new Error("Please set VACATION_AUTH_CLIENT_ID");
  }

  const client_secret_file = process.env.VACATION_AUTH_CLIENT_SECRET_FILE;
  if (!client_secret_file) {
    throw new Error("Please set VACATION_AUTH_SECRET_FILE");
  }
  const client_secret = (
    await fs.readFile(client_secret_file, { encoding: "utf-8" })
  ).trim();

  const audience = process.env.VACATION_AUDIENCE;
  if (!audience) {
    throw new Error("Please set VACATION_AUDIENCE");
  }

  return {
    audience,
    auth_provider,
    client_id,
    client_secret,
  };
}

const { audience, auth_provider, client_id, client_secret } =
  await getAppConfig();
const openIdConfig = await getOpenIdConfig(auth_provider);
const client = jwksClient({
  jwksUri: openIdConfig.jwks_uri,
});

const KeycloakCredentials = z.object({
  access_token: z.string(),
});

export async function getKeycloakCredentials() {
  const req = await fetch(openIdConfig.token_endpoint, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({
      grant_type: "client_credentials",
      client_id,
      client_secret,
    }),
  });
  if (req.status !== 200) {
    throw new Error(`Failed to get keycloak credentials status=${req.status}`);
  }
  return KeycloakCredentials.parse(await req.json());
}

const getKey: GetPublicKeyOrSecret = ({ kid }, callback) => {
  client.getSigningKey(kid, (err, key) => {
    if (err) {
      return callback(err);
    }
    callback(null, key?.getPublicKey());
  });
};

const Claims = z.object({
  sub: z.string(),
  resource_access: z
    .record(
      z.string(),
      z.object({
        roles: z.string().array(),
      }),
    )
    .optional(),
});

function jwtVerify(
  token: string,
  secret: GetPublicKeyOrSecret,
  options: VerifyOptions,
) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, options, (err, decoded) => {
      if (err !== null) {
        reject(err);
      }
      resolve(decoded);
    });
  });
}

export async function getClaims(ctx: Partial<Grafast.RequestContext>) {
  const authorization = ctx?.node?.req?.headers?.authorization;
  if (typeof authorization === "string") {
    const [bearer, token] = authorization.split(" ");
    if (bearer.toLowerCase() === "bearer") {
      return Claims.parse(
        await jwtVerify(token, getKey, {
          algorithms: ["RS256"],
          audience,
        }),
      );
    }
  }
  return null;
}

export function getRoles(claims: Awaited<ReturnType<typeof getClaims>>) {
  return claims?.resource_access?.[audience].roles ?? [];
}
