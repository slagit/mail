#!/usr/bin/env node

import { createServer } from "node:http";
import cors from "cors";
import express from "express";
import "graphile-config";
import { DatabaseError } from "pg-protocol";
import { postgraphile } from "postgraphile";
import { makePgService } from "postgraphile/adaptors/pg";
import { GraphQLError } from "postgraphile/graphql";
import { defaultMaskError } from "postgraphile/grafserv";
import { grafserv } from "postgraphile/grafserv/express/v4";
import { PostGraphileAmberPreset } from "postgraphile/presets/amber";
import { PostGraphileRelayPreset } from "postgraphile/presets/relay";
import { PgSimplifyInflectionPreset } from "@graphile/simplify-inflection";
import { getClaims, getRoles } from "./keycloak.js";
import HistoryPlugin from "./HistoryPlugin.js";
import MarkdownPlugin from "./MarkdownPlugin.js";
import ParticipantsPlugin from "./ParticipantsPlugin.js";
import UsersPlugin, { UserPermissionError } from "./UsersPlugin.js";

const pg_foreign_key_violation = "23503";
const pg_unique_violation = "23505";
const pg_check_violation = "23514";
const pg_insufficient_privilege = "42501";

const preset: GraphileConfig.Preset = {
  extends: [
    PostGraphileAmberPreset,
    PostGraphileRelayPreset,
    PgSimplifyInflectionPreset,
  ],
  grafast: {
    async context(ctx, args) {
      const claims = await getClaims(ctx);
      const role = (() => {
        const roles = getRoles(claims);
        if (roles.includes("admin")) {
          return "vacation_admin";
        }
        if (roles.includes("user")) {
          return "vacation_user";
        }
        return "vacation_anonymous";
      })();
      return {
        claims,
        pgSettings: {
          ...args.contextValue?.pgSettings,
          role,
          "vacation.user": claims?.sub,
        },
      };
    },
  },
  grafserv: {
    graphiqlPath: "/graphiql",
    maskError: (err) => {
      const { originalError } = err;
      if (originalError instanceof UserPermissionError) {
        return new GraphQLError(
          "You do not have permission to perform this query",
          err.nodes,
          err.source,
          err.positions,
          err.path,
          originalError,
          {},
        );
      }
      if (originalError instanceof DatabaseError) {
        if (originalError.code === pg_insufficient_privilege) {
          return new GraphQLError(
            "You do not have permission to perform this query",
            err.nodes,
            err.source,
            err.positions,
            err.path,
            originalError,
            {},
          );
        }
        if (originalError.schema === "app_public") {
          if (originalError.table === "ideas") {
            if (
              originalError.code === pg_foreign_key_violation &&
              originalError.constraint === "ideas_trip_id_fkey"
            ) {
              return new GraphQLError(
                "Invalid node identifier for 'Trip'",
                err.nodes,
                err.source,
                err.positions,
                err.path,
                originalError,
                {
                  field: "trip",
                },
              );
            }
          }
          if (originalError.table === "idea_comments") {
            if (
              originalError.code === pg_foreign_key_violation &&
              originalError.constraint === "idea_comments_idea_id_fkey"
            ) {
              return new GraphQLError(
                "Invalid node identifier for 'Idea'",
                err.nodes,
                err.source,
                err.positions,
                err.path,
                originalError,
                {
                  field: "idea",
                },
              );
            }
          }
          if (originalError.table === "trips") {
            if (
              originalError.code === pg_check_violation &&
              originalError.constraint === "trips_date_order"
            ) {
              return new GraphQLError(
                "End date must be after start date",
                err.nodes,
                err.source,
                err.positions,
                err.path,
                originalError,
                {
                  field: "endDate",
                },
              );
            }
            if (
              originalError.code === pg_unique_violation &&
              originalError.constraint === "trips_slug_key"
            ) {
              return new GraphQLError(
                "A trip with this name already exists",
                err.nodes,
                err.source,
                err.positions,
                err.path,
                originalError,
                {
                  field: "slug",
                },
              );
            }
          }
        }
      }
      return defaultMaskError(err);
    },
    watch: process.env.GRAPHILE_ENV === "development",
  },
  pgServices: [
    makePgService({
      connectionString: process.env.DATABASE_URL,
      schemas: ["app_public"],
    }),
  ],
  plugins: [HistoryPlugin, MarkdownPlugin, ParticipantsPlugin, UsersPlugin],
  schema: {
    pgForbidSetofFunctionsToReturnNull: true,
  },
};

const pgl = postgraphile(preset);
const serv = pgl.createServ(grafserv);

const app = express();
app.use(cors());
const server = createServer(app);
server.on("error", (e) => {
  console.error(e);
});
serv.addTo(app, server).catch((e) => {
  console.error(e);
  process.exit(1);
});
server.listen(preset.grafserv?.port ?? 5678);
