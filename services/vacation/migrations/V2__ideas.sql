BEGIN;

CREATE SCHEMA app_private;

CREATE FUNCTION app_private.tg__activity() RETURNS TRIGGER AS $$
BEGIN
	NEW.created_at := (CASE WHEN TG_OP = 'INSERT' THEN NOW() ELSE OLD.created_at END);
	NEW.created_by_id := (CASE WHEN TG_OP = 'INSERT' THEN CURRENT_SETTING('vacation.user', FALSE) ELSE OLD.created_by_id END);
	NEW.updated_at := (CASE WHEN TG_OP = 'INSERT' THEN NULL ELSE NOW() END);
	NEW.updated_by_id := (CASE WHEN TG_OP = 'INSERT' THEN NULL ELSE CURRENT_SETTING('vacation.user', FALSE) END);
	RETURN NEW;
END
$$ LANGUAGE PLPGSQL VOLATILE;

CREATE FUNCTION app_private.tg__idea_history() RETURNS TRIGGER AS $$
DECLARE
	vacation_user TEXT = CURRENT_SETTING('vacation.user', FALSE);
BEGIN
	IF OLD.description != NEW.description THEN
		INSERT INTO app_public.idea_history (idea_id, updated_at, updated_by_id, type, old_description, new_description) VALUES (NEW.id, NOW(), vacation_user, 'CHANGE_DESCRIPTION', OLD.description, NEW.description);
	END IF;
	IF OLD.trip_id != NEW.trip_id THEN
		INSERT INTO app_public.idea_history (idea_id, updated_at, updated_by_id, type, old_trip_id, new_trip_id) VALUES (NEW.id, NOW(), vacation_user, 'CHANGE_TRIP', OLD.trip_id, NEW.trip_id);
	END IF;
	IF OLD.status != NEW.status THEN
		IF NEW.status = 'open' THEN
			INSERT INTO app_public.idea_history (idea_id, updated_at, updated_by_id, type) VALUES (NEW.id, NOW(), vacation_user, 'REOPEN');
		ELSE
			INSERT INTO app_public.idea_history (idea_id, updated_at, updated_by_id, type) VALUES (NEW.id, NOW(), vacation_user, 'CLOSE');
		END IF;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE PLPGSQL SECURITY DEFINER VOLATILE;

CREATE FUNCTION app_private.tg__idea_mentions() RETURNS TRIGGER AS $$
DECLARE
	vacation_user TEXT = CURRENT_SETTING('vacation.user', FALSE);
BEGIN
	IF TG_OP = 'INSERT' OR OLD.description != NEW.description THEN
		INSERT INTO app_public.idea_history (idea_id, updated_at, updated_by_id, type, ref_idea_id) SELECT ideas.id, NOW(), vacation_user, 'MENTION_IDEA', NEW.id FROM app_public.ideas INNER JOIN (SELECT jsonb_array_elements(NEW.description_mentions #> '{IDEA}') #>> '{}' number) mentions ON mentions.number=ideas.number::text;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE PLPGSQL SECURITY DEFINER VOLATILE;

CREATE TYPE app_public.idea_status AS ENUM ('open', 'closed');

CREATE SEQUENCE app_public.ideas_id_seq AS BIGINT MINVALUE 1073741824 MAXVALUE 34359738367;
CREATE SEQUENCE app_public.ideas_number_seq AS BIGINT;
CREATE TABLE app_public.ideas (
	id BIGINT PRIMARY KEY DEFAULT permute_nextval('app_public.ideas_id_seq'::REGCLASS, x'6f9e90c94f6c1e3d'::BIGINT),
	number BIGINT UNIQUE NOT NULL DEFAULT nextval('app_public.ideas_number_seq'),
	status app_public.idea_status NOT NULL DEFAULT 'open',
	description TEXT NOT NULL,
	description_mentions JSONB NOT NULL,
	trip_id BIGINT NOT NULL REFERENCES app_public.trips ON DELETE CASCADE,
	created_at TIMESTAMP NOT NULL,
	created_by_id TEXT,
	updated_at TIMESTAMP,
	updated_by_id TEXT
);
ALTER TABLE app_public.ideas ENABLE ROW LEVEL SECURITY;
CREATE INDEX ON app_public.ideas(trip_id);
CREATE INDEX ON app_public.ideas(status);
CREATE INDEX ON app_public.ideas(created_at);
CREATE INDEX ON app_public.ideas(updated_at);

COMMENT ON TABLE app_public.ideas IS
	E'@behavior query:resource:single -resource:insert -resource:update \nIdeas for future improvement';
COMMENT ON COLUMN app_public.ideas.id IS
	E'@behavior -attribute:insert';
COMMENT ON COLUMN app_public.ideas.number IS
	E'@behavior -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.ideas.description_mentions IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.ideas.status IS
	E'@behavior -attribute:insert';
COMMENT ON COLUMN app_public.ideas.created_at IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.ideas.created_by_id IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.ideas.updated_at IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.ideas.updated_by_id IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON CONSTRAINT ideas_number_key ON app_public.ideas IS
	E'@behavior constraint:resource:update';

CREATE TRIGGER _100_activity BEFORE INSERT OR UPDATE ON app_public.ideas FOR EACH ROW EXECUTE PROCEDURE app_private.tg__activity();
CREATE TRIGGER _200_mentions AFTER INSERT OR UPDATE ON app_public.ideas FOR EACH ROW EXECUTE PROCEDURE app_private.tg__idea_mentions();

CREATE POLICY select_all ON app_public.ideas FOR SELECT USING (TRUE);
CREATE POLICY create_trip ON app_public.ideas FOR INSERT WITH CHECK (TRUE);
CREATE POLICY update_trip ON app_public.ideas FOR UPDATE USING (TRUE);
GRANT SELECT ON app_public.ideas TO vacation_user;
GRANT INSERT (description, description_mentions, trip_id) ON app_public.ideas TO vacation_user;
GRANT UPDATE (description, description_mentions, status, trip_id) ON app_public.ideas TO vacation_user;

CREATE FUNCTION app_public.current_trip() RETURNS app_public.trips AS $$
	SELECT * FROM app_public.trips WHERE name='2024 Biddeford'
$$ LANGUAGE SQL STABLE;

CREATE TYPE app_public.idea_history_type AS ENUM (
	'CHANGE_DESCRIPTION',
	'CHANGE_TRIP',
	'CLOSE',
	'MENTION_IDEA',
	'REOPEN'
);

CREATE SEQUENCE app_public.idea_history_id_seq AS BIGINT MINVALUE 1073741824 MAXVALUE 34359738367;
CREATE TABLE app_public.idea_history (
	id BIGINT PRIMARY KEY DEFAULT permute_nextval('app_public.idea_history_id_seq'::REGCLASS, x'0d60ef1b797c9ce3'::BIGINT),
	type app_public.idea_history_type NOT NULL,
	idea_id BIGINT NOT NULL REFERENCES app_public.ideas ON DELETE CASCADE,
	old_description TEXT,
	new_description TEXT,
	old_trip_id BIGINT REFERENCES app_public.trips ON DELETE CASCADE,
	new_trip_id BIGINT REFERENCES app_public.trips ON DELETE CASCADE,
	ref_idea_id BIGINT REFERENCES app_public.ideas ON DELETE CASCADE,
	updated_at TIMESTAMP,
	updated_by_id TEXT
);
ALTER TABLE app_public.idea_history ENABLE ROW LEVEL SECURITY;
CREATE INDEX ON app_public.idea_history(updated_at);
CREATE INDEX ON app_public.idea_history(idea_id);
COMMENT ON TABLE app_public.idea_history IS $$
	@interface mode:single type:type
	@type CHANGE_DESCRIPTION name:IdeaHistoryChangeDescription attributes:old_description,new_description
	@type CHANGE_TRIP name:IdeaHistoryChangeTrip attributes:old_trip_id,new_trip_id
	@type CLOSE name:IdeaHistoryClose
	@type MENTION_IDEA name:IdeaHistoryMentionIdea attributes:ref_idea_id
	@type REOPEN name:IdeaHistoryReopen
$$;
	--@behavior -resource:insert -resource:update -resource:delete';
COMMENT ON COLUMN app_public.idea_history.updated_at IS
	E'@behavior -attribute:select';
COMMENT ON COLUMN app_public.idea_history.updated_by_id IS
	E'@behavior -attribute:select';

CREATE POLICY select_all ON app_public.idea_history FOR SELECT USING (TRUE);
GRANT SELECT ON app_public.idea_history TO vacation_user;

CREATE TRIGGER _200_history AFTER UPDATE ON app_public.ideas FOR EACH ROW EXECUTE PROCEDURE app_private.tg__idea_history();

COMMIT;
