BEGIN;

CREATE EXTENSION permuteseq WITH SCHEMA public;

ALTER DEFAULT PRIVILEGES REVOKE ALL ON SEQUENCES FROM public;
ALTER DEFAULT PRIVILEGES REVOKE ALL ON FUNCTIONS FROM public;

REVOKE ALL ON SCHEMA public FROM public;

CREATE SCHEMA app_public;
GRANT USAGE ON SCHEMA public, app_public TO vacation_anonymous;

ALTER DEFAULT PRIVILEGES IN SCHEMA public, app_public GRANT usage, select ON SEQUENCES TO vacation_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA public, app_public GRANT execute ON FUNCTIONS TO vacation_user;

CREATE SEQUENCE app_public.trips_id_seq AS BIGINT MINVALUE 1073741824 MAXVALUE 34359738367;
CREATE TABLE app_public.trips (
	id BIGINT PRIMARY KEY DEFAULT permute_nextval('app_public.trips_id_seq'::REGCLASS, x'1c1f1f1aee38ea50'::BIGINT),
	slug TEXT NOT NULL UNIQUE,
	name TEXT NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE NOT NULL,
	address TEXT,
	door_code TEXT,
	CONSTRAINT trips_date_order CHECK (end_date >= start_date)
);
ALTER TABLE app_public.trips ENABLE ROW LEVEL SECURITY;
CREATE INDEX ON app_public.trips(start_date);
CREATE INDEX ON app_public.trips(slug);

COMMENT ON TABLE app_public.trips IS
	E'@behavior query:resource:single \nInformation about a specific trip.';
COMMENT ON COLUMN app_public.trips.id IS
	E'@behavior -attribute:insert';
COMMENT ON CONSTRAINT trips_slug_key ON app_public.trips IS
	E'@behavior constraint:resource:update';

CREATE POLICY select_all ON app_public.trips FOR SELECT USING (TRUE);
CREATE POLICY create_trip ON app_public.trips FOR INSERT WITH CHECK (TRUE);
CREATE POLICY update_trip ON app_public.trips FOR UPDATE USING (TRUE);
GRANT SELECT ON app_public.trips TO vacation_user;
GRANT INSERT (name, slug, start_date, end_date, address, door_code) ON app_public.trips TO vacation_admin;
GRANT UPDATE (name, slug, start_date, end_date, address, door_code) ON app_public.trips TO vacation_admin;

COMMIT;
