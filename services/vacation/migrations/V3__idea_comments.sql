BEGIN;

CREATE OR REPLACE FUNCTION app_private.tg__idea_comment_mentions() RETURNS TRIGGER AS $$
DECLARE
	vacation_user TEXT = CURRENT_SETTING('vacation.user', FALSE);
BEGIN
	IF TG_OP = 'INSERT' OR OLD."comment" != NEW."comment" THEN
		WITH mentions AS (SELECT jsonb_array_elements(NEW.comment_mentions #> '{IDEA}') #>> '{}' number)
		INSERT INTO app_public.idea_history (idea_id, updated_at, updated_by_id, type, idea_comment_id)
		SELECT ideas.id, NOW(), vacation_user, 'MENTION_IDEA_COMMENT', NEW.id FROM app_public.ideas INNER JOIN mentions ON mentions.number=ideas.number::text;
		UPDATE app_public.ideas SET updated_at=NOW() WHERE id=NEW.idea_id;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE PLPGSQL SECURITY DEFINER VOLATILE;

CREATE SEQUENCE app_public.idea_comments_id_seq AS BIGINT MINVALUE 1073741824 MAXVALUE 34359738367;
CREATE TABLE app_public.idea_comments (
	id BIGINT PRIMARY KEY DEFAULT permute_nextval('app_public.idea_comments_id_seq'::REGCLASS, x'a152f3565206d174'::BIGINT),
	idea_id BIGINT NOT NULL REFERENCES app_public.ideas ON DELETE CASCADE,
	"comment" TEXT NOT NULL,
	comment_mentions JSONB NOT NULL,
	created_at TIMESTAMP NOT NULL,
	created_by_id TEXT NOT NULL,
	updated_at TIMESTAMP,
	updated_by_id TEXT
);
ALTER TABLE app_public.idea_comments ENABLE ROW LEVEL SECURITY;
CREATE INDEX ON app_public.idea_comments(idea_id);
CREATE INDEX ON app_public.idea_comments(created_at);
CREATE INDEX ON app_public.idea_comments(updated_at);

COMMENT ON TABLE app_public.idea_comments IS
	E'@behavior query:resource:single -resource:insert -resource:update \nIdeas for future improvement';
COMMENT ON COLUMN app_public.idea_comments.id IS
	E'@behavior -attribute:insert';
COMMENT ON COLUMN app_public.idea_comments.comment_mentions IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.idea_comments.created_at IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.idea_comments.created_by_id IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.idea_comments.updated_at IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';
COMMENT ON COLUMN app_public.idea_comments.updated_by_id IS
	E'@behavior -attribute:select -attribute:insert -attribute:update';

CREATE TRIGGER _100_activity BEFORE INSERT OR UPDATE ON app_public.idea_comments FOR EACH ROW EXECUTE PROCEDURE app_private.tg__activity();
CREATE TRIGGER _200_mentions AFTER INSERT OR UPDATE ON app_public.idea_comments FOR EACH ROW EXECUTE PROCEDURE app_private.tg__idea_comment_mentions();

CREATE POLICY select_all ON app_public.idea_comments FOR SELECT USING (TRUE);
CREATE POLICY create_idea_comment ON app_public.idea_comments FOR INSERT WITH CHECK (TRUE);
-- TODO: Only update your own comments
CREATE POLICY update_idea_comment ON app_public.idea_comments FOR UPDATE USING (TRUE);
GRANT SELECT ON app_public.idea_comments TO vacation_user;
GRANT INSERT (idea_id, "comment", comment_mentions) ON app_public.idea_comments TO vacation_user;
GRANT UPDATE ("comment", comment_mentions) ON app_public.idea_comments TO vacation_user;

ALTER TYPE app_public.idea_history_type ADD VALUE 'MENTION_IDEA_COMMENT';
ALTER TABLE app_public.idea_history ADD idea_comment_id BIGINT REFERENCES app_public.idea_comments ON DELETE CASCADE;
COMMENT ON TABLE app_public.idea_history IS $$
	@interface mode:single type:type
	@type CHANGE_DESCRIPTION name:IdeaHistoryChangeDescription attributes:old_description,new_description
	@type CHANGE_TRIP name:IdeaHistoryChangeTrip attributes:old_trip_id,new_trip_id
	@type CLOSE name:IdeaHistoryClose
	@type MENTION_IDEA name:IdeaHistoryMentionIdea attributes:ref_idea_id
	@type MENTION_IDEA_COMMENT name:IdeaHistoryMentionIdeaComment attributes:idea_comment_id
	@type REOPEN name:IdeaHistoryReopen
$$;

COMMIT;
