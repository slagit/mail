/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 * Learn more about it here: https://the-guild.dev/graphql/codegen/plugins/presets/preset-client#reducing-bundle-size
 */
const documents = {
    "\n  fragment MarkdownLinkFields on MarkdownLink {\n    __typename\n    ... on Idea {\n      number\n      ...IdeaLinkFields\n    }\n    ... on Trip {\n      slug\n      ...TripLinkFields\n    }\n    ... on User {\n      username\n      ...UserLinkFields\n    }\n  }\n": types.MarkdownLinkFieldsFragmentDoc,
    "\n  fragment HistoryUserLinkFields on HistoryUser {\n    __typename\n    ... on User {\n      ...UserLinkFields\n    }\n  }\n": types.HistoryUserLinkFieldsFragmentDoc,
    "\n  fragment IdeaCommentLinkFields on IdeaComment {\n    id\n    comment\n    idea {\n      id\n      number\n    }\n  }\n": types.IdeaCommentLinkFieldsFragmentDoc,
    "\n  fragment IdeaLinkFields on Idea {\n    id\n    number\n    description\n    status\n  }\n": types.IdeaLinkFieldsFragmentDoc,
    "\n  fragment TripLinkFields on Trip {\n    id\n    slug\n    name\n    startDate\n    endDate\n  }\n": types.TripLinkFieldsFragmentDoc,
    "\n  fragment UserLinkFields on User {\n    id\n    firstName\n    lastName\n    username\n  }\n": types.UserLinkFieldsFragmentDoc,
    "\n          mutation UpdateIdea($number: BigInt!, $idea: IdeaPatch!) {\n            idea: updateIdeaByNumber(input: { number: $number, patch: $idea }) {\n              idea {\n                id\n                ...IdeaMainFields\n                ...IdeaTripFields\n                ...IdeaHistoryFields\n              }\n            }\n          }\n        ": types.UpdateIdeaDocument,
    "\n          mutation CommentOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n                idea {\n                  id\n                }\n              }\n            }\n          }\n        ": types.CommentOnIdeaDocument,
    "\n          mutation CommentCloseOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n              }\n            }\n            updateIdeaById(input: { id: $id, patch: { status: closed } }) {\n              idea {\n                id\n                status\n              }\n            }\n          }\n        ": types.CommentCloseOnIdeaDocument,
    "\n          mutation CommentReopenOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n              }\n            }\n            updateIdeaById(input: { id: $id, patch: { status: open } }) {\n              idea {\n                id\n                status\n              }\n            }\n          }\n        ": types.CommentReopenOnIdeaDocument,
    "\n  fragment IdeaMainFields on Idea {\n    createdAt\n    createdBy {\n      ...HistoryUserLinkFields\n    }\n    description\n    descriptionLinks {\n      ...MarkdownLinkFields\n    }\n    number\n    status\n  }\n": types.IdeaMainFieldsFragmentDoc,
    "\n  fragment TripChooserFields on Query {\n    trips(orderBy: START_DATE_DESC) {\n      nodes {\n        id\n        name\n      }\n    }\n  }\n": types.TripChooserFieldsFragmentDoc,
    "\n  fragment IdeaTripFields on Idea {\n    trip {\n      id\n      name\n    }\n  }\n": types.IdeaTripFieldsFragmentDoc,
    "\n  fragment IdeaHistoryFields on Idea {\n    ideaComments(orderBy: CREATED_AT_ASC) {\n      nodes {\n        id\n        comment\n        commentLinks {\n          ...MarkdownLinkFields\n        }\n        createdAt\n        createdBy {\n          ...HistoryUserLinkFields\n        }\n      }\n    }\n    ideaHistories(orderBy: UPDATED_AT_ASC) {\n      nodes {\n        id\n        updatedAt\n        updatedBy {\n          ...HistoryUserLinkFields\n        }\n        ... on IdeaHistoryChangeTrip {\n          newTrip {\n            ...TripLinkFields\n          }\n          oldTrip {\n            ...TripLinkFields\n          }\n        }\n        ... on IdeaHistoryChangeDescription {\n          newDescription\n          oldDescription\n        }\n        ... on IdeaHistoryMentionIdea {\n          refIdea {\n            ...IdeaLinkFields\n          }\n        }\n        ... on IdeaHistoryMentionIdeaComment {\n          ideaComment {\n            ...IdeaCommentLinkFields\n          }\n        }\n      }\n    }\n  }\n": types.IdeaHistoryFieldsFragmentDoc,
    "\n      fragment GetIdea on Query {\n        ...TripChooserFields\n        idea: ideaByNumber(number: $id) {\n          id\n          status\n\t  participants {\n            id\n            ...UserLinkFields\n\t  }\n          ...IdeaMainFields\n          ...IdeaTripFields\n          ...IdeaHistoryFields\n        }\n      }\n    ": types.GetIdeaFragmentDoc,
    "\n      query GetIdea($id: BigInt!) {\n        ...GetIdea\n      }\n    ": types.GetIdeaDocument,
    "\n        mutation CreateIdea($idea: IdeaInput!) {\n          idea: createIdea(input: { idea: $idea }) {\n            idea {\n              id\n              number\n            }\n          }\n        }\n      ": types.CreateIdeaDocument,
    "\n      fragment TripsForIdea on Query {\n        currentTrip {\n          id\n        }\n        trips(orderBy: START_DATE_DESC) {\n          nodes {\n            id\n            name\n          }\n        }\n      }\n    ": types.TripsForIdeaFragmentDoc,
    "\n      query GetTripsForIdea {\n        ...TripsForIdea\n      }\n    ": types.GetTripsForIdeaDocument,
    "\n  fragment IdeaCardFields on Idea {\n    id\n    number\n    status\n    description\n    descriptionLinks {\n      ...MarkdownLinkFields\n    }\n    ideaComments {\n      totalCount\n    }\n    createdAt\n    createdBy {\n      ...HistoryUserLinkFields\n    }\n    updatedAt\n  }\n": types.IdeaCardFieldsFragmentDoc,
    "\n      fragment GetIdeas on Query {\n        ideas(condition: $condition, orderBy: [$orderBy]) {\n          nodes {\n            id\n            ...IdeaCardFields\n          }\n        }\n        closed: ideas(condition: { status: closed }) {\n          totalCount\n        }\n        open: ideas(condition: { status: open }) {\n          totalCount\n        }\n        all: ideas {\n          totalCount\n        }\n      }\n    ": types.GetIdeasFragmentDoc,
    "\n      query GetIdeas($condition: IdeaCondition!, $orderBy: IdeaOrderBy!) {\n        ...GetIdeas\n      }\n    ": types.GetIdeasDocument,
    "\n      fragment GetTrip on Query {\n        trip: tripBySlug(slug: $slug) {\n          id\n          address\n          doorCode\n          endDate\n          name\n          startDate\n        }\n      }\n    ": types.GetTripFragmentDoc,
    "\n      query GetTrip($slug: String!) {\n        ...GetTrip\n        ...GetTripForEdit\n      }\n    ": types.GetTripDocument,
    "\n        mutation UpdateTrip($slug: String!, $trip: TripPatch!) {\n          trip: updateTripBySlug(input: { slug: $slug, patch: $trip }) {\n            trip {\n              id\n              slug\n            }\n          }\n        }\n      ": types.UpdateTripDocument,
    "\n      fragment GetTripForEdit on Query {\n        trip: tripBySlug(slug: $slug) {\n          id\n          endDate\n          name\n          slug\n          startDate\n        }\n      }\n    ": types.GetTripForEditFragmentDoc,
    "\n        mutation CreateTrip($trip: TripInput!) {\n          trip: createTrip(input: { trip: $trip }) {\n            trip {\n              id\n              slug\n            }\n          }\n        }\n      ": types.CreateTripDocument,
    "\n      fragment GetTrips on Query {\n        trips(orderBy: START_DATE_DESC) {\n          nodes {\n            id\n            endDate\n            name\n            slug\n            startDate\n          }\n        }\n      }\n    ": types.GetTripsFragmentDoc,
    "\n      query GetTrips {\n        ...GetTrips\n      }\n    ": types.GetTripsDocument,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = graphql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function graphql(source: string): unknown;

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment MarkdownLinkFields on MarkdownLink {\n    __typename\n    ... on Idea {\n      number\n      ...IdeaLinkFields\n    }\n    ... on Trip {\n      slug\n      ...TripLinkFields\n    }\n    ... on User {\n      username\n      ...UserLinkFields\n    }\n  }\n"): (typeof documents)["\n  fragment MarkdownLinkFields on MarkdownLink {\n    __typename\n    ... on Idea {\n      number\n      ...IdeaLinkFields\n    }\n    ... on Trip {\n      slug\n      ...TripLinkFields\n    }\n    ... on User {\n      username\n      ...UserLinkFields\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment HistoryUserLinkFields on HistoryUser {\n    __typename\n    ... on User {\n      ...UserLinkFields\n    }\n  }\n"): (typeof documents)["\n  fragment HistoryUserLinkFields on HistoryUser {\n    __typename\n    ... on User {\n      ...UserLinkFields\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment IdeaCommentLinkFields on IdeaComment {\n    id\n    comment\n    idea {\n      id\n      number\n    }\n  }\n"): (typeof documents)["\n  fragment IdeaCommentLinkFields on IdeaComment {\n    id\n    comment\n    idea {\n      id\n      number\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment IdeaLinkFields on Idea {\n    id\n    number\n    description\n    status\n  }\n"): (typeof documents)["\n  fragment IdeaLinkFields on Idea {\n    id\n    number\n    description\n    status\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment TripLinkFields on Trip {\n    id\n    slug\n    name\n    startDate\n    endDate\n  }\n"): (typeof documents)["\n  fragment TripLinkFields on Trip {\n    id\n    slug\n    name\n    startDate\n    endDate\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment UserLinkFields on User {\n    id\n    firstName\n    lastName\n    username\n  }\n"): (typeof documents)["\n  fragment UserLinkFields on User {\n    id\n    firstName\n    lastName\n    username\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n          mutation UpdateIdea($number: BigInt!, $idea: IdeaPatch!) {\n            idea: updateIdeaByNumber(input: { number: $number, patch: $idea }) {\n              idea {\n                id\n                ...IdeaMainFields\n                ...IdeaTripFields\n                ...IdeaHistoryFields\n              }\n            }\n          }\n        "): (typeof documents)["\n          mutation UpdateIdea($number: BigInt!, $idea: IdeaPatch!) {\n            idea: updateIdeaByNumber(input: { number: $number, patch: $idea }) {\n              idea {\n                id\n                ...IdeaMainFields\n                ...IdeaTripFields\n                ...IdeaHistoryFields\n              }\n            }\n          }\n        "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n          mutation CommentOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n                idea {\n                  id\n                }\n              }\n            }\n          }\n        "): (typeof documents)["\n          mutation CommentOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n                idea {\n                  id\n                }\n              }\n            }\n          }\n        "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n          mutation CommentCloseOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n              }\n            }\n            updateIdeaById(input: { id: $id, patch: { status: closed } }) {\n              idea {\n                id\n                status\n              }\n            }\n          }\n        "): (typeof documents)["\n          mutation CommentCloseOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n              }\n            }\n            updateIdeaById(input: { id: $id, patch: { status: closed } }) {\n              idea {\n                id\n                status\n              }\n            }\n          }\n        "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n          mutation CommentReopenOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n              }\n            }\n            updateIdeaById(input: { id: $id, patch: { status: open } }) {\n              idea {\n                id\n                status\n              }\n            }\n          }\n        "): (typeof documents)["\n          mutation CommentReopenOnIdea($id: ID!, $comment: String!) {\n            createIdeaComment(\n              input: { ideaComment: { comment: $comment, idea: $id } }\n            ) {\n              ideaComment {\n                id\n              }\n            }\n            updateIdeaById(input: { id: $id, patch: { status: open } }) {\n              idea {\n                id\n                status\n              }\n            }\n          }\n        "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment IdeaMainFields on Idea {\n    createdAt\n    createdBy {\n      ...HistoryUserLinkFields\n    }\n    description\n    descriptionLinks {\n      ...MarkdownLinkFields\n    }\n    number\n    status\n  }\n"): (typeof documents)["\n  fragment IdeaMainFields on Idea {\n    createdAt\n    createdBy {\n      ...HistoryUserLinkFields\n    }\n    description\n    descriptionLinks {\n      ...MarkdownLinkFields\n    }\n    number\n    status\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment TripChooserFields on Query {\n    trips(orderBy: START_DATE_DESC) {\n      nodes {\n        id\n        name\n      }\n    }\n  }\n"): (typeof documents)["\n  fragment TripChooserFields on Query {\n    trips(orderBy: START_DATE_DESC) {\n      nodes {\n        id\n        name\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment IdeaTripFields on Idea {\n    trip {\n      id\n      name\n    }\n  }\n"): (typeof documents)["\n  fragment IdeaTripFields on Idea {\n    trip {\n      id\n      name\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment IdeaHistoryFields on Idea {\n    ideaComments(orderBy: CREATED_AT_ASC) {\n      nodes {\n        id\n        comment\n        commentLinks {\n          ...MarkdownLinkFields\n        }\n        createdAt\n        createdBy {\n          ...HistoryUserLinkFields\n        }\n      }\n    }\n    ideaHistories(orderBy: UPDATED_AT_ASC) {\n      nodes {\n        id\n        updatedAt\n        updatedBy {\n          ...HistoryUserLinkFields\n        }\n        ... on IdeaHistoryChangeTrip {\n          newTrip {\n            ...TripLinkFields\n          }\n          oldTrip {\n            ...TripLinkFields\n          }\n        }\n        ... on IdeaHistoryChangeDescription {\n          newDescription\n          oldDescription\n        }\n        ... on IdeaHistoryMentionIdea {\n          refIdea {\n            ...IdeaLinkFields\n          }\n        }\n        ... on IdeaHistoryMentionIdeaComment {\n          ideaComment {\n            ...IdeaCommentLinkFields\n          }\n        }\n      }\n    }\n  }\n"): (typeof documents)["\n  fragment IdeaHistoryFields on Idea {\n    ideaComments(orderBy: CREATED_AT_ASC) {\n      nodes {\n        id\n        comment\n        commentLinks {\n          ...MarkdownLinkFields\n        }\n        createdAt\n        createdBy {\n          ...HistoryUserLinkFields\n        }\n      }\n    }\n    ideaHistories(orderBy: UPDATED_AT_ASC) {\n      nodes {\n        id\n        updatedAt\n        updatedBy {\n          ...HistoryUserLinkFields\n        }\n        ... on IdeaHistoryChangeTrip {\n          newTrip {\n            ...TripLinkFields\n          }\n          oldTrip {\n            ...TripLinkFields\n          }\n        }\n        ... on IdeaHistoryChangeDescription {\n          newDescription\n          oldDescription\n        }\n        ... on IdeaHistoryMentionIdea {\n          refIdea {\n            ...IdeaLinkFields\n          }\n        }\n        ... on IdeaHistoryMentionIdeaComment {\n          ideaComment {\n            ...IdeaCommentLinkFields\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      fragment GetIdea on Query {\n        ...TripChooserFields\n        idea: ideaByNumber(number: $id) {\n          id\n          status\n\t  participants {\n            id\n            ...UserLinkFields\n\t  }\n          ...IdeaMainFields\n          ...IdeaTripFields\n          ...IdeaHistoryFields\n        }\n      }\n    "): (typeof documents)["\n      fragment GetIdea on Query {\n        ...TripChooserFields\n        idea: ideaByNumber(number: $id) {\n          id\n          status\n\t  participants {\n            id\n            ...UserLinkFields\n\t  }\n          ...IdeaMainFields\n          ...IdeaTripFields\n          ...IdeaHistoryFields\n        }\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      query GetIdea($id: BigInt!) {\n        ...GetIdea\n      }\n    "): (typeof documents)["\n      query GetIdea($id: BigInt!) {\n        ...GetIdea\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n        mutation CreateIdea($idea: IdeaInput!) {\n          idea: createIdea(input: { idea: $idea }) {\n            idea {\n              id\n              number\n            }\n          }\n        }\n      "): (typeof documents)["\n        mutation CreateIdea($idea: IdeaInput!) {\n          idea: createIdea(input: { idea: $idea }) {\n            idea {\n              id\n              number\n            }\n          }\n        }\n      "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      fragment TripsForIdea on Query {\n        currentTrip {\n          id\n        }\n        trips(orderBy: START_DATE_DESC) {\n          nodes {\n            id\n            name\n          }\n        }\n      }\n    "): (typeof documents)["\n      fragment TripsForIdea on Query {\n        currentTrip {\n          id\n        }\n        trips(orderBy: START_DATE_DESC) {\n          nodes {\n            id\n            name\n          }\n        }\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      query GetTripsForIdea {\n        ...TripsForIdea\n      }\n    "): (typeof documents)["\n      query GetTripsForIdea {\n        ...TripsForIdea\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment IdeaCardFields on Idea {\n    id\n    number\n    status\n    description\n    descriptionLinks {\n      ...MarkdownLinkFields\n    }\n    ideaComments {\n      totalCount\n    }\n    createdAt\n    createdBy {\n      ...HistoryUserLinkFields\n    }\n    updatedAt\n  }\n"): (typeof documents)["\n  fragment IdeaCardFields on Idea {\n    id\n    number\n    status\n    description\n    descriptionLinks {\n      ...MarkdownLinkFields\n    }\n    ideaComments {\n      totalCount\n    }\n    createdAt\n    createdBy {\n      ...HistoryUserLinkFields\n    }\n    updatedAt\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      fragment GetIdeas on Query {\n        ideas(condition: $condition, orderBy: [$orderBy]) {\n          nodes {\n            id\n            ...IdeaCardFields\n          }\n        }\n        closed: ideas(condition: { status: closed }) {\n          totalCount\n        }\n        open: ideas(condition: { status: open }) {\n          totalCount\n        }\n        all: ideas {\n          totalCount\n        }\n      }\n    "): (typeof documents)["\n      fragment GetIdeas on Query {\n        ideas(condition: $condition, orderBy: [$orderBy]) {\n          nodes {\n            id\n            ...IdeaCardFields\n          }\n        }\n        closed: ideas(condition: { status: closed }) {\n          totalCount\n        }\n        open: ideas(condition: { status: open }) {\n          totalCount\n        }\n        all: ideas {\n          totalCount\n        }\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      query GetIdeas($condition: IdeaCondition!, $orderBy: IdeaOrderBy!) {\n        ...GetIdeas\n      }\n    "): (typeof documents)["\n      query GetIdeas($condition: IdeaCondition!, $orderBy: IdeaOrderBy!) {\n        ...GetIdeas\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      fragment GetTrip on Query {\n        trip: tripBySlug(slug: $slug) {\n          id\n          address\n          doorCode\n          endDate\n          name\n          startDate\n        }\n      }\n    "): (typeof documents)["\n      fragment GetTrip on Query {\n        trip: tripBySlug(slug: $slug) {\n          id\n          address\n          doorCode\n          endDate\n          name\n          startDate\n        }\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      query GetTrip($slug: String!) {\n        ...GetTrip\n        ...GetTripForEdit\n      }\n    "): (typeof documents)["\n      query GetTrip($slug: String!) {\n        ...GetTrip\n        ...GetTripForEdit\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n        mutation UpdateTrip($slug: String!, $trip: TripPatch!) {\n          trip: updateTripBySlug(input: { slug: $slug, patch: $trip }) {\n            trip {\n              id\n              slug\n            }\n          }\n        }\n      "): (typeof documents)["\n        mutation UpdateTrip($slug: String!, $trip: TripPatch!) {\n          trip: updateTripBySlug(input: { slug: $slug, patch: $trip }) {\n            trip {\n              id\n              slug\n            }\n          }\n        }\n      "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      fragment GetTripForEdit on Query {\n        trip: tripBySlug(slug: $slug) {\n          id\n          endDate\n          name\n          slug\n          startDate\n        }\n      }\n    "): (typeof documents)["\n      fragment GetTripForEdit on Query {\n        trip: tripBySlug(slug: $slug) {\n          id\n          endDate\n          name\n          slug\n          startDate\n        }\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n        mutation CreateTrip($trip: TripInput!) {\n          trip: createTrip(input: { trip: $trip }) {\n            trip {\n              id\n              slug\n            }\n          }\n        }\n      "): (typeof documents)["\n        mutation CreateTrip($trip: TripInput!) {\n          trip: createTrip(input: { trip: $trip }) {\n            trip {\n              id\n              slug\n            }\n          }\n        }\n      "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      fragment GetTrips on Query {\n        trips(orderBy: START_DATE_DESC) {\n          nodes {\n            id\n            endDate\n            name\n            slug\n            startDate\n          }\n        }\n      }\n    "): (typeof documents)["\n      fragment GetTrips on Query {\n        trips(orderBy: START_DATE_DESC) {\n          nodes {\n            id\n            endDate\n            name\n            slug\n            startDate\n          }\n        }\n      }\n    "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n      query GetTrips {\n        ...GetTrips\n      }\n    "): (typeof documents)["\n      query GetTrips {\n        ...GetTrips\n      }\n    "];

export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;