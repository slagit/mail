import TimeAgo from "javascript-time-ago";
import en from "javascript-time-ago/locale/en";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { AuthProvider } from "react-oauth2-code-pkce";
import config from "./config";
import Router from "./Router";
import "./index.css";

TimeAgo.addDefaultLocale(en);

const authConfig = {
  clientId: config.clientId,
  authorizationEndpoint:
    "https://auth.slagit.net/realms/slagit/protocol/openid-connect/auth",
  tokenEndpoint:
    "https://auth.slagit.net/realms/slagit/protocol/openid-connect/token",
  logoutEndpoint:
    "https://auth.slagit.net/realms/slagit/protocol/openid-connect/logout",
  redirectUri: config.redirectUri,
  scope: "profile openid",
  preLogin: () =>
    localStorage.setItem("preLoginPath", window.location.pathname),
  postLogin: () =>
    window.location.replace(localStorage.getItem("preLoginPath") || ""),
};

createRoot(document.getElementById("root")!).render(
  <StrictMode>
    <AuthProvider authConfig={authConfig}>
      <Router />
    </AuthProvider>
  </StrictMode>,
);
