import { Alert, Card } from "flowbite-react";
import type { ErrorResponse } from "react-router-dom";
import { isRouteErrorResponse, useRouteError } from "react-router-dom";

interface ErrorAlertProps {
  error: ErrorResponse;
}

function ErrorAlert({ error }: ErrorAlertProps) {
  return (
    <>
      <Alert color="failure" className="max-w-2xl mx-auto">
        <span className="font-medium">{error.statusText}:</span>{" "}
        {error.data?.message}
      </Alert>
      <Card className="max-w-2xl text-gray-700 dark:text-gray-400 whitespace-pre overflow-auto">
        {JSON.stringify(error, null, 2)}
      </Card>
    </>
  );
}

export default function DemoError() {
  const error = useRouteError();
  if (isRouteErrorResponse(error)) {
    return <ErrorAlert error={error} />;
  }
  console.error(error);
  return (
    <ErrorAlert
      error={{
        data: {
          message: "An unknown error occurred",
        },
        status: 500,
        statusText: "Internal Server Error",
      }}
    />
  );
}
