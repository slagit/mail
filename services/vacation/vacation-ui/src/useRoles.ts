import { useContext } from "react";
import { AuthContext } from "react-oauth2-code-pkce";
import config from "./config";

export default function useRoles() {
  const { tokenData } = useContext(AuthContext);

  const roles = tokenData?.resource_access[config.audience].roles ?? [];

  return {
    admin: roles.includes("admin"),
    user: roles.includes("user"),
  };
}
