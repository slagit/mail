import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";
import type { GraphQLFormattedError } from "graphql";
import type { ActionFunctionArgs } from "react-router-dom";
import { z } from "zod";
import { graphql } from "../__generated__";
import type { CommentOnIdeaMutation, Exact } from "../__generated__/graphql";
import { IdeaPatch } from "../__generated__/graphql";
import client from "../client";
import handleActionError from "../handleActionError";

function getCommentErrorField({ extensions, path }: GraphQLFormattedError) {
  if (path?.length === 1 && path?.[0] == "createIdeaComment") {
    const field = extensions?.field;
    if (typeof field === "string" && ["comment"].includes(field)) {
      return [field];
    }
  }
  return [];
}

function getDescriptionErrorField({ extensions, path }: GraphQLFormattedError) {
  if (path?.length === 1 && path?.[0] == "idea") {
    const field = extensions?.field;
    if (typeof field === "string" && ["description"].includes(field)) {
      return [field];
    }
  }
  return [];
}

function getTripErrorField({ extensions, path }: GraphQLFormattedError) {
  if (path?.length === 1 && path?.[0] == "idea") {
    const field = extensions?.field;
    if (typeof field === "string" && ["trip"].includes(field)) {
      return [field];
    }
  }
  return [];
}

const Payload = z.discriminatedUnion("intent", [
  z.object({
    intent: z.literal("ADD_COMMENT"),
    comment: z.string().min(1),
    ideaId: z.string().min(1),
  }),
  z.object({
    intent: z.literal("ADD_COMMENT_CLOSE"),
    comment: z.string().min(1),
    ideaId: z.string().min(1),
  }),
  z.object({
    intent: z.literal("ADD_COMMENT_REOPEN"),
    comment: z.string().min(1),
    ideaId: z.string().min(1),
  }),
  z.object({ intent: z.literal("SET_TRIP"), trip: z.string() }),
  z.object({
    intent: z.literal("SET_DESCRIPTION"),
    description: z.string().min(1),
  }),
]);

export default async function action({ request, params }: ActionFunctionArgs) {
  const formPayload = Object.fromEntries(await request.formData());
  try {
    const payload = Payload.parse(formPayload);
    async function updateIdea(idea: IdeaPatch) {
      await client.mutate({
        mutation: graphql(`
          mutation UpdateIdea($number: BigInt!, $idea: IdeaPatch!) {
            idea: updateIdeaByNumber(input: { number: $number, patch: $idea }) {
              idea {
                id
                ...IdeaMainFields
                ...IdeaTripFields
                ...IdeaHistoryFields
              }
            }
          }
        `),
        variables: {
          number: params.ideaId!,
          idea,
        },
      });
      return null;
    }
    async function addComment(
      mutation: DocumentNode<
        CommentOnIdeaMutation,
        Exact<{ comment: string; id: string }>
      >,
      payload: { comment: string; ideaId: string },
    ) {
      try {
        await client.mutate({
          mutation,
          update(cache, { data }) {
            const ideaComment = data?.createIdeaComment?.ideaComment;
            if (ideaComment?.idea) {
              cache.modify({
                id: cache.identify(ideaComment.idea),
                fields: {
                  ideaComments(cachedComments) {
                    return {
                      ...cachedComments,
                      nodes: [...cachedComments.nodes, ideaComment],
                    };
                  },
                },
              });
            }
          },
          variables: {
            comment: payload.comment,
            id: payload.ideaId,
          },
        });
        return null;
      } catch (error) {
        return handleActionError(error, getCommentErrorField);
      }
    }
    if (payload.intent === "ADD_COMMENT") {
      return addComment(
        graphql(`
          mutation CommentOnIdea($id: ID!, $comment: String!) {
            createIdeaComment(
              input: { ideaComment: { comment: $comment, idea: $id } }
            ) {
              ideaComment {
                id
                idea {
                  id
                }
              }
            }
          }
        `),
        payload,
      );
    }
    if (payload.intent === "ADD_COMMENT_CLOSE") {
      return addComment(
        graphql(`
          mutation CommentCloseOnIdea($id: ID!, $comment: String!) {
            createIdeaComment(
              input: { ideaComment: { comment: $comment, idea: $id } }
            ) {
              ideaComment {
                id
              }
            }
            updateIdeaById(input: { id: $id, patch: { status: closed } }) {
              idea {
                id
                status
              }
            }
          }
        `),
        payload,
      );
    }
    if (payload.intent === "ADD_COMMENT_REOPEN") {
      return addComment(
        graphql(`
          mutation CommentReopenOnIdea($id: ID!, $comment: String!) {
            createIdeaComment(
              input: { ideaComment: { comment: $comment, idea: $id } }
            ) {
              ideaComment {
                id
              }
            }
            updateIdeaById(input: { id: $id, patch: { status: open } }) {
              idea {
                id
                status
              }
            }
          }
        `),
        payload,
      );
    }
    if (payload.intent === "SET_TRIP") {
      try {
        await updateIdea({
          trip: payload.trip,
        });
        return null;
      } catch (error) {
        return handleActionError(error, getTripErrorField);
      }
    }
    if (payload.intent === "SET_DESCRIPTION") {
      try {
        await updateIdea({
          description: payload.description,
        });
        return null;
      } catch (error) {
        return handleActionError(error, getDescriptionErrorField);
      }
    }
    return null;
  } catch (error) {
    return handleActionError(error);
  }
}
