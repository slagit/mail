import type { GraphQLFormattedError } from "graphql";
import type { ActionFunctionArgs } from "react-router-dom";
import { redirect } from "react-router-dom";
import client from "../client";
import { graphql } from "../__generated__";
import handleActionError from "../handleActionError";
import Trip from "../validators/Trip";

function getErrorField({ extensions, path }: GraphQLFormattedError) {
  if (path?.length === 1 && path?.[0] == "trip") {
    const field = extensions?.field;
    if (typeof field === "string" && ["name", "slug"].includes(field)) {
      return [field];
    }
  }
  return [];
}

export default async function action({ request, params }: ActionFunctionArgs) {
  const formPayload = Object.fromEntries(await request.formData());
  try {
    const trip = Trip.parse(formPayload);
    const { data } = await client.mutate({
      mutation: graphql(`
        mutation UpdateTrip($slug: String!, $trip: TripPatch!) {
          trip: updateTripBySlug(input: { slug: $slug, patch: $trip }) {
            trip {
              id
              slug
            }
          }
        }
      `),
      variables: {
        slug: params.tripSlug!,
        trip,
      },
    });
    return redirect(`/trips/${data?.trip?.trip?.slug}`);
  } catch (error) {
    return handleActionError(error, getErrorField);
  }
}
