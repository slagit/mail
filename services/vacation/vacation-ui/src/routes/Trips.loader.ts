import { graphql } from "../__generated__";
import { preloadQuery } from "../client";

export default async function loader() {
  const queryRef = await preloadQuery(
    graphql(`
      query GetTrips {
        ...GetTrips
      }
    `),
    {
      fetchPolicy: "cache-and-network",
      pollInterval: 5000,
    },
  ).toPromise();
  return queryRef;
}
