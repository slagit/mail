import { useActionData } from "react-router-dom";
import { ZodError } from "zod";
import TripModal from "../components/TripModal";

export default function TripNew() {
  const error = useActionData() as ZodError;
  return <TripModal error={error} />;
}
