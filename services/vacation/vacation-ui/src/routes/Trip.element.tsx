import { useReadQuery } from "@apollo/client";
import { parseISO } from "date-fns";
import { Alert, Button } from "flowbite-react";
import type { ReactNode } from "react";
import { Link, Outlet, useLoaderData } from "react-router-dom";
import { graphql, useFragment } from "../__generated__";
import useRoles from "../useRoles";
import type loader from "./Trip.loader";

interface MenuItemProps {
  children: ReactNode;
  title: string;
}

function MenuItem({ children, title }: MenuItemProps) {
  return (
    <div className="flex flex-col py-3 first:pt-0 last:pb-0">
      <dt className="mb-2 text-gray-500 md:text-lg dark:text-gray-400">
        {title}
      </dt>
      <dd className="text-lg font-semibold">{children}</dd>
    </div>
  );
}

const dateFormat = Intl.DateTimeFormat(undefined, {
  year: "numeric",
  month: "long",
  day: "numeric",
});

export default function Trip() {
  const { admin } = useRoles();
  const queryRef = useLoaderData() as Awaited<ReturnType<typeof loader>>;
  const { data } = useReadQuery(queryRef);
  const { trip } = useFragment(
    graphql(`
      fragment GetTrip on Query {
        trip: tripBySlug(slug: $slug) {
          id
          address
          doorCode
          endDate
          name
          startDate
        }
      }
    `),
    data,
  );
  if (!trip) {
    // TODO: Maybe throw an error that the handler can render?
    return (
      <div className="p-2 max-w-6xl flex flex-col mx-auto gap-5">
        <Alert color="failure">
          <span className="font-medium">404 Not Found:</span> The requested trip
          was not found on this system.
        </Alert>
      </div>
    );
  }
  const start_date = trip.startDate ? parseISO(trip.startDate) : null;
  const end_date = trip.endDate ? parseISO(trip.endDate) : null;
  return (
    <>
      <div className="p-2 max-w-xl flex flex-col mx-auto gap-5">
        <div className="flex">
          <h1 className="text-2xl font-bold dark:text-white">{trip.name}</h1>
          <div className="flex flex-1 justify-end">
            {admin && (
              <Button as={Link} to="edit" color="gray">
                Edit
              </Button>
            )}
          </div>
        </div>
        <dl className="p-2 text-gray-900 divide-y divide-gray-200 dark:text-white dark:divide-gray-600 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600">
          {start_date && end_date && (
            <MenuItem title="Dates">
              {dateFormat.formatRange(start_date, end_date)}
            </MenuItem>
          )}
          {trip.address && (
            <MenuItem title="Address">
              <span className="whitespace-pre-line">{trip.address}</span>
            </MenuItem>
          )}
          {trip.doorCode && (
            <MenuItem title="Door Code">{trip.doorCode}</MenuItem>
          )}
        </dl>
        <Outlet />
      </div>
    </>
  );
}
