import { useReadQuery } from "@apollo/client";
import { parseISO } from "date-fns";
import {
  Alert,
  Badge,
  Button,
  Label,
  List,
  Select,
  Textarea,
} from "flowbite-react";
import type { ReactNode } from "react";
import { Fragment } from "react";
import { useContext, useEffect, useRef, useState } from "react";
import {
  HiOutlineChevronDoubleLeft,
  HiOutlineChevronDoubleRight,
} from "react-icons/hi";
import { Link, useFetcher, useLoaderData } from "react-router-dom";
import ReactTimeAgo from "react-time-ago";
import { z, ZodError } from "zod";
import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";
import type { IdeaMainFieldsFragment } from "../__generated__/graphql";
import { IdeaStatus } from "../__generated__/graphql";
import NavContext from "../NavContext";
import RenderMarkdown from "../RenderMarkdown";
import HistoryUserLink from "../components/HistoryUserLink";
import IdeaLink from "../components/IdeaLink";
import IdeaCommentLink from "../components/IdeaCommentLink";
import TripLink from "../components/TripLink";
import UserLink from "../components/UserLink";
import useLocalStorage from "../useLocalStorage";
import loader from "./Idea.loader";

function useWatchFetcher(onClose: () => unknown) {
  const fetcher = useFetcher();
  const [fetcherState, setFetcherState] = useState(fetcher.state);
  useEffect(() => {
    if (
      fetcherState !== "idle" &&
      fetcher.state === "idle" &&
      fetcher.data === null
    ) {
      onClose();
    }
    setFetcherState(fetcher.state);
  }, [fetcher, fetcherState, onClose]);
  return fetcher;
}

interface IdeaEditProps {
  idea: IdeaMainFieldsFragment;
  onClose: () => unknown;
}

function IdeaEdit({ idea, onClose }: IdeaEditProps) {
  const fetcher = useWatchFetcher(onClose);
  const issues = (fetcher.data as ZodError)?.flatten();
  return (
    <fetcher.Form method="post">
      <div className="flex flex-col gap-5">
        {(issues?.formErrors ?? []).map((issue) => (
          <div
            className="p-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
            key={issue}
          >
            {issue}
          </div>
        ))}
        <div>
          <div className="mb-2">
            <Label
              color={issues?.fieldErrors.description ? "failure" : "default"}
              htmlFor="description"
              value="Description"
            />
          </div>
          <Textarea
            color={issues?.fieldErrors.description ? "failure" : "gray"}
            rows={3}
            id="description"
            name="description"
            defaultValue={idea.description}
            helperText={issues?.fieldErrors.description}
          />
        </div>
        <div className="flex gap-2">
          <Button type="submit" name="intent" value="SET_DESCRIPTION">
            Save changes
          </Button>
          <Button color="gray" onClick={onClose}>
            Cancel
          </Button>
        </div>
      </div>
    </fetcher.Form>
  );
}

const IdeaMainFields = graphql(`
  fragment IdeaMainFields on Idea {
    createdAt
    createdBy {
      ...HistoryUserLinkFields
    }
    description
    descriptionLinks {
      ...MarkdownLinkFields
    }
    number
    status
  }
`);

interface IdeaMainProps {
  idea: FragmentType<typeof IdeaMainFields>;
  expanded: boolean;
  setExpanded: (expanded: boolean) => unknown;
}

function IdeaMain({ idea: ideaQuery, expanded, setExpanded }: IdeaMainProps) {
  const idea = useFragment(IdeaMainFields, ideaQuery);
  const [editing, setEditing] = useState(false);
  if (editing) {
    return <IdeaEdit idea={idea} onClose={() => setEditing(false)} />;
  }
  const createdBy = idea.createdBy ? (
    <HistoryUserLink user={idea.createdBy} />
  ) : null;
  return (
    <>
      <nav className="flex" aria-label="Breadcrumb">
        <ol className="inline-flex items-center space-x-1 md:space-x-2 rtl:space-x-reverse">
          <li className="inline-flex items-center">
            <Link
              to="/ideas"
              className="inline-flex items-center text-sm font-medium text-gray-700 hover:text-blue-600 dark:text-gray-400 dark:hover:text-white"
            >
              Ideas
            </Link>
          </li>
          <li aria-current="page">
            <div className="flex items-center">
              <svg
                className="rtl:rotate-180 w-3 h-3 text-gray-400 mx-1"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 6 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 9 4-4-4-4"
                />
              </svg>
              <span className="ms-1 text-sm font-medium text-gray-500 md:ms-2 dark:text-gray-400">
                #{idea.number}
              </span>
            </div>
          </li>
        </ol>
      </nav>
      <div className="flex flex-col md:flex-row">
        <div className="flex gap-2 md:order-2 pb-2 md:pb-0">
          <Button color="gray" onClick={() => setEditing(true)}>
            Edit
          </Button>
        </div>
        <div className="flex md:grow">
          <div className="text-gray-500 dark:text-gray-400 grow flex gap-2">
            {idea.status === "open" && <Badge color="success">Open</Badge>}
            {idea.status === "closed" && <Badge>Closed</Badge>}
            <span>
              Idea created <ReactTimeAgo date={parseISO(idea.createdAt)} />
              {createdBy && (
                <>
                  {" by "}
                  {createdBy}
                </>
              )}
            </span>
          </div>
          {!expanded && (
            <div>
              <Button
                className="2xl:fixed 2xl:right-0 2xl:rounded-e-none 2xl:border-r-0 2xl:z-40 2xl:bg-gray-50 2xl:dark:bg-gray-800"
                color="gray"
                onClick={() => setExpanded(true)}
              >
                <HiOutlineChevronDoubleLeft className="h-5 2xl:h-auto" />
              </Button>
            </div>
          )}
        </div>
      </div>
      <RenderMarkdown links={idea.descriptionLinks}>
        {idea.description}
      </RenderMarkdown>
    </>
  );
}

const TripChooserFields = graphql(`
  fragment TripChooserFields on Query {
    trips(orderBy: START_DATE_DESC) {
      nodes {
        id
        name
      }
    }
  }
`);

interface TripEditProps {
  tripId: string;
  query: FragmentType<typeof TripChooserFields>;
  onClose: () => unknown;
}

function TripEdit({ tripId, query, onClose }: TripEditProps) {
  const { trips } = useFragment(TripChooserFields, query);
  const fetcher = useWatchFetcher(onClose);
  const issues = (fetcher.data as ZodError)?.flatten();
  return (
    <div className="w-full">
      {(issues?.formErrors ?? []).map((issue) => (
        <div
          className="p-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
          key={issue}
        >
          {issue}
        </div>
      ))}
      <Select
        color={issues?.fieldErrors.description ? "failure" : "gray"}
        defaultValue={tripId}
        id="trip"
        name="trip"
        onChange={(e) => {
          fetcher.submit(
            {
              intent: "SET_TRIP",
              trip: e.target.value,
            },
            {
              method: "post",
            },
          );
        }}
        helperText={issues?.fieldErrors.description}
      >
        {trips?.nodes.map((trip) => (
          <option key={trip.id} value={trip.id}>
            {trip.name}
          </option>
        ))}
      </Select>
    </div>
  );
}

const IdeaTripFields = graphql(`
  fragment IdeaTripFields on Idea {
    trip {
      id
      name
    }
  }
`);

interface TripProps {
  idea: FragmentType<typeof IdeaTripFields>;
  query: FragmentType<typeof TripChooserFields>;
}

function Trip({ idea: ideaQuery, query }: TripProps) {
  const [editing, setEditing] = useState(false);
  const idea = useFragment(IdeaTripFields, ideaQuery);
  return (
    <>
      <div className="flex items-center space-x-4 rtl:space-x-reverse">
        <div className="min-w-0 flex-1">
          <p className="truncate text-sm font-medium text-gray-900 dark:text-white">
            Trip
          </p>
          {!editing && (
            <p className="truncate text-sm text-gray-500 dark:text-gray-400">
              {idea.trip?.name}
            </p>
          )}
          {editing && (
            <TripEdit
              onClose={() => setEditing(false)}
              query={query}
              tripId={idea.trip?.id ?? ""}
            />
          )}
        </div>
        <div className="inline-flex items-center text-base font-semibold">
          <Button color="gray" size="xs" onClick={() => setEditing(!editing)}>
            Edit
          </Button>
        </div>
      </div>
    </>
  );
}

interface NewCommentProps {
  ideaId: string;
  status: IdeaStatus;
}

function NewComment({ ideaId, status }: NewCommentProps) {
  const ref = useRef<HTMLTextAreaElement>(null);
  const onClose = () => {
    if (ref.current) {
      ref.current.value = "";
    }
  };
  const fetcher = useWatchFetcher(onClose);
  const issues = (fetcher.data as ZodError)?.flatten();
  return (
    <fetcher.Form method="post">
      <div className="flex flex-col gap-5">
        {(issues?.formErrors ?? []).map((issue) => (
          <div
            className="p-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
            key={issue}
          >
            {issue}
          </div>
        ))}
        <input type="hidden" name="ideaId" value={ideaId} />
        <div>
          <div className="mb-2">
            <Label
              color={issues?.fieldErrors.comment ? "failure" : "default"}
              htmlFor="comment"
              value="Comment"
            />
          </div>
          <Textarea
            color={issues?.fieldErrors.comment ? "failure" : "gray"}
            rows={3}
            id="comment"
            name="comment"
            ref={ref}
            helperText={issues?.fieldErrors.comment}
          />
        </div>
        <div className="flex gap-2">
          <Button type="submit" name="intent" value="ADD_COMMENT">
            Comment
          </Button>
          {status === IdeaStatus.Open && (
            <Button
              color="gray"
              type="submit"
              name="intent"
              value="ADD_COMMENT_CLOSE"
            >
              Comment and Close
            </Button>
          )}
          {status === IdeaStatus.Closed && (
            <Button
              color="gray"
              type="submit"
              name="intent"
              value="ADD_COMMENT_REOPEN"
            >
              Comment and Reopen
            </Button>
          )}
        </div>
      </div>
    </fetcher.Form>
  );
}

const IdeaHistoryFields = graphql(`
  fragment IdeaHistoryFields on Idea {
    ideaComments(orderBy: CREATED_AT_ASC) {
      nodes {
        id
        comment
        commentLinks {
          ...MarkdownLinkFields
        }
        createdAt
        createdBy {
          ...HistoryUserLinkFields
        }
      }
    }
    ideaHistories(orderBy: UPDATED_AT_ASC) {
      nodes {
        id
        updatedAt
        updatedBy {
          ...HistoryUserLinkFields
        }
        ... on IdeaHistoryChangeTrip {
          newTrip {
            ...TripLinkFields
          }
          oldTrip {
            ...TripLinkFields
          }
        }
        ... on IdeaHistoryChangeDescription {
          newDescription
          oldDescription
        }
        ... on IdeaHistoryMentionIdea {
          refIdea {
            ...IdeaLinkFields
          }
        }
        ... on IdeaHistoryMentionIdeaComment {
          ideaComment {
            ...IdeaCommentLinkFields
          }
        }
      }
    }
  }
`);

interface IdeaHistoryProps {
  idea: FragmentType<typeof IdeaHistoryFields>;
}

function IdeaHistory({ idea: ideaQuery }: IdeaHistoryProps) {
  const idea = useFragment(IdeaHistoryFields, ideaQuery);
  const comments: [Date, ReactNode][] = idea.ideaComments.nodes
    .filter((comment) => comment)
    .map((comment) => [
      parseISO(comment.createdAt),
      <li key={comment.id} id={comment.id} className="ms-10">
        <div className="p-4 bg-white border border-gray-200 rounded-lg shadow-sm dark:bg-gray-700 dark:border-gray-600">
          <div className="items-center justify-between sm:flex">
            <ReactTimeAgo
              className="mb-1 text-sx font-normal text-gray-400 sm:order-last sm:mb-0"
              date={parseISO(comment.createdAt)}
            />
            <div className="text-sm font-normal text-gray-500 dark:text-gray-300">
              <HistoryUserLink user={comment.createdBy} /> commented
            </div>
          </div>
          <RenderMarkdown links={comment.commentLinks}>
            {comment.comment}
          </RenderMarkdown>
        </div>
      </li>,
    ]);
  const actions: [Date, ReactNode][] = idea.ideaHistories.nodes
    .filter((history) => history)
    .map((history) => [
      parseISO(history.updatedAt),
      <li key={history.id} className="ms-10">
        <div className="px-4 items-center justify-between shadow-sm sm:flex">
          <ReactTimeAgo
            className="mb-1 text-sx font-normal text-gray-400 sm:order-last sm:mb-0"
            date={parseISO(history.updatedAt)}
          />
          <div className="text-sm font-normal text-gray-500 dark:text-gray-300">
            <HistoryUserLink user={history.updatedBy} />{" "}
            {history.__typename === "IdeaHistoryClose" && "closed this idea"}
            {history.__typename === "IdeaHistoryReopen" && "reopened this idea"}
            {history.__typename === "IdeaHistoryChangeDescription" &&
              "changed the description"}
            {history.__typename === "IdeaHistoryChangeTrip" &&
              history.oldTrip &&
              history.newTrip && (
                <span>
                  changed trip from <TripLink trip={history.oldTrip} /> to{" "}
                  <TripLink trip={history.newTrip} />
                </span>
              )}
            {history.__typename === "IdeaHistoryMentionIdea" &&
              history.refIdea && (
                <span>
                  mentioned in idea <IdeaLink idea={history.refIdea} />
                </span>
              )}
            {history.__typename === "IdeaHistoryMentionIdeaComment" &&
              history.ideaComment && (
                <span>
                  mentioned in comment on idea{" "}
                  <IdeaCommentLink ideaComment={history.ideaComment} />
                </span>
              )}
          </div>
        </div>
      </li>,
    ]);
  const activity = [...comments, ...actions];
  activity.sort(([a], [b]) => a.getTime() - b.getTime());
  return (
    <ol className="relative border-s border-gray-200 dark:border-gray-700 flex flex-col gap-2">
      {activity.map(([, c]) => c)}
    </ol>
  );
}

function useExpanded(): [boolean, (expanded: boolean) => unknown] {
  return useLocalStorage(
    "asideExpanded",
    false,
    (v): v is boolean => z.boolean().safeParse(v).success,
  );
}

export default function Idea() {
  const [expanded, setExpanded] = useExpanded();
  const navHeight = useContext(NavContext);
  const queryRef = useLoaderData() as Awaited<ReturnType<typeof loader>>;
  const { data } = useReadQuery(queryRef);
  const query = useFragment(
    graphql(`
      fragment GetIdea on Query {
        ...TripChooserFields
        idea: ideaByNumber(number: $id) {
          id
          status
          participants {
            id
            ...UserLinkFields
          }
          ...IdeaMainFields
          ...IdeaTripFields
          ...IdeaHistoryFields
        }
      }
    `),
    data,
  );
  const { idea } = query;
  if (!idea) {
    // TODO: Maybe throw an error that the handler can render?
    return (
      <div className="p-2 max-w-6xl flex flex-col mx-auto gap-5">
        <Alert color="failure">
          <span className="font-medium">404 Not Found:</span> The requested idea
          was not found on this system.
        </Alert>
      </div>
    );
  }
  return (
    <>
      {expanded && (
        <aside
          className="fixed right-0 w-64 z-40"
          style={{ top: navHeight, height: `calc(100% - ${navHeight}px)` }}
        >
          <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
            <List
              unstyled
              className="w-full divide-y divide-gray-200 dark:divide-gray-700"
            >
              <List.Item className="pb-3 sm:pb-4 flex flex-row-reverse">
                <Button color="gray" onClick={() => setExpanded(false)}>
                  <HiOutlineChevronDoubleRight />
                </Button>
              </List.Item>
              <List.Item className="py-3 sm:py-4">
                <Trip idea={idea} query={query} />
              </List.Item>
              <List.Item className="pb-0 pt-3 sm:pt-4">
                <div className="min-w-0 flex-1">
                  <p className="truncate text-sm font-medium text-gray-900 dark:text-white">
                    Participants
                  </p>
                  {idea.participants.map((user) => (
                    <Fragment key={user.id}>
                      <UserLink user={user} />
                      <br />
                    </Fragment>
                  ))}
                </div>
              </List.Item>
            </List>
          </div>
        </aside>
      )}
      <div className="p-2 max-w-6xl flex flex-col mx-auto gap-5">
        <IdeaMain idea={idea} expanded={expanded} setExpanded={setExpanded} />
        <h2 className="text-2xl font-bold dark:text-white">Activity</h2>
        <IdeaHistory idea={idea} />
        <NewComment ideaId={idea.id} status={idea.status} />
      </div>
    </>
  );
}
