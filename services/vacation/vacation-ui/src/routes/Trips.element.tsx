import { useReadQuery } from "@apollo/client";
import { parseISO } from "date-fns";
import { Button, Table } from "flowbite-react";
import { Link, Outlet, useLoaderData } from "react-router-dom";
import { graphql, useFragment } from "../__generated__";
import useRoles from "../useRoles";
import type loader from "./Trips.loader";

const dateFormat = Intl.DateTimeFormat(undefined, {
  year: "numeric",
  month: "long",
  day: "numeric",
});

export default function Trips() {
  const { admin } = useRoles();
  const queryRef = useLoaderData() as Awaited<ReturnType<typeof loader>>;
  const { data } = useReadQuery(queryRef);
  const { trips } = useFragment(
    graphql(`
      fragment GetTrips on Query {
        trips(orderBy: START_DATE_DESC) {
          nodes {
            id
            endDate
            name
            slug
            startDate
          }
        }
      }
    `),
    data,
  );

  return (
    <>
      <div className="p-2 max-w-xl flex flex-col mx-auto gap-5">
        <div className="flex">
          <h1 className="text-2xl font-bold dark:text-white">Trips</h1>
          <div className="flex flex-1 justify-end">
            {admin && (
              <Button as={Link} to="new_trip">
                New Trip
              </Button>
            )}
          </div>
        </div>
        <Table>
          <Table.Head>
            <Table.HeadCell>Name</Table.HeadCell>
            <Table.HeadCell>Dates</Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {trips?.nodes.map(
              (trip) =>
                trip && (
                  <Table.Row
                    key={trip.id}
                    className="bg-white dark:border-gray-700 dark:bg-gray-800"
                  >
                    <Table.Cell>
                      <Link
                        className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                        to={`trips/${trip.slug}`}
                      >
                        {trip.name}
                      </Link>
                    </Table.Cell>
                    <Table.Cell>
                      {trip?.startDate &&
                        trip.endDate &&
                        dateFormat.formatRange(
                          parseISO(trip.startDate),
                          parseISO(trip.endDate),
                        )}
                    </Table.Cell>
                  </Table.Row>
                ),
            )}
          </Table.Body>
        </Table>
      </div>
      <Outlet />
    </>
  );
}
