import { useReadQuery } from "@apollo/client";
import { useActionData, useRouteLoaderData } from "react-router-dom";
import { ZodError } from "zod";
import TripModal from "../components/TripModal";
import { graphql, useFragment } from "../__generated__";
import type loader from "./Trip.loader";

export default function TripEdit() {
  const queryRef = useRouteLoaderData("trip") as Awaited<
    ReturnType<typeof loader>
  >;
  const { data } = useReadQuery(queryRef);
  const { trip } = useFragment(
    graphql(`
      fragment GetTripForEdit on Query {
        trip: tripBySlug(slug: $slug) {
          id
          endDate
          name
          slug
          startDate
        }
      }
    `),
    data,
  );
  const error = useActionData() as ZodError;
  return <TripModal error={error} trip={trip!} />;
}
