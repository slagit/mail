import type { LoaderFunctionArgs } from "react-router-dom";
import { graphql } from "../__generated__";
import { preloadQuery } from "../client";

export default async function loader({ params }: LoaderFunctionArgs) {
  const queryRef = await preloadQuery(
    graphql(`
      query GetTrip($slug: String!) {
        ...GetTrip
        ...GetTripForEdit
      }
    `),
    {
      fetchPolicy: "cache-and-network",
      pollInterval: 5000,
      variables: {
        slug: params.tripSlug!,
      },
    },
  ).toPromise();
  return queryRef;
}
