import type { GraphQLFormattedError } from "graphql";
import type { ActionFunctionArgs } from "react-router-dom";
import { redirect } from "react-router-dom";
import client from "../client";
import handleActionError from "../handleActionError";
import Trip from "../validators/Trip";
import { graphql } from "../__generated__";

function getErrorField({ extensions, path }: GraphQLFormattedError) {
  if (path?.length === 1 && path?.[0] == "trip") {
    const field = extensions?.field;
    if (typeof field === "string" && ["name", "slug"].includes(field)) {
      return [field];
    }
  }
  return [];
}

export default async function action({ request }: ActionFunctionArgs) {
  const formPayload = Object.fromEntries(await request.formData());
  try {
    const trip = Trip.parse(formPayload);
    const { data } = await client.mutate({
      mutation: graphql(`
        mutation CreateTrip($trip: TripInput!) {
          trip: createTrip(input: { trip: $trip }) {
            trip {
              id
              slug
            }
          }
        }
      `),
      variables: {
        trip,
      },
    });
    return redirect(`/trips/${data?.trip?.trip?.slug}`);
  } catch (error) {
    return handleActionError(error, getErrorField);
  }
}
