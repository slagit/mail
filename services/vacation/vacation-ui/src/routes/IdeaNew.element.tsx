import { useReadQuery } from "@apollo/client";
import { useRef } from "react";
import { Button, Label, Modal, Select, Textarea } from "flowbite-react";
import {
  Form,
  useActionData,
  useLoaderData,
  useNavigate,
} from "react-router-dom";
import { ZodError } from "zod";
import { graphql, useFragment } from "../__generated__";
import loader from "./IdeaNew.loader";

export default function IdeaNew() {
  const queryRef = useLoaderData() as Awaited<ReturnType<typeof loader>>;
  const { data } = useReadQuery(queryRef);
  const { currentTrip, trips } = useFragment(
    graphql(`
      fragment TripsForIdea on Query {
        currentTrip {
          id
        }
        trips(orderBy: START_DATE_DESC) {
          nodes {
            id
            name
          }
        }
      }
    `),
    data,
  );

  const error = useActionData() as ZodError;
  const issues = error?.flatten();
  const navigate = useNavigate();
  const handleClose = () => {
    navigate(-1);
  };
  const descriptionRef = useRef(null);
  return (
    <Modal
      show
      onClose={handleClose}
      size="lg"
      initialFocus={descriptionRef}
      dismissible
    >
      <Form method="post">
        <Modal.Header>New Idea</Modal.Header>
        <Modal.Body>
          {(issues?.formErrors ?? []).map((issue) => (
            <div
              className="p-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
              key={issue}
            >
              {issue}
            </div>
          ))}
          <div className="flex flex-col gap-4">
            <div>
              <div className="mb-2">
                <Label
                  color={
                    issues?.fieldErrors.description ? "failure" : "default"
                  }
                  htmlFor="description"
                  value="Description"
                />
              </div>
              <Textarea
                color={issues?.fieldErrors.description ? "failure" : "gray"}
                ref={descriptionRef}
                rows={3}
                id="description"
                name="description"
                helperText={issues?.fieldErrors.description}
              />
            </div>
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.trip ? "failure" : "default"}
                  htmlFor="trip"
                  value="Trip"
                />
              </div>
              <Select
                color={issues?.fieldErrors.trip ? "failure" : "default"}
                defaultValue={currentTrip?.id}
                id="trip"
                name="trip"
                helperText={issues?.fieldErrors.trip}
              >
                {trips?.nodes.map(
                  (trip) =>
                    trip && (
                      <option key={trip.id} value={trip.id}>
                        {trip.name}
                      </option>
                    ),
                )}
              </Select>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit">Add Idea</Button>
          <Button color="gray" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}
