import type { GraphQLFormattedError } from "graphql";
import type { ActionFunctionArgs } from "react-router-dom";
import { redirect } from "react-router-dom";
import client from "../client";
import handleActionError from "../handleActionError";
import Idea from "../validators/Idea";
import { graphql } from "../__generated__";

function getErrorField({ extensions, path }: GraphQLFormattedError) {
  if (path?.length === 1 && path?.[0] == "idea") {
    const field = extensions?.field;
    if (typeof field === "string" && ["description", "trip"].includes(field)) {
      return [field];
    }
  }
  return [];
}

export default async function action({ request }: ActionFunctionArgs) {
  const formPayload = Object.fromEntries(await request.formData());
  try {
    const idea = Idea.parse(formPayload);
    const { data } = await client.mutate({
      mutation: graphql(`
        mutation CreateIdea($idea: IdeaInput!) {
          idea: createIdea(input: { idea: $idea }) {
            idea {
              id
              number
            }
          }
        }
      `),
      variables: {
        idea,
      },
    });
    return redirect(`/ideas/${data?.idea?.idea?.number}`);
  } catch (error) {
    return handleActionError(error, getErrorField);
  }
  return null;
}
