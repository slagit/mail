import type { LoaderFunctionArgs } from "react-router-dom";
import { graphql } from "../__generated__";
import { preloadQuery } from "../client";

export default async function loader({ params }: LoaderFunctionArgs) {
  const queryRef = await preloadQuery(
    graphql(`
      query GetIdea($id: BigInt!) {
        ...GetIdea
      }
    `),
    {
      fetchPolicy: "cache-and-network",
      pollInterval: 5000,
      variables: {
        id: params.ideaId!,
      },
    },
  ).toPromise();
  return queryRef;
}
