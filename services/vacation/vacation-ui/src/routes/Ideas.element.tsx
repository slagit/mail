import { useReadQuery } from "@apollo/client";
import { parseISO } from "date-fns";
import { Badge, Button, Card } from "flowbite-react";
import type { ReactNode } from "react";
import { HiSortAscending, HiSortDescending } from "react-icons/hi";
import { GoComment } from "react-icons/go";
import ReactTimeAgo from "react-time-ago";
import { Link, Outlet, useLoaderData, useSearchParams } from "react-router-dom";
import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";
import { IdeaStatus } from "../__generated__/graphql";
import RenderMarkdown from "../RenderMarkdown";
import HistoryUserLink from "../components/HistoryUserLink";
import loader, { Direction, getCurrentSort, sort } from "./Ideas.loader";

const IdeaCardFields = graphql(`
  fragment IdeaCardFields on Idea {
    id
    number
    status
    description
    descriptionLinks {
      ...MarkdownLinkFields
    }
    ideaComments {
      totalCount
    }
    createdAt
    createdBy {
      ...HistoryUserLinkFields
    }
    updatedAt
  }
`);

interface IdeaCardProps {
  idea: FragmentType<typeof IdeaCardFields>;
}

function IdeaCard({ idea: ideaQuery }: IdeaCardProps) {
  const idea = useFragment(IdeaCardFields, ideaQuery);
  const createdAt = parseISO(idea.createdAt);
  const updatedAt = idea.updatedAt ? parseISO(idea.updatedAt) : null;
  const createdBy = (() => {
    if (idea.createdBy) {
      return <HistoryUserLink user={idea.createdBy} />;
    }
    return null;
  })();
  return (
    <Card>
      <div className="flex gap-2">
        <div className="grow">
          <RenderMarkdown links={idea.descriptionLinks}>
            {idea.description}
          </RenderMarkdown>
          <div className="text-gray-500 dark:text-gray-400 hidden sm:block">
            created <ReactTimeAgo date={createdAt} />
            {createdBy && (
              <>
                {" by "}
                {createdBy}
              </>
            )}
          </div>
        </div>
        <div className="whitespace-nowrap flex flex-col gap-2">
          <div className="text-gray-500 dark:text-gray-400 flex flex-row-reverse">
            {idea.status === IdeaStatus.Closed && <Badge>Closed</Badge>}
            {idea.ideaComments.totalCount > 0 && (
              <div className="flex" title="Comments">
                <GoComment className="me-2" />
                {idea.ideaComments.totalCount}
              </div>
            )}
          </div>
          {updatedAt && (
            <div className="text-gray-500 dark:text-gray-400 hidden sm:block">
              updated <ReactTimeAgo date={updatedAt} />
            </div>
          )}
          <div className="flex flex-row-reverse">
            <Button
              as={Link}
              color="gray"
              size="sm"
              to={`ideas/${idea.number}`}
            >
              Details
            </Button>
          </div>
        </div>
      </div>
    </Card>
  );
}

enum TabStatus {
  Open = "open",
  Closed = "closed",
  All = "all",
}

interface TabProps {
  children: ReactNode;
  setStatus: (status: TabStatus) => unknown;
  status: TabStatus;
}

function Tab({ children, setStatus, status }: TabProps) {
  const [searchParams] = useSearchParams();
  const active = (searchParams.get("status") ?? TabStatus.Open) === status;
  const className = active
    ? "text-blue-600 active dark:text-blue-500 border-b-2 border-blue-600 dark:border-blue-500"
    : "text-gray-400 hover:border-b-2 border-grey-300";
  return (
    <li className="me-2">
      <button
        className={`inline-block px-4 pb-4 rounded-t-lg ${className} flex flex-wrap gap-2`}
        type="button"
        onClick={() => setStatus(status)}
      >
        {children}
      </button>
    </li>
  );
}

export default function Ideas() {
  const queryRef = useLoaderData() as Awaited<ReturnType<typeof loader>>;
  const { data } = useReadQuery(queryRef);
  const query = useFragment(
    graphql(`
      fragment GetIdeas on Query {
        ideas(condition: $condition, orderBy: [$orderBy]) {
          nodes {
            id
            ...IdeaCardFields
          }
        }
        closed: ideas(condition: { status: closed }) {
          totalCount
        }
        open: ideas(condition: { status: open }) {
          totalCount
        }
        all: ideas {
          totalCount
        }
      }
    `),
    data,
  );
  const [searchParams, setSearchParams] = useSearchParams();
  const currentSort = getCurrentSort(searchParams);
  const toggleSort =
    sort.find(
      (s) =>
        s.title === currentSort.title && s.direction !== currentSort.direction,
    ) || currentSort;
  const pickSort = sort.filter((s) => s.direction === currentSort.direction);
  const setSort = (key: string) => {
    searchParams.set("sort", key);
    setSearchParams(searchParams);
  };
  const setStatus = (key: string) => {
    searchParams.set("status", key);
    setSearchParams(searchParams);
  };
  return (
    <>
      <div className="p-2 max-w-6xl flex flex-col mx-auto gap-5">
        <div className="flex flex-wrap gap-2">
          <h1 className="text-2xl font-bold dark:text-white grow md:grow-0">
            Ideas
          </h1>
          <div className="md:order-2">
            <Button as={Link} to="new_idea">
              New Idea
            </Button>
          </div>
          <div className="grow flex">
            <ul className="flex flex-wrap text-sm font-medium text-center text-gray-500 border-b border-gray-200 md:border-none dark:border-gray-700 dark:text-gray-400 grow md:grow-0">
              <Tab setStatus={setStatus} status={TabStatus.Open}>
                Open
                <Badge color="gray">{query.open?.totalCount}</Badge>
              </Tab>
              <Tab setStatus={setStatus} status={TabStatus.Closed}>
                Closed
                <Badge color="gray">{query.closed?.totalCount}</Badge>
              </Tab>
              <Tab setStatus={setStatus} status={TabStatus.All}>
                All
                <Badge color="gray">{query.all?.totalCount}</Badge>
              </Tab>
            </ul>
          </div>
          <div className="w-full md:w-auto">
            <Button.Group className="w-full md:w-auto">
              <select
                className="group grow focus:z-10 focus:outline-none :ring-cyan-700 border border-gray-200 bg-white text-gray-900 focus:text-cyan-700 enabled:hover:bg-gray-100 enabled:hover:text-cyan-700 dark:border-gray-600 dark:bg-transparent dark:text-gray-400 dark:enabled:hover:bg-gray-700 dark:enabled:hover:text-white rounded-lg rounded-r-none focus:ring-2"
                onChange={(e) => setSort(e.target.value)}
                value={currentSort.key}
              >
                {pickSort.map((s) => (
                  <option key={s.key} value={s.key}>
                    {s.title}
                  </option>
                ))}
              </select>
              <Button color="gray" onClick={() => setSort(toggleSort.key)}>
                {currentSort.direction === Direction.Ascending && (
                  <HiSortAscending
                    className="h-4 w-4"
                    title="Sort direction: Ascending"
                    aria-label="Sort direction: Ascending"
                    aria-hidden
                  />
                )}
                {currentSort.direction === Direction.Descending && (
                  <HiSortDescending
                    className="h-4 w-4"
                    title="Sort direction: Descending"
                    aria-label="Sort direction: Descending"
                    aria-hidden
                  />
                )}
              </Button>
            </Button.Group>
          </div>
        </div>
        <div className="flex flex-col gap-5">
          {query.ideas?.nodes.map(
            (idea) => idea && <IdeaCard key={idea.id} idea={idea} />,
          )}
        </div>
      </div>
      <Outlet />
    </>
  );
}
