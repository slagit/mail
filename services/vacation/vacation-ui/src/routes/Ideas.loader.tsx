import type { LoaderFunctionArgs } from "react-router-dom";
import { graphql } from "../__generated__";
import type { IdeaCondition } from "../__generated__/graphql";
import { IdeaOrderBy, IdeaStatus } from "../__generated__/graphql";
import { preloadQuery } from "../client";

export enum Direction {
  Ascending,
  Descending,
}

export const sort = [
  {
    key: "created_date",
    direction: Direction.Descending,
    orderBy: IdeaOrderBy.CreatedAtDesc,
    title: "Created Date",
  },
  {
    key: "created_desc",
    direction: Direction.Ascending,
    orderBy: IdeaOrderBy.CreatedAtAsc,
    title: "Created Date",
  },
  {
    key: "updated_asc",
    direction: Direction.Ascending,
    orderBy: IdeaOrderBy.UpdatedAtAsc,
    title: "Updated Date",
  },
  {
    key: "updated_desc",
    direction: Direction.Descending,
    orderBy: IdeaOrderBy.UpdatedAtDesc,
    title: "Updated Date",
  },
];

export function getCurrentSort(searchParams: URLSearchParams) {
  return sort.find((s) => s.key === searchParams.get("sort")) || sort[0];
}

export default async function loader({ request }: LoaderFunctionArgs) {
  const url = new URL(request.url);
  const currentSort = getCurrentSort(url.searchParams);
  const condition: IdeaCondition = {};
  switch (url.searchParams.get("status") ?? "open") {
    case "open":
      condition.status = IdeaStatus.Open;
      break;
    case "closed":
      condition.status = IdeaStatus.Closed;
      break;
  }
  const queryRef = await preloadQuery(
    graphql(`
      query GetIdeas($condition: IdeaCondition!, $orderBy: IdeaOrderBy!) {
        ...GetIdeas
      }
    `),
    {
      fetchPolicy: "cache-and-network",
      pollInterval: 5000,
      variables: {
        condition,
        orderBy: currentSort.orderBy,
      },
    },
  ).toPromise();
  return queryRef;
}
