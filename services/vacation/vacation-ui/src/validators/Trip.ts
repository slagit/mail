import { parseISO } from "date-fns";
import { z } from "zod";

const Trip = z
  .object({
    name: z.string().min(1, { message: "Trip name is required" }),
    slug: z
      .string()
      .regex(/[A-Za-z0-9_-]+/, { message: "Trip slug is required" }),
    startDate: z.string().date(),
    endDate: z.string().date(),
    address: z.preprocess(
      (arg) => (arg === "" ? null : arg),
      z.string().nullable(),
    ),
    doorCode: z.preprocess(
      (arg) => (arg === "" ? null : arg),
      z.string().nullable(),
    ),
  })
  .refine((trip) => parseISO(trip.endDate) >= parseISO(trip.startDate), {
    message: "End date must be the same as or after start date",
    path: ["endDate"],
  });

export default Trip;
