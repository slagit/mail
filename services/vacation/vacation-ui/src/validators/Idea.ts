import { z } from "zod";

const Idea = z.object({
  description: z.string().min(1, { message: "Idea description is required" }),
  trip: z.string().min(1, { message: "Idea trip is required" }),
});

export default Idea;
