import type { ReactNode } from "react";
import {
  forwardRef,
  useContext,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import { CiDark } from "react-icons/ci";
import { HiLogout } from "react-icons/hi";
import { RxHamburgerMenu } from "react-icons/rx";
import { Avatar, useThemeMode } from "flowbite-react";
import { AuthContext } from "react-oauth2-code-pkce";
import type { To } from "react-router-dom";
import { Link, Outlet, ScrollRestoration } from "react-router-dom";
import NavContext from "./NavContext";
import dishwasherLight from "../dishwasher-light.svg";
import dishwasherDark from "../dishwasher-dark.svg";

interface NavMenuLinkProps {
  children: ReactNode;
  setShow: (show: boolean) => unknown;
  to: To;
}

function NavMenuLink({ children, setShow, to }: NavMenuLinkProps) {
  return (
    <li>
      <Link
        className="block text-left w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white md:hover:bg-transparent md:dark:hover:bg-transparent"
        to={to}
        onClick={() => setShow(false)}
      >
        {children}
      </Link>
    </li>
  );
}

function NavMenu() {
  const [show, setShow] = useState(false);
  return (
    <div className="md:order-1 md:grow">
      <button
        type="button"
        className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
        onClick={() => setShow(!show)}
      >
        <span className="sr-only">Open main menu</span>
        <RxHamburgerMenu className="w-7 h-7" aria-hidden="true" />
      </button>
      <div
        className={`${show ? "" : "hidden"} w-full border border-gray-100 dark:border-gray-600 flex-col absolute left-0 z-50 me-4 text-base list-none bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-800 dark:divide-gray-600 md:flex md:flex-row md:visible md:z-auto md:me-0 md:divide-y-0 md:rounded-none md:shadow-none md:dark:bg-gray-800 md:w-auto md:relative md:border-none`}
      >
        <ul className="py-2 md:py-0 md:flex md:flex-row">
          <NavMenuLink to="/trips" setShow={setShow}>
            Trips
          </NavMenuLink>
          <NavMenuLink to="/ideas" setShow={setShow}>
            Ideas
          </NavMenuLink>
        </ul>
      </div>
    </div>
  );
}

interface NavUserButtonProps {
  children: ReactNode;
  onClick: () => unknown;
  setShow: (show: boolean) => unknown;
}

function NavUserButton({ children, onClick, setShow }: NavUserButtonProps) {
  const handleClick = () => {
    setShow(false);
    onClick();
  };
  return (
    <li>
      <button
        type="button"
        className="text-left w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white whitespace-nowrap"
        onClick={handleClick}
      >
        {children}
      </button>
    </li>
  );
}

function NavUser() {
  const { logOut, tokenData } = useContext(AuthContext);
  const { toggleMode } = useThemeMode();
  const [show, setShow] = useState(false);
  return (
    <div className="md:order-2 md:relative">
      <button
        type="button"
        className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
        onClick={() => setShow(!show)}
      >
        <Avatar alt="User Settings" rounded className="w-7 h-7" />
      </button>
      <div
        className={`${show ? "" : "hidden"} w-full border border-gray-100 dark:border-gray-600 flex-col absolute left-0 z-50 me-4 text-base list-none bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-800 dark:divide-gray-600 md:w-auto md:left-auto md:right-0`}
      >
        <div className="px-4 py-3">
          <span className="block text-sm text-gray-900 dark:text-white">
            @{tokenData?.preferred_username}
          </span>
          <span className="block text-sm text-gray-900 dark:text-white">
            {`${tokenData?.given_name} ${tokenData?.family_name}`}
          </span>
          <Link
            className="font-medium text-cyan-600 hover:underline dark:text-cyan-500 whitespace-nowrap"
            to="https://auth.slagit.net/realms/slagit/account/"
          >
            Manage your account
          </Link>
        </div>
        <ul className="py-2">
          <NavUserButton onClick={toggleMode} setShow={setShow}>
            <CiDark className="inline mr-2 h-4 w-4" />
            Toggle dark mode
          </NavUserButton>
        </ul>
        <ul className="py-2">
          <NavUserButton onClick={logOut} setShow={setShow}>
            <HiLogout className="inline mr-2 h-4 w-4" />
            Sign out
          </NavUserButton>
        </ul>
      </div>
    </div>
  );
}

const Nav = forwardRef<HTMLElement>(function Nav(_, ref) {
  const { computedMode } = useThemeMode();
  return (
    <nav
      id="main-nav"
      className="fixed bg-white dark:bg-gray-800 w-full z-20 top-0 start-0 border-b border-gray-200 dark:border-gray-600"
      ref={ref}
    >
      <div className="max-w-screen-xl flex flex-wrap items-center mx-auto p-2 md:p-4 gap-2">
        <Link
          to="/"
          className="flex items-center space-x-3 rtl:space-x-reverse grow md:grow-0"
        >
          <img
            className="h-6"
            src={computedMode === "light" ? dishwasherLight : dishwasherDark}
            alt="Vacation Manager Logo"
          />
          <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
            Vacation Manager
          </span>
        </Link>
        <NavUser />
        <NavMenu />
      </div>
    </nav>
  );
});

export default function App() {
  const [navHeight, setNavHeight] = useState(0);
  const ref = useRef<HTMLElement>(null);
  useLayoutEffect(() => {
    const handleResize = () => {
      if (ref.current) {
        const { height } = ref.current.getBoundingClientRect();
        setNavHeight(height);
      }
    };
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [ref, setNavHeight]);

  return (
    <>
      <Nav ref={ref} />
      <div style={{ marginTop: navHeight, paddingTop: "1rem" }}>
        <NavContext.Provider value={navHeight}>
          <Outlet />
        </NavContext.Provider>
      </div>
      <ScrollRestoration />
    </>
  );
}
