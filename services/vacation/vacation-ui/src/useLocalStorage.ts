import type { Dispatch, SetStateAction } from "react";
import { useEffect, useState } from "react";

export default function useLocalStorage<S>(
  key: string,
  defaultValue: S,
  guard: (v: unknown) => v is S,
): [S, Dispatch<SetStateAction<S>>] {
  const [value, setValue] = useState<S>(defaultValue);
  useEffect(() => {
    const updateFromStorage = () => {
      const rawValue = localStorage.getItem(key);
      if (rawValue !== null) {
        const newValue = JSON.parse(rawValue);
        if (guard(newValue)) {
          setValue(JSON.parse(rawValue));
        }
      }
    };
    updateFromStorage();
    window.addEventListener("storage", updateFromStorage);
    return () => {
      window.removeEventListener("storage", updateFromStorage);
    };
  }, [guard, key, setValue]);
  return [
    value,
    (newValue: S | ((prevState: S) => S)) => {
      localStorage.setItem(key, JSON.stringify(newValue));
      setValue(newValue);
    },
  ];
}
