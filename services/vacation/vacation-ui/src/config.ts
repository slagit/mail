const config = import.meta.env.DEV
  ? {
      clientId: "vacation-web-dev",
      audience: "vacation-dev",
      graphqlEndpoint: "http://localhost:5678/graphql",
      redirectUri: "http://localhost:5173/",
    }
  : {
      clientId: "vacation-web",
      audience: "vacation",
      graphqlEndpoint: "https://vacation.slagit.net/graphql",
      redirectUri: "https://vacation.slagit.net/",
    };

export default config;
