import {
  ApolloClient,
  createHttpLink,
  createQueryPreloader,
  InMemoryCache,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import config from "./config";

const httpLink = createHttpLink({
  uri: config.graphqlEndpoint,
});

const authLink = setContext((_, { headers }) => {
  const rawToken = localStorage.getItem("ROCP_token");
  const token = rawToken ? JSON.parse(rawToken) : null;
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache({
    possibleTypes: {
      HistoryUser: ["DeletedUser", "User"],
      MarkdownLink: ["Idea", "Trip", "User"],
    },
  }),
  dataMasking: true,
});

export const preloadQuery = createQueryPreloader(client);

export default client;
