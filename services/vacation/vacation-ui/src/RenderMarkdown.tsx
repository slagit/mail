import type { Nodes } from "mdast";
import type { Find, Replace } from "mdast-util-find-and-replace";
import { findAndReplace } from "mdast-util-find-and-replace";
import type { Components } from "react-markdown";
import Markdown from "react-markdown";
import type { FragmentType } from "./__generated__";
import { graphql, useFragment } from "./__generated__";
import IdeaLink from "./components/IdeaLink";
import TripLink from "./components/TripLink";
import UserLink from "./components/UserLink";

const MarkdownLinkFields = graphql(`
  fragment MarkdownLinkFields on MarkdownLink {
    __typename
    ... on Idea {
      number
      ...IdeaLinkFields
    }
    ... on Trip {
      slug
      ...TripLinkFields
    }
    ... on User {
      username
      ...UserLinkFields
    }
  }
`);

const mentionPlugin = (links: FragmentType<typeof MarkdownLinkFields>[]) => {
  const mentions = links
    .map((linkQuery): [Find, Replace] => {
      const link = useFragment(MarkdownLinkFields, linkQuery);
      switch (link.__typename) {
        case "Idea":
          return [
            new RegExp(`#${link.number}([^0-9]|$)`, "g"),
            () => ({
              type: "text",
              value: `#${link.number}`,
              data: {
                hName: "idea-ref",
                hProperties: {
                  // This is a hack to pass the object through the parsing infrastructure. It seems to work.
                  idea: link as unknown as string,
                },
              },
            }),
          ];
        case "Trip":
          return [
            new RegExp(`trips/${link.slug}([^[0-9a-zA-Z_-]|$)`, "g"),
            () => ({
              type: "text",
              value: `trips/${link.slug}`,
              data: {
                hName: "trip-ref",
                hProperties: {
                  // This is a hack to pass the object through the parsing infrastructure. It seems to work.
                  trip: link as unknown as string,
                },
              },
            }),
          ];
        case "User":
          return [
            new RegExp(`@${link.username}([^0-9a-zA-Z]|$)`, "g"),
            () => ({
              type: "text",
              value: `@${link.username}`,
              data: {
                hName: "user-ref",
                hProperties: {
                  // This is a hack to pass the object through the parsing infrastructure. It seems to work.
                  user: link as unknown as string,
                },
              },
            }),
          ];
      }
    })
    .filter((x) => x !== undefined);
  return () => (tree: Nodes) => {
    if (mentions.length > 0) {
      findAndReplace(tree, mentions);
    }
  };
};

const components: Components & {
  "idea-ref": typeof IdeaLink;
  "trip-ref": typeof TripLink;
  "user-ref": typeof UserLink;
} = {
  "idea-ref": IdeaLink,
  "trip-ref": TripLink,
  "user-ref": UserLink,
};

interface RenderMarkdownProps {
  children: string;
  links: FragmentType<typeof MarkdownLinkFields>[];
}

export default function RenderMarkdown({
  children,
  links,
}: RenderMarkdownProps) {
  return (
    <Markdown
      className="max-w-none prose dark:prose-invert prose-li:my-0 prose-ul:my-0"
      components={components}
      remarkPlugins={[mentionPlugin(links)]}
    >
      {children}
    </Markdown>
  );
}
