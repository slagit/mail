import { ApolloError } from "@apollo/client";
import type { GraphQLFormattedError } from "graphql";
import { z, ZodError } from "zod";

export default function handleActionError(
  error: unknown,
  getErrorField?: (e: GraphQLFormattedError) => string[],
) {
  if (error instanceof ZodError) {
    return error;
  }
  if (error instanceof ApolloError) {
    const errors = error.graphQLErrors.map((err) => ({
      code: z.ZodIssueCode.custom,
      message: err.message,
      path: getErrorField?.(err) ?? [],
    }));
    if (errors) {
      return new ZodError(errors);
    }
    return new ZodError([
      {
        code: z.ZodIssueCode.custom,
        path: [],
        message: error.message,
      },
    ]);
  }
  console.error(error);
  return new ZodError([
    {
      code: z.ZodIssueCode.custom,
      path: [],
      message: "An unknown error occurred",
    },
  ]);
}
