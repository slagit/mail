import { ApolloProvider } from "@apollo/client";
import { useContext, useMemo } from "react";
import { Flowbite } from "flowbite-react";
import { lazy } from "react";
import { AuthContext } from "react-oauth2-code-pkce";
import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from "react-router-dom";
import App from "./App";
import ideasLoader from "./routes/Ideas.loader";
import ideaAction from "./routes/Idea.action";
import ideaLoader from "./routes/Idea.loader";
import ideaNewAction from "./routes/IdeaNew.action";
import ideaNewLoader from "./routes/IdeaNew.loader";
import tripsLoader from "./routes/Trips.loader";
import tripLoader from "./routes/Trip.loader";
import tripNewAction from "./routes/TripNew.action";
import tripEditAction from "./routes/TripEdit.action";
import DemoError from "./DemoError";
import client from "./client";
import "./index.css";

const Idea = lazy(() => import("./routes/Idea.element"));
const IdeaNew = lazy(() => import("./routes/IdeaNew.element"));
const Ideas = lazy(() => import("./routes/Ideas.element"));
const TripEdit = lazy(() => import("./routes/TripEdit.element"));
const Trip = lazy(() => import("./routes/Trip.element"));
const TripNew = lazy(() => import("./routes/TripNew.element"));
const Trips = lazy(() => import("./routes/Trips.element"));

export default function Router() {
  const { loginInProgress } = useContext(AuthContext);
  const router = useMemo(
    () =>
      loginInProgress
        ? null
        : createBrowserRouter([
            {
              path: "/",
              element: <Navigate to="/trips" replace={true} />,
            },
            {
              element: <App />,
              children: [
                {
                  element: <Ideas />,
                  errorElement: <DemoError />,
                  loader: ideasLoader,
                  children: [
                    {
                      path: "ideas",
                      element: null,
                    },
                    {
                      path: "new_idea",
                      element: <IdeaNew />,
                      loader: ideaNewLoader,
                      action: ideaNewAction,
                    },
                  ],
                },
                {
                  path: "ideas/:ideaId",
                  element: <Idea />,
                  errorElement: <DemoError />,
                  loader: ideaLoader,
                  action: ideaAction,
                },
                {
                  element: <Trips />,
                  errorElement: <DemoError />,
                  loader: tripsLoader,
                  children: [
                    {
                      path: "trips",
                      element: null,
                    },
                    {
                      path: "new_trip",
                      element: <TripNew />,
                      action: tripNewAction,
                    },
                  ],
                },
                {
                  path: "trips/:tripSlug",
                  element: <Trip />,
                  errorElement: <DemoError />,
                  id: "trip",
                  loader: tripLoader,
                  children: [
                    {
                      path: "edit",
                      element: <TripEdit />,
                      action: tripEditAction,
                    },
                  ],
                },
              ],
            },
          ]),
    [loginInProgress],
  );
  if (router === null) {
    return null;
  }
  return (
    <ApolloProvider client={client}>
      <Flowbite>
        <RouterProvider router={router} />
      </Flowbite>
    </ApolloProvider>
  );
}
