import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";
import UserLink from "./UserLink";

const HistoryUserLinkFields = graphql(`
  fragment HistoryUserLinkFields on HistoryUser {
    __typename
    ... on User {
      ...UserLinkFields
    }
  }
`);

interface HistoryUserLinkProps {
  user: FragmentType<typeof HistoryUserLinkFields>;
}

export default function HistoryUserLink({
  user: userQuery,
}: HistoryUserLinkProps) {
  const user = useFragment(HistoryUserLinkFields, userQuery);
  if (user.__typename === "DeletedUser") {
    return <span className="italic">deleted user</span>;
  }
  if (user.__typename === "User") {
    return <UserLink user={user} />;
  }
  return null;
}
