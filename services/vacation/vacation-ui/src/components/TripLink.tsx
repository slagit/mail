import { parseISO } from "date-fns";
import { Popover } from "flowbite-react";
import { Link } from "react-router-dom";
import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";

const dateFormat = Intl.DateTimeFormat(undefined, {
  year: "numeric",
  month: "long",
  day: "numeric",
});

const TripLinkFields = graphql(`
  fragment TripLinkFields on Trip {
    id
    slug
    name
    startDate
    endDate
  }
`);

interface TripLinkProps {
  trip: FragmentType<typeof TripLinkFields>;
}

export default function TripLink({ trip: tripQuery }: TripLinkProps) {
  const { slug, name, startDate, endDate } = useFragment(
    TripLinkFields,
    tripQuery,
  );
  const content = (
    <div className="not-prose max-w-lg px-3 py-2">
      <div>{name}</div>
      <div>
        {dateFormat.formatRange(parseISO(startDate), parseISO(endDate))}
      </div>
    </div>
  );
  return (
    <Popover content={content} trigger="hover">
      <Link
        className="not-prose text-blue-600 dark:text-blue-500 hover:underline"
        to={`/trips/${slug}`}
      >
        {name}
      </Link>
    </Popover>
  );
}
