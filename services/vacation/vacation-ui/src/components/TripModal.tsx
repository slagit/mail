import { useRef } from "react";
import { Button, Label, Modal, Textarea, TextInput } from "flowbite-react";
import { Form, useNavigate } from "react-router-dom";
import type { ZodError } from "zod";

interface TripModalProps {
  error?: ZodError;
  trip?: {
    name: string;
    slug: string;
    address?: string | null;
    doorCode?: string | null;
    startDate: string;
    endDate: string;
  };
}

export default function TripModal({ error, trip }: TripModalProps) {
  const issues = error?.flatten();
  const navigate = useNavigate();
  const handleClose = () => {
    navigate(-1);
  };
  const nameRef = useRef(null);
  return (
    <Modal
      show
      onClose={handleClose}
      size="sm"
      initialFocus={nameRef}
      dismissible
    >
      <Form method="post">
        <Modal.Header>{`${trip ? "Edit" : "New"} Trip`}</Modal.Header>
        <Modal.Body>
          {(issues?.formErrors ?? []).map((issue) => (
            <div
              className="p-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
              key={issue}
            >
              {issue}
            </div>
          ))}
          <div className="flex flex-col gap-4">
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.name ? "failure" : "default"}
                  htmlFor="name"
                  value="Trip Name"
                />
              </div>
              <TextInput
                color={issues?.fieldErrors.name ? "failure" : "gray"}
                id="name"
                defaultValue={trip?.name}
                name="name"
                helperText={issues?.fieldErrors.name}
                ref={nameRef}
              />
            </div>
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.slug ? "failure" : "default"}
                  htmlFor="slug"
                  value="Slug"
                />
              </div>
              <TextInput
                color={issues?.fieldErrors.slug ? "failure" : "gray"}
                id="slug"
                defaultValue={trip?.slug}
                name="slug"
                helperText={issues?.fieldErrors.slug}
              />
            </div>
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.startDate ? "failure" : "default"}
                  htmlFor="startDate"
                  value="Start Date"
                />
              </div>
              <TextInput
                color={issues?.fieldErrors.startDate ? "failure" : "gray"}
                id="startDate"
                defaultValue={trip?.startDate}
                name="startDate"
                helperText={issues?.fieldErrors.startDate}
                type="date"
              />
            </div>
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.endDate ? "failure" : "default"}
                  htmlFor="endDate"
                  value="End Date"
                />
              </div>
              <TextInput
                color={issues?.fieldErrors.endDate ? "failure" : "gray"}
                id="endDate"
                defaultValue={trip?.endDate}
                name="endDate"
                helperText={issues?.fieldErrors.endDate}
                type="date"
              />
            </div>
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.doorCode ? "failure" : "default"}
                  htmlFor="doorCode"
                  value="Door Code"
                />
              </div>
              <TextInput
                color={issues?.fieldErrors.doorCode ? "failure" : "gray"}
                id="doorCode"
                defaultValue={trip?.doorCode ?? ""}
                name="doorCode"
                helperText={issues?.fieldErrors.doorCode}
              />
            </div>
            <div>
              <div className="mb-2">
                <Label
                  color={issues?.fieldErrors.address ? "failure" : "default"}
                  htmlFor="address"
                  value="Address"
                />
              </div>
              <Textarea
                color={issues?.fieldErrors.address ? "failure" : "gray"}
                rows={3}
                id="address"
                defaultValue={trip?.address ?? ""}
                name="address"
                helperText={issues?.fieldErrors.address}
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit">{trip ? "Save " : "Add "}Trip</Button>
          <Button color="gray" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}
