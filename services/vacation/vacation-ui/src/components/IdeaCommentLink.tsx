import { Popover } from "flowbite-react";
import Markdown from "react-markdown";
import { Link } from "react-router-dom";
import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";

const IdeaCommentLinkFields = graphql(`
  fragment IdeaCommentLinkFields on IdeaComment {
    id
    comment
    idea {
      id
      number
    }
  }
`);

interface IdeaCommentLinkProps {
  ideaComment: FragmentType<typeof IdeaCommentLinkFields>;
}

export default function IdeaCommentLink({
  ideaComment: ideaCommentQuery,
}: IdeaCommentLinkProps) {
  const { id, comment, idea } = useFragment(
    IdeaCommentLinkFields,
    ideaCommentQuery,
  );
  const content = (
    <div className="not-prose max-w-lg px-3 py-2">
      <Markdown className="prose dark:prose-invert prose-li:my-0 prose-ul:my-0">
        {comment}
      </Markdown>
    </div>
  );
  if (!idea) {
    return (
      <Popover content={content} trigger="hover">
        <span className="not-prose italic">unknown idea</span>
      </Popover>
    );
  }
  return (
    <Popover content={content} trigger="hover">
      <Link
        className="not-prose text-blue-600 dark:text-blue-500 hover:underline"
        to={`/ideas/${idea.number}#${id}`}
      >
        #{idea.number}
      </Link>
    </Popover>
  );
}
