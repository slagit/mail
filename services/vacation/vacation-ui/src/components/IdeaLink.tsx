import { Popover } from "flowbite-react";
import Markdown from "react-markdown";
import { Link } from "react-router-dom";
import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";
import { IdeaStatus } from "../__generated__/graphql";

const IdeaLinkFields = graphql(`
  fragment IdeaLinkFields on Idea {
    id
    number
    description
    status
  }
`);

interface IdeaLinkProps {
  idea: FragmentType<typeof IdeaLinkFields>;
}

export default function IdeaLink({ idea: ideaQuery }: IdeaLinkProps) {
  const { status, number, description } = useFragment(
    IdeaLinkFields,
    ideaQuery,
  );
  const content = (
    <div className="not-prose max-w-lg px-3 py-2">
      <Markdown className="prose dark:prose-invert prose-li:my-0 prose-ul:my-0">
        {description}
      </Markdown>
    </div>
  );
  return (
    <Popover content={content} trigger="hover">
      <Link
        className="not-prose text-blue-600 dark:text-blue-500 hover:underline"
        to={`/ideas/${number}`}
      >
        #{number}
        {status === IdeaStatus.Closed && " (closed)"}
      </Link>
    </Popover>
  );
}
