import { Popover } from "flowbite-react";
import type { FragmentType } from "../__generated__";
import { graphql, useFragment } from "../__generated__";

const UserLinkFields = graphql(`
  fragment UserLinkFields on User {
    id
    firstName
    lastName
    username
  }
`);

interface UserLinkProps {
  user: FragmentType<typeof UserLinkFields>;
}

export default function UserLink({ user: userQuery }: UserLinkProps) {
  const { firstName, lastName, username } = useFragment(
    UserLinkFields,
    userQuery,
  );
  const content = (
    <div className="not-prose max-w-lg px-3 py-2">
      <div>
        {firstName} {lastName}
      </div>
      <div>@{username}</div>
    </div>
  );
  return (
    <Popover content={content} trigger="hover">
      <span className="not-prose underline decoration-dotted">@{username}</span>
    </Popover>
  );
}
