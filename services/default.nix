{self, ...}: {
  flake.nixosModules = {
    homepage = ./homepage/nixos.nix;
    keycloak = ./keycloak/nixos.nix;
    mail = ./mail/nixos.nix;
    monitoring = ./monitoring/nixos.nix;
    nebula = ./nebula/nixos.nix;
    nix-cache = ./nix-cache/nixos.nix;
    paperless = ./paperless/nixos.nix;
    renovate = ./renovate/nixos.nix;
    systemd-vaultd = {
      pkgs,
      lib,
      ...
    }: {
      imports = [
        ./systemd-vaultd/nixos.nix
      ];
      services.systemd-vaultd.package = lib.mkDefault self.packages.${pkgs.system}.systemd-vaultd;
    };
  };
  imports = [
    ./vacation
  ];
  ci = {
    alejandra.default.ignore = [
      "services/renovate/generated.nix"
      "services/renovate/node-env.nix"
      "services/renovate/node-packages.nix"
      "services/systemd-vaultd/Cargo.nix"
    ];
    rust.default.systemd-vaultd.path = "services/systemd-vaultd";
  };
  perSystem = {pkgs, ...}: {
    packages = {
      inherit
        (import ./renovate/generated.nix {
          inherit pkgs;
          inherit (pkgs) nodejs system;
        })
        renovate
        ;
      systemd-vaultd = let
        rustPkgs = pkgs.rustBuilder.makePackageSet {
          extraRustComponents = ["clippy" "rustfmt"];
          packageFun = import ./systemd-vaultd/Cargo.nix;
          rustVersion = "latest";
        };
      in
        (rustPkgs.workspace.systemd-vaultd {}).bin;
    };
  };
}
