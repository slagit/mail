{
  config,
  lib,
  ...
}: {
  services.nebula.networks.slagit = {
    enable = true;
    cert = "/etc/nebula/node.crt";
    key = "/etc/nebula/node.key";
    ca = ./ca.crt;
    staticHostMap = {
      "10.10.10.1" = [
        "69.164.212.128:4242"
      ];
    };
    lighthouses = [
      "10.10.10.1"
    ];
    listen.port = 4242;
    settings.tun.unsafe_routes = [
      {
        route = "10.10.1.0/24";
        via = "10.10.10.5";
      }
      {
        route = "10.10.3.0/24";
        via = "10.10.10.5";
      }
      {
        route = "10.10.6.0/24";
        via = "10.10.10.5";
      }
    ];
    firewall = {
      inbound = lib.mkMerge [
        (lib.mkIf config.services.prometheus.exporters.node.enable [
          {
            port = "9100";
            proto = "tcp";
            group = "prometheus";
          }
        ])
        (lib.mkIf config.services.openssh.enable [
          {
            port = "22";
            proto = "tcp";
            group = "admins";
          }
        ])
      ];
      outbound = [
        {
          port = "any";
          proto = "any";
          host = "any";
        }
      ];
    };
  };
  virtualisation.vmVariant.services.nebula.networks.slagit = {
    ca = lib.mkForce ../../devenv/nebula/ca.crt;
    staticHostMap."10.10.10.1" = lib.mkForce ["10.30.1.11:4242"];
  };
}
