_: {
  resource.linode_domain_record = {
    paperless_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "paperless";
      record_type = "CNAME";
      target = "compute0.slagit.net";
    };
    _acme_challenge_paperless_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "_acme-challenge.paperless";
      record_type = "CNAME";
      target = "paperless.slagit.net.slagit-cert.net";
    };
  };
}
