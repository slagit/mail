{
  config,
  lib,
  pkgs,
  ...
}: {
  networking.firewall.interfaces."nebula.slagit".allowedTCPPorts = [80 443];
  services = {
    borgmatic.settings.source_directories = [
      "/var/lib/paperless"
    ];
    nebula.networks.slagit.firewall.inbound = [
      {
        port = "80";
        proto = "tcp";
        group = "users";
      }
      {
        port = "443";
        proto = "tcp";
        group = "users";
      }
    ];
    nginx = {
      enable = true;
      virtualHosts."paperless.slagit.net" = {
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:28981";
        };
        sslCertificate = "/run/vault-agent@paperless-nginx/paperless.slagit.net.pem";
        sslCertificateKey = "/run/vault-agent@paperless-nginx/paperless.slagit.net-key.pem";
      };
    };
    paperless = {
      enable = true;
      settings = {
        PAPERLESS_FILENAME_FORMAT = "{correspondent}/{created_year}/{created_month}-{created_day}-{title}";
        PAPERLESS_OCR_USER_ARGS = builtins.toJSON {
          continue_on_soft_render_error = true;
          invalidate_digital_signatures = true;
        };
        PAPERLESS_THUMBNAIL_FONT_NAME = "${pkgs.liberation_ttf}/share/fonts/truetype/LiberationSerif-Regular.ttf";
        PAPERLESS_URL = "https://paperless.slagit.net";
      };
    };
  };
  slagit.vault-agent = {
    paperless-nginx = {
      template = [
        {
          contents = "{{- with secret \"secret/acme/paperless-nginx/paperless.slagit.net\" }}{{ index .Data.data \"fullchain.pem\" }}{{- end }}";
          destination = "/run/vault-agent@paperless-nginx/paperless.slagit.net.pem";
          error_on_missing_key = true;
          perms = "0644";
        }
        {
          contents = "{{- with secret \"secret/acme/paperless-nginx/paperless.slagit.net\" }}{{ index .Data.data \"private_key.pem\" }}{{- end }}";
          destination = "/run/vault-agent@paperless-nginx/paperless.slagit.net-key.pem";
          error_on_missing_key = true;
          perms = "0600";
        }
      ];
      role = "paperless-nginx";
      user = config.systemd.services.nginx.serviceConfig.User;
    };
    paperless-sync-drive = {
      template = [
        {
          contents = "{{- with secret \"secret/apps/paperless-sync-drive/rclone_conf\" }}{{ .Data.data.credential }}{{- end }}";
          destination = "/run/vault-agent@paperless-sync-drive/rclone_conf";
          error_on_missing_key = true;
          perms = "0600";
        }
      ];
      role = "paperless-sync-drive";
      user = "root";
    };
  };
  systemd = {
    services = {
      nginx = {
        after = ["vault-agent@paperless-nginx.service"];
        bindsTo = ["vault-agent@paperless-nginx.service"];
      };
      paperless-sync-drive = {
        after = ["network-online.target" "vault-agent@paperless-sync-drive.service"];
        bindsTo = ["vault-agent@paperless-sync-drive.service"];
        description = "Paperless sync to google drive";
        serviceConfig = {
          CapabilityBoundingSet = "CAP_DAC_READ_SEARCH CAP_NET_RAW";
          CPUSchedulingPolicy = "batch";
          ExecStart = "${pkgs.systemd}/bin/systemd-inhibit --who=\"rclone\" --what=\"sleep:shutdown\" --why=\"Prevent interrupting scheduled sync\" ${pkgs.rclone}/bin/rclone --config /run/vault-agent@paperless-sync-drive/rclone_conf sync ${config.services.paperless.mediaDir}/documents/archive drive:Paperless";
          ExecStartPre = "${pkgs.coreutils}/bin/sleep 1m";
          IOSchedulingClass = "best-effort";
          IOSchedulingPriority = 7;
          IOWeight = 100;
          LockPersonality = true;
          LogRateLimitIntervalSec = 0;
          Nice = 19;
          NoNewPrivileges = true;
          PrivateDevices = true;
          PrivateTmp = true;
          ProtectClock = true;
          ProtectControlGroups = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          ProtectKernelTunables = true;
          ProtectSystem = "full";
          Restart = "no";
          RestrictAddressFamilies = "AF_UNIX AF_INET AF_INET6 AF_NETLINK";
          RestrictNamespaces = true;
          RestrictRealtime = true;
          RestrictSUIDSGID = true;
          SystemCallArchitectures = "native";
          SystemCallErrorNumber = "EPERM";
          SystemCallFilter = "@system-service";
          Type = "oneshot";
        };
        unitConfig = {
          ConditionACPower = true;
        };
        wants = ["network-online.target"];
      };
    };
    timers.paperless-sync-drive = {
      description = "Run paperless sync to google drive";
      timerConfig = {
        OnCalendar = "hourly";
        Persistent = true;
      };
      wantedBy = ["timers.target"];
    };
  };
}
