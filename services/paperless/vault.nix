_: {
  resource = {
    vault_approle_auth_backend_role = {
      paperless-nginx = {
        backend = "\${vault_auth_backend.approle.path}";
        role_name = "paperless-nginx";
        secret_id_num_uses = 1;
        token_period = 3600;
      };
      paperless-sync-drive = {
        backend = "\${vault_auth_backend.approle.path}";
        role_name = "paperless-sync-drive";
        secret_id_num_uses = 1;
        token_period = 3600;
      };
    };
  };
  slagit.vault = {
    identities = {
      app-paperless-nginx = {
        aliases = [
          {
            backend = "vault_auth_backend.approle";
            name = "\${vault_approle_auth_backend_role.paperless-nginx.role_id}";
          }
        ];
        policies = [
          "paperless-nginx"
        ];
      };
      app-paperless-sync-drive = {
        aliases = [
          {
            backend = "vault_auth_backend.approle";
            name = "\${vault_approle_auth_backend_role.paperless-sync-drive.role_id}";
          }
        ];
        policies = [
          "paperless-sync-drive"
        ];
      };
      host-compute0-slagit-net.policies = ["paperless-nginx-host" "paperless-sync-drive-host"];
    };
    policies = {
      paperless-nginx-host.paths = {
        "auth/approle/role/paperless-nginx/role-id".capabilities = ["read"];
        "auth/approle/role/paperless-nginx/secret-id" = {
          capabilities = ["create" "update"];
          min_wrapping_ttl = "1s";
          max_wrapping_ttl = "10m";
        };
      };
      paperless-nginx.paths = {
        "secret/data/acme/paperless-nginx/paperless.slagit.net".capabilities = ["read"];
      };
      paperless-sync-drive-host.paths = {
        "auth/approle/role/paperless-sync-drive/role-id".capabilities = ["read"];
        "auth/approle/role/paperless-sync-drive/secret-id" = {
          capabilities = ["create" "update"];
          min_wrapping_ttl = "1s";
          max_wrapping_ttl = "10m";
        };
      };
      paperless-sync-drive.paths = {
        "secret/data/apps/paperless-sync-drive/rclone_conf".capabilities = ["read"];
      };
    };
  };
}
