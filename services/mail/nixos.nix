{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.slagit.mail;
  dovecot-sql = pkgs.writeText "dovecot-sql.conf.ext" ''
    driver = pgsql
    connect = dbname=mail
    default_pass_scheme = ARGON2ID
    password_query = SELECT username, password FROM dovecot.virtual_users WHERE username='%u';
  '';
  opendkim-conf = pkgs.writeText "opendkim.conf" ''
    Canonicalization relaxed/relaxed
    KeyTable dsn:pgsql://opendkim:password@localhost/mail/table=opendkim.key?keycol=id?datacol=domain,selector,key
    LogWhy yes
    MinimumKeyBits 1024
    Mode sv
    SigningTable dsn:pgsql://opendkim:password@localhost/mail/table=opendkim.signing?keycol=id?datacol=data
    Socket local:/run/opendkim/opendkim.sock
    Syslog yes
    UMask 002
    UserID opendkim:opendkim
  '';
  pgsql-sender-access = pkgs.writeText "pgsql-sender-access.cf" ''
    dbname = mail
    query = SELECT 'REJECT' FROM postfix.virtual_domains WHERE name='%s'
    hosts = unix:/run/postgresql
  '';
  pgsql-virtual-alias-maps = pkgs.writeText "pgsql-virtual-alias-maps.cf" ''
    dbname = mail
    query = SELECT destination FROM postfix.virtual_aliases WHERE source='%s'
    hosts = unix:/run/postgresql
  '';
  pgsql-virtual-mailbox-domains = pkgs.writeText "pgsql-virtual-mailbox-domains.cf" ''
    dbname = mail
    query = SELECT 1 FROM postfix.virtual_domains WHERE name='%s'
    hosts = unix:/run/postgresql
  '';
  pgsql-virtual-mailbox-maps = pkgs.writeText "pgsql-virtual-mailbox-maps.cf" ''
    dbname = mail
    query = SELECT address FROM postfix.virtual_users WHERE address='%s'
    hosts = unix:/run/postgresql
  '';
  submissionOptions = {
    milter_macro_daemon_name = "ORIGINATING";
    smtpd_client_restrictions = "permit_sasl_authenticated,reject";
    smtpd_recipient_restrictions = "permit_sasl_authenticated,reject_unauth_destination";
    smtpd_relay_restrictions = "permit_sasl_authenticated,reject_unauth_destination";
    smtpd_sasl_auth_enable = "yes";
    smtpd_sasl_path = "/run/dovecot2/auth-client";
    smtpd_sasl_type = "dovecot";
    smtpd_sender_login_maps = "pgsql:${pgsql-virtual-mailbox-maps}";
    smtpd_sender_restrictions = "reject_sender_login_mismatch";
  };
in {
  security.acme = {
    acceptTerms = true;
    defaults.email = "kocha@slagit.net";
    certs.${config.networking.fqdn} = {
      extraDomainNames = [
        "mail.slagit.net"
      ];
      group = "mail-tls";
      reloadServices = [
        "dovecot2.service"
        "postfix.service"
      ];
    };
  };
  networking.firewall.allowedTCPPorts = [25 80 143 443 465 587 993];
  nixpkgs.config.packageOverrides = super: {
    dovecot = super.dovecot.override {withPgSQL = true;};
    opendkim = super.opendkim.overrideAttrs (oldAttrs: rec {
      configureFlags = oldAttrs.configureFlags ++ ["--with-odbx"];
      buildInputs = oldAttrs.buildInputs ++ [pkgs.opendbx];
    });
    postfix = super.postfix.override {withPgSQL = true;};
  };
  services = {
    borgmatic.settings = {
      postgresql_databases = [
        {
          name = "mail";
          pg_dump_command = "/run/current-system/sw/bin/pg_dump";
        }
        {
          name = "roundcube";
          pg_dump_command = "/run/current-system/sw/bin/pg_dump";
        }
      ];
      source_directories = [
        "/var/mailboxes"
        "/var/postgrey"
      ];
    };
    dovecot2 = {
      enable = true;
      enableLmtp = true;
      enablePAM = false;
      extraConfig = ''
        service auth {
          user = $default_internal_user
          unix_listener auth-client {
            mode = 0666
          }
        }
        service auth-worker {
          user = $default_internal_user
        }
        passdb {
          driver = sql
          args = ${dovecot-sql}
        }
        userdb {
          driver = static
          args = uid=mail gid=mail home=/var/mailboxes/%d/%n
        }
      '';
      mailLocation = "maildir:/var/mailboxes/%d/%n";
      mailboxes = {
        Drafts = {specialUse = "Drafts";};
        Junk = {specialUse = "Junk";};
        Trash = {specialUse = "Trash";};
        Sent = {specialUse = "Sent";};
      };
      sslServerCert = "/var/lib/acme/${config.networking.fqdn}/cert.pem";
      sslServerKey = "/var/lib/acme/${config.networking.fqdn}/key.pem";
    };
    nginx.virtualHosts.${config.networking.fqdn} = {
      forceSSL = true;
      enableACME = true;
    };
    postfix = {
      inherit submissionOptions;
      destination = [];
      enable = true;
      enableSubmission = true;
      enableSubmissions = true;
      extraConfig = ''
        biff = no
        smtpd_client_restrictions = check_policy_service unix:/run/postgrey.sock
        smtpd_milters = unix:/run/opendkim/opendkim.sock
        smtpd_relay_restrictions = reject_unauth_destination
        smtpd_sender_restrictions = check_sender_access pgsql:${pgsql-sender-access}
        virtual_alias_maps = pgsql:${pgsql-virtual-alias-maps}
        virtual_mailbox_domains = pgsql:${pgsql-virtual-mailbox-domains}
        virtual_mailbox_maps = pgsql:${pgsql-virtual-mailbox-maps}
        virtual_transport = lmtp:unix:/run/dovecot2/lmtp
      '';
      hostname = config.networking.fqdn;
      sslCert = "/var/lib/acme/${config.networking.fqdn}/cert.pem";
      sslKey = "/var/lib/acme/${config.networking.fqdn}/key.pem";
      submissionsOptions = submissionOptions;
    };
    postgresql = {
      enable = true;
      ensureDatabases = [
        "mail"
      ];
      ensureUsers = [
        {
          name = "dovecot2";
        }
        {
          name = "opendkim";
        }
        {
          name = "postfix";
        }
      ];
    };
    postgrey.enable = true;
    roundcube = {
      enable = true;
      extraConfig = ''
        $config['identities_level'] = 3;
        $config['smtp_host'] = 'ssl://${config.networking.fqdn}:465';
      '';
      hostName = "mail.slagit.net";
    };
  };
  systemd.services = {
    dovecot2 = {
      after = ["mail-migrate.service"];
      requires = ["mail-migrate.service"];
    };
    mail-migrate = {
      after = ["postgresql.service"];
      description = "Apply mail database migrations";
      environment.DATABASE_URL = "postgres://postgres@%%2Frun%%2Fpostgresql/mail";
      requires = ["postgresql.service"];
      script = ''
        ${pkgs.refinery-cli}/bin/refinery migrate -e DATABASE_URL -p ${./migrations}
      '';
      serviceConfig = {
        RemainAfterExit = true;
        Type = "oneshot";
        User = config.users.users.postgres.name;
      };
    };
    opendkim = {
      description = "OpenDKIM signing and verification daemon";
      after = ["network.target" "mail-migrate.service"];
      requires = ["mail-migrate.service"];
      wantedBy = ["multi-user.target"];

      serviceConfig = {
        ExecStart = "${pkgs.opendkim}/bin/opendkim -x ${opendkim-conf} -f";
        User = "opendkim";
        Group = "opendkim";
        RuntimeDirectory = "opendkim";
        StateDirectory = "opendkim";
        StateDirectoryMode = "0700";

        AmbientCapabilities = [];
        CapabilityBoundingSet = "";
        DevicePolicy = "closed";
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        NoNewPrivileges = true;
        PrivateDevices = true;
        PrivateMounts = true;
        PrivateTmp = true;
        PrivateUsers = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectSystem = "strict";
        RemoveIPC = true;
        RestrictAddressFamilies = ["AF_INET" "AF_INET6 AF_UNIX"];
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = ["@system-service" "~@privileged @resources"];
        UMask = "0077";
      };
    };
    postfix = {
      after = ["mail-migrate.service"];
      requires = ["mail-migrate.service"];
    };
  };
  users = {
    groups = {
      mail = {};
      mail-tls.members = [
        "dovecot"
        "nginx"
        "postfix"
      ];
      opendkim.members = [
        "postfix"
      ];
    };
    users = {
      mail = {
        createHome = true;
        group = "mail";
        home = "/var/mailboxes";
        isSystemUser = true;
      };
      opendkim = {
        group = "opendkim";
        isSystemUser = true;
      };
    };
  };
}
