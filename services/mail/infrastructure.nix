_: {
  resource.linode_domain_record = {
    slagit_net_MX = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "";
      priority = 10;
      record_type = "MX";
      target = "franklin.slagit.net";
    };
    slagit_net_TXT = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "";
      record_type = "TXT";
      target = "v=spf1 mx -all";
    };
    _dmarc_slagit_net_TXT = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "_dmarc";
      record_type = "TXT";
      target = "v=DMARC1; p=none; sp=quarantine; rua=mailto:dmarc@slagit.net";
    };
    mail__domainkey_slagit_net_TXT = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "mail._domainkey";
      record_type = "TXT";
      target = "v=DKIM1; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3B6drIO3Zcl9D/SmtASHbIgvk69+GYpGNDzbl2hYYV67rn3dm8bLH9rZNTHnbYXREK8fD5nsn+5KJH4UZ9Ero0uZhTEF/U3TmgqOgoW/5hyG4CGe5+P7I/DXf2ykTZ86wJbey5gUm/OWqZBJK09b4VpBibu1uweuUpQql+hzeTy1uT7XOqt6FGCn/zoOA159AEkzZBlgU/CoYHR92bIRpkPcfA6mtqKOCwJrAHUUcbLQoRol4u3ZrKa5TsymYVon30MWnU8KoFlCunfSAA+xmFuQC1zErpMQzenfY4zSbXQYBYbM/XntrMMpKYceyOdILpuMnSlVA18sI6bs04YBdwIDAQAB";
    };
    mail_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "mail";
      record_type = "CNAME";
      target = "franklin.slagit.net";
    };
  };
}
