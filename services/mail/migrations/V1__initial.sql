BEGIN;

CREATE TABLE public.keys (
	id BIGSERIAL PRIMARY KEY,
	data TEXT NOT NULL
);

CREATE TABLE public.domains (
	id BIGSERIAL PRIMARY KEY,
	name TEXT NOT NULL UNIQUE,
	key_id BIGINT NOT NULL REFERENCES public.keys
);

CREATE TABLE public.users (
	id BIGSERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	password TEXT NOT NULL,
	domain_id BIGINT NOT NULL REFERENCES public.domains,
	UNIQUE(name, domain_id)
);

CREATE TABLE public.aliases (
	id BIGSERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	domain_id BIGINT NOT NULL REFERENCES public.domains,
	destination TEXT NOT NULL,
	UNIQUE(name, domain_id)
);

CREATE SCHEMA dovecot;
GRANT USAGE ON SCHEMA dovecot TO dovecot2;

CREATE VIEW dovecot.virtual_users AS
SELECT
	(users.name::text || '@'::text) || domains.name::text AS username,
	users.password
FROM public.users
	JOIN public.domains ON users.domain_id = domains.id;
GRANT SELECT ON dovecot.virtual_users TO dovecot2;

CREATE SCHEMA postfix;
GRANT USAGE ON SCHEMA postfix TO postfix;

CREATE VIEW postfix.virtual_aliases AS
SELECT
	(aliases.name::text || '@'::text) || domains.name::text AS source,
	aliases.destination
FROM public.aliases
	JOIN public.domains ON aliases.domain_id = domains.id;
GRANT SELECT ON postfix.virtual_aliases TO postfix;

CREATE VIEW postfix.virtual_domains AS
SELECT
	domains.name
FROM public.domains;
GRANT SELECT ON postfix.virtual_domains TO postfix;

CREATE VIEW postfix.virtual_users AS
SELECT
	(users.name::text || '@'::text) || domains.name::text AS address
FROM public.users
	JOIN public.domains ON users.domain_id = domains.id;
GRANT SELECT ON postfix.virtual_users TO postfix;

CREATE SCHEMA opendkim;
GRANT USAGE ON SCHEMA opendkim TO opendkim;

CREATE VIEW opendkim.key AS
SELECT
	domains.name AS id,
	domains.name AS domain,
	'mail' AS selector,
	keys.data AS key
FROM public.domains
	JOIN public.keys ON domains.key_id = keys.id;
GRANT SELECT ON opendkim.key TO opendkim;

CREATE VIEW opendkim.signing AS
SELECT
	(users.name::text || '@'::text) || domains.name::text AS id,
	domains.name AS data
FROM public.users
	JOIN public.domains ON users.domain_id = domains.id;
GRANT SELECT ON opendkim.signing TO opendkim;

COMMIT;
