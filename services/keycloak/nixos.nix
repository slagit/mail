{
  lib,
  pkgs,
  ...
}: {
  services = {
    keycloak = {
      database.passwordFile = "/run/systemd-vaultd.sock";
      enable = true;
      package = pkgs.keycloak.overrideAttrs (final: prev: {
        KC_VAULT = "file";
      });
      settings = {
        db-password = lib.mkForce {_secret = "/run/credentials/keycloak.service/db_password";};
        http-enabled = true;
        http-port = 7080;
        hostname = "auth.slagit.net";
        hostname-strict-backchannel = true;
        proxy-headers = "xforwarded";
      };
    };
    nginx = {
      enable = true;
      virtualHosts."auth.slagit.net" = {
        forceSSL = true;
        enableACME = true;
        extraConfig = ''
          proxy_buffer_size 12k;
        '';
        locations."/" = {
          proxyPass = "http://127.0.0.1:7080";
          recommendedProxySettings = true;
        };
      };
    };
  };
  systemd.services.keycloak = {
    environment.KC_VAULT_DIR = "/run/credentials/keycloak.service";
    serviceConfig.LoadCredential = lib.mkForce [
      "db_password:/run/systemd-vaultd.sock"
      "slagit_smtp:/run/systemd-vaultd.sock"
    ];
  };
}
