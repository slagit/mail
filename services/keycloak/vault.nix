{
  config,
  lib,
  ...
}: let
  deep_map_impl = f: sections: keys: objects:
    if [] == sections
    then (lib.mapAttrs' (key: value: f (keys ++ [key]) value) objects)
    else let
      next_section = builtins.elemAt sections 0;
      new_sections = lib.sublist 1 (builtins.length sections - 1) sections;
    in
      lib.concatMapAttrs (key: value: deep_map_impl f new_sections (keys ++ [key]) value."${next_section}") objects;
  deep_map = f: sections: objects: deep_map_impl f sections [] objects;
in {
  config = {
    slagit.vault.policies.admin.paths = {
      "sys/auth/oidc".capabilities = ["create" "read" "sudo" "update"];
      "sys/auth/jwt".capabilities = ["create" "read" "sudo" "update"];
      "sys/mounts/auth/oidc".capabilities = ["read"];
      "sys/mounts/auth/oidc/tune".capabilities = ["read"];
      "sys/mounts/auth/jwt".capabilities = ["read"];
      "sys/mounts/auth/jwt/tune".capabilities = ["read"];
      "auth/oidc/config".capabilities = ["read" "update"];
      "auth/oidc/role/+".capabilities = ["create" "read" "update" "delete"];
      "auth/jwt/config".capabilities = ["read" "update"];
      "auth/jwt/role/+".capabilities = ["create" "read" "update" "delete"];
    };
    resource = {
      vault_auth_backend = {
        jwt = {
          path = "jwt";
          type = "jwt";
        };
        oidc = {
          path = "oidc";
          type = "oidc";
        };
      };
      vault_jwt_auth_backend_role = {
        jwt-ssh-user = {
          backend = "\${vault_auth_backend.jwt.path}";
          bound_audiences = ["vault"];
          token_policies = ["ssh-user"];
          role_name = "ssh-user";
          user_claim = "sub";
          role_type = "jwt";
          token_ttl = 3600;
          token_max_ttl = 28800;
        };
        oidc-default = {
          allowed_redirect_uris = ["https://slagit-vault.net:8200/ui/vault/auth/oidc/oidc/callback"];
          backend = "\${vault_auth_backend.oidc.path}";
          bound_audiences = ["vault"];
          role_name = "default";
          user_claim = "sub";
          token_ttl = 3600;
          token_max_ttl = 28800;
        };
        oidc-ssh-user = {
          allowed_redirect_uris = ["https://slagit-vault.net:8200/ui/vault/auth/oidc/oidc/callback"];
          backend = "\${vault_auth_backend.oidc.path}";
          bound_audiences = ["vault"];
          bound_claims."/resource_access/vault/roles" = "ssh-user";
          oidc_scopes = ["ssh-user"];
          token_policies = ["ssh-user"];
          role_name = "ssh-user";
          user_claim = "sub";
          token_ttl = 3600;
          token_max_ttl = 28800;
        };
      };

      keycloak_realm =
        deep_map (keys: realmConfig: let
          realmName = builtins.elemAt keys 0;
        in {
          name = realmName;
          value = {
            inherit (realmConfig) display_name realm remember_me reset_password_allowed smtp_server;
          };
        }) []
        config.slagit.keycloak.realms;
      keycloak_openid_client =
        deep_map (keys: clientConfig: let
          realmName = builtins.elemAt keys 0;
          clientName = builtins.elemAt keys 1;
        in {
          name = "${realmName}_${clientName}";
          value = {
            inherit (clientConfig) access_type admin_url consent_required frontchannel_logout_enabled full_scope_allowed name oauth2_device_authorization_grant_enabled root_url service_accounts_enabled standard_flow_enabled valid_redirect_uris web_origins;
            realm_id = "\${keycloak_realm.${realmName}.id}";
            client_id = clientName;
          };
        }) ["clients"]
        config.slagit.keycloak.realms;
      keycloak_openid_client_default_scopes =
        deep_map (keys: clientConfig: let
          realmName = builtins.elemAt keys 0;
          clientName = builtins.elemAt keys 1;
        in {
          name = "${realmName}_${clientName}";
          value = {
            inherit (clientConfig) default_scopes;
            realm_id = "\${keycloak_realm.${realmName}.id}";
            client_id = "\${keycloak_openid_client.${realmName}_${clientName}.id}";
          };
        }) ["clients"]
        config.slagit.keycloak.realms;
      keycloak_openid_client_optional_scopes =
        deep_map (keys: clientConfig: let
          realmName = builtins.elemAt keys 0;
          clientName = builtins.elemAt keys 1;
        in {
          name = "${realmName}_${clientName}";
          value = {
            inherit (clientConfig) optional_scopes;
            realm_id = "\${keycloak_realm.${realmName}.id}";
            client_id = "\${keycloak_openid_client.${realmName}_${clientName}.id}";
          };
        }) ["clients"]
        config.slagit.keycloak.realms;
      keycloak_openid_client_scope =
        deep_map (keys: scopeConfig: let
          realmName = builtins.elemAt keys 0;
          scopeName = builtins.elemAt keys 1;
        in {
          name = "${realmName}_${scopeName}";
          value = {
            inherit (scopeConfig) consent_screen_text;
            realm_id = "\${keycloak_realm.${realmName}.id}";
            name = scopeName;
          };
        }) ["client_scopes"]
        config.slagit.keycloak.realms;
      keycloak_openid_audience_protocol_mapper =
        deep_map (keys: mapConfig: let
          realmName = builtins.elemAt keys 0;
          clientName = builtins.elemAt keys 1;
          mapName = builtins.elemAt keys 2;
        in {
          name = "${realmName}_client_${clientName}_${mapName}";
          value = {
            inherit (mapConfig) add_to_id_token included_client_audience name;
            client_id = "\${keycloak_openid_client.${realmName}_${clientName}.id}";
            realm_id = "\${keycloak_realm.${realmName}.id}";
          };
        }) ["clients" "audience_protocol_mappers"]
        config.slagit.keycloak.realms
        // deep_map (keys: mapConfig: let
          realmName = builtins.elemAt keys 0;
          scopeName = builtins.elemAt keys 1;
          mapName = builtins.elemAt keys 2;
        in {
          name = "${realmName}_client_scope_${scopeName}_${mapName}";
          value = {
            inherit (mapConfig) add_to_id_token included_client_audience name;
            client_scope_id = "\${keycloak_openid_client_scope.${realmName}_${scopeName}.id}";
            realm_id = "\${keycloak_realm.${realmName}.id}";
          };
        }) ["client_scopes" "audience_protocol_mappers"]
        config.slagit.keycloak.realms;
      keycloak_openid_user_client_role_protocol_mapper =
        deep_map (keys: mapConfig: let
          realmName = builtins.elemAt keys 0;
          clientName = builtins.elemAt keys 1;
          mapName = builtins.elemAt keys 2;
        in {
          name = "${realmName}_client_${clientName}_${mapName}";
          value = {
            inherit (mapConfig) claim_name client_id_for_role_mappings multivalued name;
            client_id = "\${keycloak_openid_client.${realmName}_${clientName}.id}";
            realm_id = "\${keycloak_realm.${realmName}.id}";
          };
        }) ["clients" "user_client_role_protocol_mappers"]
        config.slagit.keycloak.realms
        // deep_map (keys: mapConfig: let
          realmName = builtins.elemAt keys 0;
          scopeName = builtins.elemAt keys 1;
          mapName = builtins.elemAt keys 2;
        in {
          name = "${realmName}_client_scope_${scopeName}_${mapName}";
          value = {
            inherit (mapConfig) claim_name client_id_for_role_mappings multivalued name;
            client_scope_id = "\${keycloak_openid_client_scope.${realmName}_${scopeName}.id}";
            realm_id = "\${keycloak_realm.${realmName}.id}";
          };
        }) ["client_scopes" "user_client_role_protocol_mappers"]
        config.slagit.keycloak.realms;
      keycloak_role =
        deep_map (keys: mapConfig: let
          realmName = builtins.elemAt keys 0;
          clientName = builtins.elemAt keys 1;
          roleName = builtins.elemAt keys 2;
        in {
          name = "${realmName}_client_${clientName}_${roleName}";
          value = {
            client_id = "\${keycloak_openid_client.${realmName}_${clientName}.id}";
            name = roleName;
            realm_id = "\${keycloak_realm.${realmName}.id}";
          };
        }) ["clients" "roles"]
        config.slagit.keycloak.realms;
      keycloak_generic_role_mapper =
        deep_map (keys: mapConfig: let
          realmName = builtins.elemAt keys 0;
          scopeName = builtins.elemAt keys 1;
          mapName = builtins.elemAt keys 2;
        in {
          name = "${realmName}_client_scope_${scopeName}_${mapName}";
          value = {
            inherit (mapConfig) role_id;
            client_scope_id = "\${keycloak_openid_client_scope.${realmName}_${scopeName}.id}";
            realm_id = "\${keycloak_realm.${realmName}.id}";
          };
        }) ["client_scopes" "generic_role_mappers"]
        config.slagit.keycloak.realms;

      keycloak_realm_user_profile.slagit_net = {
        realm_id = "\${keycloak_realm.slagit_net.id}";
        attribute = [
          {
            name = "username";
            display_name = "\$\${username}";
            permissions = {
              view = ["admin" "user"];
              edit = ["admin"];
            };
            validator = [
              {
                name = "length";
                config = {
                  min = 3;
                  max = 255;
                };
              }
              {
                name = "username-prohibited-characters";
              }
              {
                name = "up-username-not-idn-homograph";
              }
            ];
          }
          {
            name = "email";
            display_name = "\$\${email}";
            permissions = {
              view = ["admin" "user"];
              edit = ["admin" "user"];
            };
            required_for_roles = [
              "user"
            ];
            validator = [
              {
                name = "email";
              }
              {
                name = "length";
                config = {
                  max = 255;
                };
              }
            ];
          }
          {
            name = "firstName";
            display_name = "\$\${firstName}";
            permissions = {
              view = ["admin" "user"];
              edit = ["admin"];
            };
            required_for_roles = [
              "user"
            ];
            validator = [
              {
                name = "length";
                config = {
                  max = 255;
                };
              }
              {
                name = "person-name-prohibited-characters";
              }
            ];
          }
          {
            name = "lastName";
            display_name = "\$\${lastName}";
            permissions = {
              view = ["admin" "user"];
              edit = ["admin"];
            };
            required_for_roles = [
              "user"
            ];
            validator = [
              {
                name = "length";
                config = {
                  max = 255;
                };
              }
              {
                name = "person-name-prohibited-characters";
              }
            ];
          }
        ];
        group = [
          {
            name = "user-metadata";
            display_header = "User metadata";
            display_description = "Attributes, which refer to user metadata";
            annotations = {};
          }
        ];
      };
    };
    slagit.keycloak.realms.slagit_net = {
      clients.vault = {
        access_type = "CONFIDENTIAL";
        admin_url = "https://slagit-vault.net:8200/ui/vault/auth/oidc/oidc/callback";
        default_scopes = [];
        full_scope_allowed = false;
        name = "Vault";
        optional_scopes = [
          "ssh-user"
        ];
        roles = {
          admin = {};
          ssh-user = {};
        };
        root_url = "https://slagit-vault.net:8200/ui/vault/auth/oidc/oidc/callback";
        standard_flow_enabled = true;
        valid_redirect_uris = ["https://slagit-vault.net:8200/ui/vault/auth/oidc/oidc/callback"];
        web_origins = ["https://slagit-vault.net:8200"];
      };
      client_scopes.ssh-user = {
        audience_protocol_mappers.vault = {
          name = "Vault Audience";
          included_client_audience = "vault";
        };
        consent_screen_text = "SSH Access to SLAGIT.net Hosts";
        generic_role_mappers.ssh-user = {
          # TODO: This should be more eganomic since the author likely doesn't know the resource name
          role_id = "\${keycloak_role.slagit_net_client_vault_ssh-user.id}";
        };
        user_client_role_protocol_mappers.roles = {
          name = "Vault Roles";
          claim_name = "resource_access.\$\${client_id}.roles";
          multivalued = true;
        };
      };
      display_name = "SLAGIT.net";
      remember_me = true;
      realm = "slagit";
      reset_password_allowed = true;
      verify_email = true;
      smtp_server = {
        host = "mail.slagit.net";
        from = "keycloak@slagit.net";
        port = 587;
        starttls = true;
        auth = {
          username = "keycloak@slagit.net";
          password = "\$\${vault.smtp}";
        };
      };
    };
  };
  options.slagit.keycloak = let
    audience_protocol_mappers_option = lib.mkOption {
      default = {};
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          add_to_id_token = lib.mkOption {
            default = true;
            type = lib.types.bool;
          };
          included_client_audience = lib.mkOption {
            default = "";
            type = lib.types.str;
          };
          name = lib.mkOption {
            type = lib.types.str;
          };
        };
      });
    };
    user_client_role_protocol_mappers_option = lib.mkOption {
      default = {};
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          claim_name = lib.mkOption {
            type = lib.types.str;
          };
          client_id_for_role_mappings = lib.mkOption {
            default = "";
            type = lib.types.str;
          };
          multivalued = lib.mkOption {
            default = false;
            type = lib.types.bool;
          };
          name = lib.mkOption {
            type = lib.types.str;
          };
        };
      });
    };
  in {
    realms = lib.mkOption {
      default = {};
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          clients = lib.mkOption {
            default = {};
            type = lib.types.attrsOf (lib.types.submodule {
              options = {
                access_type = lib.mkOption {
                  type = lib.types.enum ["BEARER-ONLY" "CONFIDENTIAL" "PUBLIC"];
                };
                admin_url = lib.mkOption {
                  default = "";
                  type = lib.types.str;
                };
                audience_protocol_mappers = audience_protocol_mappers_option;
                consent_required = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
                default_scopes = lib.mkOption {
                  default = [];
                  type = lib.types.listOf lib.types.str;
                };
                frontchannel_logout_enabled = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
                full_scope_allowed = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
                name = lib.mkOption {
                  default = "";
                  type = lib.types.str;
                };
                oauth2_device_authorization_grant_enabled = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
                optional_scopes = lib.mkOption {
                  default = [];
                  type = lib.types.listOf lib.types.str;
                };
                roles = lib.mkOption {
                  default = {};
                  type = lib.types.attrsOf (lib.types.submodule {
                    options = {
                    };
                  });
                };
                root_url = lib.mkOption {
                  default = "";
                  type = lib.types.str;
                };
                service_accounts_enabled = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
                standard_flow_enabled = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
                user_client_role_protocol_mappers = user_client_role_protocol_mappers_option;
                valid_redirect_uris = lib.mkOption {
                  default = [];
                  type = lib.types.listOf lib.types.str;
                };
                web_origins = lib.mkOption {
                  default = [];
                  type = lib.types.listOf lib.types.str;
                };
              };
            });
          };
          client_scopes = lib.mkOption {
            default = {};
            type = lib.types.attrsOf (lib.types.submodule {
              options = {
                audience_protocol_mappers = audience_protocol_mappers_option;
                consent_screen_text = lib.mkOption {
                  default = "";
                  type = lib.types.str;
                };
                generic_role_mappers = lib.mkOption {
                  default = {};
                  type = lib.types.attrsOf (lib.types.submodule {
                    options.role_id = lib.mkOption {
                      type = lib.types.str;
                    };
                  });
                };
                user_client_role_protocol_mappers = user_client_role_protocol_mappers_option;
              };
            });
          };
          display_name = lib.mkOption {
            default = "";
            type = lib.types.str;
          };
          realm = lib.mkOption {
            type = lib.types.str;
          };
          remember_me = lib.mkOption {
            default = false;
            type = lib.types.bool;
          };
          reset_password_allowed = lib.mkOption {
            default = false;
            type = lib.types.bool;
          };
          smtp_server = lib.mkOption {
            type = lib.types.submodule {
              options = {
                auth = lib.mkOption {
                  type = lib.types.submodule {
                    options = {
                      password = lib.mkOption {
                        type = lib.types.str;
                      };
                      username = lib.mkOption {
                        type = lib.types.str;
                      };
                    };
                  };
                };
                from = lib.mkOption {
                  type = lib.types.str;
                };
                host = lib.mkOption {
                  type = lib.types.str;
                };
                port = lib.mkOption {
                  default = 25;
                  type = lib.types.port;
                };
                starttls = lib.mkOption {
                  default = false;
                  type = lib.types.bool;
                };
              };
            };
          };
          verify_email = lib.mkOption {
            default = false;
            type = lib.types.bool;
          };
        };
      });
    };
  };
}
