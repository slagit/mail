_: {
  resource.linode_domain_record = {
    auth_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "auth";
      record_type = "CNAME";
      target = "franklin.slagit.net";
    };
  };
}
