{
  config,
  pkgs,
  ...
}: let
  site = pkgs.runCommand "site" {} ''
    cp -r ${./site} "$out"
    chmod 755 "$out"
    ${pkgs.tailwindcss}/bin/tailwindcss -i "$out/index.css" -o "$out/output.css" --content "$out/index.html"
  '';
in {
  networking.firewall.interfaces."nebula.slagit".allowedTCPPorts = [80 443];
  services = {
    nebula.networks.slagit.firewall.inbound = [
      {
        port = "80";
        proto = "tcp";
        group = "users";
      }
      {
        port = "443";
        proto = "tcp";
        group = "users";
      }
    ];
    nginx = {
      enable = true;
      virtualHosts."slagit.net" = {
        forceSSL = true;
        default = true;
        root = site;
        sslCertificate = "/run/vault-agent@homepage-nginx/slagit.net.pem";
        sslCertificateKey = "/run/vault-agent@homepage-nginx/slagit.net-key.pem";
      };
    };
  };
  slagit.vault-agent.homepage-nginx = {
    template = [
      {
        contents = "{{- with secret \"secret/acme/homepage-nginx/slagit.net\" }}{{ index .Data.data \"fullchain.pem\" }}{{- end }}";
        destination = "/run/vault-agent@homepage-nginx/slagit.net.pem";
        error_on_missing_key = true;
        perms = "0644";
      }
      {
        contents = "{{- with secret \"secret/acme/homepage-nginx/slagit.net\" }}{{ index .Data.data \"private_key.pem\" }}{{- end }}";
        destination = "/run/vault-agent@homepage-nginx/slagit.net-key.pem";
        error_on_missing_key = true;
        perms = "0600";
      }
    ];
    role = "homepage-nginx";
    user = config.systemd.services.nginx.serviceConfig.User;
  };
  systemd.services.nginx = {
    after = ["vault-agent@homepage-nginx.service"];
    bindsTo = ["vault-agent@homepage-nginx.service"];
  };
}
