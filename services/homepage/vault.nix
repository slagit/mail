_: {
  resource = {
    vault_approle_auth_backend_role = {
      homepage-nginx = {
        backend = "\${vault_auth_backend.approle.path}";
        role_name = "homepage-nginx";
        secret_id_num_uses = 1;
        token_period = 3600;
      };
    };
  };
  slagit.vault = {
    identities = {
      app-homepage-nginx = {
        aliases = [
          {
            backend = "vault_auth_backend.approle";
            name = "\${vault_approle_auth_backend_role.homepage-nginx.role_id}";
          }
        ];
        policies = [
          "homepage-nginx"
        ];
      };
      host-compute0-slagit-net.policies = ["homepage-nginx-host"];
    };
    policies = {
      homepage-nginx-host.paths = {
        "auth/approle/role/homepage-nginx/role-id".capabilities = ["read"];
        "auth/approle/role/homepage-nginx/secret-id" = {
          capabilities = ["create" "update"];
          min_wrapping_ttl = "1s";
          max_wrapping_ttl = "10m";
        };
      };
      homepage-nginx.paths = {
        "secret/data/acme/homepage-nginx/slagit.net".capabilities = ["read"];
      };
    };
  };
}
