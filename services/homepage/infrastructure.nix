_: {
  resource.linode_domain_record = {
    slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "";
      record_type = "A";
      target = "10.10.10.3";
    };
    _acme_challenge_slagit_net = {
      domain_id = "\${linode_domain.slagit_net.id}";
      name = "_acme-challenge";
      record_type = "CNAME";
      target = "slagit.net.slagit-cert.net";
    };
  };
}
