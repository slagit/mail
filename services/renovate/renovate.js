const fs = require("node:fs");
const path = require("node:path");

const credentials_directory = process.env.CREDENTIALS_DIRECTORY;

const token_path = path.join(credentials_directory, "renovate_token");
const token = fs.readFileSync(token_path).toString();

const gitPrivateKeyPath = path.join(
  credentials_directory,
  "renovate_git_private_key",
);
const gitPrivateKey = fs.readFileSync(gitPrivateKeyPath).toString();

const github_com_token_path = path.join(
  credentials_directory,
  "renovate_github_com_token",
);
process.env.GITHUB_COM_TOKEN = fs
  .readFileSync(github_com_token_path)
  .toString();

module.exports = {
  allowedPostUpgradeCommands: [
    "^find -name Cargo\\.nix -execdir nix run --inputs-from \\. cargo2nix -- -lo ';'$",
    "^find -name package\\.json -execdir nix run --inputs-from \\. nixpkgs#nodePackages\\.node2nix -- -18 -l package-lock\\.json -d ';'$",
  ],
  gitPrivateKey,
  platform: "gitlab",
  repositories: ["frontierscamp.org/install", "kocha/dotfiles", "slagit/mail"],
  token,
};
