{
  config,
  pkgs,
  ...
}: let
  package =
    (import ./generated.nix {
      inherit pkgs;
      inherit (pkgs) nodejs system;
    })
    .renovate;
in {
  systemd = {
    services.renovate = {
      after = ["network-online.target"];
      description = "Universal dependency update tool";
      path = [
        pkgs.cargo
        pkgs.git
        pkgs.gnupg
        pkgs.go
        pkgs.nix
        pkgs.nodejs_18
      ];
      serviceConfig = {
        CapabilityBoundingSet = "CAP_DAC_READ_SEARCH CAP_NET_RAW";
        CPUSchedulingPolicy = "batch";
        Environment = "RENOVATE_CONFIG_FILE=${./renovate.js}";
        ExecStart = "${package}/bin/renovate";
        ExecStartPre = "${pkgs.coreutils}/bin/sleep 1m";
        Group = config.users.groups.renovate.name;
        IOSchedulingClass = "best-effort";
        IOSchedulingPriority = 7;
        IOWeight = 100;
        LoadCredential = [
          "renovate_token:/run/systemd-vaultd.sock"
          "renovate_git_private_key:/run/systemd-vaultd.sock"
          "renovate_github_com_token:/run/systemd-vaultd.sock"
        ];
        LockPersonality = true;
        LogRateLimitIntervalSec = 0;
        Nice = 19;
        NoNewPrivileges = true;
        PrivateDevices = true;
        PrivateTmp = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectSystem = "full";
        Restart = "no";
        RestrictAddressFamilies = "AF_UNIX AF_INET AF_INET6 AF_NETLINK";
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
        SystemCallErrorNumber = "EPERM";
        SystemCallFilter = "@system-service";
        Type = "oneshot";
        User = config.users.users.renovate.name;
      };
      unitConfig = {
        ConditionACPower = true;
      };
      wants = ["network-online.target"];
    };
    timers.renovate = {
      description = "Run universal dependency update tool";
      timerConfig = {
        OnCalendar = "hourly";
        Persistent = true;
      };
      wantedBy = ["timers.target"];
    };
  };
  users = {
    groups.renovate = {};
    users.renovate = {
      createHome = true;
      description = "Renovate Service Account";
      isSystemUser = true;
      group = config.users.groups.renovate.name;
      home = "/var/lib/renovate";
    };
  };
}
