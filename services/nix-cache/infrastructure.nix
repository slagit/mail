_: {
  resource = {
    aws_s3_bucket.slagit-nix-cache.bucket = "slagit-nix-cache";
    aws_iam_policy = {
      assume-slagit-nix-cache = {
        name = "assume-slagit-nix-cache";
        policy = builtins.toJSON {
          Statement = [
            {
              Action = "sts:AssumeRole";
              Effect = "Allow";
              Resource = "\${aws_iam_role.slagit-nix-cache.arn}";
              Sid = "AssumeVaultAcme";
            }
          ];
          Version = "2012-10-17";
        };
      };
      slagit-nix-cache = {
        policy = builtins.toJSON {
          Statement = [
            {
              Action = [
                "s3:AbortMultipartUpload"
                "s3:GetBucketLocation"
                "s3:GetObject"
                "s3:ListBucket"
                "s3:ListBucketMultipartUploads"
                "s3:ListMultipartUploadParts"
                "s3:PutObject"
              ];
              Effect = "Allow";
              Resource = [
                "arn:aws:s3:::slagit-nix-cache"
                "arn:aws:s3:::slagit-nix-cache/*"
              ];
              Sid = "UploadToCache";
            }
          ];
          Version = "2012-10-17";
        };
      };
    };
    aws_iam_role.slagit-nix-cache = {
      assume_role_policy = builtins.toJSON {
        "Statement" = [
          {
            "Effect" = "Allow";
            "Principal" = {
              "AWS" = "arn:aws:iam::445131688516:root";
            };
            "Action" = "sts:AssumeRole";
            "Condition" = {};
          }
        ];
        "Version" = "2012-10-17";
      };
      name = "slagit-nix-cache";
    };
    aws_iam_role_policy_attachment."slagit-nix-cache-slagit-nix-cache" = {
      policy_arn = "\${aws_iam_policy.slagit-nix-cache.arn}";
      role = "\${aws_iam_role.slagit-nix-cache.name}";
    };
    aws_iam_user_policy_attachment."vault-root-assume-slagit-nix-cache" = {
      policy_arn = "\${aws_iam_policy.assume-slagit-nix-cache.arn}";
      user = "\${aws_iam_user.vault-root.name}";
    };
  };
}
