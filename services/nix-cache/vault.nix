_: {
  resource = {
    vault_approle_auth_backend_role = {
      nix-cache = {
        backend = "\${vault_auth_backend.approle.path}";
        role_name = "nix-cache";
        secret_id_num_uses = 1;
        token_period = 3600;
      };
    };
    vault_aws_secret_backend_role.nix-cache = {
      backend = "\${vault_mount.aws.path}";
      credential_type = "assumed_role";
      max_sts_ttl = 3600;
      name = "nix-cache";
      role_arns = [
        "arn:aws:iam::445131688516:role/slagit-nix-cache"
      ];
    };
  };
  slagit.vault = {
    identities = {
      app-nix-cache = {
        aliases = [
          {
            backend = "vault_auth_backend.approle";
            name = "\${vault_approle_auth_backend_role.nix-cache.role_id}";
          }
        ];
        policies = [
          "nix-cache"
        ];
      };
      host-compute1-slagit-net.policies = ["nix-cache-host"];
    };
    policies = {
      admin.paths."aws/roles/nix-cache".capabilities = ["create" "read" "update" "delete"];
      nix-cache-host.paths = {
        "auth/approle/role/nix-cache/role-id".capabilities = ["read"];
        "auth/approle/role/nix-cache/secret-id" = {
          capabilities = ["create" "update"];
          min_wrapping_ttl = "1s";
          max_wrapping_ttl = "10m";
        };
      };
      nix-cache.paths = {
        "secret/data/apps/nix-cache/cache-priv-key".capabilities = ["read"];
        "aws/sts/nix-cache".capabilities = ["read"];
      };
    };
  };
}
