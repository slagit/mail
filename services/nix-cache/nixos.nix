{
  config,
  lib,
  pkgs,
  ...
}: {
  nix.settings.post-build-hook = pkgs.writeShellScript "upload-to-cache.sh" ''
    set -f
    export IFS=' '
    export AWS_ACCESS_KEY_ID="$(< /run/vault-agent@nix-cache/aws_access_key_id)"
    export AWS_SECRET_ACCESS_KEY="$(< /run/vault-agent@nix-cache/aws_secret_access_key)"
    export AWS_SESSION_TOKEN="$(< /run/vault-agent@nix-cache/aws_session_token)"
    exec nix copy --to "s3://slagit-nix-cache?secret-key=/run/vault-agent@nix-cache/cache-priv-key" $OUT_PATHS
  '';
  slagit.vault-agent.nix-cache = {
    template = [
      {
        contents = "{{- with secret \"aws/sts/nix-cache\" }}{{ .Data.access_key }}{{- end }}";
        destination = "/run/vault-agent@nix-cache/aws_access_key_id";
        error_on_missing_key = true;
        perms = "0600";
      }
      {
        contents = "{{- with secret \"aws/sts/nix-cache\" }}{{ .Data.secret_key }}{{- end }}";
        destination = "/run/vault-agent@nix-cache/aws_secret_access_key";
        error_on_missing_key = true;
        perms = "0600";
      }
      {
        contents = "{{- with secret \"aws/sts/nix-cache\" }}{{ .Data.security_token }}{{- end }}";
        destination = "/run/vault-agent@nix-cache/aws_session_token";
        error_on_missing_key = true;
        perms = "0600";
      }
      {
        contents = "{{- with secret \"secret/apps/nix-cache/cache-priv-key\" }}{{ .Data.data.credential }}{{- end }}";
        destination = "/run/vault-agent@nix-cache/cache-priv-key";
        error_on_missing_key = true;
        perms = "0600";
      }
    ];
    role = "nix-cache";
    user = "root";
  };
  systemd.services.nix-daemon = {
    after = ["vault-agent@nix-cache.service"];
    bindsTo = ["vault-agent@nix-cache.service"];
  };
}
