_: {
  perSystem = {pkgs, ...}: {
    apps.idrac = let
      program = pkgs.writeShellScript "idrac.sh" ''
        set -eu

        IDRAC_HOST=$1

        echo -n Password:
        read -s IDRAC_PASSWORD

        TEMP_DIR="$(mktemp -d)"
        function cleanup {
            rm -rf "$TEMP_DIR"
        }
        trap cleanup EXIT

        mkdir "$TEMP_DIR/lib"
        ${pkgs.curl}/bin/curl --fail --insecure --output "$TEMP_DIR/avctKVM.jar" https://$IDRAC_HOST/software/avctKVM.jar
        ${pkgs.curl}/bin/curl --fail --insecure --output "$TEMP_DIR/lib/avctKVMIOLinux64.jar" https://$IDRAC_HOST/software/avctKVMIOLinux64.jar
        ${pkgs.curl}/bin/curl --fail --insecure --output "$TEMP_DIR/lib/avctVMLinux64.jar" https://$IDRAC_HOST/software/avctVMLinux64.jar

        (cd "$TEMP_DIR/lib" && ${pkgs.openjdk8-bootstrap}/bin/jar -xf avctKVMIOLinux64.jar)
        (cd "$TEMP_DIR/lib" && ${pkgs.openjdk8-bootstrap}/bin/jar -xf avctVMLinux64.jar)
        export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib"
        ldd $TEMP_DIR/lib/libavctKVMIO.so
        ldd $TEMP_DIR/lib/libavmlinux.so

        ${pkgs.openjdk8-bootstrap}/bin/java -cp "$TEMP_DIR/avctKVM.jar" -Djava.library.path="$TEMP_DIR/lib" -Djava.security.properties=${security_properties} com.avocent.idrac.kvm.Main ip=$IDRAC_HOST kmport=5900 vport=5900 user=albert passwd=$IDRAC_PASSWORD apcp=1 version=2 vmprivilege=true "helpurl=https://$IDRAC_HOST/help/contents.html"
      '';
      security_properties = pkgs.writeText "java.security" ''
        jdk.tls.disabledAlgorithms=TLSv1, TLSv1.1, DES, MD5withRSA, \
            DH keySize < 1024, EC keySize < 224, 3DES_EDE_CBC, anon, NULL, \
            include jdk.disabled.namedCurves
      '';
    in {
      program = toString program;
    };
  };
}
