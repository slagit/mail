_: {
  perSystem = {pkgs, ...}: {
    packages.tfgitlab = pkgs.buildGoModule {
      name = "tfgitlab";
      src = ./.;
      vendorHash = null;
    };
  };
}
