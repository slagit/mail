package main

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"

	"golang.org/x/oauth2"
)

func randomBytesInHex(count int) (string, error) {
	buf := make([]byte, count)
	if _, err := io.ReadFull(rand.Reader, buf); err != nil {
		return "", fmt.Errorf("Could not generate %d random bytes: %v", count, err)
	}
	return hex.EncodeToString(buf), nil
}

type AuthURL struct {
	URL          string
	State        string
	CodeVerifier string
}

func (u *AuthURL) String() string {
	return u.URL
}

func AuthorizationURL(config *oauth2.Config) (*AuthURL, error) {
	codeVerifier, err := randomBytesInHex(32)
	if err != nil {
		return nil, fmt.Errorf("Could not create a code verifier: %v", err)
	}
	sha2 := sha256.New()
	io.WriteString(sha2, codeVerifier)
	codeChallenge := base64.RawURLEncoding.EncodeToString(sha2.Sum(nil))
	state, err := randomBytesInHex(24)
	if err != nil {
		return nil, fmt.Errorf("Could not generate random state: %v", err)
	}
	authUrl := config.AuthCodeURL(
		state,
		oauth2.SetAuthURLParam("code_challenge_method", "S256"),
		oauth2.SetAuthURLParam("code_challenge", codeChallenge),
	)

	return &AuthURL{
		URL:          authUrl,
		State:        state,
		CodeVerifier: codeVerifier,
	}, nil
}

type OAuthRedirectHandler struct {
	State string
	CodeVerifier string
	OauthConfig *oauth2.Config
	Server *http.Server
	Code *string
}

func textResponse(w http.ResponseWriter, status int, body string) {
	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(status)
	io.WriteString(w, body)
}

func (h *OAuthRedirectHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	state := query.Get("state")
	if subtle.ConstantTimeCompare([]byte(h.State), []byte(state)) == 0 {
		textResponse(w, http.StatusBadRequest, "Invalid State")
		return
	}
	code := query.Get("code")
	if code == "" {
		textResponse(w, http.StatusBadRequest, "Missing Code")
		return
	}

	token, err := h.OauthConfig.Exchange(r.Context(), code, oauth2.SetAuthURLParam("code_verifier", h.CodeVerifier))
	if err != nil {
		textResponse(w, http.StatusInternalServerError, err.Error())
		return
	}
	h.Code = &token.AccessToken
	textResponse(w, http.StatusOK, "Authentication Sucessfull. You may close this window.")
	go h.Server.Shutdown(context.Background())
}

func main() {
	if token, ok := os.LookupEnv("GITLAB_TOKEN"); ok {
		client := &http.Client {
		}
		req, err := http.NewRequest("GET", "https://gitlab.com/oauth/token/info", nil)
		if err != nil {
			panic(err)
		}
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		if resp.StatusCode == http.StatusOK {
			return
		}
	}

	config := &oauth2.Config {
		ClientID: "e8e59bd620841fa8230b5425084d834b0c8054a878cd389aa9cbcc9fb39b2db5",
		Endpoint: oauth2.Endpoint {
			AuthURL: "https://gitlab.com/oauth/authorize",
			TokenURL: "https://gitlab.com/oauth/token",
			AuthStyle: oauth2.AuthStyleInParams,
		},
		RedirectURL: "http://localhost:12345/",
	}
	url, err := AuthorizationURL(config)
	if err != nil {
		panic(err)
	}

	server := &http.Server {
		Addr: ":12345",
	}

	handler := &OAuthRedirectHandler {
		State: url.State,
		CodeVerifier: url.CodeVerifier,
		OauthConfig: config,
		Server: server,
	}
	http.Handle("/", handler)

	cmd := exec.Command("xdg-open", url.String())
	if err := cmd.Run(); err != nil {
		panic(err)
	}

	if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
		panic(err)
	}

	if handler.Code == nil {
		panic(fmt.Errorf("No code found after authorization"))
	} else {
		fmt.Printf("export GITLAB_TOKEN=%s\n", *handler.Code)
	}
}
