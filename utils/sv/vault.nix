_: {
  slagit.keycloak.realms.slagit_net.clients.sv = {
    access_type = "PUBLIC";
    consent_required = true;
    default_scopes = ["ssh-user"];
    full_scope_allowed = false;
    name = "SV SSH Client";
    oauth2_device_authorization_grant_enabled = true;
    service_accounts_enabled = false;
    standard_flow_enabled = false;
  };
}
