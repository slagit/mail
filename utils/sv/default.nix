{
  self,
  withSystem,
  ...
}: {
  flake = {
    overlays.sv = final: prev: {
      sv = withSystem prev.stdenv.hostPlatform.system ({self', ...}: self'.packages.sv);
    };
    hmModules.sv = _: {
      nixpkgs.overlays = [
        self.overlays.sv
      ];
    };
  };
  ci = {
    alejandra.default.ignore = [
      "utils/sv/Cargo.nix"
    ];
    rust.default.sv.path = "utils/sv";
  };
  perSystem = {
    lib,
    pkgs,
    ...
  }: let
    rustPkgs = pkgs.rustBuilder.makePackageSet {
      extraRustComponents = ["clippy" "rustfmt"];
      packageFun = import ./Cargo.nix;
      rustVersion = "latest";
    };
  in {
    devShells.sv = pkgs.mkShell {
      buildInputs = [
        pkgs.openssl
        pkgs.pkg-config
        pkgs.rust-bin.stable.latest.default
      ];
    };
    packages.sv = (rustPkgs.workspace.sv {}).bin;
  };
}
