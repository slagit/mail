# SSH Vault Client

The `sv` program generates an SSH keypair and obtains an SSH certificate
for the keypair from a HashiCorp Vault. An `ssh` instance is launched
configured with an ssh agent containing the certificate and private key.

Connection information for the vault connection will be taken from the
`VAULT_ADDR` and `VAULT_TOKEN` environment variables.

## Usage

```text
sv [options] -- <ssh options ...>
```

**Options:**

- `--mount-path` - Vault path for ssh certificate authority. Defaults to `ssh-client-signer`.
- `--principal` - Ssh principal for credential. Defaults to the current users username.
- `--role` - Vault role for ssh credential. Defaults to `user`.

**Example:**

To ssh to `demo.slagit.net` with the default options, run the following:

```bash
export VAULT_ADDR=https://slagit-vault.net:8200
export VAULT_TOKEN=my_vault_token
sv demo.slagit.net
```
