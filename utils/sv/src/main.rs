use clap::Parser;
use std::{fmt::Debug, os::unix::process::ExitStatusExt};
use sv::{
    agent::Agent,
    keycloak::{Manager, OpenidConfiguration},
    keystore::KeystoreHandler,
    vault_manager::VaultManager,
};
use tempdir::TempDir;
use tokio::process::Command;
use tracing::Level;
use tracing_subscriber::FmtSubscriber;
use whoami::username;

#[derive(Parser, Debug)]
struct Args {
    /// Vault path for ssh certificate authority
    #[arg(long, default_value = "ssh-client-signer")]
    mount_path: String,

    /// Ssh principal for credential
    #[arg(long, default_value_t = username())]
    principal: String,

    /// Program to launch E.g. ssh, scp, sftp
    #[arg(long, default_value = "ssh")]
    program: String,

    /// Vault role for ssh credential
    #[arg(long, default_value = "user")]
    role: String,

    args: Vec<String>,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    let stderr_log = FmtSubscriber::builder()
        .with_max_level(Level::WARN)
        .finish();
    tracing::subscriber::set_global_default(stderr_log).unwrap();

    let config = OpenidConfiguration::load().await.unwrap();
    let mgr = VaultManager::new(Manager::new((&config).into())).unwrap();

    let tmp_dir = TempDir::new("sv").unwrap();
    let socket_path = tmp_dir.path().join("agent.sock");

    let agent = Agent::start(
        &socket_path,
        KeystoreHandler::new(mgr, &args.mount_path, &args.principal, &args.role),
    )
    .await;

    let mut child = Command::new(args.program);
    child.env("SSH_AUTH_SOCK", &socket_path);
    for arg in &args.args {
        child.arg(arg);
    }
    let child = child.status().await.unwrap();

    agent.shutdown().await;

    println!("{}", child.into_raw());
}
