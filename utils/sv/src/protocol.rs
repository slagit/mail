use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use std::{
    io::{Read, Write},
    string::FromUtf8Error,
};
use thiserror::Error;
use tokio::io::{
    AsyncBufRead, AsyncBufReadExt, AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt,
};

pub struct Reader<R: AsyncBufRead + AsyncRead + Unpin> {
    reader: R,
}

impl<R: AsyncBufRead + AsyncRead + Unpin> Reader<R> {
    pub fn new(reader: R) -> Self {
        Reader { reader }
    }
    pub async fn read_message(&mut self) -> Result<Vec<u8>, RequestError> {
        let peek = self.reader.fill_buf().await?;
        if peek.is_empty() {
            Err(RequestError::EOF)
        } else {
            let length = self.reader.read_u32().await?;
            if length < 1 {
                todo!();
            }
            let mut buffer = vec![0; length as usize];
            self.reader.read_exact(&mut buffer).await?;
            Ok(buffer)
        }
    }
}

pub fn read_string<R: Read>(reader: &mut R) -> std::io::Result<Vec<u8>> {
    let len = reader.read_u32::<BigEndian>()?;
    let mut result = vec![0u8; len as usize];
    reader.read_exact(&mut result)?;
    Ok(result)
}

pub fn write_string<W: Write>(writer: &mut W, value: &[u8]) -> std::io::Result<()> {
    WriteBytesExt::write_u32::<BigEndian>(writer, value.len() as u32)?;
    writer.write_all(value)
}

pub struct Writer<W: AsyncWrite + Unpin> {
    writer: W,
}

impl<W: AsyncWrite + Unpin> Writer<W> {
    pub fn new(writer: W) -> Self {
        Writer { writer }
    }
    pub async fn write_message(&mut self, buf: &[u8]) -> std::io::Result<()> {
        self.writer.write_u32(buf.len() as u32).await?;
        self.writer.write_all(buf).await?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct RequestIdentities {}

impl TryFrom<&[u8]> for RequestIdentities {
    type Error = RequestError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        check_empty(value)?;
        Ok(Self {})
    }
}

#[derive(Debug)]
pub struct Key {
    pub key_blob: Vec<u8>,
    pub comment: String,
}

#[derive(Debug)]
pub struct IdentitiesAnswer {
    pub keys: Vec<Key>,
}

impl From<IdentitiesAnswer> for Vec<u8> {
    fn from(value: IdentitiesAnswer) -> Self {
        let mut buf = vec![];
        WriteBytesExt::write_u8(&mut buf, 12).unwrap();
        WriteBytesExt::write_u32::<BigEndian>(&mut buf, value.keys.len() as u32).unwrap();
        for key in &value.keys {
            write_string(&mut buf, key.key_blob.as_ref()).unwrap();
            write_string(&mut buf, key.comment.as_bytes()).unwrap();
        }
        buf
    }
}

#[derive(Debug)]
pub struct SignRequest {
    pub key_blob: Vec<u8>,
    pub data: Vec<u8>,
    pub flags: u32,
}

impl TryFrom<&[u8]> for SignRequest {
    type Error = RequestError;

    fn try_from(mut value: &[u8]) -> Result<Self, Self::Error> {
        let value = &mut value;
        let key_blob = read_string(value)?;
        let data = read_string(value)?;
        let flags = ReadBytesExt::read_u32::<BigEndian>(value)?;
        check_empty(value)?;
        Ok(Self {
            key_blob,
            data,
            flags,
        })
    }
}

#[derive(Debug)]
pub struct SignResponse {
    pub signature: Vec<u8>,
}

impl From<SignResponse> for Vec<u8> {
    fn from(value: SignResponse) -> Self {
        let mut sigbuf = vec![];
        write_string(&mut sigbuf, "ssh-ed25519".as_ref()).unwrap();
        write_string(&mut sigbuf, value.signature.as_ref()).unwrap();

        let mut buf = vec![];
        WriteBytesExt::write_u8(&mut buf, 14).unwrap();
        write_string(&mut buf, &sigbuf).unwrap();
        buf
    }
}

#[derive(Debug)]
pub struct AgentFailure;

impl From<AgentFailure> for Vec<u8> {
    fn from(_: AgentFailure) -> Self {
        vec![5]
    }
}

#[derive(Debug)]
pub enum Request {
    RequestIdentities(RequestIdentities),
    SignRequest(SignRequest),
}

impl TryFrom<&[u8]> for Request {
    type Error = RequestError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        match value[0] {
            11 => Ok(Self::RequestIdentities(value[1..].try_into()?)),
            13 => Ok(Self::SignRequest(value[1..].try_into()?)),
            t => Err(RequestError::Type(t)),
        }
    }
}

#[derive(Debug, Error)]
pub enum RequestError {
    #[error("utf8 encoding error")]
    FromUtf8Error(#[from] FromUtf8Error),

    #[error("i/o error")]
    Io(#[from] std::io::Error),

    #[error("unsupported message type ({0})")]
    Type(u8),

    #[error("key not found")]
    KeyNotFound,

    #[error("extra bytes in message")]
    ExtraBytes,

    #[error("signature error")]
    Signature(#[from] osshkeys::error::Error),

    #[error("end of file")]
    EOF,
}

fn check_empty(value: &[u8]) -> Result<(), RequestError> {
    if value.is_empty() {
        Ok(())
    } else {
        Err(RequestError::ExtraBytes)
    }
}
