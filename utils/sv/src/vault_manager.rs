use crate::keycloak::Manager;
use std::{ops::Add, sync::Arc, time::SystemTime};
use tokio::{
    sync::{RwLock, RwLockReadGuard},
    time::{sleep, Duration},
};
use vaultrs::{
    auth::oidc::login,
    client::{Client, VaultClient, VaultClientSettingsBuilder, VaultClientSettingsBuilderError},
    error::ClientError,
};

#[derive(Debug, thiserror::Error)]
pub enum VaultManagerError {
    #[error("Vault client error: {0}")]
    ClientError(#[from] ClientError),
    #[error("Failed to get valid jwt: {0}")]
    JWT(#[from] crate::keycloak::Error),
    #[error("Failed to configure vault client: {0}")]
    VaultClientSettingsBuilderError(#[from] VaultClientSettingsBuilderError),
}

const REFRESH_GRACE_PERIOD: u64 = 10;

pub struct VaultManagerState {
    client: VaultClient,
    expire: Option<SystemTime>,
}

impl VaultManagerState {
    fn set_lease_duration(&mut self, lease_duration: u64) {
        if lease_duration < REFRESH_GRACE_PERIOD {
            self.expire = None;
        } else {
            self.expire = Some(SystemTime::now().add(Duration::from_secs(lease_duration - 10)));
        }
    }
}

impl AsRef<VaultClient> for VaultManagerState {
    fn as_ref(&self) -> &VaultClient {
        &self.client
    }
}

pub struct VaultManager {
    openid: Arc<Manager>,
    state: Arc<RwLock<VaultManagerState>>,
}

impl VaultManager {
    pub fn new(openid: Manager) -> Result<Self, VaultManagerError> {
        Ok(Self {
            openid: openid.into(),
            state: Arc::new(
                VaultManagerState {
                    client: VaultClient::new(VaultClientSettingsBuilder::default().build()?)?,
                    expire: None,
                }
                .into(),
            ),
        })
    }
    pub async fn as_client(
        &mut self,
    ) -> Result<RwLockReadGuard<VaultManagerState>, VaultManagerError> {
        let mut state = self.state.write().await;
        if state.expire.is_none() {
            let token = self.openid.access_token(false).await?;
            let user = match login(
                &state.client,
                "jwt",
                token.secret(),
                Some("ssh-user".to_owned()),
            )
            .await
            {
                Err(ClientError::APIError { code: 400, .. }) => {
                    let token = self.openid.access_token(true).await?;
                    login(
                        &state.client,
                        "jwt",
                        token.secret(),
                        Some("ssh-user".to_owned()),
                    )
                    .await
                }
                u => u,
            }?;
            state.client.set_token(&user.client_token);
            state.set_lease_duration(user.lease_duration);
            tokio::spawn(keep_alive(self.state.clone()));
        }
        Ok(state.downgrade())
    }
}

async fn keep_alive(state: Arc<RwLock<VaultManagerState>>) {
    loop {
        let expire = state.read().await.expire;
        match expire {
            Some(expire) => {
                sleep(
                    expire
                        .duration_since(SystemTime::now())
                        .unwrap_or(Duration::ZERO),
                )
                .await;
                let mut state = state.write().await;
                match state.client.renew(None).await {
                    Ok(user) => {
                        state.set_lease_duration(user.lease_duration);
                    }
                    _ => {
                        state.expire = None;
                        return;
                    }
                }
            }
            None => return,
        }
    }
}
