use oauth2::{
    basic::{BasicErrorResponse, BasicRequestTokenError, BasicTokenType},
    reqwest::{async_http_client, AsyncHttpClientError},
    AccessToken, AuthUrl, Client, ClientId, DeviceAuthorizationUrl, DeviceCodeErrorResponse,
    ExtraTokenFields, RefreshToken, RequestTokenError, Scope, StandardDeviceAuthorizationResponse,
    StandardRevocableToken, StandardTokenIntrospectionResponse, StandardTokenResponse,
    TokenResponse, TokenUrl,
};
use serde::{Deserialize, Serialize};
use std::{future::Future, process::Command, sync::Arc, time::Duration};
use tokio::{
    select,
    sync::Mutex,
    task::JoinHandle,
    time::{sleep, Instant},
};
use tokio_util::sync::CancellationToken;

#[derive(Debug, Deserialize, Serialize)]
pub struct KeycloakExtraTokenFields {
    refresh_expires_in: Option<u64>,
}

impl KeycloakExtraTokenFields {
    fn refresh_expires_in(&self) -> Option<Duration> {
        self.refresh_expires_in.map(Duration::from_secs)
    }
}

impl ExtraTokenFields for KeycloakExtraTokenFields {}

type KeycloakTokenResponse = StandardTokenResponse<KeycloakExtraTokenFields, BasicTokenType>;
type KeycloakTokenIntrospectionResponse =
    StandardTokenIntrospectionResponse<KeycloakExtraTokenFields, BasicTokenType>;
type KeycloakClient = Client<
    BasicErrorResponse,
    KeycloakTokenResponse,
    BasicTokenType,
    KeycloakTokenIntrospectionResponse,
    StandardRevocableToken,
    oauth2::basic::BasicRevocationErrorResponse,
>;

#[derive(Debug, Deserialize)]
pub struct OpenidConfiguration {
    authorization_endpoint: AuthUrl,
    device_authorization_endpoint: DeviceAuthorizationUrl,
    token_endpoint: TokenUrl,
}

impl OpenidConfiguration {
    pub async fn load() -> Result<Self, reqwest::Error> {
        reqwest::get("https://auth.slagit.net/realms/slagit/.well-known/openid-configuration")
            .await?
            .error_for_status()?
            .json()
            .await
    }
}

impl From<&OpenidConfiguration> for KeycloakClient {
    fn from(config: &OpenidConfiguration) -> Self {
        KeycloakClient::new(
            ClientId::new("sv".to_owned()),
            None,
            config.authorization_endpoint.clone(),
            Some(config.token_endpoint.clone()),
        )
        .set_device_authorization_url(config.device_authorization_endpoint.clone())
    }
}

fn open_browser(details: &StandardDeviceAuthorizationResponse) {
    let mut url = details.verification_uri().url().clone();
    let query = format!("user_code={}", details.user_code().secret());
    url.set_query(Some(&query));
    match Command::new("xdg-open").arg(url.as_str()).status() {
        Ok(status) if status.success() => {
            println!("SSO authorization page has automatically been opened in your default browser.\nFollow the instructions in the browser to complete this authorization request.");
        }
        _ => {
            println!(
                "Using a browser, open the following URL:\n{}\nand enter the following code:\n{}",
                **details.verification_uri(),
                details.user_code().secret()
            );
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Error requesting token {0}")]
    RequestTokenError(#[from] BasicRequestTokenError<AsyncHttpClientError>),
    #[error("Error requesting token {0}")]
    DeviceRequestTokenError(
        #[from] RequestTokenError<AsyncHttpClientError, DeviceCodeErrorResponse>,
    ),
}

#[derive(Debug)]
struct TokenState<T> {
    expires: Instant,
    token: T,
}

#[derive(Debug)]
struct KeepAlive<T> {
    handle: JoinHandle<T>,
    token: CancellationToken,
}

impl<T> KeepAlive<T>
where
    T: Send + 'static,
{
    fn spawn<C, F>(future: C) -> Self
    where
        C: FnOnce(CancellationToken) -> F,
        F: Future<Output = T> + Send + 'static,
    {
        let token = CancellationToken::new();
        let cloned_token = token.clone();
        Self {
            handle: tokio::spawn(future(cloned_token)),
            token,
        }
    }

    async fn join(self) -> T {
        self.token.cancel();
        self.handle.await.unwrap()
    }
}

#[derive(Debug)]
struct State {
    access: Option<TokenState<AccessToken>>,
    client: KeycloakClient,
    keep_alive: Option<KeepAlive<Option<()>>>,
    refresh: Option<TokenState<RefreshToken>>,
}

impl State {
    fn new(client: KeycloakClient) -> Self {
        Self {
            access: None,
            client,
            keep_alive: None,
            refresh: None,
        }
    }
    fn access_token(&self) -> Option<&AccessToken> {
        self.access
            .as_ref()
            .filter(|access| access.expires > Instant::now())
            .map(|access| &access.token)
    }

    fn update(&mut self, response: &KeycloakTokenResponse) {
        self.access = response.expires_in().map(|expires_in| TokenState {
            expires: Instant::now() + expires_in - Duration::from_secs(10),
            token: response.access_token().clone(),
        });
        self.refresh = response.refresh_token().and_then(|refresh_token| {
            response
                .extra_fields()
                .refresh_expires_in()
                .or(response.expires_in())
                .map(|refresh_expires_in| refresh_expires_in - Duration::from_secs(10))
                .filter(|refresh_expires_in| refresh_expires_in > &Duration::ZERO)
                .map(|refresh_expires_in| TokenState {
                    expires: Instant::now() + refresh_expires_in,
                    token: refresh_token.clone(),
                })
        });
    }
}

pub struct Manager {
    scopes: Vec<Scope>,
    state: Mutex<State>,
}

impl Manager {
    pub fn new(client: KeycloakClient) -> Self {
        Self {
            scopes: vec![Scope::new("ssh-user".to_owned())],
            state: State::new(client).into(),
        }
    }
    pub async fn access_token(self: &Arc<Self>, new_session: bool) -> Result<AccessToken, Error> {
        let mut state = self.state.lock().await;

        if !new_session {
            if let Some(access_token) = state.access_token() {
                return Ok(access_token.clone());
            }
        }

        if let Some(keep_alive) = state.keep_alive.take() {
            keep_alive.join().await;
        }

        if !new_session {
            if let Some(TokenState {
                token: refresh_token,
                ..
            }) = state.refresh.as_ref()
            {
                let response = state
                    .client
                    .exchange_refresh_token(refresh_token)
                    .request_async(async_http_client)
                    .await?;
                state.update(&response);
                if state.refresh.is_some() {
                    state.keep_alive =
                        Some(KeepAlive::spawn(|token| self.clone().keep_alive(token)));
                }
                return Ok(response.access_token().clone());
            }
        }

        let details: StandardDeviceAuthorizationResponse = state
            .client
            .exchange_device_code()
            .unwrap()
            .add_scopes(self.scopes.clone())
            .request_async(async_http_client)
            .await?;
        open_browser(&details);
        let response = state
            .client
            .exchange_device_access_token(&details)
            .request_async(async_http_client, tokio::time::sleep, None)
            .await?;
        state.update(&response);
        if state.refresh.is_some() {
            state.keep_alive = Some(KeepAlive::spawn(|token| self.clone().keep_alive(token)));
        }
        Ok(response.access_token().clone())
    }

    async fn keep_alive(self: Arc<Self>, token: CancellationToken) -> Option<()> {
        loop {
            let expires = {
                let state = select! {
                    _ = token.cancelled() => None,
                    s = self.state.lock() => Some(s),
                }?;
                state.refresh.as_ref()?.expires
            };
            select! {
                _ = token.cancelled() => return None,
                _ = sleep(expires.duration_since(Instant::now())) => {},
            }
            let mut state = select! {
                _ = token.cancelled() => return None,
                state = self.state.lock() => state,
            };
            let response = state
                .client
                .exchange_refresh_token(&state.refresh.as_ref()?.token)
                .request_async(async_http_client)
                .await
                .unwrap();
            state.update(&response);
        }
    }
}
