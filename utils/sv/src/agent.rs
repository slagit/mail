use crate::protocol::{
    AgentFailure, IdentitiesAnswer, Reader, Request, RequestError, RequestIdentities, SignRequest,
    SignResponse, Writer,
};
use async_trait::async_trait;
use std::{fmt::Debug, path::Path};
use tokio::{
    io::{split, BufReader},
    net::{UnixListener, UnixStream},
    select, spawn,
    sync::mpsc::{channel, Receiver, Sender},
};
use tokio_util::sync::CancellationToken;
use tracing::{debug, error, info, instrument};

pub struct Agent {
    shutdown: Receiver<()>,
    token: CancellationToken,
}

impl Agent {
    #[instrument]
    pub async fn start<P: AsRef<Path> + std::fmt::Debug, H: Handler>(path: P, handler: H) -> Self {
        let listener = UnixListener::bind(path).unwrap();
        info!("Agent listening on unix socket");
        let token = CancellationToken::new();
        let (send, recv) = channel(1);
        spawn(run(AgentState {
            handler,
            listener,
            shutdown: send,
            token: token.clone(),
        }));
        Agent {
            shutdown: recv,
            token,
        }
    }

    pub async fn shutdown(mut self) {
        debug!("Agent shutdown requested");
        self.token.cancel();
        let _ = self.shutdown.recv().await;
        info!("Agent shutdown complete");
    }
}

struct AgentConnection<H: Handler> {
    handler: H,
    _shutdown: Sender<()>,
    stream: UnixStream,
}

#[async_trait]
pub trait Handler: Clone + Debug + Send + Sync + 'static {
    async fn request_identities(
        &self,
        request: &RequestIdentities,
    ) -> Result<IdentitiesAnswer, RequestError>;
    async fn sign_request(&self, request: &SignRequest) -> Result<SignResponse, RequestError>;

    async fn handle(&self, request: &Request) -> Result<Vec<u8>, RequestError> {
        match request {
            Request::RequestIdentities(req) => Ok(self.request_identities(req).await?.into()),
            Request::SignRequest(req) => Ok(self.sign_request(req).await?.into()),
        }
    }
}

async fn handle<H: Handler>(conn: AgentConnection<H>) {
    let (reader, writer) = split(conn.stream);
    let mut reader = Reader::new(BufReader::new(reader));
    let mut writer = Writer::new(writer);
    loop {
        let request: Result<Request, RequestError> =
            reader.read_message().await.and_then(|x| x[..].try_into());
        let result: Result<Vec<u8>, RequestError> = match request {
            Ok(req) => conn.handler.handle(&req).await,
            Err(RequestError::EOF) => {
                break;
            }
            Err(err) => Err(err),
        };
        let response = result.unwrap_or_else(|err| {
            error!(error = err.to_string(), "error handling agent request");
            AgentFailure {}.into()
        });
        writer.write_message(&response).await.unwrap();
    }
}

#[derive(Debug)]
struct AgentState<H: Handler> {
    handler: H,
    listener: UnixListener,
    shutdown: Sender<()>,
    token: CancellationToken,
}

#[instrument]
async fn run<H: Handler>(state: AgentState<H>) {
    info!("Starting agent worker task");
    loop {
        debug!(
            is_cancelled = state.token.is_cancelled(),
            "Starting agent worker loop"
        );
        select! {
            _ = state.token.cancelled() => {
                debug!("Agent worker loop cancelled");
                return;
            }
            connection = state.listener.accept() => {
                match connection {
                    Ok((stream, _)) => {
                        info!("Accepted agent connection");
                        spawn(handle(AgentConnection {
                            handler: state.handler.clone(),
                            _shutdown: state.shutdown.clone(),
                            stream,
                        }));
                    }
                    Err(err) => {
                        error!(error = err.to_string(), "Error accepting agent connections");
                        return;
                    }
                }
            }
        }
    }
}
