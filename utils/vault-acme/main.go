package main

import (
	"slagit.net/mail/utils/vault-acme/cmd"
)

func main() {
	cmd.Execute()
}
