package cmd

import (
	"context"
	"strconv"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"slagit.net/mail/utils/vault-acme/mgr"
)

var rotateCmd = &cobra.Command{
	Use: "rotate [<existing key>]",
	Short: "Rotate an ACME account using a vault transit key",
	Args: cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		logger, _ := zap.NewDevelopment()
		defer logger.Sync()

		undo := zap.ReplaceGlobals(logger)
		defer undo()

		manager, err := mgr.FromPath(cfgFile)
		if err != nil {
			panic(err)
		}

		if len(args) > 0 {
			key_version, err := strconv.ParseInt(args[0], 10, 64)
			if err != nil {
				panic(err)
			}
			manager.SignerBuilder.SetKeyVersion(key_version)
		}

		if err := manager.Rotate(context.Background()); err != nil {
			panic(err)
		}
	},
}
