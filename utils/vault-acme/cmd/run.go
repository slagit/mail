package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"slagit.net/mail/utils/vault-acme/mgr"
)

var runCmd = &cobra.Command{
	Use: "run",
	Short: "Generate or renew certificates",
	Run: func(cmd *cobra.Command, args []string) {
		logger, _ := zap.NewDevelopment()
		defer logger.Sync()

		undo := zap.ReplaceGlobals(logger)
		defer undo()

		cfg, err := mgr.LoadConfig(cfgFile)
		if err != nil {
			panic(err)
		}

		manager, err := mgr.New(cfg)
		if err != nil {
			panic(err)
		}

		for name, crt := range cfg.Certificates {
			if err := manager.RunOrder(context.Background(), name, crt); err != nil {
				zap.S().Errorw("Error processing certificate request", "name", name, "error", err)
			}
		}
	},
}
