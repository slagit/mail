package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"slagit.net/mail/utils/vault-acme/mgr"
)

var initCmd = &cobra.Command{
	Use: "init",
	Short: "Create an ACME account using a vault transit key",
	Run: func(cmd *cobra.Command, args []string) {
		logger, _ := zap.NewDevelopment()
		defer logger.Sync()

		undo := zap.ReplaceGlobals(logger)
		defer undo()

		manager, err := mgr.FromPath(cfgFile)
		if err != nil {
			panic(err)
		}
		if err := manager.Init(context.Background()); err != nil {
			panic(err)
		}
	},
}
