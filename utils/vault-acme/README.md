# Vault Acme Client

To run, with `VAULT_TOKEN` set:

```bash
nix run .#vault-acme -- --config utils/vault-acme/config.yaml run
```

## Adding a new certificate

1. Add an entry to the configuration file for the certificate
1. Add an entry to the configuration file for each domain
1. Add a CNAME for `_acme-challenge` matching the configuration for each domain
