package mgr

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"strings"
	"time"

	"go.uber.org/zap"
)

func NewCSR(keytype string, names []string) ([]byte, crypto.PrivateKey, error) {
	zap.S().Infow("Generating private key")
	var private_key crypto.PrivateKey
	var err error
	if keytype == "RSA" {
		private_key, err = rsa.GenerateKey(rand.Reader, 2048)
	} else if keytype == "EC" {
		private_key, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	} else {
		return nil, nil, fmt.Errorf("Unsupported private key type: %s", keytype)
	}
	if err != nil {
		return nil, nil, err
	}
	zap.S().Infow("Generating CSR", "names", names)
	csr, err := x509.CreateCertificateRequest(rand.Reader, &x509.CertificateRequest{
		DNSNames: names,
	}, private_key)
	if err != nil {
		return nil, nil, err
	}
	return csr, private_key, nil
}

func CertificateNeedsRenewal(crt_pem string) (bool, error) {
	crt_der, _ := pem.Decode([]byte(crt_pem))
	if crt_der == nil || crt_der.Type != "CERTIFICATE" {
		return false, fmt.Errorf("No CERTIFICATE block found parsing existing certificate")
	}
	crt, err := x509.ParseCertificate(crt_der.Bytes)
	if err != nil {
		return false, err
	}
	zap.S().Infow("Existing certificate", "expires", crt.NotAfter)
	total := crt.NotAfter.Sub(crt.NotBefore).Seconds()
	current := time.Now().Sub(crt.NotBefore).Seconds()
	progress := current / total
	return progress > 0.66, nil
}

func EncodeCertificates(bundle [][]byte) string {
	var builder strings.Builder

	for _, b := range bundle {
		pem.Encode(&builder, &pem.Block{
			Type: "CERTIFICATE",
			Bytes: b,
		})
	}

	return builder.String()
}

func EncodePrivateKey(private_key crypto.PrivateKey) (string, error) {
	der, err := x509.MarshalPKCS8PrivateKey(private_key)
	if err != nil {
		return "", err
	}
	return string(pem.EncodeToMemory(&pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: der,
	})), nil
}
