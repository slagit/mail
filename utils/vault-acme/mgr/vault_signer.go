package mgr

import (
	"crypto"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/hashicorp/vault/api"
	"go.uber.org/zap"
)

type VaultSigner struct {
	client     *api.Client
	public_key crypto.PublicKey
	sign_path  string
	key_version int64
}

func (vs *VaultSigner) Public() crypto.PublicKey {
	return vs.public_key
}

func (vs *VaultSigner) Sign(rand io.Reader, digest []byte, opts crypto.SignerOpts) ([]byte, error) {
	zap.S().Debugw("Signing ACME request", "key_version", vs.key_version)
	s, err := vs.client.Logical().Write(vs.sign_path, map[string]interface{}{
		"input":     base64.StdEncoding.EncodeToString(digest),
		"key_version": vs.key_version,
		"prehashed": true,
	})
	if err != nil {
		return nil, err
	}
	sig, ok := s.Data["signature"].(string)
	if !ok {
		return nil, fmt.Errorf("Invalid signature returned from vault")
	}
	parts := strings.Split(sig, ":")
	if len(parts) != 3 || parts[0] != "vault" {
		return nil, fmt.Errorf("Signature was not in vault format")
	}
	zap.S().Debugw("Request signed", "key", parts[1])
	part := parts[2]
	return base64.StdEncoding.DecodeString(part)
}

type VaultSignerBuilder struct {
	Client *api.Client
	Mount  string
	Name   string
	key_version int64
}

func (b *VaultSignerBuilder) Build() (crypto.Signer, error) {
	zap.S().Infow("Initializing transit engine", "mount", b.Mount, "name", b.Name)
	s, err := b.Client.Logical().Read(fmt.Sprintf("%s/keys/%s", b.Mount, b.Name))
	if err != nil {
		return nil, err
	}
	key_version, public_pem, err := getLastPublicKey(s, b.key_version)
	if err != nil {
		return nil, err
	}
	public_der, _ := pem.Decode([]byte(public_pem))
	if public_der == nil || public_der.Type != "PUBLIC KEY" {
		return nil, fmt.Errorf("Error loading public key")
	}
	public_key, err := x509.ParsePKIXPublicKey(public_der.Bytes)
	if err != nil {
		return nil, err
	}
	return &VaultSigner{
		client:     b.Client,
		public_key: public_key,
		sign_path:  fmt.Sprintf("%s/sign/%s", b.Mount, b.Name),
		key_version: key_version,
	}, nil
}

func (b *VaultSignerBuilder) Rotate() error {
	_, err := b.Client.Logical().Write(fmt.Sprintf("%s/keys/%s/rotate", b.Mount, b.Name), nil)
	if err != nil {
		return err
	}
	b.key_version = 0
	return nil
}

func (b *VaultSignerBuilder) SetKeyVersion(version int64) {
	b.key_version = version
}

func getLastPublicKey(s *api.Secret, key_version int64) (int64, string, error) {
	keys, ok := s.Data["keys"].(map[string]interface{})
	if !ok {
		return 0, "", fmt.Errorf("No keys returned from vault")
	}
	var last_version int64
	var last_key string
	for vstr, k := range keys {
		if v, err := strconv.ParseInt(vstr, 10, 64); err != nil {
			zap.S().Warnw("Skipping invalid key version from vault", "key_version", vstr)
		} else if v > last_version {
			if kmap, ok := k.(map[string]interface{}); !ok {
				zap.S().Warnw("Skiping key with unknown public key", "key_version", v)
			} else if kstr, ok := kmap["public_key"].(string); !ok {
				zap.S().Warnw("Skiping key with unknown public key", "key_version", v)
			} else if key_version == 0 || key_version == v {
				last_version = v
				last_key = kstr
			}
		}
	}
	if last_version == 0 {
		return 0, "", fmt.Errorf("No valid keys returned from vault")
	}
	return last_version, last_key, nil
}
