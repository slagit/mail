package mgr

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/route53"
	"github.com/aws/aws-sdk-go-v2/service/route53/types"
	"github.com/hashicorp/vault/api"
	"go.uber.org/zap"
)

type VaultCredentialsProvider struct {
	Client *api.Client
	Role string
	Mount string
}

func (p *VaultCredentialsProvider) Retrieve(ctx context.Context) (aws.Credentials, error) {
	s, err := p.Client.Logical().Write(fmt.Sprintf("%s/sts/%s", p.Mount, p.Role), nil)
	if err != nil {
		return aws.Credentials{}, err
	}
	access_key_id, ok := s.Data["access_key"].(string)
	if !ok {
		return aws.Credentials{}, fmt.Errorf("No AWS access key id returned from vault")
	}
	secret_access_key, ok := s.Data["secret_key"].(string)
	if !ok {
		return aws.Credentials{}, fmt.Errorf("No AWS secret access key returned from vault")
	}
	security_token, ok := s.Data["security_token"].(string)
	if !ok {
		return aws.Credentials{}, fmt.Errorf("No AWS security token returned from vault")
	}
	return aws.Credentials{
		AccessKeyID: access_key_id,
		SecretAccessKey: secret_access_key,
		SessionToken: security_token,
		CanExpire: true,
		Expires: time.Now().Add(time.Duration(s.LeaseDuration) * time.Second),
	}, nil
}

type Domain struct {
	RecordName string
	ZoneId string
}

type AwsDNS struct {
	CredentialsProvider aws.CredentialsProvider
	Domains map[string]Domain
	Endpoint *string
	Region string

	svc *route53.Client
}

func (t *AwsDNS) SetTXT(ctx context.Context, host string, value string) error {
	ci, err := t.runChange(ctx, host, value, types.ChangeActionCreate)
	if err != nil {
		return err
	}
	return t.waitChange(ctx, ci)
}

func (t *AwsDNS) ClearTXT(ctx context.Context, host string, value string) error {
	_, err := t.runChange(ctx, host, value, types.ChangeActionDelete)
	return err
}

func (t *AwsDNS) getSvc(ctx context.Context) (*route53.Client, error) {
	if (t.svc == nil) {
		cfg, err := config.LoadDefaultConfig(ctx, config.WithCredentialsProvider(t.CredentialsProvider), config.WithRegion(t.Region))
		if err != nil {
			return nil, err
		}
		t.svc = route53.NewFromConfig(cfg, func (o *route53.Options) {
			if t.Endpoint != nil {
				o.BaseEndpoint = t.Endpoint
			}
		})
	}
	return t.svc, nil
}

func (t *AwsDNS) runChange(ctx context.Context, host string, value string, action types.ChangeAction) (*types.ChangeInfo, error) {
	domain, ok := t.Domains[host]
	if !ok {
		return nil, fmt.Errorf("No DNS domain configuration for %s", host)
	}

	svc, err := t.getSvc(ctx)
	if err != nil {
		return nil, err
	}

	zap.S().Infow("Running DNS change", "host", host, "value", value, "action", action, "record_name", domain.RecordName, "zone_id", domain.ZoneId)
	out, err := svc.ChangeResourceRecordSets(ctx, &route53.ChangeResourceRecordSetsInput{
		ChangeBatch: &types.ChangeBatch{
			Changes: []types.Change{
				{
					Action: action,
					ResourceRecordSet: &types.ResourceRecordSet {
						Name: &domain.RecordName,
						Type: types.RRTypeTxt,
						TTL: aws.Int64(60),
						ResourceRecords: []types.ResourceRecord{
							{
								Value: aws.String(fmt.Sprintf("\"%s\"", value)),
							},
						},
					},
				},
			},
		},
		HostedZoneId: &domain.ZoneId,
	})
	if err != nil {
		return nil, err
	}
	return out.ChangeInfo, nil
}

func (t *AwsDNS) waitChange(ctx context.Context, ci *types.ChangeInfo) error {
	svc, err := t.getSvc(ctx)
	if err != nil {
		return err
	}

	zap.S().Infow("Waiting for DNS change to propagate", "id", *ci.Id)
	for ci.Status == types.ChangeStatusPending {
		time.Sleep(1 * time.Second)
		out, err := svc.GetChange(context.Background(), &route53.GetChangeInput{
			Id: ci.Id,
		})
		if err != nil {
			return err
		}
		ci = out.ChangeInfo
	}
	return nil
}
