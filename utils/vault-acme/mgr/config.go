package mgr

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
	"go.uber.org/zap"
)

type ConfigAcme struct {
	CaCert    string
	Contact   []string
	Directory string
}

func (c *ConfigAcme) validate() bool {
	valid := true
	if c.Directory == "" {
		c.Directory = "https://acme-v02.api.letsencrypt.org/directory"
	}
	return valid
}

type ConfigCertificate struct {
	Domains []string
	Type string
}

func (c *ConfigCertificate) validate(name string) bool {
	valid := true
	if len(c.Domains) == 0 {
		zap.S().Errorw("Certificate has no domains", "name", name)
		valid = false
	}
	if c.Type == "" {
		c.Type = "EC"
	}
	if c.Type != "RSA" && c.Type != "EC" {
		zap.S().Errorw("Unsupported certiticate type", "name", name, "type", c.Type)
		valid = false
	}
	return valid
}

type ConfigDNS struct {
	Domains map[string]Domain
	Endpoint *string
	Region string
	VaultMount string
	VaultRole string
}

func (c *ConfigDNS) validate() bool {
	valid := true
	if c.VaultMount == "" {
		c.VaultMount = "aws"
	}
	if c.Region == "" {
		zap.S().Errorw("DNS region not configured")
		valid = false
	}
	if c.VaultRole == "" {
		zap.S().Errorw("DNS vault role not configured")
		valid = false
	}
	for name, domain := range c.Domains {
		if domain.RecordName == "" {
			zap.S().Errorw("DNS record name not configured", "name", name)
			valid = false;
		}
		if domain.ZoneId == "" {
			zap.S().Errorw("DNS zone id not configured", "name", name)
			valid = false;
		}
	}
	return valid
}

type ConfigVault struct {
	Transit ConfigVaultTransit
	Store   ConfigVaultStore
}

func (c *ConfigVault) validate() bool {
	valid := true
	valid = c.Transit.validate() && valid
	valid = c.Store.validate() && valid
	return valid
}

type ConfigVaultTransit struct {
	Mount string
	Name  string
}

func (c *ConfigVaultTransit) validate() bool {
	valid := true
	if c.Mount == "" {
		c.Mount = "transit"
	}
	if c.Name == "" {
		zap.S().Errorw("Vault transit key name not set")
		valid = false
	}
	return valid
}

type ConfigVaultStore struct {
	Mount string
}

func (c *ConfigVaultStore) validate() bool {
	valid := true
	if c.Mount == "" {
		c.Mount = "kv"
	}
	return valid
}

type Config struct {
	Acme         ConfigAcme
	Certificates map[string]*ConfigCertificate
	DNS          ConfigDNS
	Vault        ConfigVault
}

func (c *Config) validate() bool {
	valid := true
	valid = c.Acme.validate() && valid
	for name, certificate := range c.Certificates {
		valid = certificate.validate(name) && valid
	}
	valid = c.DNS.validate() && valid
	valid = c.Vault.validate() && valid
	return valid
}

func LoadConfig(name string) (*Config, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	var cfg Config
	if err := yaml.Unmarshal(b, &cfg); err != nil {
		return nil, err
	}
	if !cfg.validate() {
		return nil, fmt.Errorf("Invalid Configuration")
	}
	return &cfg, nil
}
