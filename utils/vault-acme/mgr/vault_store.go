package mgr

import (
	"context"
	"fmt"
	"strings"

	"github.com/hashicorp/vault/api"
)

type VaultStore struct {
	Client     *api.Client
	Mount string
}

func (vs *VaultStore) Load(ctx context.Context, name string, key string) (string, error) {
	s, err := vs.Client.KVv2(vs.Mount).Get(ctx, name)
	if err != nil {
		if strings.HasPrefix(err.Error(), "secret not found:") {
			return "", nil
		}
		return "", err
	}
	if s.Data == nil {
		return "", nil
	}
	if cert, ok := s.Data[key].(string); ok {
		return cert, nil
	}
	return "", fmt.Errorf("Invalid response from vault")
}

func (vs *VaultStore) Save(ctx context.Context, name string, values map[string]interface{}) error {
	_, err := vs.Client.KVv2(vs.Mount).Put(ctx, name, values)
	return err
}
