package mgr

import (
	"context"
	"crypto"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"

	"github.com/hashicorp/vault/api"
	"golang.org/x/crypto/acme"
	"go.uber.org/zap"
)

type DNS interface {
	SetTXT(ctx context.Context, host string, value string) error
	ClearTXT(ctx context.Context, host string, value string) error
}

type Manager struct {
	AcmeDirectoryURL string
	AcmeCaCert string
	Contact []string
	DNS              DNS
	SignerBuilder    SignerBuilder
	Store       Store

	acmeClient *acme.Client
}

func New(cfg *Config) (*Manager, error) {
	vault_cfg := api.DefaultConfig()
	vault_client, err := api.NewClient(vault_cfg)
	if err != nil {
		return nil, err
	}

	return &Manager{
		AcmeDirectoryURL: cfg.Acme.Directory,
		AcmeCaCert: cfg.Acme.CaCert,
		Contact: cfg.Acme.Contact,
		DNS: &AwsDNS{
			CredentialsProvider: &VaultCredentialsProvider {
				Client: vault_client,
				Role: cfg.DNS.VaultRole,
				Mount: cfg.DNS.VaultMount,
			},
			Domains: cfg.DNS.Domains,
			Endpoint: cfg.DNS.Endpoint,
			Region: cfg.DNS.Region,
		},
		SignerBuilder: &VaultSignerBuilder{
			Client: vault_client,
			Mount:  cfg.Vault.Transit.Mount,
			Name:   cfg.Vault.Transit.Name,
		},
		Store: &VaultStore{
			Client: vault_client,
			Mount: cfg.Vault.Store.Mount,
		},
	}, nil
}

func FromPath(path string) (*Manager, error) {
	cfg, err := LoadConfig(path)
	if err != nil {
		return nil, err
	}
	return New(cfg);
}

func (m *Manager) GetAccount(ctx context.Context) (*acme.Account, error) {
	acme_client, err := m.getAcme(ctx)
	if err != nil {
		return nil, err
	}
	return acme_client.GetReg(ctx, "")
}

func (m *Manager) Init(ctx context.Context) error {
	acme_client, err := m.getAcme(ctx)
	if err != nil {
		return err
	}
	if _, err := acme_client.Register(ctx, &acme.Account{
		Contact: m.Contact,
	}, acme.AcceptTOS); err != nil {
		return err
	}
	return nil
}

func (m *Manager) Rotate(ctx context.Context) error {
	acme_client, err := m.getAcme(ctx)
	if err != nil {
		return err
	}
	if _, err := acme_client.GetReg(ctx, ""); err != nil {
		return err
	}
	if err := m.SignerBuilder.Rotate(); err != nil {
		return err
	}
	signer, err := m.SignerBuilder.Build()
	if err != nil {
		return err
	}
	if err := acme_client.AccountKeyRollover(ctx, signer); err != nil {
		return err
	}
	return nil
}

func (m *Manager) RunAuthorization(ctx context.Context, authzUrl string) error {
	acme_client, err := m.getAcme(ctx)
	if err != nil {
		return err
	}
	authz, err := acme_client.GetAuthorization(ctx, authzUrl)
	if err != nil {
		return err
	}
	if authz.Status == acme.StatusPending {
		// TODO: Make sure challenge domain is one we requested
		challenge := getDns01Challenge(authz)
		if challenge == nil {
			return fmt.Errorf("No supported challenge received from CA")
		}
		txt, err := acme_client.DNS01ChallengeRecord(challenge.Token)
		if err != nil {
			return err
		}
		if err := m.DNS.SetTXT(ctx, authz.Identifier.Value, txt); err != nil {
			return err
		}
		defer m.DNS.ClearTXT(ctx, authz.Identifier.Value, txt)
		if _, err := acme_client.Accept(ctx, challenge); err != nil {
			return err
		}
	}
	if _, err = acme_client.WaitAuthorization(ctx, authzUrl); err != nil {
		return err
	}
	return nil
}

func (m *Manager) RunOrder(ctx context.Context, name string, crt *ConfigCertificate) error {
	zap.S().Infow("Processing certificate", "name", name, "domains", crt.Domains)
	existing, err := m.Store.Load(ctx, name, "cert.pem")
	if err != nil {
		return err
	}
	if existing != "" {
		if needs_renew, err := CertificateNeedsRenewal(existing); err != nil {
			return err
		} else if !needs_renew {
			zap.S().Infow("Certificate is not ready for renewal")
			return nil
		}
	}
	acme_client, err := m.getAcme(ctx)
	if err != nil {
		return err
	}

	authzIDs := make([]acme.AuthzID, 0, len(crt.Domains))
	for _, d := range crt.Domains {
		authzIDs = append(authzIDs, acme.AuthzID{
			Type:  "dns",
			Value: d,
		})
	}
	zap.S().Infow("Sending order to CA", "name", name)
	order, err := acme_client.AuthorizeOrder(ctx, authzIDs)
	if err != nil {
		return err
	}
	for _, authzUrl := range order.AuthzURLs {
		if err := m.RunAuthorization(ctx, authzUrl); err != nil {
			return err
		}
	}

	zap.S().Infow("Waiting for order to become ready", "name", name)
	order, err = acme_client.WaitOrder(ctx, order.URI)
	if err != nil {
		return err
	}

	csr, private_key, err := NewCSR(crt.Type, crt.Domains)
	if err != nil {
		return err
	}

	zap.S().Infow("Requesting certificate from CA", "name", name)
	bundle, _, err := acme_client.CreateOrderCert(ctx, order.FinalizeURL, csr, true)
	if err != nil {
		return err
	}

	private_key_pem, err := EncodePrivateKey(private_key)
	if err != nil {
		return err
	}

	zap.S().Infow("Saving certificate to storage", "name", name)
	if err := m.Store.Save(
		ctx,
		name,
		map[string]interface{}{
			"private_key.pem": private_key_pem,
			"fullchain.pem": EncodeCertificates(bundle),
			"cert.pem": EncodeCertificates(bundle[0:1]),
			"chain.pem": EncodeCertificates(bundle[1:]),
		},
	); err != nil {
		return err
	}

	return nil
}

func (m *Manager) getAcme(ctx context.Context) (*acme.Client, error) {
	if m.acmeClient == nil {
		signer, err := m.SignerBuilder.Build()
		if err != nil {
			return nil, err
		}
		caCertPool, err := x509.SystemCertPool()
		if err != nil {
			return nil, err
		}
		if m.AcmeCaCert != "" {
			zap.S().Infow("Adding acme CA certificate")
			caCertPool.AppendCertsFromPEM([]byte(m.AcmeCaCert))
		}
		acme_client := &acme.Client{
			DirectoryURL: m.AcmeDirectoryURL,
			HTTPClient: &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						RootCAs: caCertPool,
					},
				},
			},
			Key: signer,
		}
		m.acmeClient = acme_client
	}
	return m.acmeClient, nil
}

type Store interface {
	Load(ctx context.Context, name string, key string) (string, error)
	Save(ctx context.Context, name string, values map[string]interface{}) error
}

type SignerBuilder interface {
	Build() (crypto.Signer, error)
	Rotate() error
	SetKeyVersion(id int64)
}

func getDns01Challenge(authz *acme.Authorization) *acme.Challenge {
	for _, challenge := range authz.Challenges {
		if challenge.Type == "dns-01" {
			return challenge
		}
	}
	return nil
}
