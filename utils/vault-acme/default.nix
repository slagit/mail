_: {
  perSystem = {pkgs, ...}: {
    packages.vault-acme = pkgs.buildGoModule {
      name = "vault-acme";
      src = ./.;
      vendorHash = null;
    };
  };
}
