# Infrastructure

## Provisioning

```bash
nix develop .#infrastructure
```

```bash
export AWS_ACCESS_KEY_ID=********************
export AWS_SECRET_ACCESS_KEY=****************************************
export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/44974600/terraform/state/infrastructure"
export TF_HTTP_LOCK_ADDRESS="https://gitlab.com/api/v4/projects/44974600/terraform/state/infrastructure/lock"
export TF_HTTP_LOCK_METHOD="POST"
export TF_HTTP_PASSWORD=glpat-************
export TF_HTTP_RETRY_WAIT_MIN=5;
export TF_HTTP_UNLOCK_ADDRESS="https://gitlab.com/api/v4/projects/44974600/terraform/state/infrastructure/lock"
export TF_HTTP_UNLOCK_METHOD="DELETE";
export TF_HTTP_USERNAME=kocha
nix build .#infrastructure --out-link infrastructure.tf.json
terraform init
terraform plan -out tfplan
terraform apply
```
