_: {
  resource = {
    aws_iam_policy = {
      vault-acme = {
        policy = builtins.toJSON {
          Statement = [
            {
              Action = "route53:ChangeResourceRecordSets";
              Effect = "Allow";
              Resource = "\${aws_route53_zone.slagit-cert-net.arn}";
              Sid = "ChangeRecords";
            }
            {
              Action = "route53:GetChange";
              Effect = "Allow";
              Resource = "arn:aws:route53:::change/*";
              Sid = "WatchChanges";
            }
          ];
          Version = "2012-10-17";
        };
      };
      vault-root = {
        policy = builtins.toJSON {
          Statement = [
            {
              Action = [
                "iam:DeleteAccessKey"
                "iam:GetUser"
                "iam:CreateAccessKey"
              ];
              Effect = "Allow";
              Resource = "arn:aws:iam::445131688516:user/\$\${aws:username}";
              Sid = "RotateAccessKey";
            }
            {
              Action = "sts:AssumeRole";
              Effect = "Allow";
              Resource = "\${aws_iam_role.vault-acme.arn}";
              Sid = "AssumeVaultAcme";
            }
          ];
          Version = "2012-10-17";
        };
      };
    };
    aws_iam_role.vault-acme = {
      assume_role_policy = builtins.toJSON {
        "Statement" = [
          {
            "Effect" = "Allow";
            "Principal" = {
              "AWS" = "arn:aws:iam::445131688516:root";
            };
            "Action" = "sts:AssumeRole";
            "Condition" = {};
          }
        ];
        "Version" = "2012-10-17";
      };
      name = "vault-acme";
    };
    aws_iam_role_policy_attachment."vault-acme-vault-acme" = {
      policy_arn = "\${aws_iam_policy.vault-acme.arn}";
      role = "\${aws_iam_role.vault-acme.name}";
    };
    aws_iam_user."vault-root" = {
      name = "vault-root";
    };
    aws_iam_user_policy_attachment."vault-root-vault-root" = {
      policy_arn = "\${aws_iam_policy.vault-root.arn}";
      user = "\${aws_iam_user.vault-root.name}";
    };
    aws_route53_zone.slagit-cert-net = {
      name = "slagit-cert.net";
    };
  };
}
