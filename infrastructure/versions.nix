{pkgs, ...}: {
  provider.aws.region = "us-east-1";
  terraform = {
    backend.http = {};
    required_providers = {
      aws = {
        inherit (pkgs.terraform-providers.aws) version;
        source = "hashicorp/aws";
      };
      linode = {
        inherit (pkgs.terraform-providers.linode) version;
        source = "linode/linode";
      };
    };
    required_version = pkgs.terraform.version;
  };
}
