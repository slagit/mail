_: {
  resource = {
    aws_identitystore_user.albert = {
      identity_store_id = "d-9067e3bb74";
      display_name = "Albert Koch";
      user_name = "albert";
      name = {
        given_name = "Albert";
        family_name = "Koch";
      };
      emails = {
        value = "kocha@slagit.net";
      };
    };
    aws_ssoadmin_account_assignment.albert_albert_AdministratorAccess = {
      instance_arn = "arn:aws:sso:::instance/ssoins-7223f1f65be73c89";
      permission_set_arn = "\${aws_ssoadmin_permission_set.AdministratorAccess.arn}";
      principal_id = "\${aws_identitystore_user.albert.user_id}";
      principal_type = "USER";
      target_id = "445131688516";
      target_type = "AWS_ACCOUNT";
    };
    aws_ssoadmin_managed_policy_attachment.AdministratorAccessAdministratorAccess = {
      instance_arn = "arn:aws:sso:::instance/ssoins-7223f1f65be73c89";
      managed_policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess";
      permission_set_arn = "\${aws_ssoadmin_permission_set.AdministratorAccess.arn}";
    };
    aws_ssoadmin_permission_set.AdministratorAccess = {
      name = "AdministratorAccess";
      instance_arn = "arn:aws:sso:::instance/ssoins-7223f1f65be73c89";
    };
  };
}
