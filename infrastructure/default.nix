{inputs, ...}: {
  perSystem = {
    pkgs,
    system,
    ...
  }: {
    devShells.infrastructure = pkgs.mkShell {
      packages = [
        (pkgs.terraform.withPlugins (p: [p.aws p.linode]))
        pkgs.awscli2
      ];
    };
    packages.infrastructure = inputs.terranix.lib.terranixConfiguration {
      inherit system;
      modules = [
        ../hosts/compute0/infrastructure.nix
        ../hosts/compute1/infrastructure.nix
        ../hosts/compute2/infrastructure.nix
        ../hosts/controller/infrastructure.nix
        ../hosts/franklin/infrastructure.nix
        ../hosts/gutenberg/infrastructure.nix
        ../hosts/hail/infrastructure.nix
        ../services/homepage/infrastructure.nix
        ../services/keycloak/infrastructure.nix
        ../services/mail/infrastructure.nix
        ../services/monitoring/infrastructure.nix
        ../services/nix-cache/infrastructure.nix
        ../services/paperless/infrastructure.nix
        ../services/vacation/infrastructure.nix
        ./aws.nix
        ./linode.nix
        ./vault-acme.nix
        ./versions.nix
      ];
    };
  };
}
