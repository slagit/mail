_: {
  resource.linode_domain.slagit_net = {
    domain = "slagit.net";
    expire_sec = 604800;
    refresh_sec = 300;
    retry_sec = 300;
    soa_email = "root@slagit.net";
    ttl_sec = 300;
    type = "master";
  };
}
