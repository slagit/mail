{
  self,
  inputs,
  ...
}: {
  flake.nixosConfigurations.install-linode = inputs.nixpkgs.lib.nixosSystem {
    modules = [
      "${inputs.nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
      self.nixosModules.common
      ./hosts/install-linode.nix
    ];
  };
  perSystem = {pkgs, ...}: {
    packages.install-linode = pkgs.buildGoModule {
      name = "install-linode";
      src = ./linode;
      vendorHash = null;
    };
  };
}
