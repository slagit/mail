{
  lib,
  pkgs,
  ...
}: {
  boot = {
    kernelParams = ["console=ttyS0,19200n8"];
    loader.grub = {
      extraConfig = ''
        serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
        terminal_input serial;
        terminal_output serial;
      '';
      memtest86.enable = lib.mkForce false;
    };
    supportedFilesystems = lib.mkForce [];
  };
  documentation = {
    enable = lib.mkForce false;
    nixos.enable = lib.mkForce false;
  };
  environment.systemPackages = [
    pkgs.git
  ];
  hardware.enableRedistributableFirmware = lib.mkForce false;
  networking = {
    tempAddresses = "disabled";
    wireless.enable = lib.mkForce false;
  };
  nixpkgs.hostPlatform = "x86_64-linux";
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = lib.mkForce "no";
  };
  slagit.users.enable = false;
  system.extraDependencies = lib.mkForce [];
}
