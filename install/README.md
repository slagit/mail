# NixOS Installer for Linode

## Building

```bash
nix build .#nixosConfigurations.install-linode.config.system.build.isoImage
scp result/iso/*.iso cloudy.slagit.net:nixos-install-linode.iso
```

## Installation

1. Create a new Linode

   ```bash
   export LINODE_CLI_TOKEN=....
   nix run .#install-linode <instance_label>
   ```

1. In rescue mode, copy installer

   ```bash
   ssh -t kocha@lish-us-east.linode.com <instance_label>
   curl -k -u albert \
       sftp://cloudy.slagit.net/home/albert/nixos-install-linode.iso \
       | dd of=/dev/sda
   ```

1. In install mode, perform installation

   ```bash
   sv -principal=nixos -role=nixos nixos@192.0.2.16
   sudo e2label /dev/sda nixos
   sudo mount /dev/disk/by-label/nixos /mnt
   sudo nixos-install --flake path/to/repo#hostname
   ```
