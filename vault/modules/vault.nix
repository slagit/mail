{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.slagit.vault;
in {
  config = {
    security.acme = {
      acceptTerms = true;
      defaults.email = "kocha@slagit.net";
      certs.${config.networking.fqdn} = {
        extraDomainNames = [
          cfg.hostName
        ];
        group = "vault-tls";
        reloadServices = [
          "vault.service"
        ];
      };
    };
    networking.firewall.allowedTCPPorts = [80 443 8200];

    services = {
      nginx = {
        enable = true;
        virtualHosts.${config.networking.fqdn} = {
          forceSSL = true;
          enableACME = true;
        };
      };
      vault = {
        address = "[::]:8200";
        enable = true;
        extraConfig = ''
          api_addr = "https://${config.networking.fqdn}:8200"
          cluster_addr = "https://${config.networking.fqdn}:8201"
          ui = true
        '';
        package = pkgs.vault-bin;
        storageBackend = "raft";
        tlsCertFile = "/var/lib/acme/${config.networking.fqdn}/cert.pem";
        tlsKeyFile = "/var/lib/acme/${config.networking.fqdn}/key.pem";
      };
    };

    slagit.unfree = ["vault-bin"];

    systemd.services.vault.serviceConfig = {
      LogsDirectory = "vault";
    };

    users.groups.vault-tls.members = [
      "nginx"
      "vault"
    ];
  };
  options.slagit.vault.hostName = lib.mkOption {
    description = "Vault hostname";
    type = lib.types.str;
  };
}
