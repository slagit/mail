# Vault

## Provision vault resources

```bash
export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/44974600/terraform/state/vault";
export TF_HTTP_LOCK_ADDRESS="https://gitlab.com/api/v4/projects/44974600/terraform/state/vault/lock";
export TF_HTTP_LOCK_METHOD="POST";
export TF_HTTP_PASSWORD=glpat-************
export TF_HTTP_RETRY_WAIT_MIN=5;
export TF_HTTP_UNLOCK_ADDRESS="https://gitlab.com/api/v4/projects/44974600/terraform/state/vault/lock";
export TF_HTTP_UNLOCK_METHOD="DELETE";
export TF_HTTP_USERNAME=kocha
export VAULT_ADDR=https://slagit-vault.net:8200
export VAULT_TOKEN=hvs.************
nix build .#provisioning-http --out-link vault.tf.json
terraform plan -out tfplan
terraform apply
```
