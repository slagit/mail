{
  config,
  lib,
  ...
}: {
  networking = {
    domain = "slagit-vault.net";
    hostName = "morgan";
  };
  services.openssh.enable = true;
  slagit.vault.hostName = "slagit-vault.net";
  virtualisation.vmVariant = {
    environment.etc = {
      "nebula/node.crt" = {
        source = ../../devenv/nebula/morgan.slagit-vault.net.crt;
      };
      "nebula/node.key" = {
        source = ../../devenv/nebula/morgan.slagit-vault.net.key;
        group = "nebula-slagit";
        mode = "0640";
      };
    };
    virtualisation.qemu.networkingOptions = lib.mkForce (config.dev-lib.mkNetworkingOptions "linode" "linode-morgan" config.dev-lib.networking.networks.linode.ports.morgan);
  };
}
