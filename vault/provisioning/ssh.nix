{lib, ...}: let
  role_defaults = {
    backend = "\${vault_mount.ssh_client_signer.path}";
    key_type = "ca";
    allow_user_certificates = true;
    default_extensions = {
      permit-pty = "";
    };
    allowed_extensions = "permit-pty";
    max_ttl = "300";
  };
  role_policy = roles:
    {
      "ssh-client-signer/roles".capabilities = ["list"];
    }
    // builtins.listToAttrs (map (r: {
        name = "ssh-client-signer/sign/${r}";
        value = {
          capabilities = ["update"];
          denied_parameters = {
            key_id = [];
          };
        };
      })
      roles);
in {
  resource = {
    vault_mount.ssh_client_signer = {
      type = "ssh";
      path = "ssh-client-signer";
    };
    vault_ssh_secret_backend_ca.client_signer = {
      backend = "\${vault_mount.ssh_client_signer.path}";
      generate_signing_key = true;
    };
    vault_ssh_secret_backend_role = {
      client_user =
        role_defaults
        // {
          name = "user";
          allowed_users = "{{identity.entity.metadata.ssh_username}}";
          allowed_users_template = true;
        };
      client_nixos =
        role_defaults
        // {
          name = "nixos";
          allow_user_certificates = true;
          allowed_users = "nixos";
          default_user = "nixos";
        };
    };
    vault_policy = {
      ssh_client_nixos.name = "ssh-nixos";
      ssh_client_user.name = "ssh-user";
    };
  };
  slagit.vault = {
    groups.server-installers = {
      policies = ["ssh-nixos"];
    };
    policies = {
      admin.paths = {
        "ssh-client-signer/config/ca".capabilities = ["delete" "read" "update"];
        "ssh-client-signer/roles/nixos".capabilities = ["read" "update"];
        "ssh-client-signer/roles/user".capabilities = ["read" "update"];
        "sys/mounts/ssh-client-signer".capabilities = ["create" "read" "update"];
        "sys/policies/acl/ssh-nixos".capabilities = ["read" "update"];
        "sys/policies/acl/ssh-user".capabilities = ["read" "update"];
      };
      ssh_client_nixos.paths = role_policy ["nixos"];
      ssh_client_user.paths = role_policy ["user"];
    };
  };
}
