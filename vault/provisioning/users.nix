{
  config,
  lib,
  ...
}: let
  cfg = config.slagit.vault;
in {
  config = {
    slagit.vault.identities = {
      user-albert = {
        aliases = [
          {
            backend = "vault_auth_backend.userpass";
            name = "albert";
          }
          {
            backend = "vault_auth_backend.jwt";
            name = "90319fd9-e7fb-4127-9518-ba6d3d5efb42";
          }
          {
            backend = "vault_auth_backend.oidc";
            name = "90319fd9-e7fb-4127-9518-ba6d3d5efb42";
          }
        ];
        groups = [
          "server-installers"
        ];
        metadata.ssh_username = "albert";
        policies = [
          "acme"
          "admin"
        ];
      };
    };
    resource = {
      vault_auth_backend.userpass = {
        type = "userpass";
      };
      vault_identity_group =
        lib.attrsets.mapAttrs (k: {policies, ...}: {
          inherit policies;
          member_entity_ids = builtins.map (x: "\${vault_identity_entity.${x}.id}") (builtins.attrNames (lib.filterAttrs (_: v: builtins.elem k v.groups) cfg.identities));
          name = k;
        })
        cfg.groups;
      vault_identity_entity =
        lib.attrsets.mapAttrs (k: {
          metadata,
          policies,
          ...
        }: {
          inherit metadata policies;
          name = k;
        })
        cfg.identities;
      vault_identity_entity_alias = builtins.listToAttrs (builtins.foldl' (acc: a: acc ++ a) [] (lib.attrsets.mapAttrsToList (k: v:
        lib.imap0 (i: {
          backend,
          name,
          ...
        }: {
          name = "${k}_${toString i}";
          value = {
            inherit name;
            canonical_id = "\${vault_identity_entity.${k}.id}";
            mount_accessor = "\${${backend}.accessor}";
          };
        })
        v.aliases)
      cfg.identities));
    };
  };
  options.slagit.vault = {
    groups = lib.mkOption {
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          policies = lib.mkOption {
            default = [];
            type = lib.types.listOf lib.types.str;
          };
        };
      });
    };
    identities = lib.mkOption {
      description = "vault identities";
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          aliases = lib.mkOption {
            type = lib.types.listOf (lib.types.submodule {
              options = {
                backend = lib.mkOption {
                  type = lib.types.str;
                };
                name = lib.mkOption {
                  type = lib.types.str;
                };
              };
            });
          };
          groups = lib.mkOption {
            default = [];
            type = lib.types.listOf lib.types.str;
          };
          metadata = lib.mkOption {
            default = {};
            type = lib.types.attrsOf lib.types.anything;
          };
          policies = lib.mkOption {
            default = [];
            type = lib.types.listOf lib.types.str;
          };
        };
      });
    };
  };
}
