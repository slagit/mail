_: {
  slagit.vault.policies.admin.paths = {
    "auth/approle/role".capabilities = ["list"];
    "auth/approle/role/+".capabilities = ["create" "read" "update" "delete"];
    "auth/approle/role/+/role-id".capabilities = ["read"];
    "auth/approle/role/+/secret-id".capabilities = ["create" "list" "update"];
    "auth/approle/role/+/secret-id-accessor/destroy".capabilities = ["update"];
    "auth/userpass/users".capabilities = ["list"];
    "auth/userpass/users/+".capabilities = ["create" "read" "update"];
    "identity/group".capabilities = ["update"];
    "identity/group/id".capabilities = ["list"];
    "identity/group/id/+".capabilities = ["delete" "read" "update"];
    "identity/entity".capabilities = ["update"];
    "identity/entity/merge".capabilities = ["update"];
    "identity/entity/id".capabilities = ["list"];
    "identity/entity/id/+".capabilities = ["delete" "read" "update"];
    "identity/entity-alias".capabilities = ["update"];
    "identity/entity-alias/id/+".capabilities = ["delete" "read"];
    "identity/lookup/entity".capabilities = ["update"];
    "secret/metadata/*".capabilities = ["list"];
    "secret/data/*".capabilities = ["create" "read" "update"];
    "sys/audit".capabilities = ["read" "sudo"];
    "sys/auth".capabilities = ["read"];
    "sys/auth/approle".capabilities = ["create" "read" "sudo" "update"];
    "sys/auth/userpass".capabilities = ["create" "read" "sudo" "update"];
    "sys/mounts".capabilities = ["read"];
    "sys/mounts/auth/approle".capabilities = ["read"];
    "sys/mounts/auth/userpass".capabilities = ["read"];
    "sys/mounts/secret".capabilities = ["create" "read" "update"];
    "sys/policies/acl".capabilities = ["list"];
    "sys/policies/acl/+".capabilities = ["read" "update" "delete"];
  };
}
