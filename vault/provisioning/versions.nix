{
  config,
  lib,
  pkgs,
  ...
}: {
  config = {
    terraform = {
      inherit (config.slagit.vault) backend;
      required_providers.keycloak = {
        inherit (pkgs.terraform-providers.keycloak) version;
        source = "mrparkers/keycloak";
      };
      required_providers.vault = {
        inherit (pkgs.terraform-providers.vault) version;
        source = "hashicorp/vault";
      };
      required_version = pkgs.terraform.version;
    };
  };
  options.slagit.vault.backend = lib.mkOption {
    default = {};
    type = lib.types.attrsOf lib.types.anything;
  };
}
