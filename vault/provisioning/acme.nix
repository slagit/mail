_: {
  resource = {
    vault_approle_auth_backend_role = {
      unifi = {
        backend = "\${vault_auth_backend.approle.path}";
        role_name = "unifi";
        secret_id_num_uses = 1;
        token_period = 3600;
      };
    };
    vault_aws_secret_backend_role.acme = {
      backend = "\${vault_mount.aws.path}";
      credential_type = "assumed_role";
      max_sts_ttl = 3600;
      name = "acme";
      role_arns = [
        "arn:aws:iam::445131688516:role/vault-acme"
      ];
    };
    vault_mount = {
      aws = {
        type = "aws";
        path = "aws";
      };
      transit = {
        type = "transit";
        path = "transit";
      };
    };
    vault_transit_secret_backend_key.acme = {
      backend = "\${vault_mount.transit.path}";
      name = "acme";
      type = "ecdsa-p256";
    };
  };
  slagit.vault = {
    identities = {
      app-unifi = {
        aliases = [
          {
            backend = "vault_auth_backend.approle";
            name = "\${vault_approle_auth_backend_role.unifi.role_id}";
          }
        ];
        policies = [
          "unifi"
        ];
      };
      host-hail-slagit-net.policies = ["unifi-host"];
    };
    policies = {
      unifi-host.paths = {
        "auth/approle/role/unifi/role-id".capabilities = ["read"];
        "auth/approle/role/unifi/secret-id" = {
          capabilities = ["create" "update"];
          min_wrapping_ttl = "1s";
          max_wrapping_ttl = "10m";
        };
      };
      unifi.paths = {
        "secret/data/acme/unifi/unifi.slagit.net".capabilities = ["read"];
      };
      acme.paths = {
        "\${vault_aws_secret_backend_role.acme.backend}/sts/\${vault_aws_secret_backend_role.acme.name}".capabilities = ["update"];
        "\${vault_mount.secret.path}/data/acme/*".capabilities = ["create" "read" "update"];
        "\${vault_transit_secret_backend_key.acme.backend}/keys/\${vault_transit_secret_backend_key.acme.name}".capabilities = ["read"];
        "\${vault_transit_secret_backend_key.acme.backend}/sign/\${vault_transit_secret_backend_key.acme.name}".capabilities = ["update"];
      };
      admin.paths = {
        "auth/token/create".capabilities = ["update"];
        "aws/roles/acme".capabilities = ["create" "read" "update"];
        "aws/config/root".capabilities = ["update"];
        "aws/config/rotate-root".capabilities = ["update"];
        "sys/mounts/aws".capabilities = ["create" "read" "update"];
        "sys/mounts/transit".capabilities = ["create" "read" "update"];
        "transit/keys/acme".capabilities = ["create" "read" "update"];
        "transit/keys/acme/config".capabilities = ["read" "update"];
      };
    };
  };
}
