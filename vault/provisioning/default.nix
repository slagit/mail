{...}: {
  imports = [
    ../../hosts/compute0/vault.nix
    ../../hosts/compute1/vault.nix
    ../../hosts/franklin/vault.nix
    ../../services/homepage/vault.nix
    ../../services/keycloak/vault.nix
    ../../services/monitoring/vault.nix
    ../../services/paperless/vault.nix
    ../../services/vacation/vault.nix
    ../../services/nix-cache/vault.nix
    ../../services/systemd-vaultd/vault.nix
    ../../utils/sv/vault.nix
    ./acme.nix
    ./audit.nix
    ./admin.nix
    ./hosts.nix
    ./policy.nix
    ./ssh.nix
    ./users.nix
    ./versions.nix
  ];
}
