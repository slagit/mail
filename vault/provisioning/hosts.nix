{
  config,
  lib,
  ...
}: let
  cfg = config.slagit.vault;
in {
  config = {
    slagit.vault.hosts = {
      "powell.frontierscamp.org" = {
      };
      "hail.slagit.net" = {
      };
    };
    resource = {
      vault_auth_backend.approle = {
        type = "approle";
      };
      vault_mount.secret = {
        type = "kv";
        path = "secret";
        options = {
          version = 2;
        };
      };
      vault_approle_auth_backend_role =
        lib.attrsets.mapAttrs' (k: v: let
          terraform_name = builtins.replaceStrings ["."] ["-"] k;
        in {
          name = terraform_name;
          value = {
            backend = "\${vault_auth_backend.approle.path}";
            role_name = k;
            token_ttl = 600;
            token_explicit_max_ttl = 600;
          };
        })
        cfg.hosts;
    };
    slagit.vault.identities =
      lib.attrsets.mapAttrs' (k: v: let
        terraform_name = builtins.replaceStrings ["."] ["-"] k;
      in {
        name = "host-${terraform_name}";
        value = {
          aliases = [
            {
              backend = "vault_auth_backend.approle";
              name = "\${vault_approle_auth_backend_role.${terraform_name}.role_id}";
            }
          ];
          metadata.hostname = k;
          policies = [
            "host"
          ];
        };
      })
      cfg.hosts;
  };
  options.slagit.vault = {
    hosts = lib.mkOption {
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
        };
      });
    };
  };
}
