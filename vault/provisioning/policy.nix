{
  config,
  lib,
  ...
}: {
  config.resource = {
    vault_policy =
      lib.attrsets.mapAttrs (name: policy: {
        name = lib.mkDefault name;
        policy = builtins.toJSON {
          path = lib.attrsets.mapAttrs (path: v:
            {
              inherit (v) capabilities;
            }
            // (
              # TODO: Can we use lib.mkMerge and lib.mkIf after all?
              if v.denied_parameters == null
              then {}
              else {
                inherit (v) denied_parameters;
              }
            )
            // (
              if v.min_wrapping_ttl == null
              then {}
              else {
                inherit (v) min_wrapping_ttl;
              }
            )
            // (
              if v.max_wrapping_ttl == null
              then {}
              else {
                inherit (v) max_wrapping_ttl;
              }
            ))
          policy.paths;
        };
      })
      config.slagit.vault.policies;
  };
  options.slagit.vault.policies = lib.mkOption {
    type = lib.types.attrsOf (lib.types.submodule {
      options = {
        paths = lib.mkOption {
          type = lib.types.attrsOf (lib.types.submodule {
            options = {
              capabilities = lib.mkOption {
                type = lib.types.listOf lib.types.str;
              };
              denied_parameters = lib.mkOption {
                default = null;
                type = lib.types.nullOr (lib.types.attrsOf (lib.types.listOf lib.types.str));
              };
              min_wrapping_ttl = lib.mkOption {
                default = null;
                type = lib.types.nullOr lib.types.str;
              };
              max_wrapping_ttl = lib.mkOption {
                default = null;
                type = lib.types.nullOr lib.types.str;
              };
            };
          });
        };
      };
    });
  };
}
