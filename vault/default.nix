{
  self,
  inputs,
  ...
}: {
  flake = {
    nixosConfigurations.morgan = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        ./hosts/morgan.nix
        self.common.hardware.nixosModules.linode
        self.nixosModules.common
        self.nixosModules.dev-lib
        self.nixosModules.nebula
        self.vault.nixosModules.vault
      ];
    };
    vault.nixosModules.vault = ./modules/vault.nix;
  };
  perSystem = {
    pkgs,
    system,
    ...
  }: {
    devShells.vault = pkgs.mkShell {
      packages = [
        (pkgs.terraform.withPlugins (p: [p.vault p.keycloak]))
        pkgs.vault
      ];
    };
    packages = {
      provisioning = inputs.terranix.lib.terranixConfiguration {
        inherit system;
        modules = [
          ./provisioning
          ./provisioning/local.nix
        ];
      };
      provisioning-http = inputs.terranix.lib.terranixConfiguration {
        inherit system;
        modules = [
          ./provisioning
          ./provisioning/http.nix
        ];
      };
    };
  };
}
