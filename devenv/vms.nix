{
  config,
  lib,
  withSystem,
  ...
}: {
  config = {
    flake.dev-svc = withSystem "x86_64-linux" ({pkgs, ...}:
      lib.attrsets.mapAttrs (k: v: {
        start = pkgs.writeShellScriptBin "${k}.start.sh" ''
          set -euo pipefail

          export NIX_DISK_IMAGE=./.devenv/state/${k}.qcow2
          export SHARED_DIR="$DEVENV_ROOT"
          export QEMU_OPTS="-monitor unix:$DEVENV_RUNTIME/${k}-monitor.sock,server,nowait -vnc unix:$DEVENV_RUNTIME/${k}-vnc.sock"
          exec nix run .#nixosConfigurations.${k}.config.system.build.vm
        '';
        stop = pkgs.writeShellScriptBin "${k}.stop.sh" ''
          set -euo pipefail

          echo system_powerdown | ${pkgs.socat}/bin/socat - unix-connect:$DEVENV_RUNTIME/${k}-monitor.sock
        '';
      })
      config.vms);
    perSystem = {
      pkgs,
      system,
      ...
    }: {
      devenv.shells.default.processes =
        lib.attrsets.mapAttrs (k: v: {
          exec = "nix run .#dev-svc.${k}.start";
          process-compose = {
            depends_on = {
              ca.condition = "process_healthy";
              dns.condition = "process_healthy";
              ovsdb-server.condition = "process_healthy";
              ovn-controller.condition = "process_healthy";
            };
            namespace = "VMs";
            shutdown.command = "nix run .#dev-svc.${k}.stop";
          };
        })
        config.vms;
    };
  };
  options.vms = lib.mkOption {
    type = lib.types.attrsOf (lib.types.submodule {
      options = {
      };
    });
  };
}
