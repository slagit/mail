{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.services.openvswitch;
  runtimeDir = "${config.env.DEVENV_RUNTIME}/openvswitch";
  stateDir = "${config.env.DEVENV_STATE}/openvswitch";
  ovsdb-conf = pkgs.writeText "ovsdb-server.conf" (builtins.toJSON {
    inherit (cfg) databases;
    remotes."punix:db.sock" = {};
  });
  start-ovsdb-server = pkgs.writeShellScript "start-ovsdb-server.sh" ''
    set -euo pipefail

    mkdir -p ${lib.escapeShellArg runtimeDir} ${lib.escapeShellArg stateDir}

    if [ ! -f ${lib.escapeShellArg stateDir}/conf.db ]; then
      ${pkgs.openvswitch}/bin/ovsdb-tool create ${lib.escapeShellArg stateDir}/conf.db
    fi

    cp ${ovsdb-conf} ${lib.escapeShellArg stateDir}/ovsdb-server.conf

    ${pkgs.openvswitch}/bin/ovsdb-server \
      --config-file ${lib.escapeShellArg stateDir}/ovsdb-server.conf \
      --pidfile
  '';
  start-ovs-vswitchd = pkgs.writeShellScript "start-ovs-vswitchd.sh" ''
    set -euo pipefail

    exec ${pkgs.openvswitch}/bin/ovs-vswitchd --pidfile
  '';
in {
  config = lib.mkIf cfg.enable {
    env.OVS_RUNDIR = runtimeDir;
    packages = [pkgs.openvswitch];
    processes = {
      ovsdb-server = {
        exec = toString start-ovsdb-server;
        process-compose = {
          namespace = "Open vSwitch";
          readiness_probe = {
            exec.command = "test -S ${lib.escapeShellArg runtimeDir}/db.sock";
            failure_threshold = 10;
            period_seconds = 2;
          };
          shutdown.command = "${pkgs.openvswitch}/bin/ovs-appctl -t ovsdb-server exit";
        };
      };
      ovs-vswitchd = {
        exec = toString start-ovs-vswitchd;
        process-compose = {
          depends_on.ovsdb-server.condition = "process_healthy";
          namespace = "Open vSwitch";
          shutdown.command = "${pkgs.openvswitch}/bin/ovs-appctl -t ovs-vswitchd exit";
        };
      };
    };
    services.openvswitch.databases."${stateDir}/conf.db" = {};
  };
  options.services.openvswitch = {
    enable = lib.mkEnableOption ''
      Add Open vSwitch services
    '';
    databases = lib.mkOption {
      type =
        lib.types.attrsOf (lib.types.submodule {
          });
    };
  };
}
