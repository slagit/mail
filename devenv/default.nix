{
  config,
  inputs,
  lib,
  withSystem,
  ...
}: {
  imports = [
    inputs.devenv.flakeModule
    ./backup.nix
    ./ca.nix
    ./dev-vm.nix
    ./dns.nix
    ./lib.nix
    ./mock-nvr.nix
    ./networks.nix
    ./setup-networks.nix
    ./vms.nix
  ];
  config = {
    flake = {
      dev-svc = withSystem "x86_64-linux" ({pkgs, ...}: {
        morgan.ready = pkgs.writeShellScriptBin "morgan.ready.sh" ''
          set -euo pipefail

          CURL="ip netns exec util ${pkgs.curl}/bin/curl --cacert ${./root.pem}"
          JQ="${pkgs.jq}/bin/jq"

          response=$($CURL -s --max-time 5 https://slagit-vault.net:8200/v1/sys/init | $JQ '.initialized' || true)
          [ ! -z "$response" ]
          echo "READY"
        '';
        vault-provision.start = pkgs.writeShellScriptBin "vault-provision.start.sh" ''
          set -euo pipefail

          TERRAFORM="ip netns exec util ${pkgs.terraform.withPlugins (p: [p.vault])}/bin/terraform"
          VAULT="ip netns exec util ${pkgs.vault}/bin/vault"

          export VAULT_ADDR=https://slagit-vault.net:8200
          export VAULT_CACERT=${./root.pem}

          source env_file
          mkdir -p $DEVENV_STATE/vault-provision
          cd $DEVENV_STATE/vault-provision

          nix build "$DEVENV_ROOT"#provisioning --out-link vault.tf.json
          $TERRAFORM init
          $TERRAFORM plan -out tfplan -target vault_audit.file -target vault_policy.admin
          $TERRAFORM apply tfplan

          export VAULT_TOKEN="$($VAULT token create -policy admin -field token)"

          $TERRAFORM plan -out tfplan
          $TERRAFORM apply tfplan

          $VAULT delete ssh-client-signer/config/ca
          $VAULT write ssh-client-signer/config/ca \
              private_key="-----BEGIN OPENSSH PRIVATE KEY-----
          b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
          QyNTUxOQAAACDmj0DrspCMdZ20YJG8kfvTQOKIa7ul5EUUNRcqkrZl1wAAAJjcjA9u3IwP
          bgAAAAtzc2gtZWQyNTUxOQAAACDmj0DrspCMdZ20YJG8kfvTQOKIa7ul5EUUNRcqkrZl1w
          AAAEA3JvPkvWMPb2sWzk9fBpJ03DUee9lCKeLLcaeSb4kjteaPQOuykIx1nbRgkbyR+9NA
          4ohru6XkRRQ1FyqStmXXAAAADmFsYmVydEBjeWNsb25lAQIDBAUGBw==
          -----END OPENSSH PRIVATE KEY-----
          " \
              public_key="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOaPQOuykIx1nbRgkbyR+9NA4ohru6XkRRQ1FyqStmXX slagit-vault.net ssh-client-signer" \
              generate_signing_key=false

          $VAULT kv put secret/servers/compute0.slagit.net/borgmatic_key \
              credential="-----BEGIN OPENSSH PRIVATE KEY-----
          b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
          QyNTUxOQAAACBvqJwy2iioHXRAqqCSPguMHhiw4J1B8MuJTtGsH6IKOAAAAJjPId/3zyHf
          9wAAAAtzc2gtZWQyNTUxOQAAACBvqJwy2iioHXRAqqCSPguMHhiw4J1B8MuJTtGsH6IKOA
          AAAEAOu6v3XGilLv3pZXhmhL3aap77iajAyraL9S6dUjrLBm+onDLaKKgddECqoJI+C4we
          GLDgnUHwy4lO0awfogo4AAAADmFsYmVydEBjeWNsb25lAQIDBAUGBw==
          -----END OPENSSH PRIVATE KEY-----
          "
          $VAULT kv put secret/servers/compute0.slagit.net/borgmatic_passphrase credential="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

          $VAULT kv put secret/servers/franklin.slagit.net/borgmatic_key \
              credential="-----BEGIN OPENSSH PRIVATE KEY-----
          b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
          QyNTUxOQAAACBzcHwRox8RGXbof9/Otj/VO0TrSk3r1bNuEpypGpLxpgAAAJibJ7utmye7
          rQAAAAtzc2gtZWQyNTUxOQAAACBzcHwRox8RGXbof9/Otj/VO0TrSk3r1bNuEpypGpLxpg
          AAAEDZfQbyN2uX2JPSMkHegK/BQSeBCgPOgpdrixbvX514+HNwfBGjHxEZduh/3862P9U7
          ROtKTevVs24SnKkakvGmAAAADmFsYmVydEBjeWNsb25lAQIDBAUGBw==
          -----END OPENSSH PRIVATE KEY-----
          "
          $VAULT kv put secret/servers/franklin.slagit.net/borgmatic_passphrase credential="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

          $VAULT kv put secret/servers/compute1.slagit.net/aws credential="[default]
          aws_access_key_id=minioadmin
          aws_secret_access_key=minioadmin"
          $VAULT kv put secret/servers/compute1.slagit.net/cache-priv-key credential="stirling.dev-slagit.net-1:PNVDuVyckm8xq1f8utc1tRrV+UHOhWf94yE7Ne6gY5hWvBpVV5Bhom7PCoDmAovsuJ3wIAXD5xHsbCDr3Vz/UQ=="

          $VAULT write auth/userpass/users/albert password=password token_ttl=1h token_max_ttl=8h
        '';
        vault-unseal.start = pkgs.writeShellScriptBin "vault-unseal.start.sh" ''
          set -euo pipefail

          CURL="ip netns exec util ${pkgs.curl}/bin/curl --cacert ${./root.pem}"
          JQ="${pkgs.jq}/bin/jq"

          response=$($CURL -s --max-time 5 "https://slagit-vault.net:8200/v1/sys/init" | $JQ '.initialized' || true)

          if [ -f env_file ]; then
            source env_file
          fi

          if [ "$response" == "false" ]; then
            echo "Performing initialization"
            response=$($CURL -s --request POST --data '{"secret_shares": 1, "secret_threshold": 1}' "https://slagit-vault.net:8200/v1/sys/init")
            root_token=$(echo "$response" | $JQ -r '.root_token')
            first_key_base64=$(echo "$response" | $JQ -r '.keys_base64[0]')
            export VAULT_TOKEN="$root_token"
            export UNSEAL_KEY="$first_key_base64"
            echo "export VAULT_TOKEN=$VAULT_TOKEN" > env_file
            echo "export UNSEAL_KEY=$UNSEAL_KEY" >> env_file
          fi

          echo "Vault Unseal key is $UNSEAL_KEY"
          echo "Vault Root token is $VAULT_TOKEN"

          is_sealed=$($CURL -s "https://slagit-vault.net:8200/v1/sys/seal-status" | $JQ '.sealed' || true)
          if [ "$is_sealed" == "true" ]; then
            echo "Vault is sealed. Attempting to unseal automatically..."
            response=$($CURL -s --request POST --data "{\"key\": \"$UNSEAL_KEY\"}" "https://slagit-vault.net:8200/v1/sys/unseal")
            if $JQ -e '.errors' <<< "$response" > /dev/null; then
              echo "Failed to unseal the vault: $response"
            fi
          fi
        '';
      });
    };
    ci.gitlab.default.jobs = {
      "devShell:x86_64-linux:default".jobConfiguration.script = lib.mkForce [
        "nix build .#devShells.x86_64-linux.default --impure"
      ];
      "package:x86_64-linux:container-processes".jobConfiguration.when = "manual";
      "package:x86_64-linux:container-shell".jobConfiguration.when = "manual";
    };
    perSystem = {
      pkgs,
      system,
      ...
    }: {
      devenv.shells.default = {
        imports = [
          ./ovn.nix
        ];
        processes = {
          morgan = {
            process-compose.readiness_probe = {
              exec.command = "nix run .#dev-svc.morgan.ready";
              failure_threshold = 60;
              initial_delay_seconds = 10;
              period_seconds = 5;
            };
          };
          setup-networks = {
            exec = "nix run .#dev-svc.setup-networks.start";
            process-compose = {
              depends_on.ovsdb-server.condition = "process_healthy";
              namespace = "Open vSwitch";
            };
          };
          vault-provision = {
            exec = "nix run .#dev-svc.vault-provision.start";
            process-compose.depends_on.vault-unseal.condition = "process_completed_successfully";
          };
          vault-unseal = {
            exec = "nix run .#dev-svc.vault-unseal.start";
            process-compose.depends_on.morgan.condition = "process_healthy";
          };
        };
        scripts = {
          create-root.exec = ''
            exec ${pkgs.rootlesskit}/bin/rootlesskit \
              --copy-up=/etc \
              --copy-up=/run \
              --copy-up=/var \
              --net=slirp4netns \
              --disable-host-loopback \
              --slirp4netns-binary ${pkgs.slirp4netns}/bin/slirp4netns \
              --state-dir $DEVENV_RUNTIME/rootlesskit \
              ${pkgs.bashInteractive}/bin/bash
          '';
          join-root.exec = ''
            exec nsenter -t "$(<$DEVENV_RUNTIME/rootlesskit/child_pid)" -U -n -m
          '';
          view-console.exec = ''
            exec ${pkgs.tigervnc}/bin/vncviewer "$DEVENV_RUNTIME/$1-vnc.sock"
          '';
        };
        services.ovn.enable = true;
      };
    };
    vms = {
      compute0 = {
      };
      compute1 = {
      };
      franklin = {
      };
      hail = {
      };
      morgan = {
      };
    };
  };
  options.flake.dev-svc = lib.mkOption {
    type = lib.types.attrsOf (lib.types.submodule {
      options = {
        ready = lib.mkOption {
          type = lib.types.package;
        };
        start = lib.mkOption {
          type = lib.types.package;
        };
        stop = lib.mkOption {
          type = lib.types.package;
        };
      };
    });
  };
}
