{config, ...}: let
  inherit (config) networking;
in {
  flake.nixosModules.dev-lib = {
    config,
    lib,
    pkgs,
    ...
  }: let
    qemu-ifup = iface-id:
      pkgs.writeShellScript "qemu-ifup" ''
        set -euo pipefail

        ip link set "$1" up
        ovs-vsctl --may-exist add-port br-int "$1"
        ovs-vsctl set Interface "$1" external-ids:iface-id=${iface-id}
      '';
    qemu-ifdown = pkgs.writeShellScript "qemu-ifdown" ''
      set -euo pipefail

      ovs-vsctl del-port br-int "$1"
    '';
  in {
    config.dev-lib = {
      inherit networking;
      mkNetworkingOptions = id: ifname: port: [
        "-netdev tap,id=${id},ifname=${ifname},script=${qemu-ifup port.name},downscript=${qemu-ifdown}"
        "-device virtio-net,mac=${port.dl_address},netdev=${id}"
      ];
    };
    options.dev-lib =
      lib.mkOption {
      };
  };
}
