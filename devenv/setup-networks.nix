{
  config,
  lib,
  withSystem,
  ...
}: {
  config.flake.dev-svc.setup-networks = withSystem "x86_64-linux" ({pkgs, ...}: {
    start = let
      all_net_ports = lib.attrsets.mergeAttrsList (lib.attrsets.mapAttrsToList (k: v: lib.attrsets.mapAttrs' (k: v: lib.nameValuePair v.name v) v.ports) config.networking.networks);
      reverse_router = lib.attrsets.mergeAttrsList (lib.attrsets.mapAttrsToList (k: v: lib.attrsets.mapAttrs' (k: v: lib.nameValuePair v.port v.name) v.ports) config.networking.routers);
      make_dhcp = _: dhcp_def: [
        "if ! ovn-nbctl list dhcp-options ${lib.escapeShellArg dhcp_def.uuid}"
        "then"
        (lib.escapeShellArgs (["ovn-nbctl" "--id=${dhcp_def.uuid}" "create" "dhcp-options" "cidr=${dhcp_def.cidr}"] ++ (lib.attrsets.mapAttrsToList (k: v: "options:${k}=${v}") dhcp_def.options)))
        "fi"
      ];
      make_router = _: router_def:
        [
          (lib.escapeShellArgs ["ovn-nbctl" "--may-exist" "lr-add" "${router_def.name}"])
        ]
        ++ (builtins.concatLists (lib.attrsets.mapAttrsToList (make_router_port router_def) router_def.ports));
      make_router_port = router_def: _: port_def: let
        net_port = all_net_ports."${port_def.port}";
      in [
        (lib.escapeShellArgs ["ovn-nbctl" "--may-exist" "lrp-add" "${router_def.name}" "${port_def.name}" "${net_port.dl_address}" "${net_port.ip_address}/24"])
      ];
      make_network = _: net_def:
        [
          (lib.escapeShellArgs ["ovn-nbctl" "--may-exist" "ls-add" "${net_def.name}"])
        ]
        ++ (builtins.concatLists (lib.attrsets.mapAttrsToList (make_network_port net_def) net_def.ports));
      make_network_port = net_def: _: port_def: [
        (lib.escapeShellArgs (["ovn-nbctl" "--may-exist" "lsp-add" "${net_def.name}" "${port_def.name}"]
          ++ (
            if port_def.master == null
            then []
            else ["${port_def.master.port}" "${toString port_def.master.vlan}"]
          )))
        (lib.strings.optionalString (port_def.type == "router") (lib.escapeShellArgs ["ovn-nbctl" "lsp-set-type" "${port_def.name}" "router"]))
        (lib.strings.optionalString (port_def.type == "router") (lib.escapeShellArgs ["ovn-nbctl" "lsp-set-addresses" "${port_def.name}" "router"]))
        (lib.strings.optionalString (port_def.type == "router") (lib.escapeShellArgs ["ovn-nbctl" "lsp-set-options" "${port_def.name}" "router-port=${reverse_router."${port_def.name}"}"]))
        (lib.strings.optionalString (port_def.type == "vm" && port_def.ip_address == null) (lib.escapeShellArgs ["ovn-nbctl" "lsp-set-addresses" "${port_def.name}" "${port_def.dl_address}"]))
        (lib.strings.optionalString (port_def.type == "vm" && port_def.ip_address != null) (lib.escapeShellArgs ["ovn-nbctl" "lsp-set-addresses" "${port_def.name}" "${port_def.dl_address} ${port_def.ip_address}"]))
        (lib.strings.optionalString (port_def.dhcpv4Options != null) (lib.escapeShellArgs ["ovn-nbctl" "lsp-set-dhcpv4-options" "${port_def.name}" "${port_def.dhcpv4Options}"]))
      ];
    in
      pkgs.writeShellScriptBin "setup-networks.star.sh" (lib.strings.concatStringsSep "\n" ([
          "set -euxo pipefail"
        ]
        ++ (builtins.filter (v: v != "") (builtins.concatLists (builtins.concatLists [
          (lib.attrsets.mapAttrsToList make_dhcp config.networking.dhcpOptions)
          (lib.attrsets.mapAttrsToList make_router config.networking.routers)
          (lib.attrsets.mapAttrsToList make_network config.networking.networks)
        ])))
        ++ [
          "rm -f /run/nscd"
          "mkdir -p /etc/netns/util"
          "echo \"nameserver ${config.networking.networks."3rd_party".ports.dns.ip_address}\" > /etc/netns/util/resolv.conf"
          "if [ ! -f /var/run/netns/util ]"
          "then"
          "ip netns add util"
          "ip link add 3rd_party-util type veth peer name eth0 address ${config.networking.networks."3rd_party".ports.util.dl_address} netns util"
          "ip link set 3rd_party-util up"
          "ovs-vsctl --may-exist add-port br-int 3rd_party-util"
          "ovs-vsctl set Interface 3rd_party-util external-ids:iface-id=${config.networking.networks."3rd_party".ports.util.name}"
          "ip netns exec util ip link set lo up"
          "ip netns exec util ip link set eth0 up"
          "ip netns exec util ip addr add ${config.networking.networks."3rd_party".ports.util.ip_address}/24 dev eth0"
          "ip netns exec util ip route add default via ${config.networking.networks."3rd_party".ports.internet.ip_address}"
          "fi"
        ]));
  });
}
