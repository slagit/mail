{
  config,
  withSystem,
  ...
}: {
  flake.dev-svc.mock-nvr = withSystem "x86_64-linux" ({pkgs, ...}: let
    nginx-conf = pkgs.writeText "nginx.conf" ''
      daemon off;
      worker_processes 1;
      events {
        use epoll;
        worker_connections 128;
      }
      error_log stderr info;
      pid /dev/null;
      http {
        server_tokens off;
        charset utf-8;
        access_log /dev/stdout;
        server {
          listen 0.0.0.0:80;
          location / {
            root /;
          }
        }
      }
    '';
  in {
    start = pkgs.writeShellScriptBin "mock-nvr.start.sh" ''
      set -euxo pipefail

      ip netns add mock-nvr
      function cleanup_ns {
        ip netns del mock-nvr
      }
      trap cleanup_ns EXIT

      ip link add camera-mock-nvr type veth peer name eth0 address ${config.networking.networks.camera.ports.mock-nvr.dl_address} netns mock-nvr
      ip link set camera-mock-nvr up
      ovs-vsctl --may-exist add-port br-int camera-mock-nvr
      function cleanup {
        ip netns del mock-nvr
        ovs-vsctl del-port br-int camera-mock-nvr
      }
      trap cleanup EXIT

      ovs-vsctl set Interface camera-mock-nvr external-ids:iface-id=${config.networking.networks.camera.ports.mock-nvr.name}
      ip netns exec mock-nvr ip link set lo up
      ip netns exec mock-nvr ip link set eth0 up
      ip netns exec mock-nvr ip addr add ${config.networking.networks.camera.ports.mock-nvr.ip_address}/24 dev eth0
      ip netns exec mock-nvr ip route add default via ${config.networking.networks.camera.ports.hail.ip_address}

      ip netns exec mock-nvr ${pkgs.nginx}/bin/nginx -c ${nginx-conf}
    '';
  });
  perSystem = _: {
    devenv.shells.default.processes.mock-nvr = {
      exec = "nix run .#dev-svc.mock-nvr.start";
      process-compose = {
        depends_on = {
          ovsdb-server.condition = "process_healthy";
          ovn-controller.condition = "process_healthy";
        };
        namespace = "Mock Services";
      };
    };
  };
}
