{
  config,
  inputs,
  lib,
  self,
  ...
}: let
  common = {pkgs, ...}: {
    imports = [
      self.common.hardware.nixosModules.linode
      self.nixosModules.common
      self.nixosModules.dev-lib
      self.nixosModules.nebula
    ];
    environment.systemPackages = [
      pkgs.firefox
      pkgs.tcpdump
      self.packages.x86_64-linux.sv
    ];
    networking.domain = "dev-slagit.net";
    services.xserver = {
      enable = true;
      windowManager.twm.enable = true;
      displayManager.autoLogin = {
        enable = true;
        user = "albert";
      };
    };
  };
in {
  flake.nixosConfigurations = {
    dev-in = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        common
        ({config, ...}: {
          environment.etc = {
            "nebula/node.crt" = {
              source = ./nebula/dev-in.dev-slagit.net.crt;
            };
            "nebula/node.key" = {
              source = ./nebula/dev-in.dev-slagit.net.key;
              group = "nebula-slagit";
              mode = "0640";
            };
          };
          networking.hostName = "dev-in";
          virtualisation.vmVariant.virtualisation.qemu.networkingOptions = lib.mkForce (config.dev-lib.mkNetworkingOptions "wireless" "wireless-dev-in" config.dev-lib.networking.networks.wireless.ports.dev-in);
        })
      ];
    };
    dev-out = inputs.nixpkgs.lib.nixosSystem {
      modules = [
        common
        ({config, ...}: {
          environment.etc = {
            "nebula/node.crt" = {
              source = ./nebula/dev-out.dev-slagit.net.crt;
            };
            "nebula/node.key" = {
              source = ./nebula/dev-out.dev-slagit.net.key;
              group = "nebula-slagit";
              mode = "0640";
            };
          };
          networking.hostName = "dev-out";
          virtualisation.vmVariant.virtualisation.qemu.networkingOptions = lib.mkForce (config.dev-lib.mkNetworkingOptions "linode" "linode-dev-out" config.dev-lib.networking.networks.linode.ports.dev-out);
        })
      ];
    };
  };
  vms = {
    dev-in = {
    };
    dev-out = {
    };
  };
}
