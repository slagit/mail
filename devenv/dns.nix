{
  config,
  withSystem,
  ...
}: {
  flake.dev-svc = {
    dns = withSystem "x86_64-linux" ({pkgs, ...}: let
      unbound-conf = pkgs.writeText "unbound.conf" ''
        server:
          access-control: 0.0.0.0/0 allow
          chroot: ""
          cache-max-ttl: 0
          interface: 0.0.0.0
          log-queries: yes
          pidfile: ""
          root-hints: ${root-hints}
          username: root
          use-syslog: no
      '';
      root-hints = pkgs.writeText "root.hints" ''
        . 3600 IN NS nsd.dev-slagit.net.
        nsd.dev-slagit.net. 3600 IN A ${config.networking.networks."3rd_party".ports.nsd.ip_address}
      '';
    in {
      ready = pkgs.writeShellScriptBin "dns.ready.sh" ''
        set -euo pipefail

        DIG="ip netns exec util ${pkgs.dig.dnsutils}/bin/dig"

        $DIG a slagit-vault.net +tries=1 +time=1
      '';
      start = pkgs.writeShellScriptBin "dns.start.sh" ''
        set -euxo pipefail

        rm -f /run/nscd
        mkdir -p /etc/netns/dns
        echo "nameserver ${config.networking.networks."3rd_party".ports.dns.ip_address}" > /etc/netns/dns/resolv.conf

        ip netns add dns
        function cleanup_ns {
          ip netns del dns
        }
        trap cleanup_ns EXIT

        ip link add 3rd_party-dns type veth peer name eth0 address ${config.networking.networks."3rd_party".ports.dns.dl_address} netns dns
        ip link set 3rd_party-dns up
        ovs-vsctl --may-exist add-port br-int 3rd_party-dns
        function cleanup {
          ip netns del dns
          ovs-vsctl del-port br-int 3rd_party-dns
        }
        trap cleanup EXIT

        ovs-vsctl set Interface 3rd_party-dns external-ids:iface-id=${config.networking.networks."3rd_party".ports.dns.name}
        ip netns exec dns ip link set lo up
        ip netns exec dns ip link set eth0 up
        ip netns exec dns ip addr add ${config.networking.networks."3rd_party".ports.dns.ip_address}/24 dev eth0
        ip netns exec dns ip route add default via ${config.networking.networks."3rd_party".ports.internet.ip_address}

        mkdir -p /etc/unbound
        ip netns exec dns ${pkgs.unbound}/bin/unbound -c ${unbound-conf} -d
      '';
    });
    nsd = withSystem "x86_64-linux" ({pkgs, ...}: let
      root-zone = pkgs.writeText "root.zone" ''
        . 3600 IN SOA nsd.dev-slagit.net. root.dev-slagit.net. 2024060601 1800 900 604800 86400
        . 3600 IN NS nsd.dev-slagit.net.

        acme.dev-slagit.net. 3600 IN A ${config.networking.networks."3rd_party".ports.ca.ip_address}
        backup.dev-slagit.net. 3600 IN A ${config.networking.networks."3rd_party".ports.backup.ip_address}
        nsd.dev-slagit.net. 3600 IN A ${config.networking.networks."3rd_party".ports.nsd.ip_address}

        slagit.net. 3600 IN A 10.10.10.3
        compute0.slagit.net. 3600 IN A 10.10.10.3
        compute1.slagit.net. 3600 IN A 10.10.10.6
        franklin.slagit.net. 3600 IN A ${config.networking.networks.linode.ports.franklin.ip_address}
        franklin.zt.slagit.net. 3600 IN A 10.10.10.1
        hail.slagit.net. 3600 IN A 10.10.10.5

        grafana.slagit.net. 3600 IN CNAME compute0.slagit.net.
        mail.slagit.net. 3600 IN CNAME franklin.slagit.net.
        paperless.slagit.net. 3600 IN CNAME compute0.slagit.net.

        slagit-vault.net. 3600 IN A ${config.networking.networks.linode.ports.morgan.ip_address}
        morgan.slagit-vault.net. 3600 IN A ${config.networking.networks.linode.ports.morgan.ip_address}
        morgan.zt.slagit-vault.net. 3600 IN A 10.10.10.8
      '';
      nsd-conf = pkgs.writeText "nsd.conf" ''
        server:
          pidfile: ""
          username: root
          zonesdir: /var/empty
        zone:
          name: .
          zonefile: ${root-zone}
      '';
    in {
      ready = pkgs.writeShellScriptBin "nsd.ready.sh" ''
        set -euo pipefail

        DIG="ip netns exec util ${pkgs.dig.dnsutils}/bin/dig"

        $DIG @${config.networking.networks."3rd_party".ports.nsd.ip_address} a slagit-vault.net +tries=1 +time=1
      '';
      start = pkgs.writeShellScriptBin "nsd.start.sh" ''
        set -euxo pipefail

        rm -f /run/nscd
        mkdir -p /etc/netns/nsd
        echo "nameserver ${config.networking.networks."3rd_party".ports.dns.ip_address}" > /etc/netns/nsd/resolv.conf

        ip netns add nsd
        function cleanup_ns {
          ip netns del nsd
        }
        trap cleanup_ns EXIT

        ip link add 3rd_party-nsd type veth peer name eth0 address ${config.networking.networks."3rd_party".ports.nsd.dl_address} netns nsd
        ip link set 3rd_party-nsd up
        ovs-vsctl --may-exist add-port br-int 3rd_party-nsd
        function cleanup {
          ip netns del nsd
          ovs-vsctl del-port br-int 3rd_party-nsd
        }
        trap cleanup EXIT

        ovs-vsctl set Interface 3rd_party-nsd external-ids:iface-id=${config.networking.networks."3rd_party".ports.nsd.name}
        ip netns exec nsd ip link set lo up
        ip netns exec nsd ip link set eth0 up
        ip netns exec nsd ip addr add ${config.networking.networks."3rd_party".ports.nsd.ip_address}/24 dev eth0
        ip netns exec nsd ip route add default via ${config.networking.networks."3rd_party".ports.internet.ip_address}

        ip netns exec nsd ${pkgs.nsd}/bin/nsd -d -c ${nsd-conf}
      '';
    });
  };
  perSystem = _: {
    devenv.shells.default.processes = {
      dns = {
        exec = "nix run .#dev-svc.dns.start";
        process-compose = {
          depends_on = {
            nsd.condition = "process_healthy";
            ovsdb-server.condition = "process_healthy";
            ovs-vswitchd = {};
            ovn-controller.condition = "process_healthy";
            ovn-northd = {};
          };
          namespace = "3rd Party";
          readiness_probe = {
            exec.command = "nix run .#dev-svc.dns.ready";
            initial_delay_seconds = 1;
          };
        };
      };
      nsd = {
        exec = "nix run .#dev-svc.nsd.start";
        process-compose = {
          depends_on = {
            ovsdb-server.condition = "process_healthy";
            ovs-vswitchd = {};
            ovn-controller.condition = "process_healthy";
            ovn-northd = {};
          };
          namespace = "3rd Party";
          readiness_probe = {
            exec.command = "nix run .#dev-svc.nsd.ready";
            initial_delay_seconds = 1;
          };
        };
      };
    };
  };
}
