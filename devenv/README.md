# Development Environment

Enter a fake root environment by running:

```bash
create-root
```

Launch the services:

```bash
devenv up
```

In the franklin vm, run:

```bash
sudo devenv-register-self
sudo devenv-init-backup
```

In the hail vm, run:

```bash
sudo devenv-register-self
```
