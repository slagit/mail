{
  config,
  lib,
  ...
}: let
  cfg = config.networking;
in {
  config.networking = {
    dhcpOptions = {
      linode = {
        uuid = "935c838c-e79d-4d9d-b138-cc4eccd39075";
        cidr = "10.30.1.0/24";
        options = {
          lease_time = "300";
          router = cfg.networks.linode.ports.internet.ip_address;
          server_id = cfg.networks.linode.ports.internet.ip_address;
          server_mac = cfg.networks.linode.ports.internet.dl_address;
          dns_server = cfg.networks."3rd_party".ports.dns.ip_address;
        };
      };
      ocrt = {
        uuid = "7437f6f5-1f73-463a-aa80-ba21c06d1a85";
        cidr = "10.30.3.0/24";
        options = {
          lease_time = "300";
          router = cfg.networks.ocrt.ports.internet.ip_address;
          server_id = cfg.networks.ocrt.ports.internet.ip_address;
          server_mac = cfg.networks.ocrt.ports.internet.dl_address;
          dns_server = cfg.networks."3rd_party".ports.dns.ip_address;
        };
      };
    };
    networks = {
      "3rd_party" = {
        ports = {
          backup = {
            dl_address = "52:54:00:5c:f4:e0";
            ip_address = "10.30.2.5";
          };
          ca = {
            dl_address = "52:54:00:6c:2c:1e";
            ip_address = "10.30.2.3";
          };
          dns = {
            dl_address = "52:54:00:fc:3f:aa";
            ip_address = "10.30.2.2";
          };
          internet = {
            type = "router";
            dl_address = "00:11:22:33:44:55";
            ip_address = "10.30.2.1";
          };
          nsd = {
            dl_address = "52:54:00:ff:80:3e";
            ip_address = "10.30.2.7";
          };
          util = {
            dl_address = "52:54:00:8f:7b:71";
            ip_address = "10.30.2.4";
          };
        };
      };
      "camera" = {
        ports = {
          hail = {
            dl_address = "52:54:00:df:42:fd";
            ip_address = "10.10.6.1";
            master = {
              port = cfg.networks.lan.ports.hail.name;
              vlan = 6;
            };
          };
          mock-nvr = {
            dl_address = "52:54:00:a2:73:73";
            ip_address = "10.10.6.180";
          };
        };
      };
      "cloud" = {
        ports = {
          compute0 = {
            dl_address = "52:54:00:6b:ce:5b";
          };
          compute1 = {
            dl_address = "52:54:00:3b:d7:55";
          };
          hail = {
            dl_address = "52:54:00:df:42:fd";
            ip_address = "10.10.4.1";
            master = {
              port = cfg.networks.lan.ports.hail.name;
              vlan = 4;
            };
          };
        };
      };
      "lan" = {
        ports = {
          hail = {
            dl_address = "52:54:00:df:42:fd";
            ip_address = "10.10.1.1";
          };
        };
      };
      "linode" = {
        ports = {
          dev-out = {
            dhcpv4Options = cfg.dhcpOptions.linode.uuid;
            dl_address = "52:54:00:02:f6:90";
            ip_address = "10.30.1.12";
          };
          franklin = {
            dhcpv4Options = cfg.dhcpOptions.linode.uuid;
            dl_address = "52:54:00:0e:68:db";
            ip_address = "10.30.1.11";
          };
          internet = {
            type = "router";
            dl_address = "00:11:22:33:44:55";
            ip_address = "10.30.1.1";
          };
          morgan = {
            dhcpv4Options = cfg.dhcpOptions.linode.uuid;
            dl_address = "52:54:00:fc:4f:af";
            ip_address = "10.30.1.10";
          };
        };
      };
      "ocrt" = {
        ports = {
          hail = {
            dhcpv4Options = cfg.dhcpOptions.ocrt.uuid;
            dl_address = "52:54:00:df:42:fc";
            ip_address = "10.30.3.2";
          };
          internet = {
            type = "router";
            dl_address = "00:11:22:33:44:55";
            ip_address = "10.30.3.1";
          };
        };
      };
      "wireless" = {
        ports = {
          dev-in = {
            dl_address = "52:54:00:66:92:84";
          };
          hail = {
            dl_address = "52:54:00:df:42:fd";
            ip_address = "10.10.2.1";
            master = {
              port = cfg.networks.lan.ports.hail.name;
              vlan = 2;
            };
          };
        };
      };
    };
    routers.internet.ports = {
      "3rd_party".port = cfg.networks."3rd_party".ports.internet.name;
      "linode".port = cfg.networks.linode.ports.internet.name;
      "ocrt".port = cfg.networks.ocrt.ports.internet.name;
    };
  };
  options.networking = {
    dhcpOptions = lib.mkOption {
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          cidr = lib.mkOption {
            type = lib.types.str;
          };
          options = lib.mkOption {
            type = lib.types.attrsOf lib.types.str;
          };
          uuid = lib.mkOption {
            type = lib.types.str;
          };
        };
      });
    };
    networks = lib.mkOption {
      type = lib.types.attrsOf (lib.types.submodule ({name, ...}: let
        net_name = name;
      in {
        options = {
          name = lib.mkOption {
            default = name;
            type = lib.types.str;
          };
          ports = lib.mkOption {
            type = lib.types.attrsOf (lib.types.submodule ({name, ...}: {
              options = {
                dhcpv4Options = lib.mkOption {
                  default = null;
                  type = lib.types.nullOr lib.types.str;
                };
                dl_address = lib.mkOption {
                  type = lib.types.str;
                };
                ip_address = lib.mkOption {
                  default = null;
                  type = lib.types.nullOr lib.types.str;
                };
                master = lib.mkOption {
                  default = null;
                  type = lib.types.nullOr (lib.types.submodule {
                    options = {
                      port = lib.mkOption {
                        type = lib.types.str;
                      };
                      vlan = lib.mkOption {
                        type = lib.types.int;
                      };
                    };
                  });
                };
                name = lib.mkOption {
                  default = "${net_name}-${name}";
                  type = lib.types.str;
                };
                type = lib.mkOption {
                  default = "vm";
                  type = lib.types.enum ["router" "vm"];
                };
              };
            }));
          };
        };
      }));
    };
    routers = lib.mkOption {
      type = lib.types.attrsOf (lib.types.submodule ({name, ...}: let
        router_name = name;
      in {
        options = {
          name = lib.mkOption {
            default = name;
            type = lib.types.str;
          };
          ports = lib.mkOption {
            type = lib.types.attrsOf (lib.types.submodule ({name, ...}: {
              options = {
                name = lib.mkOption {
                  default = "${router_name}-${name}";
                  type = lib.types.str;
                };
                network = lib.mkOption {
                  type = lib.types.str;
                };
                port = lib.mkOption {
                  type = lib.types.str;
                };
              };
            }));
          };
        };
      }));
    };
  };
}
