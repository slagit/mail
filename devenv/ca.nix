{
  config,
  lib,
  self,
  withSystem,
  ...
}: {
  flake.dev-svc.ca = withSystem "x86_64-linux" ({pkgs, ...}: let
    ca-config = pkgs.writeText "ca.json" (builtins.toJSON {
      root = ./root.pem;
      crt = ./intermediate.pem;
      key = ./intermediate-key.pem;
      address = ":443";
      dnsNames = [
        "acme.dev-slagit.net"
      ];
      logger.format = "text";
      db = {
        type = "badgerv2";
        dataSource = "${self.devShells.x86_64-linux.default.DEVENV_STATE}/ca/db";
        badgerFileLoadingMode = "";
      };
      authority = {
        provisioners = [
          {
            type = "ACME";
            name = "acme";
          }
        ];
      };
      tls = {
        cipherSuites = [
          "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256"
          "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"
        ];
        minVersion = 1.2;
        maxVersion = 1.3;
        renegotiation = false;
      };
      password = "password";
    });
  in {
    ready = pkgs.writeShellScriptBin "ca.ready.sh" ''
      set -euo pipefail

      CURL="ip netns exec util ${pkgs.curl}/bin/curl --cacert ${./root.pem}"

      $CURL -s --max-time 5 https://acme.dev-slagit.net/
    '';
    start = pkgs.writeShellScriptBin "ca.start.sh" ''
      set -euxo pipefail

      rm -f /run/nscd
      mkdir -p /etc/netns/ca
      echo "nameserver ${config.networking.networks."3rd_party".ports.dns.ip_address}" > /etc/netns/ca/resolv.conf

      ip netns add ca
      function cleanup_ns {
        ip netns del ca
      }
      trap cleanup_ns EXIT

      ip link add 3rd_party-ca type veth peer name eth0 address ${config.networking.networks."3rd_party".ports.ca.dl_address} netns ca
      ip link set 3rd_party-ca up
      ovs-vsctl --may-exist add-port br-int 3rd_party-ca
      function cleanup {
        ip netns del ca
        ovs-vsctl del-port br-int 3rd_party-ca
      }
      trap cleanup EXIT

      ovs-vsctl set Interface 3rd_party-ca external-ids:iface-id=${config.networking.networks."3rd_party".ports.ca.name}
      ip netns exec ca ip link set lo up
      ip netns exec ca ip link set eth0 up
      ip netns exec ca ip addr add ${config.networking.networks."3rd_party".ports.ca.ip_address}/24 dev eth0
      ip netns exec ca ip route add default via ${config.networking.networks."3rd_party".ports.internet.ip_address}

      mkdir -p ${lib.escapeShellArg self.devShells.x86_64-linux.default.DEVENV_STATE}/ca/db
      ip netns exec ca ${pkgs.step-ca}/bin/step-ca ${ca-config}
    '';
  });
  perSystem = _: {
    devenv.shells.default.processes.ca = {
      exec = "nix run --impure .#dev-svc.ca.start";
      process-compose = {
        depends_on = {
          ovsdb-server.condition = "process_healthy";
          ovn-controller.condition = "process_healthy";
        };
        namespace = "3rd Party";
        readiness_probe = {
          exec.command = "nix run .#dev-svc.ca.ready";
          initial_delay_seconds = 1;
        };
      };
    };
  };
}
