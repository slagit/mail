{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.services.ovn;
  runtimeDir = "${config.env.DEVENV_RUNTIME}/ovn";
  stateDir = "${config.env.DEVENV_STATE}/ovn";
  create-ovn-database = pkgs.writeShellScript "create-ovn-database.sh" ''
    set -euxo pipefail

    mkdir -p ${lib.escapeShellArg runtimeDir} ${lib.escapeShellArg stateDir}

    if [ ! -f ${lib.escapeShellArg stateDir}/ovnnb.db ]; then
      ${pkgs.openvswitch}/bin/ovsdb-tool create ${lib.escapeShellArg stateDir}/ovnnb.db ${pkgs.ovn}/share/ovn/ovn-nb.ovsschema
    fi

    if [ ! -f ${lib.escapeShellArg stateDir}/ovnsb.db ]; then
      ${pkgs.openvswitch}/bin/ovsdb-tool create ${lib.escapeShellArg stateDir}/ovnsb.db ${pkgs.ovn}/share/ovn/ovn-sb.ovsschema
    fi
  '';
  set-system-id = pkgs.writeShellScript "set-system-id.sh" ''
    set -euxo pipefail

    ${pkgs.openvswitch}/bin/ovs-vsctl set Open_vSwitch . external_ids:system-id=b0455ab5-3ad9-4aa1-82c4-045231e5e13c
    ${pkgs.openvswitch}/bin/ovs-vsctl set Open_vSwitch . external-ids:ovn-remote=unix:${lib.escapeShellArg config.env.DEVENV_RUNTIME}/openvswitch/db.sock
    ${pkgs.openvswitch}/bin/ovs-vsctl set Open_vSwitch . external-ids:ovn-encap-type=geneve
    ${pkgs.openvswitch}/bin/ovs-vsctl set Open_vSwitch . external-ids:ovn-encap-ip=127.0.0.1
  '';
  start-ovn-northd = pkgs.writeShellScript "start-ovn-northd.sh" ''
    set -euo pipefail

    exec ${pkgs.ovn}/bin/ovn-northd --pidfile
  '';
  start-ovn-controller = pkgs.writeShellScript "start-ovn-controller.sh" ''
    set -euo pipefail

    exec ${pkgs.ovn}/bin/ovn-controller --pidfile
  '';
in {
  config = lib.mkIf cfg.enable {
    env = {
      OVN_NB_DB = "unix:${config.env.DEVENV_RUNTIME}/openvswitch/db.sock";
      OVN_SB_DB = "unix:${config.env.DEVENV_RUNTIME}/openvswitch/db.sock";
      OVN_RUNDIR = runtimeDir;
    };
    packages = [pkgs.ovn];
    processes = {
      create-ovn-database = {
        exec = toString create-ovn-database;
        process-compose.namespace = "Open vSwitch";
      };
      set-system-id = {
        exec = toString set-system-id;
        process-compose = {
          depends_on.ovsdb-server.condition = "process_healthy";
          namespace = "Open vSwitch";
        };
      };
      ovsdb-server.process-compose.depends_on.create-ovn-database.condition = "process_completed_successfully";
      ovn-controller = {
        exec = toString start-ovn-controller;
        process-compose = {
          depends_on = {
            ovsdb-server.condition = "process_healthy";
            set-system-id.condition = "process_completed_successfully";
          };
          namespace = "Open vSwitch";
          readiness_probe = {
            exec.command = "test -d /sys/class/net/br-int";
            failure_threshold = 10;
            period_seconds = 2;
          };
          shutdown.command = "${pkgs.ovn}/bin/ovn-appctl -t ovn-controller exit";
        };
      };
      ovn-northd = {
        exec = toString start-ovn-northd;
        process-compose = {
          depends_on.ovsdb-server.condition = "process_healthy";
          namespace = "Open vSwitch";
          shutdown.command = "${pkgs.ovn}/bin/ovn-appctl -t ovn-northd exit";
        };
      };
    };
    services.openvswitch = {
      databases = {
        "${stateDir}/ovnnb.db" = {};
        "${stateDir}/ovnsb.db" = {};
      };
      enable = true;
    };
  };
  imports = [
    ./openvswitch.nix
  ];
  options.services.ovn = {
    enable = lib.mkEnableOption ''
      Add Open Virtual Network services
    '';
  };
}
