{
  config,
  withSystem,
  ...
}: {
  flake.dev-svc.backup = withSystem "x86_64-linux" ({pkgs, ...}: let
    sshd_config = pkgs.writeText "sshd_config" ''
      Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
      HostKey ${./ssh_host_ed25519_key}
      KexAlgorithms sntrup761x25519-sha512@openssh.com,curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
      Macs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com
      PasswordAuthentication no
      PermitRootLogin no
      PrintMotd no # handled by pam_motd
      Subsystem sftp ${pkgs.openssh}/libexec/sftp-server
      UseDns no
    '';
  in {
    start = pkgs.writeShellScriptBin "backup.start.sh" ''
      set -euxo pipefail

      rm -f /run/nscd
      mkdir -p /etc/netns/backup
      echo "nameserver ${config.networking.networks."3rd_party".ports.dns.ip_address}" > /etc/netns/backup/resolv.conf
      cat >/etc/netns/backup/passwd <<EOF
      root:x:0:0:System administrator:/root:/run/current-system/sw/bin/bash
      sshd:x:100:100::/var/empty:/run/current-system/sw/bin/nologin
      franklin:x:1000:100::/tmp/backup/franklin:/run/current-system/sw/bin/bash
      compute0:x:1001:100::/tmp/backup/compute0:/run/current-system/sw/bin/bash
      nobody:x:65534:65534:Unprivileged account (don't use!):/var/empty:/run/current-system/sw/bin/nologin
      EOF
      if [ -h /var/empty ]
      then
        rm /var/empty
        mkdir /var/empty
      fi

      mkdir -p /tmp/backup/franklin/.ssh
      cat >/tmp/backup/franklin/.ssh/authorized_keys <<EOF
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHNwfBGjHxEZduh/3862P9U7ROtKTevVs24SnKkakvGm root@franklin
      EOF
      chown 1000 /tmp/backup/franklin/.ssh/authorized_keys
      chown 1000 /tmp/backup/franklin
      chmod 0400 /tmp/backup/franklin/.ssh/authorized_keys
      cat >/tmp/backup/franklin/.bashrc <<EOF
      export PATH=${pkgs.borgbackup}/bin
      EOF
      mkdir -p /tmp/backup/compute0/.ssh
      cat >/tmp/backup/compute0/.ssh/authorized_keys <<EOF
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG+onDLaKKgddECqoJI+C4weGLDgnUHwy4lO0awfogo4 root@compute0
      EOF
      chown 1001 /tmp/backup/compute0/.ssh/authorized_keys
      chown 1001 /tmp/backup/compute0
      chmod 0400 /tmp/backup/compute0/.ssh/authorized_keys
      cat >/tmp/backup/compute0/.bashrc <<EOF
      export PATH=${pkgs.borgbackup}/bin
      EOF

      ip netns add backup
      function cleanup_ns {
        ip netns del backup
      }
      trap cleanup_ns EXIT

      ip link add 3rd_party-backu type veth peer name eth0 address ${config.networking.networks."3rd_party".ports.backup.dl_address} netns backup
      ip link set 3rd_party-backu up
      ovs-vsctl --may-exist add-port br-int 3rd_party-backu
      function cleanup {
        ip netns del backup
        ovs-vsctl del-port br-int 3rd_party-backu
      }
      trap cleanup EXIT

      ovs-vsctl set Interface 3rd_party-backu external-ids:iface-id=${config.networking.networks."3rd_party".ports.backup.name}
      ip netns exec backup ip link set lo up
      ip netns exec backup ip link set eth0 up
      ip netns exec backup ip addr add ${config.networking.networks."3rd_party".ports.backup.ip_address}/24 dev eth0
      ip netns exec backup ip route add default via ${config.networking.networks."3rd_party".ports.internet.ip_address}

      ip netns exec backup ${pkgs.openssh}/bin/sshd -D -e -f ${sshd_config}
    '';
  });
  perSystem = _: {
    devenv.shells.default.processes.backup = {
      exec = "nix run --impure .#dev-svc.backup.start";
      process-compose = {
        depends_on = {
          ovsdb-server.condition = "process_healthy";
          ovn-controller.condition = "process_healthy";
        };
        namespace = "3rd Party";
      };
    };
  };
}
